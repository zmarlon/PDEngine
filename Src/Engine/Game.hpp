//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 05.04.2020.
//
#ifndef PD_ENGINE_GAME_HPP
#define PD_ENGINE_GAME_HPP

#pragma once

#include "Core/Disposable.hpp"
#include "Core/Singleton.hpp"
#include "Core/Event/Listeners.hpp"
#include "GameInfo.hpp"
#include "GameState.hpp"
#include "Core/Time/Timer.hpp"
#include "Core/Config/ConfigClient.hpp"

namespace PD
{
    class CProxyAllocator;
    class CMemoryStack;
    class CLogger;
    class CWindow;
    class CEventHandler;
    class CFontLibrary;
    class CPakManager;
    class CCommonLanguageRuntime;
    class CRenderer;
    class CStaticEntityRegistry;
    class CGameConsole;

    class CGame : public IWindowResizeListener, IGamePauseListener, IGameResumeListener
    {
        PD_MAKE_CLASS_DISPOSABLE(CGame)
        PD_MAKE_CLASS_SINGLETON(CGame)
    private:
        SGameInfo m_gameInfo = SGameInfo("WingertVoxel", 0, 0, 1);
        SGameState m_gameState;
        SConfigClient m_config;

        CProxyAllocator* m_pProxyAllocator = nullptr;
        CMemoryStack* m_pMemoryStack = nullptr;
        CLogger* m_pLogger = nullptr;
        CWindow* m_pWindow = nullptr;
        CTimer m_loopTimer;
        CEventHandler* m_pEventHandler = nullptr;
        CFontLibrary* m_pFontLibrary = nullptr;
        CPakManager* m_pPakManager = nullptr;
        CCommonLanguageRuntime* m_pCLRuntime = nullptr;
        CRenderer* m_pRenderer = nullptr;
		CGameConsole* m_pGameConsole = nullptr;
        CStaticEntityRegistry* m_pStaticEntityRegistry = nullptr;

        void(*m_pfn_Game_CreateInstance)(void*);
        void(*m_pfn_Game_PreInit)();
        void(*m_pfn_Game_Init)();
        void(*m_pfn_Game_PostInit)();
        void(*m_pfn_Game_DisposeInstance)();
        void(*m_pfn_Game_TickInstance)(float, float);

        inline void CreateLogger();
        inline void DestroyLogger();
        inline void CreateEventHandler();
        inline void DestroyEventHandler();
        inline void CreateManagedDelegates();

        void OnWindowResize(u32 newWidth, u32 newHeight) override;
        void OnGamePause() override;
        void OnGameResume() override;

        void PreInit();
        void Init();
        void PostInit();

        void OnDisposing();

    public:
        void Initialize();
        void Crash();

        PD_FORCEINLINE const SGameInfo& GetGameInfo() const {return m_gameInfo; }
		PD_FORCEINLINE SGameState& GetGameState() {return m_gameState; }

		PD_FORCEINLINE const SGameState& GetGameState() const {return m_gameState; }

		PD_FORCEINLINE SConfigClient& GetConfig() {return m_config; }
        PD_FORCEINLINE const SConfigClient& GetConfig() const {return m_config; }
    };
}

#endif //PD_ENGINE_GAME_HPP
