//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 05.04.2020.
//
#ifndef PD_ENGINE_GAMEINFO_HPP
#define PD_ENGINE_GAMEINFO_HPP

#pragma once

#include "Core/Types.hpp"
#include "Core/Singleton.hpp"
#include "Core/String/StringHelper.hpp"
#include <ostream>

namespace PD
{
    struct SGameInfo
    {
        const char* pName;
        u32 Major, Minor, Patch;

        SGameInfo() = default;
        SGameInfo(const char* pName, u32 major, u32 minor, u32 patch) : pName(pName), Major(major), Minor(minor), Patch(patch) {}

        PD_FORCEINLINE bool operator ==(const SGameInfo& other) const
		{
        	return StringHelper::AreEqual(pName, other.pName) && Major == other.Major && Minor == other.Minor && Patch == other.Patch;
		}

		PD_FORCEINLINE bool operator !=(const SGameInfo& other) const
		{
        	return !StringHelper::AreEqual(pName, other.pName) || Major != other.Major || Minor != other.Minor || Patch != other.Patch;
		}

		friend std::ostream& operator <<(std::ostream& stream, const SGameInfo& gameInfo)
		{
        	stream << '[' << gameInfo.pName << ' ' << gameInfo.Major << '.' << gameInfo.Minor << '.' << gameInfo.Patch << ']';
        	return stream;
		}

		friend std::wostream& operator <<(std::wostream& stream, const SGameInfo& gameInfo)
		{
        	stream << L'[' << gameInfo.pName << L' ' << gameInfo.Major << L'.' << gameInfo.Minor << L'.' << gameInfo.Patch << L']';
        	return stream;
		}
    };
}

#endif //PD_ENGINE_GAMEINFO_HPP
