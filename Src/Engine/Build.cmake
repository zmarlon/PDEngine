set(PD_ENGINE_DIRECTORY Src/Engine)

include(Src/Engine/Client/Build.cmake)
include(Src/Engine/Core/Build.cmake)
include(Src/Engine/Entity/Build.cmake)
include(Src/Engine/Server/Build.cmake)
include(Src/Engine/World/Build.cmake)

set(PD_ENGINE_SOURCE_FILES
        ${PD_ENGINE_DIRECTORY}/Game.cpp
        ${PD_ENGINE_DIRECTORY}/Game.hpp
        ${PD_ENGINE_DIRECTORY}/GameInfo.cpp
        ${PD_ENGINE_DIRECTORY}/GameInfo.hpp
        ${PD_ENGINE_DIRECTORY}/GameLauncher.cpp
        ${PD_ENGINE_DIRECTORY}/GameLauncher.hpp
        ${PD_ENGINE_DIRECTORY}/GameState.hpp
        ${PD_ENGINE_CLIENT_SOURCE_FILES}
        ${PD_ENGINE_CORE_SOURCE_FILES}
        ${PD_ENGINE_ENTITY_SOURCE_FILES}
        ${PD_ENGINE_SERVER_SOURCE_FILES}
        ${PD_ENGINE_WORLD_SOURCE_FILES})