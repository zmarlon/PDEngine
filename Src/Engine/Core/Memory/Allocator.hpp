//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_MEMORY_ALLOCATOR_HPP
#define PD_ENGINE_CORE_MEMORY_ALLOCATOR_HPP

#pragma once

#include "../Types.hpp"

namespace PD
{
    class IAllocator
    {
    public:
        virtual void* Allocate(usize size) = 0;
        virtual void* AllocateAligned(usize size, usize alignment) = 0;
        virtual void Deallocate(void* pData) = 0;
        virtual void DeallocateAligned(void* pData) = 0;
    };
}

#endif //PD_ENGINE_CORE_MEMORY_ALLOCATOR_HPP
