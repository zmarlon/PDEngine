//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.05.2020.
//
#include "MemoryStack.hpp"
#include "../Logging/Logger.hpp"

namespace PD
{
    PD_POSTDEFINE_SINGLETON(CMemoryStack)
    void CMemoryStack::OnDisposing()
    {
        for(auto& iterator : m_stackAllocators) iterator.second.Dispose();
        DetachSingleton();
    }

    void CMemoryStack::Create()
    {
        AttachSingleton();
        PD_END_INITIALIZE;
    }

    void CMemoryStack::AttachThread(std::thread::id threadId)
    {
        if(m_stackAllocators.find(threadId) != m_stackAllocators.end())
        {
            CLogger::GetInstance()->Log(ELogLevel::Error) << L"Thread Id " << threadId << L" has been attached multiple times!"
                                                          << CLogger::GetInstance()->Flush();
        }
        else
        {
            m_stackAllocators.emplace(std::piecewise_construct, std::make_tuple(threadId), std::make_tuple());
            m_stackAllocators[threadId].Initialize(1024 * 1024, 32);
        }
    }
}