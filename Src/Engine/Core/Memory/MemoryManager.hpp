//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_MEMORY_MEMORYMANAGER_HPP
#define PD_ENGINE_CORE_MEMORY_MEMORYMANAGER_HPP

#include "../Types.hpp"
#include "../Singleton.hpp"

namespace PD
{
    class CMemoryManager
    {
        PD_MAKE_CLASS_SINGLETON(CMemoryManager)
    private:
        void OnDisposing();
    };
}

#endif //PDENGINE_MEMORYMANAGER_HPP
