//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_MEMORY_PROXYALLOCATOR_HPP
#define PD_ENGINE_CORE_MEMORY_PROXYALLOCATOR_HPP

#pragma once

#ifdef min
#undef min
#endif

#include "../Disposable.hpp"
#include "../Singleton.hpp"
#include "Allocator.hpp"
#include <SmMalloc/smmalloc.h>
#include <utility>

namespace PD
{
    class CProxyAllocator final : public IAllocator
    {
        PD_MAKE_CLASS_DISPOSABLE(CProxyAllocator)
        PD_MAKE_CLASS_SINGLETON(CProxyAllocator)
    private:
        sm_allocator m_allocator;

        void OnDisposing();
    public:
        void Initialize();

        void* Allocate(usize size) final ;
        void* AllocateAligned(usize size, usize alignment) final ;
        void Deallocate(void* pData) final ;
        void DeallocateAligned(void* pData) final ;

        template<typename T, typename... TArguments>
        T* AllocateObject(TArguments&&... arguments)
		{
        	T* pData = reinterpret_cast<T*>(AllocateAligned(sizeof(T), alignof(T)));
        	new (pData) T(std::forward<TArguments>(arguments)...);
        	return pData;
		}

		template<typename T, typename... TArguments>
		T* AllocateObjects(usize numObjects, TArguments&&... arguments)
        {
		    T* pData = reinterpret_cast<T*>(AllocateAligned(sizeof(T) * numObjects, alignof(T)));
		    for(usize ix = 0; ix < numObjects; ix++)
            {
		        new(pData + ix) T(std::forward<TArguments>(arguments)...);
            }
		    return pData;
        }

		template<typename T>
		void DeallocateObject(T* pObject)
		{
			pObject->~T();
			DeallocateAligned(pObject);
		}

		template<typename T>
		void DeallocateObjects(T* pObjects, usize numObjects)
        {
		    for(usize ix = 0; ix < numObjects; ix++) pObjects[ix].~T();
		    DeallocateAligned(pObjects);
        }
    };
}

#endif //PD_ENGINE_CORE_MEMORY_PROXYALLOCATOR_HPP
