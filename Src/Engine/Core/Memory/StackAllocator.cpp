//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.05.2020.
//
#include "StackAllocator.hpp"
#include "SystemAllocator.hpp"
#include "../Logging/Logger.hpp"

namespace PD
{
    void CStackAllocator::OnDisposing()
    {
        PD_ASSERT(m_pData = m_pHead);

        CSystemAllocator::GetInstance()->Deallocate(m_pData);
        m_pData = nullptr;
        m_pHead = nullptr;
        m_size = 0;
        m_memoryOverflowReported = false;
    }

    void CStackAllocator::ReportMemoryOverflow()
    {
        auto& logger = CLogger::GetInstance()->Log(ELogLevel::Warning, L"Stack Allocator");
        if(m_pName)
        {
            logger << L'[' << m_pName << L"] ";
        }
        logger << L"Memory overflow: capacity is " << m_size << L" bytes";

        CLogger::GetInstance()->Flush();
    }

    CStackAllocator::CStackAllocator(usize size, usize numReservedDestructors, usize numReservedProxyAllocations)
    {
        Initialize(size, numReservedDestructors, numReservedProxyAllocations);
    }

    void CStackAllocator::Initialize(usize size, usize numReservedDestructors, usize numReservedProxyAllocations)
    {
        m_size = size;
        m_pData = reinterpret_cast<byte*>(CSystemAllocator::GetInstance()->Allocate(size));
        m_pHead = m_pData;

        m_destructors.reserve(numReservedDestructors);
        m_proxyAllocations.reserve(numReservedProxyAllocations);

        PD_END_INITIALIZE;
    }
}