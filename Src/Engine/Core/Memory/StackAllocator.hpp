//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.05.2020.
//
#ifndef PD_ENGINE_CORE_MEMORY_STACKALLOCATOR_HPP
#define PD_ENGINE_CORE_MEMORY_STACKALLOCATOR_HPP

#pragma once

#include "../Disposable.hpp"
#include "../Types.hpp"
#include "../STL/Vector.hpp"
#include "../Math/Multiples.hpp"
#include <memory>
#include <type_traits>

namespace PD
{
    class CStackAllocator final : public IAllocator
    {
        PD_MAKE_CLASS_DISPOSABLE(CStackAllocator)
    private:
        class CDestructor
        {
        private:
            const void* m_pData;
            void (*m_pfnDestructor)(const void*);

        public:
            template<typename T>
            explicit CDestructor(const T& data) : m_pData(std::addressof(data))
            {
                m_pfnDestructor = [](const void* pLambdaData)
                {
                    auto* pOriginalType = static_cast<const T*>(pLambdaData);
                    pOriginalType->~T();
                };
            }

            PD_FORCEINLINE void operator()()
            {
                m_pfnDestructor(m_pData);
            }
        };

        struct SProxyAllocation
        {
            void* pData;
            bool isAligned;

            SProxyAllocation(void* pData, bool isAligned) : pData(pData), isAligned(isAligned) { PD_ASSERT(pData); }
        };

        byte* m_pData = nullptr;
        byte* m_pHead = nullptr;
        usize m_size = 0;
        STL::CVector<CDestructor> m_destructors;
        STL::CVector<SProxyAllocation> m_proxyAllocations;
        const wchar_t* m_pName;
        bool m_memoryOverflowReported = false;

        template<typename T>
        PD_FORCEINLINE typename std::enable_if<std::is_trivially_destructible<T>::value>::type
        AddDestructorToList(T* pObject) {}

        template<typename T>
        PD_FORCEINLINE typename std::enable_if<!std::is_trivially_destructible<T>::value>::type
        AddDestructorToList(T* pObject)
        {
            m_destructors.emplace_back(*pObject);
        }

        static const usize _DEFAULT_ALIGNMENT = 4;

        void ReportMemoryOverflow();
        void OnDisposing();
    public:
        explicit CStackAllocator(usize size, usize numReservedDestructors = 16, usize numReservedProxyAllocations = 16);
        void Initialize(usize size, usize numReservedDestructors = 16, usize numReservedProxyAllocations = 16);

        PD_FORCEINLINE void SetName(const wchar_t* pName)
        {
            m_pName = pName;
        }

        void* Allocate(usize size) final
        {
            byte* pAllocationLocation = (byte*)Multiples::Next(_DEFAULT_ALIGNMENT, (usize)m_pHead);
            byte* pNewHead = pAllocationLocation + size;

            if(pNewHead <= m_pData + m_size)
            {
                m_pHead = pNewHead;
                return pAllocationLocation;
            }

            if(!m_memoryOverflowReported)
            {
                ReportMemoryOverflow();
                m_memoryOverflowReported = true;
            }

            void* pProxyAllocation = CProxyAllocator::GetInstance()->Allocate(size);
            m_proxyAllocations.emplace_back(pProxyAllocation, false);
            return pProxyAllocation;
        }

        void* AllocateAligned(usize size, usize alignment) override
        {
            byte* pAllocationLocation = (byte*)Multiples::Next(alignment, (usize)m_pHead);
            byte* pNewHead = pAllocationLocation + size;

            if(pNewHead <= m_pData + m_size)
            {
                m_pHead = pNewHead;
                return pAllocationLocation;
            }

            if(!m_memoryOverflowReported)
            {
                ReportMemoryOverflow();
                m_memoryOverflowReported = true;
            }

            void* pProxyAllocation = CProxyAllocator::GetInstance()->AllocateAligned(size, alignment);
            m_proxyAllocations.emplace_back(pProxyAllocation, true);
            return pProxyAllocation;
        }

        template<typename T, typename... TArguments>
        PD_FORCEINLINE T* AllocateObject(TArguments&&... arguments)
        {
            byte* pAllocationLocation = (byte*)Multiples::Next(alignof(T), (usize)m_pHead);
            byte* pNewHead = pAllocationLocation + sizeof(T);

            if(pNewHead <= m_pData + m_size)
            {
                auto* pReturn = reinterpret_cast<T*>(pAllocationLocation);
                m_pHead = pNewHead;

                new (pReturn) T(std::forward<TArguments>(arguments)...);
                AddDestructorToList(pReturn);
                return pReturn;
            }

            if(!m_memoryOverflowReported)
            {
                ReportMemoryOverflow();
                m_memoryOverflowReported = true;
            }

            auto* pProxyAllocation = reinterpret_cast<T*>(CProxyAllocator::GetInstance()->AllocateAligned(sizeof(T), alignof(T)));
            new (pProxyAllocation) T(std::forward<TArguments>(arguments)...);
            AddDestructorToList(pProxyAllocation);
            m_proxyAllocations.emplace_back(pProxyAllocation, true);
            return pProxyAllocation;
        }

        template<typename T, typename... TArguments>
        PD_FORCEINLINE T* AllocateObjects(usize numObjects, TArguments&&... arguments)
        {
            byte* pAllocationLocation = (byte*)Multiples::Next(alignof(T), (usize)m_pHead);
            byte* pNewHead = pAllocationLocation + numObjects * sizeof(T);

            if(pNewHead <= m_pData + m_size)
            {
                auto* pReturn = reinterpret_cast<T*>(pAllocationLocation);
                m_pHead = pNewHead;

                for(usize ix = 0; ix < numObjects; ix++)
                {
                    new (pReturn + ix) T(std::forward<TArguments>(arguments)...);
                    AddDestructorToList(pReturn);
                }
                return pReturn;
            }

            if(!m_memoryOverflowReported)
            {
                ReportMemoryOverflow();
                m_memoryOverflowReported = true;
            }

            auto* pProxyAllocation = reinterpret_cast<T*>(CProxyAllocator::GetInstance()->AllocateAligned(sizeof(T), alignof(T)));
            for(usize ix = 0; ix < numObjects; ix++)
            {
                new (pProxyAllocation + ix) T(std::forward<TArguments>(arguments)...);
                AddDestructorToList(pProxyAllocation);
            }
            m_proxyAllocations.emplace_back(pProxyAllocation, true);
            return pProxyAllocation;
        }

        void Deallocate(void* pData) override {}
        void DeallocateAligned(void* pData) override {}

        PD_FORCEINLINE void DeallocateAll()
        {
            m_pHead = m_pData;
            for(auto& destructor : m_destructors) destructor();
            m_destructors.clear();

            for(auto& proxyAllocation : m_proxyAllocations)
            {
                if(proxyAllocation.isAligned)
                {
                    CProxyAllocator::GetInstance()->DeallocateAligned(proxyAllocation.pData);
                }
                else
                {
                    CProxyAllocator::GetInstance()->Deallocate(proxyAllocation.pData);
                }
            }
            m_proxyAllocations.clear();
        }
    };
}

#endif //PD_ENGINE_CORE_MEMORY_STACKALLOCATOR_HPP
