//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.05.2020.
//
#ifndef PD_ENGINE_CORE_MEMORY_MEMORYSTACK_HPP
#define PD_ENGINE_CORE_MEMORY_MEMORYSTACK_HPP

#pragma once

#include "StackAllocator.hpp"
#include "../STL/Map.hpp"
#include <thread>

namespace PD
{
    class CMemoryStack
    {
        PD_MAKE_CLASS_DISPOSABLE(CMemoryStack)
        PD_MAKE_CLASS_SINGLETON(CMemoryStack)
    private:
        STL::CMap<std::thread::id, CStackAllocator> m_stackAllocators;

        void OnDisposing();
    public:
        void Create();
        void AttachThread(std::thread::id threadId);

        PD_FORCEINLINE void AttachCurrentThread()
        {
            AttachThread(std::this_thread::get_id());
        }

        PD_FORCEINLINE CStackAllocator& Push()
        {
            auto threadId = std::this_thread::get_id();
            PD_ASSERT(m_stackAllocators.find(threadId) != m_stackAllocators.end());
            return m_stackAllocators[threadId];
        }

        PD_FORCEINLINE void Pop(CStackAllocator& stackAllocator)
        {
            stackAllocator.DeallocateAll();
        }
    };
}

#endif //PD_ENGINE_CORE_MEMORY_MEMORYSTACK_HPP
