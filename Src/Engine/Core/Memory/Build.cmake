set(PD_ENGINE_CORE_MEMORY_DIRECTORY Src/Engine/Core/Memory)

set(PD_ENGINE_CORE_MEMORY_SOURCE_FILES
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/Allocator.hpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/MemoryHelper.hpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/MemoryManager.cpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/MemoryManager.hpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/MemoryStack.cpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/MemoryStack.hpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/PoolAllocator.cpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/PoolAllocator.hpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/ProxyAllocator.cpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/ProxyAllocator.hpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/StackAllocator.cpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/StackAllocator.hpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/SystemAllocator.cpp
        ${PD_ENGINE_CORE_MEMORY_DIRECTORY}/SystemAllocator.hpp)