//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#include "SystemAllocator.hpp"
#include <cstdlib>

namespace PD
{
    static CSystemAllocator _S_SYSTEM_ALLOCATOR;

    PD_POSTDEFINE_SINGLETON(CSystemAllocator)
    CSystemAllocator::CSystemAllocator()
    {
        AttachSingleton();
    }

    CSystemAllocator::~CSystemAllocator()
    {
        DetachSingleton();
    }

    void* CSystemAllocator::Allocate(usize size)
    {
        return malloc(size);
    }

    void* CSystemAllocator::AllocateAligned(usize size, usize alignment)
    {
#ifdef PD_PLATFORM_WINDOWS
        return _aligned_malloc(size, alignment);
#elif defined(PD_PLATFORM_LINUX)
        return aligned_alloc(alignment, size);
#else
#error "Unsupported operating system"
#endif
    }

    void CSystemAllocator::Deallocate(void* pData)
    {
        PD_ASSERT(pData);
        free(pData);
    }

    void CSystemAllocator::DeallocateAligned(void* pData)
    {
        PD_ASSERT(pData);
#ifdef PD_PLATFORM_WINDOWS
        _aligned_free(pData);
#elif defined(PD_PLATFORM_LINUX)
        free(pData);
#else
#error "Unsupported operating system"
#endif
    }
}