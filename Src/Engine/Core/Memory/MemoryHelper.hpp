//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 05.04.2020.
//
#ifndef PD_ENGINE_CORE_MEMORY_MEMORYHELPER_HPP
#define PD_ENGINE_CORE_MEMORY_MEMORYHELPER_HPP

#pragma once

#include "../Hints/ForceInline.hpp"
#include "../Types.hpp"
#include <cstring>
#include <utility>

#define PD_DEFAULT_ALIGNMENT 4

namespace PD::MemoryHelper
{
    PD_FORCEINLINE void Copy(void* pDestination, const void* pSource, usize size)
    {
        memcpy(pDestination, pSource, size);
    }

    template<typename T>
    PD_FORCEINLINE void CopyElements(T* pDestination, T* pSource, usize numElements)
    {
        for(usize ix = 0; ix < numElements; ix++)
        {
            new (pDestination + ix) T(pSource[ix]);
            pSource->~T();
        }
    }

    PD_FORCEINLINE void Move(void* pDestination, void* pSource, usize size)
    {
        memmove(pDestination, pSource, size);
    }

    template<typename T>
    PD_FORCEINLINE void MoveElements(T* pDestination, T* pSource, usize numElements)
    {
        for(usize ix = 0; ix < numElements; ix++)
        {
            new (pDestination + ix) T(std::move(pSource[ix]));
            pSource->~T();
        }
    }

    PD_FORCEINLINE void Set(void* pData, i32 value, usize);

    PD_FORCEINLINE void Zero(void* pData, usize size)
    {
        memset(pData, 0, size);
    }

    template<typename T>
    PD_FORCEINLINE void ZeroElements(T* pData, usize numElements)
    {
        Zero(pData, numElements * sizeof(T));
    }
}

#endif //PD_ENGINE_CORE_MEMORY_MEMORYHELPER_HPP
