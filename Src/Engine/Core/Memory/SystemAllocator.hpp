//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_MEMORY_SYSTEMALLOCATOR_HPP
#define PD_ENGINE_CORE_MEMORY_SYSTEMALLOCATOR_HPP

#pragma once

#include "Allocator.hpp"
#include "../Singleton.hpp"
#include <utility>

namespace PD
{
    class CSystemAllocator final : public IAllocator
    {
        PD_MAKE_CLASS_SINGLETON(CSystemAllocator)
    public:
        CSystemAllocator();
        ~CSystemAllocator();

        void* Allocate(usize size) final;
        void* AllocateAligned(usize size, usize alignment) final;
        void Deallocate(void* pData) final;
        void DeallocateAligned(void* pData) final;

        template<typename T, typename... TArguments>
        PD_FORCEINLINE T* AllocateObject(TArguments&&... arguments)
        {
            T* pData = reinterpret_cast<T*>(AllocateAligned(sizeof(T), alignof(T)));
            new (pData) T(std::forward<TArguments>(arguments)...);
            return pData;
        }

        template<typename T>
        PD_FORCEINLINE void DeallocateObject(T* pData)
        {
            pData->~T();
            DeallocateAligned(pData);
        }
    };
}


#endif //PD_ENGINE_CORE_MEMORY_SYSTEMALLOCATOR_HPP
