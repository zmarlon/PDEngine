//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.05.2020.
//
#ifndef PD_ENGINE_CORE_MEMORY_POOLALLOCATOR_HPP
#define PD_ENGINE_CORE_MEMORY_POOLALLOCATOR_HPP

#pragma once

#include "../Disposable.hpp"
#include "../Types.hpp"
#include "../Memory/ProxyAllocator.hpp"

namespace PD
{
    namespace INTERNAL
    {
        void ReportPoolAllocatorMemoryOverflow(const wchar_t* pName, usize allocatorSize);
    }

    template<typename T>
    class CPoolAllocator
    {
        PD_MAKE_CLASS_DISPOSABLE(CPoolAllocator)
    private:
        union UChunk
        {
            T Value;
            UChunk* pNext;

            UChunk() {}
            ~UChunk() {}
        };

        usize m_size = 0;
        UChunk* m_pData = nullptr;
        UChunk* m_pHead = nullptr;
        usize m_openAllocations = 0;
        IAllocator* m_pAllocator = nullptr;
        bool m_memoryOverflowReported = false;
        const wchar_t* m_pName;

        void OnDisposing()
        {
            PD_ASSERT(m_openAllocations == 0);
            m_pAllocator->DeallocateAligned(m_pData);

            m_pData = nullptr;
            m_pHead = nullptr;
            m_pAllocator = nullptr;
            m_size = 0;
            m_memoryOverflowReported = false;
        }
    public:
        PD_FORCEINLINE explicit CPoolAllocator(usize size, IAllocator* pAllocator = CProxyAllocator::GetInstance())
        {
            Create(size, pAllocator);
        }

        void Create(usize size, IAllocator* pAllocator = CProxyAllocator::GetInstance())
        {
            PD_ASSERT(size);
            PD_ASSERT(pAllocator);

            m_size = size;
            m_pAllocator = pAllocator;

            m_pData = reinterpret_cast<UChunk*>(pAllocator->AllocateAligned(size * sizeof(T), alignof(T)));
            m_pHead = m_pData;

            for(usize ix = 0; ix < m_size - 1; ix++)
            {
                m_pData[ix].pNext = m_pData + ix + 1;
            }
            m_pData[m_size - 1].pNext = nullptr;

            PD_END_INITIALIZE;
        }

        PD_FORCEINLINE void SetName(const wchar_t* pName)
        {
            m_pName = pName;
        }

        template<typename... TArguments>
        T* AllocateObject(TArguments&&... arguments)
        {
            if(m_pHead)
            {
                auto* pPoolChunk = m_pHead;
                m_pHead = m_pHead->pNext;

                T* pObject = new(std::addressof(pPoolChunk->Value)) T(std::forward<TArguments>(arguments)...);
                m_openAllocations++;
                return pObject;
            }

            if(!m_memoryOverflowReported)
            {
                INTERNAL::ReportPoolAllocatorMemoryOverflow(m_pName, m_size);
                m_memoryOverflowReported = true;
            }

            auto* pProxyAllocation = reinterpret_cast<T*>(m_pAllocator->AllocateAligned(sizeof(T), alignof(T)));
            new (pProxyAllocation) T(std::forward<TArguments>(arguments)...);
            return pProxyAllocation;
        }

        void DeallocateObject(T* pObject)
        {
            PD_ASSERT(pObject);

            pObject->~T();
            auto* pPoolChunk = reinterpret_cast<UChunk*>(pObject);

            if(pPoolChunk >= m_pData && pPoolChunk <= m_pData + m_size - 1)
            {
                pPoolChunk->pNext = m_pHead;
                m_pHead = pPoolChunk;
            } else m_pAllocator->DeallocateAligned(pObject);
            m_openAllocations--;
        }
    };
}

#endif //PD_ENGINE_CORE_MEMORY_POOLALLOCATOR_HPP
