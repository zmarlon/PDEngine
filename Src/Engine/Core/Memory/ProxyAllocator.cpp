//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#include "ProxyAllocator.hpp"
#include "MemoryHelper.hpp"
#include "../Logging/LogLevel.hpp"
#include "../../Game.hpp"
#include "SystemAllocator.hpp"

#include <SmMalloc/smmalloc.cpp>
#include <SmMalloc/smmalloc_generic.cpp>
#include <SmMalloc/smmalloc_tls.cpp>

namespace PD
{
    PD_POSTDEFINE_SINGLETON(CProxyAllocator)
#if 0
    void CProxyAllocator::OnDisposing()
    {
        DetachSingleton();

        _sm_allocator_thread_cache_destroy(m_allocator);
        _sm_allocator_destroy(m_allocator);
    }


    void CProxyAllocator::Initialize()
    {
        m_allocator = _sm_allocator_create(5, (48 * 1024 * 1024));
        if(!m_allocator)
        {
            printf("Failed to create proxy allocator!\n");
            CGame::GetInstance()->Crash();
        }
        _sm_allocator_thread_cache_create(m_allocator, sm::CACHE_WARM, { 16384, 131072, 131072, 131072, 131072 });

        AttachSingleton();

        PD_END_INITIALIZE;
    }

    void* CProxyAllocator::Allocate(usize size)
    {
        return _sm_malloc(m_allocator, size, PD_DEFAULT_ALIGNMENT);
    }

    void* CProxyAllocator::AllocateAligned(usize size, usize alignment)
    {
        return _sm_malloc(m_allocator, size, alignment);
    }

    void CProxyAllocator::Deallocate(void* pData)
    {
        PD_ASSERT(pData);
        _sm_free(m_allocator, pData);
    }

    void CProxyAllocator::DeallocateAligned(void* pData)
    {
        PD_ASSERT(pData);
        _sm_free(m_allocator, pData);
    }
#else

    void CProxyAllocator::OnDisposing()
    {
        DetachSingleton();
    }

    void CProxyAllocator::Initialize()
    {
        AttachSingleton();
        PD_END_INITIALIZE;
    }

    void* CProxyAllocator::Allocate(usize size)
    {
        return CSystemAllocator::GetInstance()->Allocate(size);
    }

    void* CProxyAllocator::AllocateAligned(usize size, usize alignment)
    {
        return CSystemAllocator::GetInstance()->AllocateAligned(size, alignment);
    }

    void CProxyAllocator::Deallocate(void* pData)
    {
        CSystemAllocator::GetInstance()->Deallocate(pData);
    }

    void CProxyAllocator::DeallocateAligned(void* pData)
    {
        CSystemAllocator::GetInstance()->DeallocateAligned(pData);
    }

#endif
}