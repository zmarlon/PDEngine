//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.05.2020.
//
#include "PoolAllocator.hpp"
#include "../Logging/Logger.hpp"

namespace PD::INTERNAL
{
    void ReportPoolAllocatorMemoryOverflow(const wchar_t* pName, usize allocatorSize)
    {
        auto& logger = CLogger::GetInstance()->Log(ELogLevel::Warning, L"Pool Allocator");
        if(pName)
        {
            logger << L'[' << pName << L"] ";
        }
        logger << L"Memory overflow: capacity is " << allocatorSize << L" bytes";

        CLogger::GetInstance()->Flush();
    }
}