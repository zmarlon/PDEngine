//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#include "LogAppender.hpp"

namespace PD
{
	ILogAppender::ILogAppender(const STL::CWString& name) : m_name(name) {}
}