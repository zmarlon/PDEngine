//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#include "SimpleLayout.hpp"

namespace PD
{
	const STL::CWString& CSimpleLayout::Format(const SLogEvent& logEvent)
	{
		return logEvent.Message;
	}

	void CSimpleLayout::OutputFormat(std::wostream& ostream, const SLogEvent& logEvent)
	{
		ostream << logEvent.Message;
	}
}