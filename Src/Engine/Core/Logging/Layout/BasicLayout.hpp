//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_LOGGING_LAYOUT_BASICLAYOUT_HPP
#define PD_ENGINE_CORE_LOGGING_LAYOUT_BASICLAYOUT_HPP

#pragma once

#include "../LogLayout.hpp"
#include "../../STL/StringStream.hpp"

namespace PD
{
    class CBasicLayout : public ILogLayout
    {
    private:
        STL::CWStringStream m_stream;
        const wchar_t* m_pTimeFormat;

        STL::CWString m_formatString;
    public:
        explicit CBasicLayout(const wchar_t* pTimeFormat);

        PD_FORCEINLINE void SetFormat(const wchar_t* pTimeFormat)
        {
            PD_ASSERT(pTimeFormat);
            m_pTimeFormat = pTimeFormat;
        }

        const STL::CWString& Format(const SLogEvent& logEvent) override;
        void OutputFormat(std::wostream& ostream, const SLogEvent& logEvent) override;
    };

}


#endif //PD_ENGINE_CORE_LOGGING_LAYOUT_BASICLAYOUT_HPP
