//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#include "BasicLayout.hpp"
#include "../LogEvent.hpp"
#include "../../Threading/ThreadExtensions.hpp"

namespace PD
{
    CBasicLayout::CBasicLayout(const wchar_t* pTimeFormat) : m_pTimeFormat(pTimeFormat)
    {
        PD_ASSERT(pTimeFormat);
    }

    const STL::CWString& CBasicLayout::Format(const SLogEvent& logEvent)
    {
        m_stream.clear();

        OutputFormat(m_stream, logEvent);

        m_formatString = m_stream.str();
        return m_formatString;
    }

    void CBasicLayout::OutputFormat(std::wostream& ostream, const SLogEvent& logEvent)
    {
        auto time = logEvent.Time;
        time.SetFormat(m_pTimeFormat);

        ostream << L'[' << time << L"] [" << ThreadExtensions::GetCurrentThreadName() << '/' << LogLevel::GetName(logEvent.LogLevel) << L"] ";
        if(logEvent.pPrefix)
        {
            ostream << L'[' << logEvent.pPrefix << L"] ";
        }
        ostream << logEvent.Message;
    }
}