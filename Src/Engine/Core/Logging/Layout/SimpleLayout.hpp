//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_LOGGING_LAYOUT_SIMPLELAYOUT_HPP
#define PD_ENGINE_CORE_LOGGING_LAYOUT_SIMPLELAYOUT_HPP

#pragma once

#include "../LogLayout.hpp"
#include "../../STL/String.hpp"
#include "../LogEvent.hpp"

namespace PD
{
	class CSimpleLayout : public ILogLayout
	{
	public:
		CSimpleLayout() = default;

		const STL::CWString& Format(const SLogEvent& logEvent) override;
		void OutputFormat(std::wostream& ostream, const SLogEvent& logEvent) override;
	};
}

#endif //PD_ENGINE_CORE_LOGGING_LAYOUT_SIMPLELAYOUT_HPP
