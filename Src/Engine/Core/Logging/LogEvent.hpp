//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_LOGGING_LOGEVENT_HPP
#define PD_ENGINE_CORE_LOGGING_LOGEVENT_HPP

#include "../STL/String.hpp"
#include "LogLevel.hpp"
#include "../Time/DateTime.hpp"

namespace PD
{
	struct SLogEvent
	{
		STL::CWString Message;
		ELogLevel LogLevel;
		CDateTime Time;
		const wchar_t* pPrefix;
	};
}

#endif //PD_ENGINE_CORE_LOGGING_LOGEVENT_HPP
