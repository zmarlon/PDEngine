//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#include "LogLevel.hpp"

namespace PD::LogLevel
{
    const wchar_t* GetName(ELogLevel logLevel)
    {
        switch(logLevel)
        {
            case ELogLevel::Information: return L"Information";
            case ELogLevel::Success: return L"Success";
            case ELogLevel::Warning: return L"Warning";
            case ELogLevel::Error: return L"Error";
            case ELogLevel::Fatal: return L"Fatal";
            default: return L"Unknown LogLevel";
        }
    }

    const wchar_t* GetParenthesizedName(ELogLevel logLevel)
    {
        switch(logLevel)
        {
            case ELogLevel::Information: return L"[Information]";
            case ELogLevel::Success: return L"[Success]";
            case ELogLevel::Warning: return L"[Warning]";
            case ELogLevel::Error: return L"[Error]";
            case ELogLevel::Fatal: return L"[Fatal]";
            default: return L"[Unknown LogLevel]";
        }
    }

	EConsoleColor GetConsoleColor(ELogLevel logLevel)
	{
		switch(logLevel)
		{
			case ELogLevel::Information: return EConsoleColor::Gray;
			case ELogLevel::Success: return EConsoleColor::Green;
			case ELogLevel::Warning: return EConsoleColor::Yellow;
			case ELogLevel::Error:
			case ELogLevel::Fatal:
				return EConsoleColor::Red;
			default:
				return EConsoleColor::Green;
		}
	}
}