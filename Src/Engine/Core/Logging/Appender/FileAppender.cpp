//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#include "FileAppender.hpp"
#include "../Logger.hpp"
#include "../LogLayout.hpp"
#include "../LogEvent.hpp"
#include <iostream>

namespace PD
{
	CFileAppender::CFileAppender(const STL::CWString& name, const STL::CString& fileName, const char* pFileEnding, bool appendDate, const wchar_t* pTimeFormat) : ILogAppender(name)
	{
        STL::CString totalPath = fileName;
        if(appendDate)
        {
            PD_ASSERT(pTimeFormat);

            CDateTime dateTime = CDateTime::Now();
            dateTime.SetFormat(pTimeFormat);

            dateTime.AppendToString(totalPath);
        }
        totalPath += '.';
        totalPath += pFileEnding;

		m_stream.open(totalPath.c_str());
		if(!m_stream.is_open())
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to open file appender on file: " << totalPath.c_str() << CLogger::GetInstance()->Flush();
		}
	}

	void CFileAppender::Append(const SLogEvent& logEvent)
	{
		if(m_layout)
		{
			m_layout->OutputFormat(m_stream, logEvent);
		} else
		{
			m_stream << logEvent.Message;
		}
		m_stream.flush();
	}

	void CFileAppender::Close()
	{
		m_stream.close();
	}
}