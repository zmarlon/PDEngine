//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#include "OstreamAppender.hpp"
#include "../LogLayout.hpp"
#include "../LogEvent.hpp"
#include "../../Console/Console.hpp"

namespace PD
{
	COstreamAppender::COstreamAppender(const STL::CWString& name, std::wostream* pOstream) : m_pOstream(pOstream), ILogAppender(name)
	{
		PD_ASSERT(pOstream);
	}

	void COstreamAppender::Append(const SLogEvent& logEvent)
	{
		Console::SetForegroundColor(LogLevel::GetConsoleColor(logEvent.LogLevel));

		if(m_layout)
		{
			m_layout->OutputFormat(*m_pOstream, logEvent);
		}
		else
		{
			(*m_pOstream) << logEvent.Message;
		}
		m_pOstream->flush();

		Console::ResetColors();
	}

	void COstreamAppender::Close() {}
}