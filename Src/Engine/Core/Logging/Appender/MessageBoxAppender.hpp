//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_LOGGING_APPENDER_MESSAGEBOXAPPENDER_HPP
#define PD_ENGINE_CORE_LOGGING_APPENDER_MESSAGEBOXAPPENDER_HPP

#pragma once

#include "../LogAppender.hpp"

namespace PD
{
    class CMessageBoxAppender : public ILogAppender
    {
    public:
        CMessageBoxAppender(const STL::CWString& name);

        void Append(const SLogEvent& logEvent) override;
        void Close() override;
    };

}

#endif //PD_ENGINE_CORE_LOGGING_APPENDER_MESSAGEBOXAPPENDER_HPP
