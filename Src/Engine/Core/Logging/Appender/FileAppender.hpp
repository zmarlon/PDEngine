//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_LOGGING_APPENDER_FILEAPPENDER_HPP
#define PD_ENGINE_CORE_LOGGING_APPENDER_FILEAPPENDER_HPP

#pragma once

#include "../LogAppender.hpp"
#include "../../STL/String.hpp"
#include <fstream>
#include <ostream>

namespace PD
{
	class CFileAppender : public ILogAppender
	{
	private:
		std::wofstream m_stream;

	public:
		CFileAppender(const STL::CWString& name, const STL::CString& fileName, const char* pFileEnding, bool appendDate = false, const wchar_t* pTimeFormat = nullptr);

		void Append(const SLogEvent& logEvent) override;
		void Close() override;
	};
}


#endif //PD_ENGINE_CORE_LOGGING_APPENDER_FILEAPPENDER_HPP
