//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#include "MessageBoxAppender.hpp"
#include "../LogLayout.hpp"
#include "../LogEvent.hpp"
#include "../../Window/MessageBox.hpp"
#include "../../Window/Window.hpp"

namespace PD
{
    CMessageBoxAppender::CMessageBoxAppender(const STL::CWString& name) : ILogAppender(name) {}

    void CMessageBoxAppender::Append(const SLogEvent& logEvent)
    {
        const auto& str = m_layout->Format(logEvent);
        auto style = MessageBoxStyle::FromLogLevel(logEvent.LogLevel);

        if(CWindow::IsValid())
        {
            MessageBox::Show(*CWindow::GetInstance(), str.c_str(), MessageBoxStyle::GetName(style), style);
        }
        else
        {
            MessageBox::Show(str.c_str(), MessageBoxStyle::GetName(style), style);
        }
    }

    void CMessageBoxAppender::Close() {}
}
