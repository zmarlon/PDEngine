set(PD_ENGINE_CORE_LOGGING_DIRECTORY Src/Engine/Core/Logging)

set(PD_ENGINE_CORE_LOGGING_SOURCE_FILES
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Appender/FileAppender.cpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Appender/FileAppender.hpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Appender/MessageBoxAppender.cpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Appender/MessageBoxAppender.hpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Appender/OstreamAppender.cpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Appender/OstreamAppender.hpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Layout/BasicLayout.cpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Layout/BasicLayout.hpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Layout/SimpleLayout.cpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Layout/SimpleLayout.hpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/LogLevel.cpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/LogLevel.hpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/LogLayout.hpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/LogEvent.hpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Logger.cpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/Logger.hpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/LogAppender.cpp
        ${PD_ENGINE_CORE_LOGGING_DIRECTORY}/LogAppender.hpp)