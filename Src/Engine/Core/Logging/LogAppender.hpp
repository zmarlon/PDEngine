//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_LOGGING_LOGAPPENDER_HPP
#define PD_ENGINE_CORE_LOGGING_LOGAPPENDER_HPP

#pragma once

#include "../STL/String.hpp"
#include "LogLevel.hpp"
#include <memory>

namespace PD
{
	struct SLogEvent;
	class ILogLayout;
	class ILogAppender
	{
	protected:
		const STL::CWString m_name;
		std::shared_ptr<ILogLayout> m_layout;
		ELogLevel m_minLogLevel = ELogLevel::Information;

	public:
		ILogAppender(const STL::CWString& name);

		PD_FORCEINLINE void SetLayout(std::shared_ptr<ILogLayout> layout)
		{
			PD_ASSERT(layout);
			m_layout = layout;
		}

		PD_FORCEINLINE bool HasLayout() const { return m_layout != nullptr; }
		PD_FORCEINLINE void SetMinLevel(ELogLevel logLevel) { m_minLogLevel = logLevel; }
		PD_FORCEINLINE ELogLevel GetMinLevel() const { return m_minLogLevel; }

		virtual void Append(const SLogEvent& logEvent) = 0;
		virtual void Close() = 0;

		PD_FORCEINLINE const STL::CWString& GetName() const { return m_name; }
	};

}

#endif //PD_ENGINE_CORE_LOGGING_LOGAPPENDER_HPP
