//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_LOGGING_LOGLEVEL_HPP
#define PD_ENGINE_CORE_LOGGING_LOGLEVEL_HPP

#pragma once

#include "../Types.hpp"
#include "../Console/ConsoleColor.hpp"

namespace PD
{
	enum class ELogLevel : u32
	{
		Information = 0,
		Success = 1,
		Warning = 2,
		Error = 3,
		Fatal = 4
	};

	namespace LogLevel
	{
		const wchar_t* GetName(ELogLevel logLevel);
		const wchar_t* GetParenthesizedName(ELogLevel logLevel);
		EConsoleColor GetConsoleColor(ELogLevel logLevel);
	}
}

#endif //PD_ENGINE_CORE_LOGGING_LOGLEVEL_HPP
