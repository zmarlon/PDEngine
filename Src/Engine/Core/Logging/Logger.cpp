//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#include "Logger.hpp"
#include "LogEvent.hpp"
#include "LogAppender.hpp"
#include "../../Game.hpp"

namespace PD
{
	static const wchar_t _S_NULL_CHAR = L'\0';

	PD_POSTDEFINE_SINGLETON(CLogger)

	void CLogger::OnDisposing()
	{
		DetachSingleton();
		Close();
	}

	void CLogger::Close()
	{
		for(auto& appender : m_appenders)
		{
			appender->Close();
		}
	}

	void CLogger::Initialize()
	{
		AttachSingleton();

		PD_END_INITIALIZE;
	}

	void CLogger::AddAppender(std::shared_ptr<ILogAppender> appender)
	{
		PD_ASSERT(appender);
		m_appenders.push_back(appender);
	}

	STL::CWStringStream& CLogger::Log(ELogLevel logLevel, const wchar_t* pPrefix)
	{
#ifdef PD_DEBUG
		PD_ASSERT(!m_isLogging);
#endif

		m_mutex.lock();

		m_lastLogLevel = logLevel;
		m_pLastPrefix = pPrefix;

#ifdef PD_DEBUG
		m_isLogging = true;
#endif
		return m_stream;
	}

	STL::CWStringStream& CLogger::Out()
	{
#ifdef PD_DEBUG
		PD_ASSERT(m_isLogging);
#endif
		return m_stream;
	}

	const wchar_t* CLogger::Flush()
	{
#ifdef PD_DEBUG
		PD_ASSERT(m_isLogging);
#endif

		m_stream << '\n';

		SLogEvent logEvent;
		logEvent.Message = m_stream.str();
		logEvent.LogLevel = m_lastLogLevel;
		logEvent.Time = CDateTime::Now();
		logEvent.pPrefix = m_pLastPrefix;

		for(auto& appender : m_appenders)
		{
			if(static_cast<int>(m_lastLogLevel) >= static_cast<int>(appender->GetMinLevel()))
			{
				appender->Append(logEvent);
			}
		}

		m_stream = STL::CWStringStream();

		if(m_lastLogLevel == ELogLevel::Fatal)
		{
			Close();
			CGame::GetInstance()->Crash();
			std::exit(1);
		}
#ifdef PD_DEBUG
		m_isLogging = false;
#endif

		m_mutex.unlock();

		return &_S_NULL_CHAR;
	}
}
