//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_LOGGING_LOGLAYOUT_HPP
#define PD_ENGINE_CORE_LOGGING_LOGLAYOUT_HPP

#pragma once

#include "../STL/String.hpp"
#include <ostream>

namespace PD
{
	struct SLogEvent;
	class ILogLayout
	{
	public:
		virtual ~ILogLayout() {}

		virtual const STL::CWString& Format(const SLogEvent& logEvent) = 0;
		virtual void OutputFormat(std::wostream& ostream, const SLogEvent& logEvent) = 0;
	};
}

#endif //PD_ENGINE_CORE_LOGGING_LOGLAYOUT_HPP
