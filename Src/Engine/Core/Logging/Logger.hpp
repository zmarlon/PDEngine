//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_LOGGING_LOGGER_HPP
#define PD_ENGINE_CORE_LOGGING_LOGGER_HPP

#pragma once

#include "../Disposable.hpp"
#include "../Singleton.hpp"
#include "../STL/StringStream.hpp"
#include "../STL/Vector.hpp"
#include "LogLevel.hpp"
#include <mutex>
#include <memory>

namespace PD
{
	class ILogAppender;
	class CLogger
	{
		PD_MAKE_CLASS_DISPOSABLE(CLogger)
		PD_MAKE_CLASS_SINGLETON(CLogger)
	private:
		STL::CWStringStream m_stream;
		STL::CVector<std::shared_ptr<ILogAppender>> m_appenders;
		ELogLevel m_lastLogLevel;
		const wchar_t* m_pLastPrefix;

		std::mutex m_mutex;

#ifdef PD_DEBUG
		bool m_isLogging = false;
#endif
		inline void Close();
		void OnDisposing();
	public:
		void Initialize();
		void AddAppender(std::shared_ptr<ILogAppender> appender);

		STL::CWStringStream& Log(ELogLevel logLevel, const wchar_t* pPrefix = nullptr);
		STL::CWStringStream& Out();
		const wchar_t* Flush();
	};

}

#endif //PD_ENGINE_CORE_LOGGING_LOGGER_HPP
