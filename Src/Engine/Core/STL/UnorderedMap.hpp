//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_STL_UNORDEREDMAP_H
#define PD_ENGINE_CORE_STL_UNORDEREDMAP_H

#include "STLAllocator.hpp"
#include <unordered_map>

namespace PD::STL
{
	template<typename K, typename V, typename THasher = std::hash<K>, typename TEqualityComparer = std::equal_to<K>>
	using CUnorderedMap = std::unordered_map<K, V, THasher, TEqualityComparer, CAllocator<std::pair<const K, V>>>;
}

#endif //PD_ENGINE_CORE_STL_UNORDEREDMAP_H
