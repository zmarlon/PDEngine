//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 27.04.2020.
//
#ifndef PD_ENGINE_CORE_STL_DEQUE_HPP
#define PD_ENGINE_CORE_STL_DEQUE_HPP

#pragma once

#include "STLAllocator.hpp"
#include <deque>

namespace PD::STL
{
    template<typename T>
    using CDeque = std::deque<T, CAllocator<T>>;
}

#endif //PD_ENGINE_CORE_STL_DEQUE_HPP
