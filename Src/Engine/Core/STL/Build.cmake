set(PD_ENGINE_CORE_STL_DIRECTORY Src/Engine/Core/STL)

set(PD_ENGINE_CORE_STL_SOURCE_FILES
        ${PD_ENGINE_CORE_STL_DIRECTORY}/Deque.hpp
        ${PD_ENGINE_CORE_STL_DIRECTORY}/List.hpp
        ${PD_ENGINE_CORE_STL_DIRECTORY}/Map.hpp
        ${PD_ENGINE_CORE_STL_DIRECTORY}/Queue.hpp
        ${PD_ENGINE_CORE_STL_DIRECTORY}/Set.hpp
        ${PD_ENGINE_CORE_STL_DIRECTORY}/STLAllocator.hpp
        ${PD_ENGINE_CORE_STL_DIRECTORY}/String.hpp
        ${PD_ENGINE_CORE_STL_DIRECTORY}/StringStream.hpp
        ${PD_ENGINE_CORE_STL_DIRECTORY}/UnorderedMap.hpp
        ${PD_ENGINE_CORE_STL_DIRECTORY}/UnorderedSet.hpp
        ${PD_ENGINE_CORE_STL_DIRECTORY}/Vector.hpp)

