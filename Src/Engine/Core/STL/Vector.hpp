//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_STL_VECTOR_HPP
#define PD_ENGINE_CORE_STL_VECTOR_HPP

#include "STLAllocator.hpp"
#include <vector>

namespace PD::STL
{
	template<typename T>
	using CVector = std::vector<T, CAllocator<T>>;
}

#endif //PD_ENGINE_CORE_STL_VECTOR_HPP
