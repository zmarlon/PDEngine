//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_STL_UNORDEREDSET_HPP
#define PD_ENGINE_CORE_STL_UNORDEREDSET_HPP

#include "STLAllocator.hpp"
#include <unordered_set>

namespace PD::STL
{
	template<typename T, typename THasher = std::hash<T>, typename TEqualityComparer = std::equal_to<T>>
	using CUnorderedSet = std::unordered_set<T, THasher, TEqualityComparer, CAllocator<T>>;
}

#endif //PD_ENGINE_CORE_STL_UNORDEREDSET_HPP
