//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_STL_STRING_HPP
#define PD_ENGINE_CORE_STL_STRING_HPP

#include "STLAllocator.hpp"
#include <string>
#include <utility>

namespace PD::STL
{
	template<typename TChar>
	using CBasicString = std::basic_string<TChar, std::char_traits<TChar>, CAllocator<TChar>>;

    using CString = CBasicString<char>;
    using CWString = CBasicString<wchar_t>;
}

namespace std
{
    using namespace PD;

    template<typename TChar>
    struct hash<STL::CBasicString<TChar>>
    {
        usize operator ()(const STL::CBasicString<TChar>& str) const
        {
            return std::hash<std::basic_string_view<TChar, std::char_traits<TChar>>>()(str);
        }
    };
}

#endif //PD_ENGINE_CORE_STL_STRING_HPP