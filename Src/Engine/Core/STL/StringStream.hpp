//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_STL_STRINGSTREAM_HPP
#define PD_ENGINE_CORE_STL_STRINGSTREAM_HPP

#include "STLAllocator.hpp"
#include <sstream>

namespace PD::STL
{
	using CStringStream = std::basic_stringstream<char, std::char_traits<char>, CAllocator<char>>;
	using CWStringStream = std::basic_stringstream<wchar_t, std::char_traits<wchar_t>, CAllocator<wchar_t>>;
}

#endif //PD_ENGINE_CORE_STL_STRINGSTREAM_HPP
