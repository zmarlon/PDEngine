//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_STL_LIST_HPP
#define PD_ENGINE_CORE_STL_LIST_HPP

#include "STLAllocator.hpp"
#include <list>

namespace PD::STL
{
	template<typename T>
	using CList = std::list<T, CAllocator<T>>;
}

#endif //PD_ENGINE_CORE_STL_LIST_HPP
