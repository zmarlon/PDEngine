//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_STL_STLALLOCATOR_HPP
#define PD_ENGINE_CORE_STL_STLALLOCATOR_HPP

#include "../Memory/ProxyAllocator.hpp"
#include <cstddef>
#include <memory>

namespace PD::STL
{
	template<typename T>
	class CAllocator
	{
	public:
	    IAllocator* m_pAllocator = nullptr;
	public:
		typedef usize size_type;
		typedef uptrdiff difference_type;
		typedef T* pointer;
		typedef const T* const_pointer;
		typedef T& reference;
		typedef const T& const_reference;
		typedef T value_type;

        CAllocator() : m_pAllocator(CProxyAllocator::GetInstance()){}
        CAllocator(IAllocator* pAllocator) : m_pAllocator(pAllocator){}
        ~CAllocator(){}

		template <class U> struct rebind { typedef CAllocator<U> other; };
		template <class U> CAllocator(const CAllocator<U>& allocator)
		{
			m_pAllocator = allocator.m_pAllocator;
		}

		pointer address(reference x) const {return std::addressof(x);}
		const_pointer address(const_reference x) const {return std::addressof(x);}
		size_type max_size() const throw() {return size_t(-1) / sizeof(value_type);}

		pointer allocate(size_type numObjects, const void* pHint = nullptr)
		{
			PD_ASSERT(m_pAllocator);
			return reinterpret_cast<pointer>(m_pAllocator->AllocateAligned(numObjects * sizeof(T), alignof(T)));
		}

		void deallocate(pointer pData, size_type numObjects)
		{
        	PD_ASSERT(m_pAllocator);
            m_pAllocator->DeallocateAligned(pData);
		}

		void construct(pointer pData, const T& value)
		{
			new (pData) T(value);
		}

		void construct(pointer pData)
		{
			new (pData) T();
		}

		void destroy(pointer pData)
		{
			pData->~T();
		}

		PD_FORCEINLINE bool operator ==(const CAllocator<T>& other) {return m_pAllocator == other.m_pAllocator; }
		PD_FORCEINLINE bool operator !=(const CAllocator<T>& other) {return m_pAllocator != other.m_pAllocator; }
	};
}

#endif //PD_ENGINE_CORE_STL_STLALLOCATOR_HPP
