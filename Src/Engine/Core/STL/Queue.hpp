//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 27.04.2020.
//
#ifndef PD_ENGINE_CORE_STL_QUEUE_HPP
#define PD_ENGINE_CORE_STL_QUEUE_HPP

#pragma once

#include "Deque.hpp"
#include "Vector.hpp"
#include <queue>

namespace PD::STL
{
    template<typename T, typename TContainer = CDeque<T>>
    using CQueue = std::queue<T, TContainer>;

    template<typename T, typename TContainer = CVector<T>, typename TPriority = std::less<typename TContainer::value_type>>
    using CPriorityQueue = std::priority_queue<T, TContainer, TPriority>;
}

#endif //PD_ENGINE_CORE_STL_QUEUE_HPP
