//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_STL_SET_HPP
#define PD_ENGINE_CORE_STL_SET_HPP

#include "STLAllocator.hpp"
#include <set>

namespace PD::STL
{
	template<typename T, typename TPriority = std::less<T>>
	using CSet = std::set<T, TPriority, CAllocator<T>>;
}

#endif //PD_ENGINE_CORE_STL_SET_HPP
