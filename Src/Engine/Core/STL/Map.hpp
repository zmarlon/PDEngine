//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_STL_MAP_H
#define PD_ENGINE_CORE_STL_MAP_H

#include "STLAllocator.hpp"
#include <map>

namespace PD::STL
{
	template<typename K, typename V, typename TPriority = std::less<K>>
	using CMap = std::map<K, V, TPriority, CAllocator<std::pair<const K, V>>>;
}

#endif //PD_ENGINE_CORE_STL_MAP_H
