//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "../Hints/LibraryExport.hpp"
#include "../Window/Window.hpp"
#include "../Window/MessageBox.hpp"
#include "../Logging/Logger.hpp"
#include "../../Client/PlayerComponent.hpp"
#include "../../GameState.hpp"
#include "../../Game.hpp"
#include "../../Server/GameServer.hpp"
#include "../Event/EventHandler.hpp"
#include "../Pak/PakFile.hpp"
#include "../Pak/PakLocation.hpp"
#include "../../Client/Renderer/Renderer.hpp"
#include "../../Client/Renderer/Vulkan/Image/VulkanTexture2D.hpp"
#include "../../Client/Renderer/Common/SpriteBatch.hpp"
#include "../../World/WorldClient.hpp"
#include "../../World/WorldServer.hpp"
#include "../../Core/Console/GameConsole.hpp"

namespace PD
{
	extern "C"
	{
		//PDEngine.Managed.Core:Window
		PD_LIBRARY_EXPORT CWindow* NWindow_GetInstance()
		{
			return CWindow::GetInstance();
		}

		PD_LIBRARY_EXPORT CWindow::SData* NWindow_GetData(CWindow* pWindow)
		{
			PD_ASSERT(pWindow);
			return pWindow->GetData();
		}

		PD_LIBRARY_EXPORT void NWindow_SetX(CWindow* pWindow, i32 x)
		{
			PD_ASSERT(pWindow);
			pWindow->SetX(x);
		}

		PD_LIBRARY_EXPORT void NWindow_SetY(CWindow* pWindow, i32 y)
		{
			PD_ASSERT(pWindow);
			pWindow->SetY(y);
		}

		PD_LIBRARY_EXPORT void NWindow_SetPosition(CWindow* pWindow, i32 x, i32 y)
		{
			PD_ASSERT(pWindow);
			pWindow->SetPosition(x, y);
		}

		PD_LIBRARY_EXPORT void NWindow_SetWidth(CWindow* pWindow, u32 width)
		{
			PD_ASSERT(pWindow);
			pWindow->SetWidth(width);
		}

		PD_LIBRARY_EXPORT void NWindow_SetHeight(CWindow* pWindow, u32 height)
		{
			PD_ASSERT(pWindow);
			pWindow->SetHeight(height);
		}

		PD_LIBRARY_EXPORT void NWindow_SetSize(CWindow* pWindow, u32 width, u32 height)
		{
			PD_ASSERT(pWindow);
			pWindow->SetSize(width, height);
		}

		//PDEngine.Managed.Core.Logging:Logger
		PD_LIBRARY_EXPORT CLogger* NLogger_GetInstance()
		{
			return CLogger::GetInstance();
		}

		PD_LIBRARY_EXPORT void NLogger_Log(CLogger* pLogger, ELogLevel logLevel, const wchar_t* pMessage, usize length)
		{
			PD_ASSERT(pLogger);
			PD_ASSERT(pMessage);
			PD_ASSERT(length);

#ifdef PD_PLATFORM_WINDOWS
			pLogger->Log(logLevel, L"CLR") << pMessage << pLogger->Flush();
#elif defined(PD_PLATFORM_LINUX)
			pLogger->Log(logLevel, L"CLR");
			auto& out = pLogger->Out();

			char* pData = (char*)pMessage;
			for(u32 ix = 0; ix < length; ix++) out << pData[ix << 1];

			pLogger->Flush();
#else
#error "Unsupported operating system"
#endif
		}


		//PDEngine.Managed.Core:MessageBox
		PD_LIBRARY_EXPORT void NMessageBox_ShowWithWindow(CWindow* pWindow, const wchar_t* pMessage, const wchar_t* pCaption, EMessageBoxStyle style)
		{
			PD_ASSERT(pWindow);
			PD_ASSERT(pMessage);
			PD_ASSERT(pCaption);

			MessageBox::Show(*pWindow, pMessage, pCaption, style);
		}

		PD_LIBRARY_EXPORT void NMessageBox_Show(const wchar_t* pMessage, const wchar_t* pCaption, EMessageBoxStyle style)
		{
			PD_ASSERT(pMessage);
			PD_ASSERT(pCaption);

			MessageBox::Show(pMessage, pCaption, style);
		}

		//PDEngine.Managed.Core.Memory.ProxyAllocator
		PD_LIBRARY_EXPORT void* NProxyAllocator_Allocate(usize size)
		{
			PD_ASSERT(size);
			return CProxyAllocator::GetInstance()->Allocate(size);
		}

		PD_LIBRARY_EXPORT void NProxyAllocator_Deallocate(void* pData)
		{
			PD_ASSERT(pData);
			CProxyAllocator::GetInstance()->Deallocate(pData);
		}

		PD_LIBRARY_EXPORT void* NProxyAllocator_AllocateAligned(usize size, usize alignment)
		{
			PD_ASSERT(size);
			PD_ASSERT(alignment);
			return CProxyAllocator::GetInstance()->AllocateAligned(size, alignment);
		}

		PD_LIBRARY_EXPORT void NProxyAllocator_DeallocateAligned(void* pData)
		{
			PD_ASSERT(pData);
			CProxyAllocator::GetInstance()->DeallocateAligned(pData);
		}

		//PDEngine.Managed.Entity.Component.ComponentPlayer
		PD_LIBRARY_EXPORT CPlayerComponent* NPlayerComponent_Create(SEntityData* pData)
		{
			PD_ASSERT(pData);

			auto* pPlayerComponent = CProxyAllocator::GetInstance()->AllocateObject<CPlayerComponent>();
			pPlayerComponent->Initialize(pData);
			return pPlayerComponent;
		}

		PD_LIBRARY_EXPORT void NPlayerComponent_Update(CPlayerComponent* pPlayerComponent, const SFrameEventArgs* pFrameEventArgs)
		{
			PD_ASSERT(pPlayerComponent);
			PD_ASSERT(pFrameEventArgs);

			pPlayerComponent->Update(*pFrameEventArgs);
		}

		PD_LIBRARY_EXPORT void NPlayerComponent_Dispose(CPlayerComponent* pPlayerComponent)
		{
			PD_ASSERT(pPlayerComponent);

			pPlayerComponent->Dispose();
			CProxyAllocator::GetInstance()->DeallocateObject(pPlayerComponent);
		}

		//PDEngine.Managed.Game
		PD_LIBRARY_EXPORT SGameState* NGame_GetGameState(CGame* pInstance)
		{
		    PD_ASSERT(pInstance);
			return &pInstance->GetGameState();
		}

		//PDEngine.Managed.Server.GameServer
		PD_LIBRARY_EXPORT CGameServer::SData* NGameServer_GetData(CGameServer* pInstance)
        {
		    PD_ASSERT(pInstance);
		    return &pInstance->GetData();
        }

        //PDEngine.Managed.Core.Config.ConfigClient
        PD_LIBRARY_EXPORT SConfigClient* NConfigClient_GetGameData(CGame* pInstance)
        {
		    PD_ASSERT(pInstance);
		    return &pInstance->GetConfig();
        }

        //PDEngine.Managed.Core.Event.ManagedEventHandler
        PD_LIBRARY_EXPORT void NManagedEventHandler_BeginTextInput()
        {
		    CEventHandler::GetInstance()->BeginTextInput();
        }

        PD_LIBRARY_EXPORT void NManagedEventHandler_EndTextInput()
        {
		    CEventHandler::GetInstance()->EndTextInput();
        }

        //PDEngine.Managed.Core.Pak.PakFile
        PD_LIBRARY_EXPORT CPakFile* NPakFile_Open(const char* pName, const char* pMd5)
        {
		    PD_ASSERT(pName);
		    PD_ASSERT(pMd5);

		    auto* pPakFile = CProxyAllocator::GetInstance()->AllocateObject<CPakFile>();
		    pPakFile->Open(pName, pMd5);

		    return pPakFile;
        }

        struct SPakFileNativeData
        {
		    const char* pName;
		    const char* pMd5;
        };

        PD_LIBRARY_EXPORT SPakFileNativeData NPakFile_GetData(CPakFile* pPakFile)
        {
            PD_ASSERT(pPakFile);

            SPakFileNativeData data;
            data.pName = pPakFile->GetName();
            data.pMd5 = pPakFile->GetMd5();

            return data;
        }

        PD_LIBRARY_EXPORT void NPakFile_Close(CPakFile* pPakFile)
        {
            PD_ASSERT(pPakFile);

            pPakFile->Dispose();
            CProxyAllocator::GetInstance()->DeallocateObject(pPakFile);
        }

        //PDEngine.Managed.Core.Pak.PakFileStream
        struct SPakFileStreamNativeData
        {
            byte* pData;
            usize Length;
        };

        PD_LIBRARY_EXPORT SPakFileStreamNativeData NPakFileStream_GetEntry(CPakFile* pPakFile, const char* pEntryName)
        {
            PD_ASSERT(pPakFile);
            PD_ASSERT(pEntryName);

            SPakFileStreamNativeData data;
            pPakFile->GetEntry(pEntryName, &data.pData, &data.Length, CProxyAllocator::GetInstance());

            return data;
        }

        PD_LIBRARY_EXPORT void NPakFileStream_FreeData(byte* pData)
        {
            PD_ASSERT(pData);

            CProxyAllocator::GetInstance()->Deallocate(pData);
        }

        //PDEngine.Managed.Client.Renderer.Common.Texture2D
        struct STexture2DNativeData
        {
            CVulkanTexture2D* pHandle;
            CVulkanImageView* pViewHandle;
            u32 Width;
            u32 Height;
            u32 MipLevels;
        };

        PD_LIBRARY_EXPORT STexture2DNativeData NTexture2D_Load(CPakFile* pPakFile, const char* pEntryName)
        {
            PD_ASSERT(pPakFile);
            PD_ASSERT(pEntryName);

            auto* pTexture = CProxyAllocator::GetInstance()->AllocateObject<CVulkanTexture2D>();
            pTexture->Load(CRenderer::GetInstance()->GetDevice(), SPakLocation(pPakFile, pEntryName));

            STexture2DNativeData data;
            data.pHandle = pTexture;
            data.pViewHandle = &pTexture->GetImageView();
            data.Width = pTexture->GetWidth();
            data.Height = pTexture->GetHeight();
            data.MipLevels = pTexture->GetMipLevels();

            return data;
        }

        PD_LIBRARY_EXPORT STexture2DNativeData NTexture2D_GetData(CVulkanTexture2D* pTexture)
        {
            PD_ASSERT(pTexture);

            STexture2DNativeData data;
            data.pHandle = pTexture;
            data.pViewHandle = &pTexture->GetImageView();
            data.Width = pTexture->GetWidth();
            data.Height = pTexture->GetHeight();
            data.MipLevels = pTexture->GetMipLevels();

            return data;
        }

        PD_LIBRARY_EXPORT void NTexture2D_Dispose(CVulkanTexture2D* pTexture)
        {
            PD_ASSERT(pTexture);

            pTexture->Dispose(CRenderer::GetInstance()->GetDevice());
            CProxyAllocator::GetInstance()->DeallocateObject(pTexture);
        }

        //PDEngine.Managed.Client.Renderer.Common.SpriteBatch
        struct SSpriteBatchNativeData
        {
            CSpriteBatch* pHandle;
            SSpriteBatchData* pData;
        };

        PD_LIBRARY_EXPORT SSpriteBatchNativeData NSpriteBatch_Create()
        {
            auto* pSpriteBatch = CSpriteBatchFactory::GetInstance()->CreateBatch();

            SSpriteBatchNativeData data;
            data.pHandle = pSpriteBatch;
            data.pData = &pSpriteBatch->GetData();

            return data;
        }

        PD_LIBRARY_EXPORT SSpriteBatchNativeData NSpriteBatch_GetDefault()
        {
            auto* pSpriteBatch = &CRenderer::GetInstance()->GetSpriteBatch();

            SSpriteBatchNativeData data;
            data.pHandle = pSpriteBatch;
            data.pData = &pSpriteBatch->GetData();

            return data;
        }

        PD_LIBRARY_EXPORT void NSpriteBatch_Dispose(CSpriteBatch* pSpriteBatch)
        {
            PD_ASSERT(pSpriteBatch);

            CSpriteBatchFactory::GetInstance()->DestroyBatch(CRenderer::GetInstance()->GetDevice(), pSpriteBatch);
        }

        PD_LIBRARY_EXPORT void NSpriteBatch_Flush(CSpriteBatch* pSpriteBatch, SSpriteInfo* pSpriteInfos, u32 numSpriteInfos)
        {
            PD_ASSERT(pSpriteBatch);
            PD_ASSERT(pSpriteInfos);
            PD_ASSERT(numSpriteInfos);

            pSpriteBatch->Flush(pSpriteInfos, numSpriteInfos);
        }

        //PDEngine.Managed.Client.Renderer.Renderer
        PD_LIBRARY_EXPORT CRenderer* NRenderer_GetInstance()
        {
            return CRenderer::GetInstance();
        }

        //PDEngine.Managed.Client.Renderer.Common.SpriteFont
        PD_LIBRARY_EXPORT CSpriteFont* NSpriteFont_Load(CPakFile* pPakFile, const char* pEntryName, u32 fontSize)
		{
        	PD_ASSERT(pPakFile);
        	PD_ASSERT(pEntryName);

        	auto* pSpriteFont = CProxyAllocator::GetInstance()->AllocateObject<CSpriteFont>();
        	pSpriteFont->Load(CRenderer::GetInstance()->GetDevice(), SPakLocation(pPakFile, pEntryName), fontSize);
        	return pSpriteFont;
		}

		PD_LIBRARY_EXPORT CFontAtlas::SData* NSpriteFont_GetData(CSpriteFont* pSpriteFont)
		{
        	PD_ASSERT(pSpriteFont);
        	return pSpriteFont->GetFontAtlas().GetData();
		}

		PD_LIBRARY_EXPORT CVulkanTexture2D* NSpriteFont_GetTexture(CSpriteFont* pSpriteFont)
		{
        	PD_ASSERT(pSpriteFont);
        	return &pSpriteFont->GetTexture();
		}

		PD_LIBRARY_EXPORT void NSpriteFont_Dispose(CSpriteFont* pSpriteFont)
		{
        	PD_ASSERT(pSpriteFont);
        	pSpriteFont->Dispose(CRenderer::GetInstance()->GetDevice());

        	CProxyAllocator::GetInstance()->DeallocateObject(pSpriteFont);
		}

		//PDEngine.Managed.World.WorldClient
		PD_LIBRARY_EXPORT CWorldClient* NWorldClient_GetInstance()
        {
            return CWorldClient::GetInstance();
        }

        //PDEngine.Managed.World.WorldServer
        PD_LIBRARY_EXPORT CWorldServer* NWorldServer_GetInstance()
        {
            return CWorldServer::GetInstance();
        }

        //PDEngine.Managed.Core.Console.ConsoleVariable
        PD_LIBRARY_EXPORT void NGameConsole_RegisterVariable(const wchar_t* pName, usize length, CGameConsole::SVariable variable)
        {
            PD_ASSERT(pName);
            PD_ASSERT(length);

            STL::CWString name;
#ifdef PD_PLATFORM_WINDOWS
            name = pName;
#elif defined(PD_PLATFORM_LINUX)
            char* pData = (char*)pName;

            name.reserve(length);
            for(u32 ix = 0; ix < length; ix++) name += pData[ix << 1]; //TODO:
#else
#error "Unsupported operating system"
#endif
            CGameConsole::GetInstance()->RegisterVariable(name, variable);
        }
	}
}
