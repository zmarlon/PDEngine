//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "CommonLanguageRuntime.hpp"
#include "../Logging/Logger.hpp"
#include "../String/StringHelper.hpp"
#include <array>

namespace PD
{
	PD_POSTDEFINE_SINGLETON(CCommonLanguageRuntime)

	void CCommonLanguageRuntime::OnDisposing()
	{
		m_clrLibrary.Dispose();

		DetachSingleton();
	}

	void CCommonLanguageRuntime::BuildTpaList(STL::CString& tpaList, const std::filesystem::path& commonDirectory, const std::filesystem::path& clrDirectory)
	{
		for(const auto& entry : std::filesystem::directory_iterator(clrDirectory))
		{
			if(!entry.is_directory())
			{
				auto currentPath = entry.path().string();
				if(StringHelper::EndsWith(currentPath, ".dll"))
				{
					tpaList.append(entry.path().string());
#ifdef PD_PLATFORM_WINDOWS
					tpaList.push_back(';');
#elif defined(PD_PLATFORM_LINUX)
					tpaList.push_back(':');
#else
#error "Unsupported operating system"
#endif
				}

			}
		}

		for(const auto& entry : std::filesystem::directory_iterator(commonDirectory))
        {
		    if(!entry.is_directory())
            {
		        auto currentPath = entry.path().string();
		        if(StringHelper::EndsWith(currentPath, ".dll"))
                {
		            tpaList.append(entry.path().string());
#ifdef PD_PLATFORM_WINDOWS
					tpaList.push_back(';');
#elif defined(PD_PLATFORM_LINUX)
		            tpaList.push_back(':');
#else
#error "Unsupported operating system"
#endif
                }
            }
        }
	}

	void* CCommonLanguageRuntime::GetDelegateInternal(const char* pClassName, const char* pMethodName)
	{
		void* pDelegate;
		m_pfn_coreclr_create_delegate(m_pHostHandle, m_domainId, "PDEngine.Managed, Version=1.0.0.0", pClassName,
									 pMethodName, &pDelegate);

		if(!pDelegate)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal, L"CLR") << L"Failed to load internal method: "
																						 << pClassName << L':' << pMethodName << CLogger::GetInstance()->Flush();
		}
		return pDelegate;
	}

	void CCommonLanguageRuntime::Create()
	{
		AttachSingleton();

		std::filesystem::path currentDirectory = std::filesystem::current_path();
		std::filesystem::path commonDirectory = currentDirectory / L"Common";
		std::filesystem::path clrDirectory = commonDirectory / L"CLR";

		std::string clrPathString = clrDirectory.string();
		std::string currentDirectoryString = currentDirectory.string();

		m_clrLibrary.Load(clrPathString.c_str(), "coreclr", false);

		m_pfn_coreclr_initialize = m_clrLibrary.GetFunctionAddressPtr<coreclr_initialize_ptr>("coreclr_initialize");
		m_pfn_coreclr_create_delegate = m_clrLibrary.GetFunctionAddressPtr<coreclr_create_delegate_ptr>("coreclr_create_delegate");
		m_pfn_coreclr_shutdown = m_clrLibrary.GetFunctionAddressPtr<coreclr_shutdown_ptr>("coreclr_shutdown");

		STL::CString tpaList;
		tpaList.reserve(20000);

		BuildTpaList(tpaList, commonDirectory, clrDirectory);

		std::array<const char*, 2> propertyKeys = {"TRUSTED_PLATFORM_ASSEMBLIES",
												   "NATIVE_DLL_SEARCH_DIRECTORIES"};

		std::array<const char*, 2> propertyValues = {tpaList.c_str(), currentDirectoryString.c_str()};

		auto result = m_pfn_coreclr_initialize(currentDirectoryString.c_str(), "PDEngineAppDomain",
				static_cast<i32>(propertyKeys.size()), propertyKeys.data(), propertyValues.data(), &m_pHostHandle, &m_domainId);

		if(result < 0)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal, L"CLR") << L"coreclr_initialize failed: " << result << CLogger::GetInstance()->Flush();
		}

		PD_END_INITIALIZE;
	}
}