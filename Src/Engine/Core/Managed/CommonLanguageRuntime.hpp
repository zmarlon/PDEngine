//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_MANAGED_COMMONLANGUAGERUNTIME_HPP
#define PD_ENGINE_CORE_MANAGED_COMMONLANGUAGERUNTIME_HPP

#pragma once

#include "../Disposable.hpp"
#include "../Singleton.hpp"
#include "../Platform/DynamicLibrary.hpp"
#include "../Types.hpp"
#include "../STL/String.hpp"
#include <coreclrhost.h>
#include <filesystem>

namespace PD
{
	class CCommonLanguageRuntime
	{
		PD_MAKE_CLASS_DISPOSABLE(CCommonLanguageRuntime)
		PD_MAKE_CLASS_SINGLETON(CCommonLanguageRuntime)
	private:
		CDynamicLibrary m_clrLibrary;

		coreclr_initialize_ptr m_pfn_coreclr_initialize;
		coreclr_create_delegate_ptr m_pfn_coreclr_create_delegate;
		coreclr_shutdown_ptr m_pfn_coreclr_shutdown;

		void* m_pHostHandle;
		u32 m_domainId;

		void BuildTpaList(STL::CString& tpaList, const std::filesystem::path& commonDirectory, const std::filesystem::path& clrDirectory);
		void* GetDelegateInternal(const char* pClassName, const char* pMethodName);

		void OnDisposing();
	public:
		void Create();

		template<typename F>
		PD_FORCEINLINE F* GetDelegate(const char* pClassName, const char* pMethodName)
		{
			return (F*)GetDelegateInternal(pClassName, pMethodName);
		}
	};
}

#endif //PD_ENGINE_CORE_MANAGED_COMMONLANGUAGERUNTIME_HPP
