set(PD_ENGINE_CORE_MANAGED_DIRECTORY Src/Engine/Core/Managed)

set(PD_ENGINE_CORE_MANAGED_SOURCE_FILES
        ${PD_ENGINE_CORE_MANAGED_DIRECTORY}/CommonLanguageRuntime.cpp
        ${PD_ENGINE_CORE_MANAGED_DIRECTORY}/CommonLanguageRuntime.hpp
        ${PD_ENGINE_CORE_MANAGED_DIRECTORY}/ExternalMethods.cpp)