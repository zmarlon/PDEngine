//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_PAK_PAKFILE_HPP
#define PD_ENGINE_CORE_PAK_PAKFILE_HPP

#pragma once

#include "../Disposable.hpp"
#include "../Collections/Array.hpp"
#include "../Types.hpp"
#include "../STL/String.hpp"

struct zip_t;
namespace PD
{
	class CPakFile
	{
		PD_MAKE_CLASS_DISPOSABLE(CPakFile);
	private:
		zip_t* m_pZipFile;
		const char* m_pFileName;
		const char* m_pMd5;

		void OnDisposing();
	public:
		void Open(const char* pFileName, const char* pMd5);

		void GetEntry(const char* pFileName, CArray<byte>& buffer, IAllocator* pAllocator);
		void GetEntry(const char* pFileName, byte** ppData, usize* pSize, IAllocator* pAllocator);
		void GetTextEntry(const char* pFileName, STL::CString& buffer, IAllocator* pAllocator);

		PD_FORCEINLINE const char* GetName() const { return m_pFileName; }
		PD_FORCEINLINE const char* GetMd5() const { return m_pMd5; }
	};
}


#endif //PD_ENGINE_CORE_PAK_PAKFILE_HPP
