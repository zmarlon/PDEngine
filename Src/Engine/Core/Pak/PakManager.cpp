//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "PakManager.hpp"
#include "../Managed/CommonLanguageRuntime.hpp"

namespace PD
{
	PD_POSTDEFINE_SINGLETON(CPakManager)
	void CPakManager::OnDisposing()
	{
		m_userInterface.Dispose();
		m_shaders.Dispose();

		DetachSingleton();
	}

	void CPakManager::Create()
	{
		AttachSingleton();
		m_shaders.Open("Common/Shaders", nullptr);//TODO: md5
		m_userInterface.Open("Common/UserInterface", nullptr); //TODO: md5

		struct SNativeData
        {
            CPakFile* pShaders;
            CPakFile* pUserInterface;
        } data;

		data.pShaders = &m_shaders;
		data.pUserInterface = &m_userInterface;

		auto m_pfn_NPakManager_RegisterNatives = CCommonLanguageRuntime::GetInstance()->GetDelegate<void(SNativeData*)>("PDEngine.Managed.Core.Pak.PakManager", "NRegisterNatives");
		m_pfn_NPakManager_RegisterNatives(&data);

		PD_END_INITIALIZE;
	}
}