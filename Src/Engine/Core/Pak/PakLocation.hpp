//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_PAK_PAKLOCATION_HPP
#define PD_ENGINE_CORE_PAK_PAKLOCATION_HPP

#pragma once

#include "../Assert.hpp"
#include "../Memory/ProxyAllocator.hpp"
#include "../STL/String.hpp"
#include "../Collections/Array.hpp"

namespace PD
{
	class CPakFile;
	struct SPakLocation
	{
		CPakFile* pPakFile;
		const char* pEntryName;

		SPakLocation() = default;
		SPakLocation(CPakFile& pakFile, const char* pEntryName) : pPakFile(&pakFile), pEntryName(pEntryName) {}
		SPakLocation(CPakFile* pPakFile, const char* pEntryName) : pPakFile(pPakFile), pEntryName(pEntryName) {PD_ASSERT(pPakFile); }

		void Load(CArray<byte>& buffer, IAllocator* pAllocator = CProxyAllocator::GetInstance()) const;
		void LoadText(STL::CString& buffer, IAllocator* pAllocator = CProxyAllocator::GetInstance()) const;

		friend std::ostream& operator<<(std::ostream& stream, const SPakLocation& location);
		friend std::wostream& operator<<(std::wostream& stream, const SPakLocation& location);
	};

}

#endif //PD_ENGINE_CORE_PAK_PAKLOCATION_HPP
