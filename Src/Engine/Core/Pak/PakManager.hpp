//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_PAK_PAKMANAGER_HPP
#define PD_ENGINE_CORE_PAK_PAKMANAGER_HPP

#pragma once

#include "../Disposable.hpp"
#include "../Singleton.hpp"
#include "PakFile.hpp"

namespace PD
{
	class CPakManager
	{
		PD_MAKE_CLASS_DISPOSABLE(CPakManager)
		PD_MAKE_CLASS_SINGLETON(CPakManager)
	private:
		CPakFile m_shaders;
		CPakFile m_userInterface;

		void OnDisposing();
	public:
		void Create();

		PD_FORCEINLINE CPakFile& GetShaders() {return m_shaders; }
		PD_FORCEINLINE CPakFile& GetUserInterface() {return m_userInterface; }
	};
}

#endif //PD_ENGINE_CORE_PAK_PAKMANAGER_HPP
