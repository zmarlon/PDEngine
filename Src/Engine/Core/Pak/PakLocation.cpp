//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "PakLocation.hpp"
#include "PakFile.hpp"
#include <ostream>

namespace PD
{
	void SPakLocation::Load(CArray<byte>& buffer, IAllocator* pAllocator) const
	{
		PD_ASSERT(pPakFile);
		PD_ASSERT(pEntryName);

		pPakFile->GetEntry(pEntryName, buffer, pAllocator);
	}

	void SPakLocation::LoadText(STL::CString& buffer, IAllocator* pAllocator) const
	{
		PD_ASSERT(pPakFile);
		PD_ASSERT(pEntryName);

		pPakFile->GetTextEntry(pEntryName, buffer, pAllocator);
	}

	std::ostream& operator<<(std::ostream& stream, const SPakLocation& location)
	{
		PD_ASSERT(location.pPakFile);
		stream << '[' << location.pPakFile->GetName() << ':' << location.pEntryName << ']';
		return stream;
	}

	std::wostream& operator<<(std::wostream& stream, const SPakLocation& location)
	{
		PD_ASSERT(location.pPakFile);
		stream << L'[' << location.pPakFile->GetName() << L':' << location.pEntryName << L']';
		return stream;
	}
}