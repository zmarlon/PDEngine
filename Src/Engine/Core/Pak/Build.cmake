set(PD_ENGINE_CORE_PAK_DIRECTORY Src/Engine/Core/Pak)

set(PD_ENGINE_CORE_PAK_SOURCE_FILES
        ${PD_ENGINE_CORE_PAK_DIRECTORY}/PakLocation.cpp
        ${PD_ENGINE_CORE_PAK_DIRECTORY}/PakLocation.hpp
        ${PD_ENGINE_CORE_PAK_DIRECTORY}/PakFile.cpp
        ${PD_ENGINE_CORE_PAK_DIRECTORY}/PakFile.hpp
        ${PD_ENGINE_CORE_PAK_DIRECTORY}/PakManager.cpp
        ${PD_ENGINE_CORE_PAK_DIRECTORY}/PakManager.hpp)