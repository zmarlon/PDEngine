//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "PakFile.hpp"
#include "../Logging/Logger.hpp"
#include <miniz/zip.h>
#include <miniz/zip.c>

namespace PD
{
	void CPakFile::OnDisposing()
	{
		zip_close(m_pZipFile);
	}

	void CPakFile::Open(const char* pFileName, const char* pMd5)
	{
		m_pFileName = pFileName;
		m_pMd5 = pMd5;

		STL::CString totalPath = pFileName;
		totalPath.append(".pdpak");

		if(m_pMd5)
		{
			//TODO: handle md5
		}

		m_pZipFile = zip_open(totalPath.c_str(), 0, 'r');
		if(!m_pZipFile)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to open PAK file: " << totalPath.c_str()
														  << CLogger::GetInstance()->Flush();
		}

		PD_END_INITIALIZE;
	}

	void CPakFile::GetEntry(const char* pFileName, CArray<byte>& buffer, IAllocator* pAllocator)
	{
		if(zip_entry_open(m_pZipFile, pFileName) < 0)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to get entry " << pFileName << L" from PAK file: "
														  << m_pFileName << L".pdpak" << CLogger::GetInstance()->Flush();
		}

		usize entryLen = zip_entry_size(m_pZipFile);
		buffer.Allocate(entryLen, false, pAllocator);
		zip_entry_noallocread(m_pZipFile, (void*)buffer.GetData(), entryLen);

		if(zip_entry_close(m_pZipFile) < 0)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to get entry " << pFileName << L" from PAK file: "
														  << m_pFileName << L".pdpak" << CLogger::GetInstance()->Flush();
		}
	}

	void CPakFile::GetEntry(const char* pFileName, byte** ppData, usize* pSize, IAllocator* pAllocator)
	{
		PD_ASSERT(ppData);
		PD_ASSERT(pSize);

		if(zip_entry_open(m_pZipFile, pFileName) < 0)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to get entry " << pFileName << L" from PAK file: "
														  << m_pFileName << L".pdpak" << CLogger::GetInstance()->Flush();
		}

		usize entryLen = zip_entry_size(m_pZipFile);
		*pSize = static_cast<i64>(entryLen);

		auto* pData = reinterpret_cast<byte*>(pAllocator->Allocate(entryLen));
		*ppData = pData;

		zip_entry_noallocread(m_pZipFile, pData, entryLen);

		if(zip_entry_close(m_pZipFile) < 0)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to get entry " << pFileName << L" from PAK file: "
														  << m_pFileName << L".pdpak" << CLogger::GetInstance()->Flush();
		}
	}

	void CPakFile::GetTextEntry(const char* pFileName, STL::CString& buffer, IAllocator* pAllocator)
	{
		if(zip_entry_open(m_pZipFile, pFileName) < 0)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to get text entry " << pFileName << L" from PAK file: "
														  << m_pFileName << L".pdpak" << CLogger::GetInstance()->Flush();
		}

		usize entryLen = zip_entry_size(m_pZipFile);
		buffer = STL::CString(pAllocator);
		buffer.resize(entryLen);
		zip_entry_noallocread(m_pZipFile, (void*)buffer.data(), entryLen);

		if(zip_entry_close(m_pZipFile) < 0)
		{
			CLogger::GetInstance()->Log(ELogLevel::Error) << L"Failed to get text entry " << pFileName << L" from PAK file: "
														  << m_pFileName << L".pdpak" << CLogger::GetInstance()->Flush();
		}
	}
}