//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PD_ENGINE_CORE_INPUT_MOUSESTATE_HPP
#define PD_ENGINE_CORE_INPUT_MOUSESTATE_HPP

#pragma once

#include <glm/glm.hpp>
#include "../Types.hpp"
#include "../Singleton.hpp"

namespace PD
{
	class CMouseState
	{
	private:
		u32 m_state;
		int m_x;
		int m_y;

		CMouseState(u32 state, int x, int y) : m_state(state), m_x(x), m_y(y) {}

	public:
		static CMouseState Get();

		template<typename T>
		glm::tvec2<T> GetPosition() const
		{
			return glm::tvec2<T>(static_cast<T>(m_x), static_cast<T>(m_y));
		}

		PD_FORCEINLINE int GetX() const { return m_x; }

		PD_FORCEINLINE int GetY() const { return m_y; }

		bool IsLeftButtonPressed() const;
		bool IsLeftButtonReleased() const;
		bool IsMiddleButtonPressed() const;
		bool IsMiddleButtonReleased() const;
		bool IsRightButtonPressed() const;
		bool IsRightButtonReleased() const;
	};
}


#endif //PD_ENGINE_CORE_INPUT_MOUSESTATE_HPP
