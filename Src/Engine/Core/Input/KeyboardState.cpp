//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#include "KeyboardState.hpp"
#include <SDL2/SDL.h>

namespace PD
{
	CKeyboardState::CKeyboardState(const u8* pData)
	{
		for(u32 ix = 0; ix < 512; ix++)
		{
			m_pressedKeys.set(ix, pData[ix]);
		}
	}

	CKeyboardState CKeyboardState::Get()
	{
		return CKeyboardState(SDL_GetKeyboardState(nullptr));
	}
}