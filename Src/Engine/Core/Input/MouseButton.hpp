//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PD_ENGINE_CORE_INPUT_MOUSEBUTTON_HPP
#define PD_ENGINE_CORE_INPUT_MOUSEBUTTON_HPP

#pragma once

#include "../Types.hpp"

namespace PD
{
	enum class EMouseButton : u32
	{
		Left = 0,
		Middle = 1,
		Right = 2
	};
}

#endif //PD_ENGINE_CORE_INPUT_MOUSEBUTTON_HPP
