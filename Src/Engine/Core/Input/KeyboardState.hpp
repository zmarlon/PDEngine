//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PD_ENGINE_CORE_INPUT_KEYBOARDSTATE_HPP
#define PD_ENGINE_CORE_INPUT_KEYBOARDSTATE_HPP

#pragma once

#include "Keys.hpp"
#include "../Types.hpp"
#include "../Singleton.hpp"
#include <bitset>

namespace PD
{
	class CKeyboardState
	{
	private:
		std::bitset<512> m_pressedKeys;

		CKeyboardState(const u8* pData);
	public:
		static CKeyboardState Get();

		PD_FORCEINLINE bool IsKeyDown(EKeys key) const
		{
			return m_pressedKeys[static_cast<usize>(key)];
		}

		PD_FORCEINLINE bool IsKeyUp(EKeys key) const
		{
			return !m_pressedKeys[static_cast<usize>(key)];
		}
	};

}

#endif //PD_ENGINE_CORE_INPUT_KEYBOARDSTATE_HPP
