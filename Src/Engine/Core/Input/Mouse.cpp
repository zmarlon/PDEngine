//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#include "Mouse.hpp"
#include "../Window/Window.hpp"
#include <SDL2/SDL.h>

namespace PD::Mouse
{
	void SetPosition(const glm::ivec2& position)
	{
		auto* pInstance = CWindow::GetInstance();

		SDL_WarpMouseGlobal(position.x + pInstance->GetX(), position.y + pInstance->GetY());
	}

	void SetPositionCentered()
	{
		auto* pInstance = CWindow::GetInstance();

		SDL_WarpMouseGlobal(pInstance->GetX() + pInstance->GetWidth() / 2, pInstance->GetY() + pInstance->GetHeight() / 2);
	}

    void EnableCursor()
    {
        SDL_ShowCursor(SDL_ENABLE);
    }

    void DisableCursor()
    {
        SDL_ShowCursor(SDL_DISABLE);
    }
}