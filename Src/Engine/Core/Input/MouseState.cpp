//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#include "MouseState.hpp"
#include "../Window/Window.hpp"
#include <SDL2/SDL.h>

namespace PD
{
	CMouseState CMouseState::Get()
	{
		auto* pWindow = CWindow::GetInstance();

		int x, y;
		u32 state = SDL_GetGlobalMouseState(&x, &y);

		return CMouseState(state, x - pWindow->GetX(), y - pWindow->GetY());
	}

	bool CMouseState::IsLeftButtonPressed() const
	{
		return m_state & SDL_BUTTON(SDL_BUTTON_LEFT);
	}

	bool CMouseState::IsLeftButtonReleased() const
	{
		return !(m_state & SDL_BUTTON(SDL_BUTTON_LEFT));
	}

	bool CMouseState::IsMiddleButtonPressed() const
	{
		return m_state & SDL_BUTTON(SDL_BUTTON_MIDDLE);
	}

	bool CMouseState::IsMiddleButtonReleased() const
	{
		return !(m_state & SDL_BUTTON(SDL_BUTTON_MIDDLE));
	}

	bool CMouseState::IsRightButtonPressed() const
	{
		return m_state & SDL_BUTTON(SDL_BUTTON_RIGHT);
	}

	bool CMouseState::IsRightButtonReleased() const
	{
		return !(m_state & SDL_BUTTON(SDL_BUTTON_RIGHT));
	}
}