//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PD_ENGINE_CORE_INPUT_MOUSE_HPP
#define PD_ENGINE_CORE_INPUT_MOUSE_HPP

#pragma once

#include <glm/glm.hpp>

namespace PD
{
	namespace Mouse
	{
		void SetPosition(const glm::ivec2& position);
		void SetPositionCentered();

		void EnableCursor();
		void DisableCursor();
	}
}

#endif //PD_ENGINE_CORE_INPUT_MOUSE_HPP
