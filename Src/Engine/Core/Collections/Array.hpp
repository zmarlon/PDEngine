//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#ifndef PD_ENGINE_CORE_COLLECTIONS_ARRAY_HPP
#define PD_ENGINE_CORE_COLLECTIONS_ARRAY_HPP

#pragma once

#include "../Types.hpp"
#include "../Memory/ProxyAllocator.hpp"
#include "../Memory/MemoryHelper.hpp"
#include <initializer_list>

namespace PD
{
    template<typename T>
    class CArray
    {
    private:
        using TArray = CArray<T>;

        usize m_length;
        T* m_pData;
        IAllocator* m_pAllocator;
    public:
        static constexpr usize INVALID_INDEX = ~0ull;

        explicit PD_FORCEINLINE CArray(IAllocator* pAllocator = CProxyAllocator::GetInstance()) : m_length(0), m_pData(nullptr), m_pAllocator(pAllocator)
        {
            PD_ASSERT(pAllocator);
        }

        explicit PD_FORCEINLINE CArray(usize length, bool zeroMemory = true, IAllocator* pAllocator = CProxyAllocator::GetInstance())
        {
            Allocate(length, pAllocator, zeroMemory);
        }

        CArray(const std::initializer_list<T>& initializerList)
        {
            Allocate(initializerList.size(), false);

            usize index = 0;
            for(auto* pIterator = initializerList.begin(); pIterator != initializerList.end(); pIterator++) m_pData[index++] = *pIterator;
        }

        CArray(std::initializer_list<T>&& initializerList)
        {
            Allocate(initializerList.size(), false);

            usize index = 0;
            for(auto* pIterator = initializerList.begin(); pIterator != initializerList.end(); pIterator++) m_pData[index++] = std::move(*pIterator);
        }

        void Allocate(usize length, bool zeroMemory = true, IAllocator* pAllocator = CProxyAllocator::GetInstance())
        {
            PD_ASSERT(pAllocator);

            m_length = length;
            m_pAllocator = pAllocator;

            if(length)
            {
                m_pData = reinterpret_cast<T*>(m_pAllocator->AllocateAligned(length * sizeof(T), alignof(T)));

                if(zeroMemory) MemoryHelper::ZeroElements(m_pData, length);
            }
            else
            {
                m_pData = nullptr;
            }
        }

        CArray(const TArray& other)
        {
            m_length = other.m_length;
            m_pAllocator = other.m_pAllocator;

            m_pData = reinterpret_cast<T*>(m_pAllocator->AllocateAligned(m_length * sizeof(T), alignof(T)));
            MemoryHelper::CopyElements(m_pData, other.m_pData, m_length);
        }

        CArray(TArray&& other)
        {
            m_length = other.m_length;
            m_pAllocator = other.m_pAllocator;
            m_pData = other.m_pData;

            other.m_length = 0;
            other.m_pAllocator = nullptr;
            other.m_pData = nullptr;
        }

        TArray& operator =(const TArray& other)
        {
            if(m_pData)
            {
                for(usize ix = 0; ix < m_length; ix++) m_pData[ix].~T();
                m_pAllocator->DeallocateAligned(m_pData);
            }

            m_length = other.m_length;
            m_pAllocator = other.m_pAllocator;

            m_pData = reinterpret_cast<T*>(m_pAllocator->AllocateAligned(m_length * sizeof(T), alignof(T)));
            MemoryHelper::CopyElements(m_pData, other.m_pData, m_length);

            return *this;
        }

        TArray& operator =(TArray&& other)
        {
            if(m_pData)
            {
                for(usize ix = 0; ix < m_length; ix++) m_pData[ix].~T();
                m_pAllocator->DeallocateAligned(m_pData);
            }

            m_length = other.m_length;
            m_pAllocator = other.m_pAllocator;
            m_pData = other.m_pData;

            other.m_length = 0;
            other.m_pAllocator = nullptr;
            other.m_pData = nullptr;

            return *this;
        }

        void Deallocate()
        {
            for(usize ix = 0; ix < m_length; ix++) m_pData[ix].~T();

            if(m_pData) m_pAllocator->DeallocateAligned(m_pData);

            m_length = 0;
            m_pData = nullptr;
            m_pAllocator = nullptr;
        }

        virtual ~CArray()
        {
            Deallocate();
        }

        PD_FORCEINLINE T& operator[](usize index)
        {
            PD_ASSERT(index < m_length);
            return m_pData[index];
        }

        PD_FORCEINLINE const T& operator[](usize index) const
        {
            PD_ASSERT(index < m_length);
            return m_pData[index];
        }

        bool Contains(const T& element) const
        {
            for(usize ix = 0; ix < m_length; ix++)
            {
                if(m_pData[ix] == element) return true;
            }
            return false;
        }

        template<typename TFunction>
        void ForEach(TFunction&& function)
        {
            for(usize ix = 0; ix < m_length; ix++) function(m_pData[ix]);
        }

        usize IndexOf(const T& element) const
        {
            for(usize ix = 0; ix < m_length; ix++)
            {
                if(m_pData[ix] == element) return ix;
            }
            return INVALID_INDEX;
        }

        usize IndexOf(const T& element, usize startIndex) const
        {
            for(usize ix = startIndex; ix < m_length; ix++)
            {
                if(m_pData[ix] == element) return ix;
            }
            return INVALID_INDEX;
        }

        usize IndexOf(const T& element, usize startIndex, usize endIndex) const
        {
            PD_ASSERT(startIndex <= m_length);

            for(usize ix = startIndex; ix < endIndex; ix++)
            {
                if(m_pData[ix] == element) return ix;
            }
            return INVALID_INDEX;
        }

        PD_FORCEINLINE T* begin() {return GetBegin(); }
        PD_FORCEINLINE const T* begin() const {return GetBegin(); }
        PD_FORCEINLINE T* end() {return GetEnd(); }
        PD_FORCEINLINE const T* end() const {return GetEnd(); }

        PD_FORCEINLINE T* GetBegin() {return m_pData; }
        PD_FORCEINLINE const T* GetBegin() const {return m_pData; }
        PD_FORCEINLINE T* GetEnd() {return m_pData + m_length; }
        PD_FORCEINLINE const T* GetEnd() const {return m_pData + m_length; }

        PD_FORCEINLINE T& First() {return m_pData[0]; }
        PD_FORCEINLINE const T& First() const {return m_pData[0]; }
        PD_FORCEINLINE T& Last() {return m_pData[m_length - 1]; }
        PD_FORCEINLINE const T& Last() const {return m_pData[m_length - 1]; }

        PD_FORCEINLINE usize GetSize() const {return m_length; }
        PD_FORCEINLINE T* GetData() {return m_pData; }
        PD_FORCEINLINE const T* GetData() const {return m_pData; }
        PD_FORCEINLINE IAllocator* GetAllocator() {return m_pAllocator; }
        PD_FORCEINLINE const IAllocator* GetAllocator() const {return m_pAllocator; }

    };
}

#endif //PD_ENGINE_CORE_COLLECTIONS_ARRAY_HPP
