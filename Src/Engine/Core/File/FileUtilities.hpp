//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 17.04.2020.
//

#ifndef PD_ENGINE_CORE_FILE_FILEUTILITIES_HPP
#define PD_ENGINE_CORE_FILE_FILEUTILITIES_HPP

#pragma once

#include "../Types.hpp"
#include "../STL/Vector.hpp"
#include "../STL/String.hpp"
#include "../Collections/Array.hpp"
#include <fstream>
#include <filesystem>

namespace PD
{
	namespace FileUtilities
	{
		void GetGameDataDirectory(std::filesystem::path& gameDataDirectory);

		template<typename TChar>
		PD_FORCEINLINE bool ReadTextFile(const char* pFilePath, STL::CBasicString<TChar>& buffer)
		{
			std::basic_ifstream<TChar, std::char_traits<TChar>> fileStream(pFilePath);
			if(!fileStream.good()) return false;

			fileStream.seekg(0, std::ios::end);
			buffer.resize(fileStream.tellg());
			fileStream.seekg(0, std::ios::beg);
			buffer.assign((std::istreambuf_iterator<TChar>(fileStream)), std::istreambuf_iterator<TChar>());

			return true;
		}

		PD_FORCEINLINE bool ReadBinaryFile(const char* pFilePath, STL::CVector<byte>& buffer)
		{
			std::ifstream fileStream(pFilePath, std::ios::binary | std::ios::ate);
			if(!fileStream.good()) return false;

			usize size = fileStream.tellg();
			buffer.resize(size);
			fileStream.seekg(0);
			fileStream.read((char*)buffer.data(), size);

			return true;
		}

		PD_FORCEINLINE bool ReadBinaryFile(const char* pFilePath, CArray<byte>& buffer)
		{
			std::ifstream fileStream(pFilePath, std::ios::binary | std::ios::ate);
			if(!fileStream.good()) return false;

			usize size = fileStream.tellg();
			buffer.Allocate(size, false);
			fileStream.seekg(0);
			fileStream.read((char*)buffer.GetData(), size);

			return true;
		}

		PD_FORCEINLINE bool WriteBinaryFile(const char* pFilePath, STL::CVector<byte>& buffer)
		{
			std::ofstream fileStream(pFilePath, std::ios::binary);
			if(!fileStream.good()) return false;

			fileStream.write((const char*)buffer.data(), buffer.size());
			return true;
		}

		PD_FORCEINLINE bool WriteBinaryFile(const char* pFilePath, CArray<byte>& buffer)
		{
			std::ofstream fileStream(pFilePath, std::ios::binary);
			if(!fileStream.good()) return false;

			fileStream.write((const char*)buffer.GetData(), buffer.GetSize());
			return true;
		}

		template<typename TChar>
		PD_FORCEINLINE bool WriteTextFile(const char* pFilePath, STL::CBasicString<TChar>& buffer)
		{
			std::basic_ofstream<TChar, std::char_traits<TChar>> fileStream(pFilePath);
			if(!fileStream.good()) return false;

			fileStream.write(buffer.c_str(), buffer.size());
			return true;
		}
	}
}

#endif //PD_ENGINE_CORE_FILE_FILEUTILITIES_HPP
