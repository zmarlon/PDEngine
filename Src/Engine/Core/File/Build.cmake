set(PD_ENGINE_CORE_FILE_DIRECTORY Src/Engine/Core/File)

set(PD_ENGINE_CORE_FILE_SOURCE_FILES
        ${PD_ENGINE_CORE_FILE_DIRECTORY}/FileUtilities.cpp
        ${PD_ENGINE_CORE_FILE_DIRECTORY}/FileUtilities.hpp)
