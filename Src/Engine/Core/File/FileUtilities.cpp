//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 17.04.2020.
//

#include "FileUtilities.hpp"
#include "../Platform.hpp"
#include "../Logging/Logger.hpp"
#include "../../Game.hpp"

#ifdef PD_PLATFORM_WINDOWS
#include <shlobj.h>
#include <objbase.h>
#endif

namespace PD::FileUtilities
{
	void GetGameDataDirectory(std::filesystem::path& gameDataDirectory)
	{
#ifdef PD_PLATFORM_WINDOWS
		std::filesystem::path savedGamesFolderPath;

		PWSTR pPath = nullptr;
		HRESULT result = SHGetKnownFolderPath(FOLDERID_SavedGames, KF_FLAG_CREATE, nullptr, &pPath);
		if(FAILED(result))
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal, L"SHGetKnownFolderPath failed with HRESULT ") << result << CLogger::GetInstance()->Flush();
		}

		if(pPath != nullptr)
		{
			savedGamesFolderPath = std::filesystem::path(pPath);
			CoTaskMemFree(pPath);
		}

		gameDataDirectory = savedGamesFolderPath / CGame::GetInstance()->GetGameInfo().pName;

		if(!std::filesystem::exists(gameDataDirectory) || !std::filesystem::is_directory(gameDataDirectory))
		{
			std::filesystem::create_directories(gameDataDirectory);
		}
#elif defined(PD_PLATFORM_LINUX)
		gameDataDirectory =  std::filesystem::path("~/.PDEngine");
#else
#error "Unsupported operating system"
#endif
	}
}