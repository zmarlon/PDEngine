//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 05.04.2020.
//
#ifndef PD_ENGINE_CORE_PLATFORM_HPP
#define PD_ENGINE_CORE_PLATFORM_HPP

#pragma once

#ifdef _WIN32
#define PD_PLATFORM_WINDOWS
#ifdef _MSC_VER
#define PD_COMPILER_MSVC
#elif defined(__GNUC__)
#define PD_COMPILER_GCC
#elif defined(_clang_)
#define PD_COMPILER_CLANG
#endif
#elif defined(__linux__)
#define PD_PLATFORM_LINUX
#ifdef __GNUC__
#define PD_COMPILER_GCC
#elif defined(_clang_)
#define PPD_COMPILER_CLANG
#endif
#else
#error "Only Win64 and Linux(x64) are supported!"
#endif

#if !defined(PD_COMPILER_MSVC) && !defined(PD_COMPILER_GCC) && !defined(PD_COMPILER_CLANG)
#error "Unsupported compiler"
#endif

#define PD_WIDE_STRINGIFY(x) L ## #x
#define PD_STRINGIFY_CASE(x) case x: return PD_WIDE_STRINGIFY(x);

static_assert(sizeof(void*) == 8, "Only 64-Bit operating systems are supported! Come on, it's 2020!");
static_assert(sizeof(bool) == 1, "Compatibility to C#");

#endif //PD_ENGINE_CORE_PLATFORM_HPP
