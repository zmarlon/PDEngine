//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 05.04.2020.
//
#ifndef PD_ENGINE_CORE_SINGLETON_HPP
#define PD_ENGINE_CORE_SINGLETON_HPP

#pragma once

#include "Assert.hpp"
#include "Platform.hpp"
#include "Hints/ForceInline.hpp"

//@formatter:off
#define __PD_MAKE_OBJECT_SINGLETON_BASE(singletonVisibility, object)\
	singletonVisibility:\
		static object* _INSTANCE;\
		PD_FORCEINLINE void AttachSingleton()\
		{\
			PD_ASSERT(!_INSTANCE);\
			_INSTANCE = this;\
		}\
		PD_FORCEINLINE void DetachSingleton()\
		{\
			PD_ASSERT(_INSTANCE);\
			_INSTANCE = nullptr;\
		}\

#ifdef PD_DEBUG
#define PD_MAKE_OBJECT_SINGLETON(singletonVisibility, object, defaultVisibility)\
	__PD_MAKE_OBJECT_SINGLETON_BASE(singletonVisibility, object)\
	public:\
		PD_FORCEINLINE static object* GetInstance()\
		{\
			PD_ASSERT(_INSTANCE);\
			return _INSTANCE;\
		}\
		PD_FORCEINLINE static bool IsValid()\
		{\
			return _INSTANCE != nullptr;\
		}\
	defaultVisibility:
#else
#define PD_MAKE_OBJECT_SINGLETON(singletonVisibility, object, defaultVisibility)\
	__PD_MAKE_OBJECT_SINGLETON_BASE(singletonVisibility, object)\
	public:\
		PD_FORCEINLINE static object* GetInstance() {return _INSTANCE; };\
		PD_FORCEINLINE static bool IsValid()\
		{\
			return _INSTANCE != nullptr;\
		}\
	defaultVisibility:
#endif

#define PD_POSTDEFINE_SINGLETON(object) object* object::_INSTANCE = nullptr;
//@formatter:on

#define PD_MAKE_CLASS_SINGLETON(object) PD_MAKE_OBJECT_SINGLETON(private, object, private)
#define PD_MAKE_STRUCT_SINGLETON(object) PD_MAKE_OBJECT_SINGLETON(private, object, public)
#define PD_MAKE_INTERFACE_SINGLETON(object) PD_MAKE_OBJECT_SINGLETON(protected, object, private)

#endif //PD_ENGINE_CORE_SINGLETON_HPP
