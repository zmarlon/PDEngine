//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 05.04.2020.
//
#ifndef PD_ENGINE_CORE_ASSERT_HPP
#define PD_ENGINE_CORE_ASSERT_HPP

#pragma once

#ifdef PD_DEBUG
#include <cassert>
#define PD_ASSERT(x) assert(x)
#else
#define PD_ASSERT(x) x
#endif

#endif //PD_ENGINE_CORE_ASSERT_HPP
