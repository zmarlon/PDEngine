//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 27.04.2020.
//
#ifndef PD_ENGINE_CORE_THREADING_RUNNABLE_HPP
#define PD_ENGINE_CORE_THREADING_RUNNABLE_HPP

#pragma once

#include "../Types.hpp"

namespace PD
{
    class IRunnable
    {
    public:
        virtual bool Initialize() {return true; }

        virtual u32 Run() = 0;
        virtual void Stop() {}
        virtual void Exit() {}

        virtual ~IRunnable() {}
    };
}

#endif //PD_ENGINE_CORE_THREADING_RUNNABLE_HPP
