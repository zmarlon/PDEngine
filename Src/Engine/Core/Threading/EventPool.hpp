//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 27.04.2020.
//
#ifndef PD_ENGINE_CORE_THREADING_EVENTPOOL_HPP
#define PD_ENGINE_CORE_THREADING_EVENTPOOL_HPP

#pragma once

#include "../Disposable.hpp"
#include "../Singleton.hpp"

namespace PD
{
    class CEventPool
    {
        PD_MAKE_CLASS_DISPOSABLE(CEventPool);
        PD_MAKE_CLASS_SINGLETON(CEventPool);
    private:
        void OnDisposing();
    };
}

#endif //PD_ENGINE_CORE_THREADING_EVENTPOOL_HPP
