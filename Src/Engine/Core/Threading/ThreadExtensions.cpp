//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#include "ThreadExtensions.hpp"
#include "../STL/Map.hpp"
#include "../Memory/SystemAllocator.hpp"

namespace PD::ThreadExtensions
{
	static STL::CMap<std::thread::id, const wchar_t*> _S_THREAD_NAMES = STL::CMap<std::thread::id, const wchar_t*>(CSystemAllocator::GetInstance());

	void SetName(std::thread::id threadId, const wchar_t* pName)
	{
		PD_ASSERT(_S_THREAD_NAMES.find(threadId) == _S_THREAD_NAMES.end());
		PD_ASSERT(pName);

		_S_THREAD_NAMES[threadId] = pName;
	}

	const wchar_t* GetName(std::thread::id threadId)
	{
		PD_ASSERT(_S_THREAD_NAMES.find(threadId) != _S_THREAD_NAMES.end());
		return _S_THREAD_NAMES[threadId];
	}

    void InsertMemoryBarrier()
    {
	    //TODO:
    }
}
