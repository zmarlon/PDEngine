set(PD_ENGINE_CORE_THREADING_DIRECTORY Src/Engine/Core/Threading)

set(PD_ENGINE_CORE_THREADING_SOURCE_FILES
        ${PD_ENGINE_CORE_THREADING_DIRECTORY}/Event.hpp
        ${PD_ENGINE_CORE_THREADING_DIRECTORY}/EventPool.cpp
        ${PD_ENGINE_CORE_THREADING_DIRECTORY}/EventPool.hpp
        ${PD_ENGINE_CORE_THREADING_DIRECTORY}/Runnable.hpp
        ${PD_ENGINE_CORE_THREADING_DIRECTORY}/RunnableThread.cpp
        ${PD_ENGINE_CORE_THREADING_DIRECTORY}/RunnableThread.hpp
        ${PD_ENGINE_CORE_THREADING_DIRECTORY}/ThreadExtensions.cpp
        ${PD_ENGINE_CORE_THREADING_DIRECTORY}/ThreadExtensions.hpp
        ${PD_ENGINE_CORE_THREADING_DIRECTORY}/ThreadPriority.hpp)