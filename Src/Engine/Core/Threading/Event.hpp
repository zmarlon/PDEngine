//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 27.04.2020.
//
#ifndef PD_ENGINE_CORE_THREADING_EVENT_HPP
#define PD_ENGINE_CORE_THREADING_EVENT_HPP

#pragma once

#include "../Types.hpp"
#include "../Hints/ForceInline.hpp"

#ifdef PD_PLATFORM_WINDOWS
#include "../Platform/IncludeWindows.hpp"
#include "../Assert.hpp"
#elif defined(PD_PLATFORM_LINUX)
#include <pthread.h>
#include <sys/time.h>
#endif

//TODO: wurde von unserem Dirk geschrieben

namespace PD
{
    class CEvent
    {
    private:
        bool m_resetManually;
#ifdef PD_PLATFORM_WINDOWS
        HANDLE m_hEvent;
#elif defined(PD_PLATFORM_LINUX)
        enum class ETriggerType
        {
            None,
            One,
            All
        };

        bool m_initialized;
        volatile ETriggerType m_triggerType;
        volatile i32 m_waitingThreads;
        pthread_mutex_t m_mutex;
        pthread_cond_t m_condition;
#endif
    public:
        CEvent() : m_resetManually(false)
#ifdef PD_PLATFORM_WINDOWS
                   ,m_hEvent(nullptr)
#elif defined(PD_PLATFORM_LINUX)
                   ,m_initialized(false), m_triggerType(ETriggerType::None),
                   m_waitingThreads(0)
#endif
        {}

        ~CEvent()
        {
#ifdef PD_PLATFORM_WINDOWS
            if(m_hEvent) CloseHandle(m_hEvent);
#elif defined(PD_PLATFORM_LINUX)
            if(m_initialized)
            {
                PD_ASSERT(pthread_mutex_lock(&m_mutex) == 0);
                m_resetManually = true;
                PD_ASSERT(pthread_mutex_unlock(&m_mutex) == 0);
                Trigger();

                PD_ASSERT(pthread_mutex_lock(&m_mutex) == 0);
                m_initialized = false;
                while(m_waitingThreads)
                {
                    PD_ASSERT(pthread_mutex_unlock(&m_mutex) == 0);
                    PD_ASSERT(pthread_mutex_lock(&m_mutex) == 0);
                }
                pthread_cond_destroy(&m_condition);
                PD_ASSERT(pthread_mutex_unlock(&m_mutex) == 0);
                pthread_mutex_destroy(&m_mutex);
            }
#else
#error "Unsupported operating system"
#endif
        }

        PD_FORCEINLINE bool Create(bool resetManually = false)
        {
            m_resetManually = resetManually;
#ifdef PD_PLATFORM_WINDOWS
            m_hEvent = CreateEventA(nullptr, resetManually, 0, nullptr);
            return m_hEvent != nullptr;
#elif defined(PD_PLATFORM_LINUX)
            PD_ASSERT(!m_initialized);
            bool result = false;
            m_triggerType = ETriggerType::None;

            if(pthread_mutex_init(&m_mutex, nullptr) == 0)
            {
                if(pthread_cond_init(&m_condition, nullptr) == 0)
                {
                    m_initialized = true;
                    result = true;
                }
                else
                {
                    pthread_mutex_destroy(&m_mutex);
                }
            }
            return result;
#else
#error "Unsupported operating system"
#endif
        }

        PD_FORCEINLINE bool IsResettedManually() const {return m_resetManually; }

        PD_FORCEINLINE void Trigger()
        {
#ifdef PD_PLATFORM_WINDOWS
            PD_ASSERT(m_hEvent);
            SetEvent(m_hEvent);
#elif defined(PD_PLATFORM_LINUX)
            PD_ASSERT(m_initialized);

            PD_ASSERT(pthread_mutex_lock(&m_mutex) == 0);
            if(m_resetManually)
            {
                m_triggerType = ETriggerType::All;
                PD_ASSERT(pthread_cond_broadcast(&m_condition) == 0);
            }
            else
            {
                m_triggerType = ETriggerType::One;
                PD_ASSERT(pthread_cond_signal(&m_condition) == 0);
            }

            PD_ASSERT(pthread_mutex_unlock(&m_mutex) == 0);
#else
#error "Unsupported operating system"
#endif
        }

        PD_FORCEINLINE void Reset()
        {
#ifdef PD_PLATFORM_WINDOWS
            PD_ASSERT(m_hEvent);
            ResetEvent(m_hEvent);
#elif defined(PD_PLATFORM_LINUX)
            PD_ASSERT(m_initialized);
            PD_ASSERT(pthread_mutex_lock(&m_mutex) == 0);
            m_triggerType = ETriggerType::None;
            PD_ASSERT(pthread_mutex_unlock(&m_mutex) == 0);
#else
#error "Unsupported operating system"
#endif
        }

#ifdef PD_PLATFORM_LINUX
        static inline void SubtractTimevals(const struct timeval* pFromThis, struct timeval* pSubThis, struct timeval* pDifference )
        {
            if (pFromThis->tv_usec < pSubThis->tv_usec)
            {
                int nsec = (pSubThis->tv_usec - pFromThis->tv_usec) / 1000000 + 1;
                pSubThis->tv_usec -= 1000000 * nsec;
                pSubThis->tv_sec += nsec;
            }

            if (pFromThis->tv_usec - pSubThis->tv_usec > 1000000)
            {
                int nsec = (pFromThis->tv_usec - pSubThis->tv_usec) / 1000000;
                pSubThis->tv_usec += 1000000 * nsec;
                pSubThis->tv_sec -= nsec;
            }

            pDifference->tv_sec = pFromThis->tv_sec - pSubThis->tv_sec;
            pDifference->tv_usec = pFromThis->tv_usec - pSubThis->tv_usec;
        }
#endif

        PD_FORCEINLINE bool Wait(u32 waitTime, bool ignoreThreadIdleStatus = false)
        {
#ifdef PD_PLATFORM_WINDOWS
            PD_ASSERT(m_hEvent);
            return WaitForSingleObject(m_hEvent, waitTime) == WAIT_OBJECT_0;
#elif defined(PD_PLATFORM_LINUX)
            PD_ASSERT(m_initialized);

            struct timeval startTime;

            if ((waitTime > 0) && (waitTime != ((u32)-1))) gettimeofday(&startTime, NULL);
            PD_ASSERT(pthread_mutex_lock(&m_mutex) == 0);
            bool result = false;

            do
            {
                if (m_triggerType == ETriggerType::One)
                {
                    m_triggerType = ETriggerType::None;
                    result = true;
                }
                else if (m_triggerType == ETriggerType::All) result = true;
                else if (waitTime != 0)
                {
                    m_waitingThreads++;
                    if (waitTime == ((u32)-1)) PD_ASSERT(pthread_cond_wait(&m_condition, &m_mutex) == 0);
                    else
                    {
                        struct timespec TimeOut;
                        const u32 ms = (startTime.tv_usec / 1000) + waitTime;
                        TimeOut.tv_sec = startTime.tv_sec + (ms / 1000);
                        TimeOut.tv_nsec = (ms % 1000) * 1000000;
                        int rc = pthread_cond_timedwait(&m_condition, &m_mutex, &TimeOut);
                        PD_ASSERT((rc == 0) || (rc == ETIMEDOUT));

                        struct timeval now, difference;
                        gettimeofday(&now, NULL);
                        SubtractTimevals(&now, &startTime, &difference);
                        const u32 differenceMS = ((difference.tv_sec * 1000) + (difference.tv_usec / 1000));
                        waitTime = ((differenceMS >= waitTime) ? 0 : (waitTime - differenceMS));
                        startTime = now;
                    }
                    m_waitingThreads--;
                    PD_ASSERT(m_waitingThreads >= 0);
                }

            } while ((!result) && (waitTime != 0));

            PD_ASSERT(pthread_mutex_unlock(&m_mutex) == 0);
            return result;
#else
#error "Unsupported operating system"
#endif
        }

        PD_FORCEINLINE bool Wait()
        {
            return Wait(std::numeric_limits<u32>::max());
        }

        //TODO: maybe wait specific time
    };
}

#endif //PD_ENGINE_CORE_THREADING_EVENT_HPP
