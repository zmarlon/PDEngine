//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_THREADING_THREADEXTENSIONS_HPP
#define PD_ENGINE_CORE_THREADING_THREADEXTENSIONS_HPP

#pragma once

#include "../Singleton.hpp"
#include <thread>

namespace PD::ThreadExtensions
{
	void SetName(std::thread::id threadId, const wchar_t* pName);
	const wchar_t* GetName(std::thread::id threadId);

	PD_FORCEINLINE void SetName(std::thread& thread, const wchar_t* pName)
	{
		SetName(thread.get_id(), pName);
	}

	PD_FORCEINLINE const wchar_t* GetName(std::thread& thread)
	{
		return GetName(thread.get_id());
	}

	PD_FORCEINLINE void SetCurrentThreadName(const wchar_t* pName)
	{
		SetName(std::this_thread::get_id(), pName);
	}

	PD_FORCEINLINE const wchar_t* GetCurrentThreadName()
	{
		return GetName(std::this_thread::get_id());
	}

	void InsertMemoryBarrier();
}

#endif //PD_ENGINE_CORE_THREADING_THREADEXTENSIONS_HPP
