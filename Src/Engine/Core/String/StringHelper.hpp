//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_STRING_STRINGHELPER_HPP
#define PD_ENGINE_CORE_STRING_STRINGHELPER_HPP

#pragma once

#include "../Types.hpp"
#include "../Singleton.hpp"
#include "../STL/String.hpp"
#include "../STL/Vector.hpp"
#include <cstring>
#include <cwchar>

namespace PD::StringHelper
{
	PD_FORCEINLINE bool AreEqual(const char* p1, const char* p2)
	{
		return strcmp(p1, p2) == 0;
	}

	template<typename T>
	PD_FORCEINLINE usize GetLength(const T* pString);

	template<>
	PD_FORCEINLINE usize GetLength(const char* pString) {return strlen(pString);}

	template<>
	PD_FORCEINLINE usize GetLength(const wchar_t* pString) {return wcslen(pString);}

	template<typename T>
	PD_FORCEINLINE const T* Find(const T* pString, const T* pSubString);

	template<>
	PD_FORCEINLINE const char* Find(const char* pString, const char* pSubString) {return strstr(pString, pSubString);}

	template<>
	PD_FORCEINLINE const wchar_t* Find(const wchar_t* pString, const wchar_t* pSubString) {return wcsstr(pString, pSubString);}

	template<typename T>
	PD_FORCEINLINE bool EndsWith(const T* pString, const T* pEnding)
	{
		usize lengthStr = GetLength<T>(pString);
		usize lengthEnding = GetLength<T>(pEnding);

		if(lengthEnding > lengthStr) return false;
		for(usize ix = 0; ix < lengthEnding; ix++)
		{
			if(pString[lengthStr - lengthEnding + ix] != pEnding[ix]) return false;
		}
		return true;
	}

	template<typename TString>
	PD_FORCEINLINE bool EndsWith(const TString& string, const TString& ending)
	{
		usize lengthStr = string.length();
		usize lengthEnding = ending.length();

		if(lengthEnding > lengthStr) return false;
		for(usize ix = 0; ix < lengthEnding; ix++)
		{
			if(string[lengthStr - lengthEnding + ix] != ending[ix]) return false;
		}
		return true;
	}

	template<typename TString, typename TChar>
	PD_FORCEINLINE bool EndsWith(const TString& string, const TChar* pEnding)
    {
        usize lengthStr = string.length();
        usize lengthEnding = GetLength<TChar>(pEnding);

        if(lengthEnding > lengthStr) return false;
        for(usize ix = 0; ix < lengthEnding; ix++)
        {
            if(string[lengthStr - lengthEnding + ix] != pEnding[ix]) return false;
        }
        return true;
    }

    PD_FORCEINLINE u8 UpperHexCharToNumber(char c)
    {
        PD_ASSERT((c >= '0' && c <= '9') || (c >= 'A' && c <= 'F'));
        return c <= '9' ? c - '0' : c - 'A';
    }

    PD_FORCEINLINE u8 LowerHexCharToNumber(char c)
    {
        PD_ASSERT((c >= '0' && c <= '9') ||(c >= 'a' && c <= 'f'));
        return c <= '9' ? c - '0' : c - 'a';
    }

    char HexNumberToUpperChar(u8 hexNumber);
	char HexNumberToLowerChar(u8 hexNumber);

    wchar_t HexNumberToUpperWideChar(u8 hexNumber);
    wchar_t HexNumberToLowerWideChar(u8 hexNumber);

	void Split(const STL::CWString& str, const STL::CWString& delim, STL::CVector<STL::CWString>& tokens);
}

#endif //PD_ENGINE_CORE_STRING_STRINGHELPER_HPP
