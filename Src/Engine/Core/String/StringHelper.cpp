//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 16.04.2020.
//

#include "StringHelper.hpp"
#include <array>

namespace PD::StringHelper
{
    static std::array<char, 16> _S_UPPER_HEX_CHAR_MAP = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    char HexNumberToUpperChar(u8 hexNumber)
    {
        PD_ASSERT(hexNumber < 16);
        return _S_UPPER_HEX_CHAR_MAP[hexNumber];
    }

    static std::array<char, 16> _S_LOWER_HEX_CHAR_MAP = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    char HexNumberToLowerChar(u8 hexNumber)
    {
        PD_ASSERT(hexNumber < 16);
        return _S_LOWER_HEX_CHAR_MAP[hexNumber];
    }

    wchar_t HexNumberToUpperWideChar(u8 hexNumber)
    {
        PD_ASSERT(hexNumber < 16);
        return static_cast<wchar_t>(_S_UPPER_HEX_CHAR_MAP[hexNumber]);
    }

    wchar_t HexNumberToLowerWideChar(u8 hexNumber)
    {
        PD_ASSERT(hexNumber < 16);
        return static_cast<wchar_t>(_S_LOWER_HEX_CHAR_MAP[hexNumber]);
    }

	void Split(const STL::CWString& str, const STL::CWString& delim, STL::CVector<STL::CWString>& tokens)
	{
		size_t prev = 0, pos = 0;
		do
		{
			pos = str.find(delim, prev);
			if (pos == std::string::npos) pos = str.length();
			STL::CWString token = str.substr(prev, pos-prev);
			if (!token.empty()) tokens.push_back(token);
			prev = pos + delim.length();
		}
		while (pos < str.length() && prev < str.length());
	}
}