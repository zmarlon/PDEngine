set(PD_ENGINE_CORE_STRING_DIRECTORY Src/Engine/Core/String)

set(PD_ENGINE_CORE_STRING_SOURCE_FILES
        ${PD_ENGINE_CORE_STRING_DIRECTORY}/StringHelper.cpp
        ${PD_ENGINE_CORE_STRING_DIRECTORY}/StringHelper.hpp
        ${PD_ENGINE_CORE_STRING_DIRECTORY}/CharacterUtilities.cpp
        ${PD_ENGINE_CORE_STRING_DIRECTORY}/CharacterUtilities.hpp)