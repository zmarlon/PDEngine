//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 17.04.2020.
//

#ifndef PD_ENGINE_CORE_STRING_CHARACTERUTILITIES_HPP
#define PD_ENGINE_CORE_STRING_CHARACTERUTILITIES_HPP

#pragma once

#include "../Singleton.hpp"

namespace PD
{
	namespace CharacterUtilities
	{
		PD_FORCEINLINE bool IsWhitespace(char c)
		{
			return c == ' ' || c == '\r' || c == '\t';
		}

		PD_FORCEINLINE bool IsNewLine(char c)
		{
			return c == '\n';
		}

		PD_FORCEINLINE bool IsExponent(char c)
		{
			return c == 'e' || c == 'E';
		}

		PD_FORCEINLINE bool IsDigit(char c)
		{
			return c >= '0' && c <= '9';
		}

		const char* ParseFloat(const char* pCurrent, float& value);
		const char* ParseInt(const char* pCurrent, int& value);
	}
}

#endif //PD_ENGINE_CORE_STRING_CHARACTERUTILITIES_HPP
