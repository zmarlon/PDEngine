//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 17.04.2020.
//

#include "CharacterUtilities.hpp"
#include <array>

#define PD_FLOAT_MAX_POWER 16

//@formatter:off
static std::array<double, PD_FLOAT_MAX_POWER> _POWER_10_POSITIVE =
		{
				1.0e0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5, 1.0e6, 1.0e7,
				1.0e8, 1.0e9, 1.0e10, 1.0e11, 1.0e12, 1.0e13, 1.0e14, 1.0e15
		};

static std::array<double, PD_FLOAT_MAX_POWER> _POWER_10_NEGATIVE =
		{
				1.0e0, 1.0e-1, 1.0e-2, 1.0e-3, 1.0e-4, 1.0e-5, 1.0e-6, 1.0e-7,
				1.0e-8, 1.0e-9, 1.0e-10, 1.0e-11, 1.0e-12, 1.0e-13, 1.0e-14, 1.0e-15
		};

//@formatter:on

namespace PD::CharacterUtilities
{
	const char* ParseFloat(const char* pCurrent, float& value)
	{
		double sign, num, fra, div;
		int eval;
		const double* pPowers;

		while(CharacterUtilities::IsWhitespace(*pCurrent)) pCurrent++;

		switch(*pCurrent)
		{
			case '+': sign = 1.0;
				pCurrent++;
				break;
			case '-': sign = -1.0;
				pCurrent++;
				break;
			default: sign = 1.0;
				break;
		}

		num = 0.0;
		while(CharacterUtilities::IsDigit(*pCurrent))
		{
			num = 10.0 * num + (double)(*pCurrent++ - '0');
		}
		if(*pCurrent == '.') pCurrent++;

		fra = 0.0;
		div = 1.0;

		while(CharacterUtilities::IsDigit(*pCurrent))
		{
			fra = 10.0 * fra + (double)(*pCurrent++ - '0');
			div *= 10.0;
		}

		num += fra / div;
		if(CharacterUtilities::IsExponent(*pCurrent))
		{
			pCurrent++;

			switch(*pCurrent)
			{
				case '+': pPowers = _POWER_10_POSITIVE.data();
					break;
				case '-': pPowers = _POWER_10_NEGATIVE.data();
					break;
				default: pPowers = _POWER_10_POSITIVE.data();
					break;
			}

			eval = 0;
			while(CharacterUtilities::IsDigit(*pCurrent))
			{
				eval = 10 * eval + (*pCurrent++ - '0');
			}
			num *= (eval >= PD_FLOAT_MAX_POWER) ? 0.0 : pPowers[eval];
		}
		value = static_cast<float>(sign * num);
		return pCurrent;
	}

	const char* ParseInt(const char* pCurrent, int& value)
	{
		int sign = 0;
		int num;

		if(*pCurrent == '-')
		{
			sign = -1;
			pCurrent++;
		} else
		{
			sign = 1;
		}

		num = 0;
		while(CharacterUtilities::IsDigit(*pCurrent))
		{
			num = 10 * num + (*pCurrent++ - '0');
		}
		value = sign * num;
		return pCurrent;
	}
}