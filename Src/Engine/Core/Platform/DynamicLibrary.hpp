//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_PLATFORM_DYNAMICLIBRARY_HPP
#define PD_ENGINE_CORE_PLATFORM_DYNAMICLIBRARY_HPP

#pragma once

#include "../Platform.hpp"
#include "../Disposable.hpp"

#ifdef PD_PLATFORM_WINDOWS
#define NOMINMAX
#include <Windows.h>
#elif defined(PD_PLATFORM_LINUX)
#include <dlfcn.h>
#endif

namespace PD
{
	class CDynamicLibrary
	{
		PD_MAKE_CLASS_DISPOSABLE(CDynamicLibrary)
	private:
		void* m_pHandle;
		bool m_unloadLibrary;

		void* GetFunctionAddressInternal(const char* pName);

		void OnDisposing();
	public:
		void Load(const char* pPath, const char* pName, bool unloadLibrary = true);

		template<typename F>
		PD_FORCEINLINE F* GetFunctionAddress(const char* pName)
		{
			return (F*)GetFunctionAddressInternal(pName);
		}

		template<typename PFN>
		PD_FORCEINLINE PFN GetFunctionAddressPtr(const char* pName)
		{
			return (PFN)GetFunctionAddressInternal(pName);
		}
	};
}


#endif //PD_ENGINE_CORE_PLATFORM_DYNAMICLIBRARY_HPP
