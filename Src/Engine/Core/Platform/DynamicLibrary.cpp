//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#include "DynamicLibrary.hpp"
#include "../STL/String.hpp"
#include "../Logging/Logger.hpp"
#include <filesystem>

namespace PD
{
	void CDynamicLibrary::OnDisposing()
	{
	    if(m_unloadLibrary)
        {
#ifdef PD_PLATFORM_WINDOWS
		    FreeLibrary((HMODULE)m_pHandle);
#elif defined(PD_PLATFORM_LINUX)
		    dlclose(m_pHandle);
#else
#error "Unsupported operating system"
#endif
        }
	}

	void* CDynamicLibrary::GetFunctionAddressInternal(const char* pName)
	{
		PD_ASSERT(pName);
#ifdef PD_PLATFORM_WINDOWS
		void* pAddress = (void*)GetProcAddress((HMODULE)m_pHandle, pName);
#elif defined(PD_PLATFORM_LINUX)
		void* pAddress = dlsym(m_pHandle, pName);
#else
#error "Unsupported operating system"
#endif
		if(!pAddress)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to get function address: " << pName << CLogger::GetInstance()->Flush();
		}
		return pAddress;

	}

	void CDynamicLibrary::Load(const char* pPath, const char* pName, bool unloadLibrary)
	{
		PD_ASSERT(pPath);
		PD_ASSERT(pName);

		m_unloadLibrary = unloadLibrary;

		STL::CString buffer;
		buffer.reserve(256);
		char separator = static_cast<char>(std::filesystem::path::preferred_separator);

		buffer.append(pPath);
		buffer.push_back(separator);
#ifdef PD_PLATFORM_WINDOWS
		buffer.append(pName);
		buffer.append(".dll");

		m_pHandle = LoadLibraryA(buffer.c_str());
#elif defined PD_PLATFORM_LINUX
		buffer.append("lib");
        buffer.append(pName);
        buffer.append(".so");

        m_pHandle = dlopen(buffer.c_str(), RTLD_NOW);
#else
#error "Unsupported operating system"
#endif

		if(!m_pHandle)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to load dynamic library: " << buffer.data() << CLogger::GetInstance()->Flush();
		}

		PD_END_INITIALIZE;
	}
}