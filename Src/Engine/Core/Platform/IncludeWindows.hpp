//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 22.04.2020.
//
#ifndef PD_ENGINE_CORE_PLATFORM_INCLUDEWINDOWS_HPP
#define PD_ENGINE_CORE_PLATFORM_INCLUDEWINDOWS_HPP

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#define NOMINMAX
#ifndef _WINSOCKAPI_
#define _WINSOCKAPI_
#endif

#include <Windows.h>

#ifndef PD_SUPPRESS_WINDOWS_H_DEFINES
#undef DrawText
#undef GetCharWidth
#undef GetCommandLine
#undef GetObject
#undef GetUserName
#undef LoadLibrary
#undef max
#undef min
#undef PlaySound
#endif

#endif //PD_ENGINE_CORE_PLATFORM_INCLUDEWINDOWS_HPP
