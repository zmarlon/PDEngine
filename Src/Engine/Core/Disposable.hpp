//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 05.04.2020.
//
#ifndef PD_ENGINE_CORE_DISPOSABLE_HPP
#define PD_ENGINE_CORE_DISPOSABLE_HPP

#pragma once

#include "Hints/ForceInline.hpp"
#include "Assert.hpp"

#define PD_MAKE_OBJECT_UNCOPYABLE(object, defaultVisibility)\
    public:\
        object(const object& o) = delete;\
        object(object&& o) = delete;\
        object& operator=(const object& o) = delete;\
        object& operator=(object&& o) = delete;\
    defaultVisibility:

#define PD_MAKE_CLASS_UNCOPYABLE(object) PD_MAKE_OBJECT_UNCOPYABLE(object, private)
#define PD_MAKE_STRUCT_UNCOPYABLE(object) PD_MAKE_OBJECT_UNCOPYABLE(object, public)

//@formatter:off
#ifdef PD_DEBUG
#define PD_MAKE_OBJECT_DISPOSABLE(object, defaultVisibility)\
	PD_MAKE_OBJECT_UNCOPYABLE(object, defaultVisibility)\
	protected:\
		bool m_isInitialized = false;\
	public:\
		object() {}\
		PD_FORCEINLINE void Dispose()\
		{\
			PD_ASSERT(m_isInitialized && #object);\
			OnDisposing();\
			m_isInitialized = false;\
		}\
		virtual ~object()\
		{\
			PD_ASSERT(!m_isInitialized && #object);\
		}\
	defaultVisibility:
#define PD_MAKE_OBJECT_DISPOSABLE_WITH_ARGUMENT(object, defaultVisibility, argumentType)\
    PD_MAKE_OBJECT_UNCOPYABLE(object, defaultVisibility)\
    protected:\
        bool m_isInitialized = false;\
    public:\
	    object() {}\
	    PD_FORCEINLINE void Dispose(argumentType ___argumentName___)\
	    {\
	    	PD_ASSERT(m_isInitialized && #object);\
	    	OnDisposing(___argumentName___);\
	    	m_isInitialized = false;\
	    }\
	    virtual ~object()\
	    {\
	    	PD_ASSERT(!m_isInitialized && #object);\
	    }\
	defaultVisibility:
#define PD_END_INITIALIZE m_isInitialized = true
#else
#define PD_MAKE_OBJECT_DISPOSABLE(object, defaultVisibility)\
	PD_MAKE_OBJECT_UNCOPYABLE(object, defaultVisibility)\
	public:\
		object() = default;\
		PD_FORCEINLINE void Dispose() {OnDisposing(); }\
	defaultVisibility:
#define PD_MAKE_OBJECT_DISPOSABLE_WITH_ARGUMENT(object, defaultVisibility, argumentType)\
    PD_MAKE_OBJECT_UNCOPYABLE(object, defaultVisibility)\
    public:\
        object() = default;\
        PD_FORCEINLINE void Dispose(argumentType ___argumentName___) {OnDisposing(___argumentName___); }\
    defaultVisibility:
#define PD_END_INITIALIZE
#endif
//@formatter:on

#define PD_MAKE_CLASS_DISPOSABLE(object) PD_MAKE_OBJECT_DISPOSABLE(object, private)
#define PD_MAKE_STRUCT_DISPOSABLE(object) PD_MAKE_OBJECT_DISPOSABLE(object, public)
#define PD_MAKE_INTERFACE_DISPOSABLE(object) PD_MAKE_OBJECT_DISPOSABLE(object, protected)\
    virtual void OnDisposing() = 0; private:

#define PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(object, argumentType) PD_MAKE_OBJECT_DISPOSABLE_WITH_ARGUMENT(object, private, argumentType)
#define PD_MAKE_STRUCT_DISPOSABLE_WITH_ARGUMENT(object, argumentType) PD_MAKE_OBJECT_DISPOSABLE_WITH_ARGUMENT(object, public, argumentType)

#endif //PD_ENGINE_CORE_DISPOSABLE_HPP
