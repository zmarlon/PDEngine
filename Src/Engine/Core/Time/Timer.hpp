//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 21.04.2020.
//
#ifndef PD_ENGINE_CORE_TIME_TIMER_HPP
#define PD_ENGINE_CORE_TIME_TIMER_HPP

#pragma once

#include "FrameEventArgs.hpp"
#include "../Types.hpp"
#include "../Singleton.hpp"
#include <functional>

namespace PD
{
    class CTimer
    {
    private:
        u32 m_frameLimit = 0;
        u64 m_lastTime;

        float m_deltaTime;
        float m_elapsedTime = 0.0f;
    public:
        PD_FORCEINLINE void SetFrameLimit(u32 frameLimit)
        {
            m_frameLimit = frameLimit;
        }

        void Tick(const std::function<void(const SFrameEventArgs&)>& function);
    };
}

#endif //PD_ENGINE_CORE_TIME_TIMER_HPP
