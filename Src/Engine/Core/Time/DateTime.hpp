//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#ifndef PD_ENGINE_CORE_TIME_DATETIME_HPP
#define PD_ENGINE_CORE_TIME_DATETIME_HPP

#include "../Types.hpp"
#include "../Singleton.hpp"
#include "../STL/String.hpp"
#include <ctime>

namespace PD
{
	class CDateTime
	{
	private:
		std::time_t m_time;
		std::tm m_timeStruct;
		const wchar_t* m_pFormat = nullptr;

		explicit CDateTime(std::time_t time, const wchar_t* pFormat);
	public:
		CDateTime() = default;

		PD_FORCEINLINE static CDateTime Now(const wchar_t* pFormat = nullptr)
		{
			return CDateTime(std::time(nullptr), pFormat);
		}

		PD_FORCEINLINE void SetFormat(const wchar_t* pFormat)
		{
			PD_ASSERT(pFormat);
			m_pFormat = pFormat;
		}

		friend std::wostream& operator <<(std::wostream& stream, const CDateTime& dateTime);
		void AppendToString(STL::CString& str);
		void AppendToWString(STL::CWString& str);
		STL::CWString ToString() const;

		PD_FORCEINLINE i32 GetSeconds() const { return m_timeStruct.tm_sec; }
		PD_FORCEINLINE i32 GetMinutes() const { return m_timeStruct.tm_min; }
		PD_FORCEINLINE i32 GetHours() const { return m_timeStruct.tm_hour; }
		PD_FORCEINLINE i32 GetDayOfMonth() const { return m_timeStruct.tm_mday; }
		PD_FORCEINLINE i32 GetMonth() const { return m_timeStruct.tm_mon; }
		PD_FORCEINLINE i32 GetYear() const { return m_timeStruct.tm_year + 1900; }
		PD_FORCEINLINE i32 GetDayOfWeek() const { return m_timeStruct.tm_wday; }
		PD_FORCEINLINE i32 GetDayOfYear() const { return m_timeStruct.tm_yday; }
	};

}

#endif //PD_ENGINE_CORE_TIME_DATETIME_HPP
