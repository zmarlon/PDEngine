//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//

#include "DateTime.hpp"
#include <ostream>
#include <mutex>

namespace PD
{
    static std::mutex _LOCAL_TIME_MUTEX;

	std::wostream& operator<<(std::wostream& stream, const CDateTime& dateTime)
	{
		PD_ASSERT(dateTime.m_pFormat);

		wchar_t buffer[80];
		wcsftime(buffer, sizeof(buffer) / sizeof(wchar_t), dateTime.m_pFormat, &dateTime.m_timeStruct);
		stream << buffer;
		return stream;
	}

	CDateTime::CDateTime(std::time_t time, const wchar_t* pFormat) : m_time(time), m_pFormat(pFormat)
	{
	    _LOCAL_TIME_MUTEX.lock();
		m_timeStruct = *localtime(&m_time);
		_LOCAL_TIME_MUTEX.unlock();
	}

    void CDateTime::AppendToString(STL::CString& str)
    {
        PD_ASSERT(m_pFormat);

        usize formatLength = wcslen(m_pFormat);
        STL::CString formatBuffer;
        formatBuffer.resize(formatLength);
        wcstombs(formatBuffer.data(), m_pFormat, formatLength);

        char buffer[80];
        strftime(buffer, sizeof(buffer), formatBuffer.c_str(), &m_timeStruct);
        str.append(buffer);
    }

    void CDateTime::AppendToWString(STL::CWString& str)
    {
        PD_ASSERT(m_pFormat);

        wchar_t buffer[80];
        wcsftime(buffer, sizeof(buffer) / sizeof(wchar_t), m_pFormat, &m_timeStruct);
        str.append(buffer);
    }

    STL::CWString CDateTime::ToString() const
	{
		PD_ASSERT(m_pFormat);

		wchar_t buffer[80];
		wcsftime(buffer, sizeof(buffer) / sizeof(wchar_t), m_pFormat, &m_timeStruct);
		return STL::CWString(buffer);
	}
}