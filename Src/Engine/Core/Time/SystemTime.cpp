//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 22.04.2020.
//
#include "SystemTime.hpp"
#include <chrono>

namespace PD::SystemTime
{
    u64 GetCurrentTimeSeconds()
    {
        return std::chrono::high_resolution_clock::now().time_since_epoch().count() / 1000000000;
    }

    u64 GetCurrentTimeMilliseconds()
    {
        return std::chrono::high_resolution_clock::now().time_since_epoch().count() / 1000000;
    }

    u64 GetCurrentTimeNanoseconds()
    {
        return std::chrono::high_resolution_clock::now().time_since_epoch().count();
    }
}