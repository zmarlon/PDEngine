//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 21.04.2020.
//
#ifndef PD_ENGINE_CORE_TIME_FRAMEEVENTARGS_HPP
#define PD_ENGINE_CORE_TIME_FRAMEEVENTARGS_HPP

#pragma once

namespace PD
{
    struct SFrameEventArgs
    {
        float DeltaTime;
        float ElapsedTime;

        SFrameEventArgs(float deltaTime, float elapsedTime) : DeltaTime(deltaTime), ElapsedTime(elapsedTime) {}
    };
}

#endif //PD_ENGINE_CORE_TIME_FRAMEEVENTARGS_HPP
