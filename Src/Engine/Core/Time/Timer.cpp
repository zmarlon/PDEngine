//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 21.04.2020.
//
#include "Timer.hpp"
#include "SystemTime.hpp"
#include <thread>
#include <iostream>

namespace PD
{
    void CTimer::Tick(const std::function<void(const SFrameEventArgs&)>& function)
    {
        PD_ASSERT(function);

        u64 startTime = SystemTime::GetCurrentTimeNanoseconds();

        SFrameEventArgs frameEventArgs(m_deltaTime, m_elapsedTime);
        function(frameEventArgs);

        u64 elapsedTime = SystemTime::GetCurrentTimeNanoseconds() - startTime;
        if(1000000000 / m_frameLimit > elapsedTime)
        {
            std::this_thread::sleep_for(std::chrono::nanoseconds(1000000000 / m_frameLimit - elapsedTime));
        }

        m_deltaTime = (float)(static_cast<double>((SystemTime::GetCurrentTimeNanoseconds() - startTime)) * 0.000000001);
    }
}