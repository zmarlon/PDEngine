//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 22.04.2020.
//
#ifndef PD_ENGINE_CORE_TIME_SYSTEMTIME_HPP
#define PD_ENGINE_CORE_TIME_SYSTEMTIME_HPP

#pragma once

#include "../Types.hpp"

namespace PD::SystemTime
{
    u64 GetCurrentTimeSeconds();
    u64 GetCurrentTimeMilliseconds();
    u64 GetCurrentTimeNanoseconds();
}

#endif //PD_ENGINE_CORE_TIME_SYSTEMTIME_HPP
