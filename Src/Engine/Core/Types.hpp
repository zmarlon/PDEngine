//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 05.04.2020.
//
#ifndef PD_ENGINE_TYPES_HPP
#define PD_ENGINE_TYPES_HPP

#pragma once

namespace PD
{
    typedef unsigned char u8;
    static_assert(sizeof(u8) == 1, "sizeof(u8) must be 1!");
    typedef signed char i8;
    static_assert(sizeof(i8) == 1, "sizeof(i8) must be 1!");

    typedef unsigned char byte;

    typedef unsigned short u16;
    static_assert(sizeof(u16) == 2, "sizeof(u16) must be 2!");
    typedef signed short i16;
    static_assert(sizeof(i16) == 2, "sizeof(i16) must be 2!");

    typedef unsigned int u32;
    static_assert(sizeof(u32) == 4, "sizeof(u32) must be 4!");
    typedef signed int i32;
    static_assert(sizeof(i32) == 4, "sizeof(i32) must be 4!");

    typedef unsigned long long u64;
    static_assert(sizeof(u64) == 8, "sizeof(u64) must be 8!");
    typedef signed long long i64;
    static_assert(sizeof(i64) == 8, "sizeof(i64) must be 8!");

    typedef float f32;
    static_assert(sizeof(f32) == 4, "sizeof(f32) must be 4!");
    typedef double f64;
    static_assert(sizeof(f64) == 8, "sizeof(f64) must be 8!");

    using usize = decltype(sizeof(void*));
    using uptrdiff = decltype((byte*)nullptr - (byte*)nullptr);
}

#endif //PD_ENGINE_TYPES_HPP
