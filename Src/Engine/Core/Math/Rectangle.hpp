//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_MATH_RECTANGLE_HPP
#define PD_ENGINE_CORE_MATH_RECTANGLE_HPP

#include "Point.hpp"
#include "../Types.hpp"

namespace PD
{
    template<typename T>
    struct SRectangle
    {
        T X;
        T Y;
        T Width;
        T Height;

        constexpr SRectangle(T x = static_cast<T>(0), T y = static_cast<T>(0), T width = static_cast<T>(0), T height = static_cast<T>(0))
            : X(x), Y(y), Width(width), Height(height) {}
        constexpr SRectangle(const SPoint<T>& position, const SPoint<T>& size) : X(position.X), Y(position.Y), Width(size.X), Height(size.Y) {}

        PD_FORCEINLINE T GetLeft() const {return X; }
        PD_FORCEINLINE T GetRight() const {return X + Width; }
        PD_FORCEINLINE T GetTop() const {return Y; }
        PD_FORCEINLINE T GetBottom() const {return Y + Height; }

        PD_FORCEINLINE SPoint<T> GetCenter() const
        {
            return SPoint<T>(X + Width / static_cast<T>(2), Y + Height / static_cast<T>(2));
        }

        PD_FORCEINLINE bool Contains(const SPoint<T>& point) const
        {
            return point.X >= X && point.X <= X + Width && point.Y >= Y && point.Y <= Y + Height;
        }

        PD_FORCEINLINE bool Intersects(const SRectangle<T>& rect) const
        {
            if (rect.X < X + Width && X < rect.X + rect.Width && rect.Y < Y + Height) return Y < rect.Y + rect.Height;
            return false;
        }

        template<typename U>
        PD_FORCEINLINE SRectangle<U> As() const
        {
            return SRectangle<U>(static_cast<U>(X), static_cast<U>(Y), static_cast<U>(Width), static_cast<U>(Height));
        }

        PD_FORCEINLINE bool operator ==(const SRectangle<T>& other) const
        {
            return X == other.X && Y == other.Y && Width == other.Width && Height == other.Height;
        }

        PD_FORCEINLINE bool operator !=(const SRectangle<T>& other) const
        {
            return X != other.X || Y != other.Y || Width != other.Width || Height != other.Height;
        }
    };

    typedef SRectangle<f32> SRectangleF;
    typedef SRectangle<f64> SRectangleD;
    typedef SRectangle<byte> SRectangleB;
    typedef SRectangle<i32> SRectangleI;
    typedef SRectangle<u32> SRectangleUI;
}

#endif //PD_ENGINE_CORE_MATH_RECTANGLE_HPP
