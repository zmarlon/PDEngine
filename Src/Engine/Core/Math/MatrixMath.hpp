//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 12.04.2020.
//

#pragma once

#ifndef PD_ENGINE_CORE_MATH_MATRIXMATH_HPP
#define PD_ENGINE_CORE_MATH_MATRIXMATH_HPP

#include "../Singleton.hpp"
#include <glm/glm.hpp>

namespace PD::MatrixMath
{
    template<typename T>
    PD_FORCEINLINE glm::mat<4, 4, T, glm::defaultp> Ortho(T left, T right, T bottom, T top)
    {
        glm::mat<4, 4, T, glm::defaultp> result(static_cast<T>(1));
        result[0][0] = static_cast<T>(2) / (right - left);
        result[1][1] = static_cast<T>(2) / (bottom - top);
        result[2][2] = -static_cast<T>(1);
        result[3][0] = -(right + left) / (right - left);
        result[3][1] = -(top + bottom) / (bottom - top);
        return result;
    }
}

#endif //PD_ENGINE_CORE_MATH_MATRIXMATH_HPP
