//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_MATH_POINT_HPP
#define PD_ENGINE_CORE_MATH_POINT_HPP

#include "../Singleton.hpp"

namespace PD
{
    template<typename T>
    struct SPoint
    {
        T X;
        T Y;

        constexpr SPoint(T x = static_cast<T>(0), T y = static_cast<T>(0)) : X(x), Y(y) {}

        template<typename U>
        PD_FORCEINLINE SPoint<U> As() const
        {
            return SPoint<U>(static_cast<U>(X), static_cast<U>(Y));
        }

        PD_FORCEINLINE bool operator ==(const SPoint<T>& other) const
        {
            return X == other.X && Y == other.Y;
        }

        PD_FORCEINLINE bool operator !=(const SPoint<T>& other) const
        {
            return X != other.X || Y != other.Y;
        }
    };
}

#endif //PD_ENGINE_CORE_MATH_POINT_HPP
