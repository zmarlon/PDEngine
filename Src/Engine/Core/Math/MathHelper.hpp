//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 19.04.2020.
//
#ifndef PD_ENGINE_CORE_MATH_MATHHELPER_HPP
#define PD_ENGINE_CORE_MATH_MATHHELPER_HPP

#include "../Singleton.hpp"

namespace PD
{
	namespace MathHelper
	{
		template<typename T>
		PD_FORCEINLINE constexpr T Min(T x, T y)
		{
			return x > y ? y : x;
		}

		template<typename T>
		PD_FORCEINLINE constexpr T Max(T x, T y)
		{
			return x > y ? x : y;
		}

		template<typename T>
		PD_FORCEINLINE constexpr T Clamp(const T x, const T min, const T max)
		{
			return x < min ? min : x < max ? x : max;
		}

		PD_FORCEINLINE constexpr int Floor(float f)
		{
			if(static_cast<int>(f) == f)
			{
				return static_cast<int>(f);
			}
			if(f < 0)
			{
				return static_cast<int>(f) - 1;
			}
			else
			{
				return static_cast<int>(f);
			}
		}

		PD_FORCEINLINE constexpr int RoundToInt(float f)
		{
			return Floor(f + 0.5f);
		}
	};

}

#endif //PD_ENGINE_CORE_MATH_MATHHELPER_HPP
