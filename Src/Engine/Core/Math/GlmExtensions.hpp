//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PD_ENGINE_CORE_MATH_GLMEXTENSIONS_HPP
#define PD_ENGINE_CORE_MATH_GLMEXTENSIONS_HPP

#pragma once

#include "../Hints/ForceInline.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#ifdef PD_GLM_OSTREAM_OPERATOR
#include <ostream>
#endif

namespace PD
{
	namespace GlmExtensions
	{
		template<typename T>
		constexpr glm::tvec2<T> Vec2_Zero()
		{
			return glm::tvec2<T>(static_cast<T>(0), static_cast<T>(0));
		}

		template<typename T>
		constexpr glm::tvec3<T> Vec3_Zero()
		{
			return glm::tvec3<T>(static_cast<T>(0), static_cast<T>(0), static_cast<T>(0));
		}

		template<typename T>
		constexpr glm::tvec4<T> Vec4_Zero()
		{
			return glm::tvec4<T>(static_cast<T>(0), static_cast<T>(0), static_cast<T>(0), static_cast<T>(0));
		}

		template<typename T>
		constexpr glm::tvec2<T> Vec2_One()
		{
			return glm::tvec2<T>(static_cast<T>(1), static_cast<T>(1));
		}

		template<typename T>
		constexpr glm::tvec3<T> Vec3_One()
		{
			return glm::tvec3<T>(static_cast<T>(1), static_cast<T>(1), static_cast<T>(1));
		}

		template<typename T>
		constexpr glm::tvec4<T> Vec4_One()
		{
			return glm::tvec4<T>(static_cast<T>(1), static_cast<T>(1), static_cast<T>(1), static_cast<T>(1));
		}

		template<typename T>
		constexpr glm::tvec3<T> Vec3_UnitX()
		{
			return glm::tvec3<T>(static_cast<T>(1), static_cast<T>(0), static_cast<T>(0));
		}

		template<typename T>
		constexpr glm::tvec3<T> Vec3_UnitY()
		{
			return glm::tvec3<T>(static_cast<T>(0), static_cast<T>(1), static_cast<T>(0));
		}

		template<typename T>
		constexpr glm::tvec3<T> Vec3_UnitZ()
		{
			return glm::tvec3<T>(static_cast<T>(0), static_cast<T>(0), static_cast<T>(1));
		}

		PD_FORCEINLINE glm::mat4 ReverseDepthProjectionMatrixLH(float degFoV, float aspect, float zNear, float zFar)
		{
			float const tanHalfFovy = glm::tan(glm::radians(degFoV) / 2.0f);

			glm::mat4 result(0.0f);
			result[0][0] = 1.0f / (aspect * tanHalfFovy);
			result[1][1] = -1.0f / (tanHalfFovy);
			result[2][2] = zFar / (zFar - zNear) - 1.0f;
			result[2][3] = -1.0f;
			result[3][2] = (zFar * zNear) / (zFar - zNear);
			return result;
		}

		PD_FORCEINLINE glm::vec3 RotationToDirection(const glm::vec3& rotation)
		{
			glm::vec3 v;
			v.x = glm::sin(rotation.x) * glm::cos(rotation.y);
			v.y = glm::sin(rotation.y);
			v.z = glm::cos(rotation.x) * glm::cos(rotation.y);
			return v;
		}
#ifdef PD_GLM_OSTREAM_OPERATOR
		template<typename T>
		std::ostream& operator <<(std::ostream& stream, const glm::tvec2<T>& position)
		{
			stream << "{x:" << position.x << ", y:" << position.y << '}';
			return stream;
		}

		template<typename T>
		std::wostream& operator <<(std::wostream& stream, const glm::tvec2<T>& position)
		{
			stream << L"{x:" << position.x << L", y:" << position.y << L'}';
			return stream;
		}

		template<typename T>
		std::ostream& operator <<(std::ostream& stream, const glm::tvec3<T>& position)
		{
			stream << "{x:" << position.x << ", y:" << position.y << ", z:" << position.z << '}';
			return stream;
		}

		template<typename T>
		std::wostream& operator <<(std::wostream& stream, const glm::tvec3<T>& position)
		{
			stream << L"{x:" << position.x << L", y:" << position.y << L", z:" << position.z << L'}';
			return stream;
		}

		template<typename T>
		std::ostream& operator <<(std::ostream& stream, const glm::tvec4<T>& position)
		{
			stream << "{x:" << position.x << ", y:" << position.y << ", z:" << position.z << ", w:" << position.w << '}';
			return stream;
		}

		template<typename T>
		std::wostream& operator <<(std::wostream& stream, const glm::tvec4<T>& position)
		{
			stream << L"{x:" << position.x << L", y:" << position.y << L", z:" << position.z << L", w:" << position.w << L'}';
			return stream;
		}
#endif
	}

}

#endif //PD_ENGINE_CORE_MATH_GLMEXTENSIONS_HPP
