//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.05.2020.
//
#ifndef PD_ENGINE_CORE_MATH_MULTIPLES_HPP
#define PD_ENGINE_CORE_MATH_MULTIPLES_HPP

#pragma once

namespace PD::Multiples
{
    template<typename T>
    T Next(T multipleOf, T value)
    {
        T multiple = value + multipleOf - 1;
        multiple -= (multiple % multipleOf);
        return multiple;
    }
}

#endif //PD_ENGINE_CORE_MATH_MULTIPLES_HPP
