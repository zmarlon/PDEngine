//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_MATH_COLOR3F_HPP
#define PD_ENGINE_CORE_MATH_COLOR3F_HPP

#pragma once

#include "../Hints/ForceInline.hpp"
#include "../Types.hpp"

#define PD_ENABLE_HASH_FUNCTIONS
#ifdef PD_ENABLE_HASH_FUNCTIONS
#include <functional>
#endif

namespace PD
{
    struct SColor3f
    {
        float R, G, B;

        SColor3f() = default;
        constexpr SColor3f(float r, float g, float b) : R(r), G(g), B(b) {}
        constexpr SColor3f(u32 packedValue) : R(static_cast<float>((packedValue >> 16u) & 0xFFu) * 255.0f),
                                              G(static_cast<float>((packedValue >> 8u) & 0xFFu) * 255.0f),
                                              B(static_cast<float>(packedValue & 0xFFu) * 255.0f) {}

        PD_FORCEINLINE u32 Pack() const
        {
            u8 rr = static_cast<u8>(R * 255.0f);
            u8 rg = static_cast<u8>(G * 255.0f);
            u8 rb = static_cast<u8>(B * 255.0f);
            return (rr << 16u) | (rg << 8u) | rb;
        }
    };

    namespace Color3f
    {
        constexpr SColor3f Black = SColor3f(0.0f, 0.0f, 0.0f);
        constexpr SColor3f White = SColor3f(1.0f, 1.0f, 1.0f);
        constexpr SColor3f Red = SColor3f(1.0f, 0.0f, 0.0f);
        constexpr SColor3f CornflowerBlue = SColor3f(100.0f / 255.0f, 149.0f / 255.0f, 237.0f / 255.0f);
        constexpr SColor3f Yellow = SColor3f(0xFFD800);
        constexpr SColor3f Cyan = SColor3f(0x00FFAE);
        constexpr SColor3f Green = SColor3f(0.1f, 1.0f, 0.1f);

        PD_FORCEINLINE const float* GetRaw(const SColor3f& color)
        {
            return &color.R;
        }
    }
}

#ifdef PD_ENABLE_HASH_FUNCTIONS
namespace std
{
    using namespace PD;

    template<>
    class hash<SColor3f>
    {
    public:
        PD_FORCEINLINE usize operator()(const SColor3f& color)
        {
            return hash<float>()(color.R) ^ hash<float>()(color.G) << 2 ^ hash<float>()(color.B) >> 2;
        }
    };
}
#endif

#endif //PD_ENGINE_CORE_MATH_COLOR3F_HPP
