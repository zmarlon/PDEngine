//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_MATH_COLOR4F_HPP
#define PD_ENGINE_CORE_MATH_COLOR4F_HPP

#pragma once

#include "Color3f.hpp"

namespace PD
{
    struct SColor4f
    {
        float R, G, B, A;

        SColor4f() = default;
        constexpr SColor4f(float r, float g, float b, float a = 1.0f) : R(r), G(g), B(b), A(a) {}
        constexpr SColor4f(const SColor3f& rgb, float a = 1.0f) : R(rgb.R), G(rgb.G), B(rgb.B), A(A) {}
        constexpr SColor4f(u32 packedValue) : R(static_cast<float>((packedValue >> 24u) & 0xFFu) / 255.0f),
                                              G(static_cast<float>((packedValue >> 16u) & 0xFFu) / 255.0f),
                                              B(static_cast<float>((packedValue >> 8u) & 0xFFu) / 255.0f),
                                              A(static_cast<float>(packedValue & 0xFFu) / 255.0f) {}

        PD_FORCEINLINE u32 Pack() const
        {
            u8 br = static_cast<u8>(R * 255.0f);
            u8 bg = static_cast<u8>(G * 255.0f);
            u8 bb = static_cast<u8>(B * 255.0f);
            u8 ba = static_cast<u8>(A * 255.0f);
            return (br << 24u) | (bg << 16u) | (bb << 8u) | ba;
        }
    };

    namespace Color4f
    {
        constexpr SColor4f Black = SColor4f(Color3f::Black.R, Color3f::Black.G, Color3f::Black.B, 1.0f);
        constexpr SColor4f White = SColor4f(Color3f::White.R, Color3f::White.G, Color3f::White.B, 1.0f);
        constexpr SColor4f Red = SColor4f(Color3f::Red.R, Color3f::Red.G, Color3f::Red.B, 1.0f);
        constexpr SColor4f CornflowerBlue = SColor4f(Color3f::CornflowerBlue.R, Color3f::CornflowerBlue.G, Color3f::CornflowerBlue.B, 1.0f);
		constexpr SColor4f Yellow = SColor4f(Color3f::Yellow.R, Color3f::Yellow.G, Color3f::Yellow.B, 1.0f);
		constexpr SColor4f Cyan = SColor4f(Color3f::Cyan.R, Color3f::Cyan.G, Color3f::Cyan.B, 1.0f);
		constexpr SColor4f Green = SColor4f(Color3f::Green.R, Color3f::Green.G, Color3f::Green.B, 1.0f);

        PD_FORCEINLINE const float* GetRaw(const SColor4f& color)
        {
            return &color.R;
        }
    }
}

#endif //PD_ENGINE_CORE_MATH_COLOR4F_HPP
