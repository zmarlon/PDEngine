//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 27.04.2020.
//

#ifndef PD_ENGINE_CORE_MATH_MERSENNETWISTER_HPP
#define PD_ENGINE_CORE_MATH_MERSENNETWISTER_HPP

#include "../Types.hpp"
#include "../Singleton.hpp"
#include <array>
#include <ctime>

namespace PD
{
	template<typename TReturnType, u16 N, int M, int R, u32 A, int F, int U, int S, u32 B, int T, u32 C, int L>
	class CMersenneTwisterBase
	{
	private:
		static constexpr u64 MASK_LOWER = (1ull << R) - 1;
		static constexpr u64 MASK_UPPER = (1ull << R);

		u16 m_index = 0;
		std::array<TReturnType, N> m_data;

		PD_FORCEINLINE void TwistIteration(u32 i)
		{
			u32 x = (m_data[i] & MASK_UPPER) + (m_data[(i + 1) % N] & MASK_LOWER);

			u32 xA = x >> 1;

			if (x & 1)
			{
				xA ^= A;
			}

			m_data[i] = m_data[(i + M) % N] ^ xA;
		}

		void Twist()
		{
			for (u32 i = 0; i < N - 1; i++)
			{
				TwistIteration(i);
			}

			//compiler optimization
			TwistIteration(N - 1);

			m_index = 0;
		}

	public:
		CMersenneTwisterBase()
		{
			std::time_t timeStamp = std::time(nullptr);
			SetSeed(static_cast<TReturnType>(timeStamp));
		}

		void SetSeed(TReturnType seed)
		{
			m_data[0] = seed;

			for (u32 i = 1; i < N; i++)
			{
				m_data[i] = (F * (m_data[i - 1] ^ (m_data[i - 1] >> 30)) + i);
			}

			//random shit xDD
			m_data[22] ^= 0xBBA0BAAB;

			m_index = N;

			for (u32 i = 0; i < 1000 * 1000 * 9; i++)
			{
				Next();
			}
		}

		TReturnType Next()
		{
			if (m_index >= N)
			{
				Twist();
			}

			TReturnType x = m_data[m_index];
			m_index++;

			x ^= (x >> U);
			x ^= (x << S) & B;
			x ^= (x << T) & C;
			x ^= (x >> L);

			return x;
		}

		PD_FORCEINLINE TReturnType Next(TReturnType maxValue)
		{
			return Next() % maxValue;
		}

        template<TReturnType TMaxValue>
        PD_FORCEINLINE TReturnType NextStatic()
        {
            if constexpr((TMaxValue & (TMaxValue - 1)) == 0)
            {
                return Next() & (TMaxValue - 1);
            }
            return Next() % TMaxValue;
        }

		PD_FORCEINLINE TReturnType Next(TReturnType minValue, TReturnType maxValue)
		{
			return minValue + Next() % (maxValue - minValue);
		}

        template<TReturnType TMinValue, TReturnType TMaxValue>
        PD_FORCEINLINE TReturnType NextStatic()
        {
		    constexpr auto difference = TMaxValue - TMinValue;
		    if constexpr((difference & (difference - 1)) == 0)
            {
		        return TMinValue + Next() & (difference - 1);
            }
            return TMinValue + Next() % difference;
        }

		float NextFloat()
		{
			return static_cast<float>(Next()) / static_cast<float>(std::numeric_limits<TReturnType>::max());
		}

		double NextDouble()
		{
			return static_cast<double>(Next()) / static_cast<double>(std::numeric_limits<TReturnType>::max());
		}
	};

	typedef CMersenneTwisterBase<u32, 624, 397, 31, 0x9908B0DF, 1812433253, 11, 7, 0x9D2C5680, 15, 0xEFC60000, 18> CMT19937;
}

#endif //PD_ENGINE_CORE_MATH_MERSENNETWISTER_HPP
