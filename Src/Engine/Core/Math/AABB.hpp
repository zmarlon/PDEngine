//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 19.04.2020.
//
#ifndef PD_ENGINE_CORE_MATH_AABB_HPP
#define PD_ENGINE_CORE_MATH_AABB_HPP

#include "../Types.hpp"
#include "../Singleton.hpp"
#include "MathHelper.hpp"
#include <glm/glm.hpp>

#ifdef PD_AABB_OSTREAM_OPERATOR
#include <ostream>
#endif

namespace PD
{
	template<typename T>
	struct SAABB
	{
		using TVec3 = glm::tvec3<T>;
		TVec3 Min, Max;

		SAABB() : Min({}), Max({}) {}

		SAABB(const TVec3& min, const TVec3& max) : Min(min), Max(max) {}

		SAABB(const TVec3& position) : Min(position), Max(position + TVec3(static_cast<T>(1), static_cast<T>(1), static_cast<T>(1))) {}

		PD_FORCEINLINE TVec3 GetSize() const { return Max - Min; }

		PD_FORCEINLINE TVec3 GetCenter() const { return (Min + Max) / static_cast<T>(2); }

		PD_FORCEINLINE bool IsValid() const { return Min.x < Max.x && Min.y < Max.y && Min.z < Max.z; }

		PD_FORCEINLINE bool Contains(const TVec3& position) const
		{
			return position.x >= Min.x && position.x <= Max.x && position.y >= Min.y && position.y <= Max.y && position.z >= Max.z && position.z <= Min.z;
		}

		PD_FORCEINLINE bool Intersects(const SAABB& boundingBox) const
		{
			return Min.x <= boundingBox.Max.x && Max.x >= boundingBox.Min.x
				   && Min.y <= boundingBox.Max.y && Max.y >= boundingBox.Min.y
				   && Min.z <= boundingBox.Max.z && Max.z >= boundingBox.Min.z;
		}

		PD_FORCEINLINE bool Contains(const SAABB& boundingBox) const
		{
			return boundingBox.Min.x >= Min.x && boundingBox.Max.x <= Max.x
				   && boundingBox.Min.y >= Min.y && boundingBox.Max.y <= Max.y
				   && boundingBox.Min.z >= Min.z && boundingBox.Max.z <= Max.z;
		}

		PD_FORCEINLINE SAABB GetOverlappedBox(const SAABB& boundingBox) const
		{
			if(!Intersects(boundingBox)) return SAABB();

			return SAABB(
					TVec3(MathHelper::Max<T>(Min.x, boundingBox.Min.x), MathHelper::Max<T>(Min.y, boundingBox.Min.y), MathHelper::Max<T>(Min.z, boundingBox.Min.z)),
			TVec3(MathHelper::Min<T>(Max.x, boundingBox.Max.x), MathHelper::Min<T>(Max.y, boundingBox.Max.y), MathHelper::Min<T>(Max.z, boundingBox.Max.z)));
		}

#ifdef PD_AABB_OSTREAM_OPERATOR
		friend std::ostream& operator <<(std::ostream& stream, SAABB& boundingBox)
		{
			stream << "Min: {x:" << boundingBox.Min.x << ", y:" << boundingBox.Min.y << ", z:" << boundingBox.Min.z <<
				"} Max: {x:" << boundingBox.Max.x << ", y:" << boundingBox.Max.y << ", z:" << boundingBox.Max.z << '}';
			return stream;
		}

		friend std::wostream& operator <<(std::wostream& stream, SAABB& boundingBox)
		{
			stream << L"Min: {x:" << boundingBox.Min.x << L", y:" << boundingBox.Min.y << L", z:" << boundingBox.Min.z <<
				   L"} Max: {x:" << boundingBox.Max.x << L", y:" << boundingBox.Max.y << L", z:" << boundingBox.Max.z << L'}';
			return stream;
		}
#endif

	};

	typedef SAABB<u8> SAABBB;

	typedef SAABB<i32> SAABBI;

	typedef SAABB<u32> SAABBUI;

	typedef SAABB<u64> SAABBUL;

	typedef SAABB<float> SAABBF;

	typedef SAABB<double> SAABBD;

}

#endif //PD_ENGINE_CORE_MATH_AABB_HPP
