set(PD_ENGINE_CORE_MATH_DIRECTORY Src/Engine/Core/Math)

set(PD_ENGINE_CORE_MATH_SOURCE_FILES
        ${PD_ENGINE_CORE_MATH_DIRECTORY}/AABB.hpp
        ${PD_ENGINE_CORE_MATH_DIRECTORY}/Color3f.hpp
        ${PD_ENGINE_CORE_MATH_DIRECTORY}/Color4f.hpp
        ${PD_ENGINE_CORE_MATH_DIRECTORY}/GlmExtensions.hpp
        ${PD_ENGINE_CORE_MATH_DIRECTORY}/MatrixMath.hpp
        ${PD_ENGINE_CORE_MATH_DIRECTORY}/MathHelper.hpp
        ${PD_ENGINE_CORE_MATH_DIRECTORY}/MersenneTwister.hpp
        ${PD_ENGINE_CORE_MATH_DIRECTORY}/Multiples.hpp
        ${PD_ENGINE_CORE_MATH_DIRECTORY}/Point.hpp
        ${PD_ENGINE_CORE_MATH_DIRECTORY}/Rectangle.hpp)