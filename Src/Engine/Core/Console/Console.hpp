//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 13.04.2020.
//

#ifndef PD_ENGINE_CORE_CONSOLE_CONSOLE_HPP
#define PD_ENGINE_CORE_CONSOLE_CONSOLE_HPP

#pragma once

#include "ConsoleColor.hpp"

namespace PD::Console
{
	EConsoleColor GetForegroundColor();
	EConsoleColor GetBackgroundColor();
	void SetForegroundColor(EConsoleColor consoleColor);
	void SetBackgroundColor(EConsoleColor consoleColor);

	void ResetColors();
}

#endif //PD_ENGINE_CORE_CONSOLE_CONSOLE_HPP
