//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 04.05.2020.
//

#ifndef PDENGINE_GAMECONSOLE_HPP
#define PDENGINE_GAMECONSOLE_HPP

#include "../Disposable.hpp"
#include "../Types.hpp"
#include "../../Client/Renderer/Common/StaticFontMesh.hpp"
#include "../STL/String.hpp"
#include "../STL/Vector.hpp"
#include "../STL/UnorderedMap.hpp"
#include "../Event/Listeners.hpp"
#include "../../Client/Renderer/Common/SpriteFont.hpp"
#include <glm/glm.hpp>
#include <array>
#include <functional>

namespace PD
{
	struct SFrameEventArgs;
	class CSpriteBatch;
	class CVulkanDevice;
	class CGameConsole : public ITextInputListener, public IKeyPressListener, public IKeyDownListener, public IWindowResizeListener
	{
		PD_MAKE_CLASS_DISPOSABLE(CGameConsole)
		PD_MAKE_CLASS_SINGLETON(CGameConsole)
	public:
        struct SVariable
        {
            enum class EType : u32
            {
                I32 = 0,
                I64 = 1,
                U32 = 2,
                U64 = 3,
                Float = 4,
                Double = 5
            };

            EType Type;
            void* pData;
            void(*pCallback)() = nullptr;

            PD_FORCEINLINE void ToString(STL::CWString& string)
            {
                switch(Type)
                {
                    case EType::I32:
                        string = std::to_wstring(*(i32*)pData);
                        break;
                    case EType::I64:
                        string = std::to_wstring(*(i64*)pData);
                        break;
                    case EType::U32:
                        string = std::to_wstring(*(u32*)pData);
                        break;
                    case EType::U64:
                        string = std::to_wstring(*(u64*)pData);
                        break;
                    case EType::Float:
                        string = std::to_wstring(*(float*)pData);
                        break;
                    case EType::Double:
                        string = std::to_wstring(*(double*)pData);
                        break;
                }
            }
        };
	private:
		struct STextInfo
		{
			CStaticFontMesh Mesh;
			STL::CWString Text;

			STextInfo() = default;

			STextInfo(const CStaticFontMesh& mesh, const STL::CWString& text) : Mesh(mesh), Text(text) {}
		};

		CVulkanDevice* m_pDevice;
		CVulkanTexture2D* m_pWhitePixel;

		SColor4f m_colorConsoleBackground = SColor4f(0.0f, 0.0f, 0.0f, 0.5f);

		CSpriteFont m_font;

		bool m_opened = false;
		STL::CWString m_inputText;

		STL::CVector<STextInfo> m_consoleChunks;
		std::array<STL::CWString, 8> m_lastInputs;

		STL::CUnorderedMap<STL::CWString, std::function<void(CGameConsole&, const STL::CVector<STL::CWString>&)>> m_functions;
		STL::CUnorderedMap<STL::CWString, SVariable> m_variables;

		u32 m_selectedIndex = 0;
		u32 m_currentFontSize = 0;
		int m_consoleLines = 0;

		glm::vec2 m_glyphSize;
		glm::vec2 m_textOrigin;
		glm::vec2 m_otherTextOrigin;
		glm::vec2 m_beginPoint;
		glm::vec2 m_shadowOffset = glm::vec2(2.0f, 2.0f);

		u32 m_tabIndex = 0;
		u32 m_tabElements = 0;

		void OnTextInput(wchar_t c) override;
		void OnKeyDown(EKeys key) override;
		void OnKeyPress(EKeys key) override;
		void OnWindowResize(u32 newWidth, u32 newHeight) override;

		inline void ResizeMeshes();

		inline void Invoke();

		inline void SetIntVariable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, SVariable& variable);
		inline void SetInt64Variable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, SVariable& variable);
		inline void SetUintVariable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, SVariable& variable);
		inline void SetUint64Variable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, SVariable& variable);
		inline void SetFloatVariable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, SVariable& variable);
		inline void SetDoubleVariable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, SVariable& variable);

		void OnDisposing();

		PD_FORCEINLINE u32 GetFontSize(u32 windowHeight)
		{
			u32 fontSize = static_cast<u32>(24.0f / 900.0f * static_cast<float>(windowHeight));
			if(fontSize < 8) fontSize = 8;
			return fontSize;
		}
	public:
		//TODO: remove test variables
		int a , b;
		SVariable varA;
		SVariable varB;
		float c;
		SVariable varC;

		void Initialize(CVulkanDevice& device);

		void Toggle();

		PD_FORCEINLINE void GoUp()
		{
			if(m_selectedIndex + m_consoleLines < m_consoleChunks.size())
			{
				m_selectedIndex++;
			}
		}

		PD_FORCEINLINE void GoDown()
		{
			if(m_selectedIndex > 0)
			{
				m_selectedIndex--;
			}
		}

		PD_FORCEINLINE bool IsAtStartOfLine() const
		{
			if(m_consoleChunks.empty()) return true;
			const auto& text = m_consoleChunks.back().Text;
			return text[text.length() - 1] == L' ';
		}

		void Write(const wchar_t* pText, usize length, const SColor4f& color);

		PD_FORCEINLINE void Write(const wchar_t* pText, const SColor4f& color) { Write(pText, wcslen(pText), color); }

		PD_FORCEINLINE void Write(const STL::CWString& text, const SColor4f& color)
		{
			Write(text.c_str(), text.length(), color);
		}

		void WriteToNextLine(const wchar_t* pText, usize length, const SColor4f& color);

		PD_FORCEINLINE void WriteToNextLine(const wchar_t* pText, const SColor4f& color)
		{
			WriteToNextLine(pText, wcslen(pText), color);
		}

		PD_FORCEINLINE void WriteToNextLine(const STL::CWString& text, const SColor4f& color)
		{
			WriteToNextLine(text.c_str(), text.length(), color);
		}

		PD_FORCEINLINE void RegisterFunction(const STL::CWString& string, const std::function<void(CGameConsole&, const STL::CVector<STL::CWString>&)>& function)
		{
			m_functions.insert(std::make_pair(string, function));
		}

		PD_FORCEINLINE void RegisterVariable(const STL::CWString& string, SVariable variable)
		{
			m_variables.insert(std::make_pair(string, variable));
		}

		void Render(const SFrameEventArgs& frameEventArgs, CSpriteBatch& spriteBatch);
	};
}

#endif //PDENGINE_GAMECONSOLE_HPP
