//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 13.04.2020.
//

#include "Console.hpp"
#include "../Platform.hpp"
#include "../Types.hpp"

#ifdef PD_PLATFORM_WINDOWS
#include "../Assert.hpp"
#include <Windows.h>
#elif defined(PD_PLATFORM_LINUX)
#include <array>
#include <iostream>
#else
#error "Unsupported operating system"
#endif

namespace PD::Console
{
#ifdef PD_PLATFORM_WINDOWS
#define BACKGROUND_MASK 0xF0
#define FOREGROUND_MASK 0xF

	static volatile bool _S_DEFAULT_COLORS_INITIALIZED = false;
	static volatile u32 _S_DEFAULT_COLORS;

	EConsoleColor ColorAttributeToConsoleColor(u32 colorAttribute)
	{
		if((colorAttribute & BACKGROUND_MASK) != 0) colorAttribute >>= 4;
		return static_cast<EConsoleColor>(colorAttribute);
	}

	u32 ConsoleColorToColorAttribute(EConsoleColor consoleColor, bool isBackground)
	{
		PD_ASSERT(((int)consoleColor & ~0xF) == 0);
		return isBackground ? static_cast<u32>(consoleColor) << 4 : static_cast<u32>(consoleColor);
	}
#elif defined (PD_PLATFORM_LINUX)
	static std::array<const char*, 16> _S_CONSOLE_COLOR_TO_FOREGROUND_ANSI_CODE =
	{
		"\u001b[30m",
		"\u001b[34m",
		"\u001b[32m",
		"\u001b[36m",
		"\u001b[31m",
		"\u001b[35m",
		"\u001b[33m",
		"\u001b[37m",
		"\u001b[37m",
		"\u001b[34;1m",
		"\u001b[32;1m",
		"\u001b[36;1m",
		"\u001b[31;1m",
		"\u001b[35;1m",
		"\u001b[33;1m",
		"\u001b[37;1m"
	};

	static std::array<const char*, 16> _S_CONSOLE_COLOR_TO_BACKGROUND_ANSI_CODE =
	{
		"\u001b[40m",
		"\u001b[44m",
		"\u001b[42m",
		"\u001b[46m",
		"\u001b[41m",
		"\u001b[45m",
		"\u001b[43m",
		"\u001b[47m",
		"\u001b[47m",
		"\u001b[44;1m",
		"\u001b[42;1m",
		"\u001b[46;1m",
		"\u001b[41;1m",
		"\u001b[45;1m",
		"\u001b[43;1m",
		"\u001b[47;1m"
	};

	static EConsoleColor _S_FOREGROUND_COLOR = EConsoleColor::Gray;
	static EConsoleColor _S_BACKGROUND_COLOR = EConsoleColor::Black;
#else
#error "Unsupported operating system"
#endif

	EConsoleColor GetForegroundColor()
	{
#ifdef PD_PLATFORM_WINDOWS
		HANDLE hOutput = GetStdHandle(STD_OUTPUT_HANDLE);

		CONSOLE_SCREEN_BUFFER_INFO csbi;
		if(!hOutput || !GetConsoleScreenBufferInfo(hOutput, &csbi)) return EConsoleColor::Gray;

		return ColorAttributeToConsoleColor(csbi.wAttributes & FOREGROUND_MASK);
#elif defined(PD_PLATFORM_LINUX)
		return _S_FOREGROUND_COLOR;
#else
#error "Unsupported operating system"
#endif
	}

	EConsoleColor GetBackgroundColor()
	{
#ifdef PD_PLATFORM_WINDOWS
		HANDLE hOutput = GetStdHandle(STD_OUTPUT_HANDLE);

		CONSOLE_SCREEN_BUFFER_INFO csbi;
		if(!hOutput || !GetConsoleScreenBufferInfo(hOutput, &csbi)) return EConsoleColor::Black;

		return ColorAttributeToConsoleColor(csbi.wAttributes & BACKGROUND_MASK);
#elif defined(PD_PLATFORM_LINUX)
		return _S_BACKGROUND_COLOR;
#else
#error "Unsupported operating system"
#endif
	}

	void SetForegroundColor(EConsoleColor consoleColor)
	{
#ifdef PD_PLATFORM_WINDOWS
		auto color = ConsoleColorToColorAttribute(consoleColor, false);

		HANDLE hOutput = GetStdHandle(STD_OUTPUT_HANDLE);

		CONSOLE_SCREEN_BUFFER_INFO csbi;
		if(!hOutput || !GetConsoleScreenBufferInfo(hOutput, &csbi)) return;

		if(!_S_DEFAULT_COLORS_INITIALIZED)
		{
			_S_DEFAULT_COLORS = csbi.wAttributes;
			_S_DEFAULT_COLORS_INITIALIZED = true;
		}

		auto attributes = csbi.wAttributes;
		attributes &= ~FOREGROUND_MASK;
		attributes = attributes | color;
		SetConsoleTextAttribute(hOutput, attributes);
#elif defined(PD_PLATFORM_LINUX)
		_S_FOREGROUND_COLOR = consoleColor;
		std::cout << _S_CONSOLE_COLOR_TO_FOREGROUND_ANSI_CODE[static_cast<u32>(consoleColor)];
#else
#error "Unsupported operating system"
#endif
	}

	void SetBackgroundColor(EConsoleColor consoleColor)
	{
#ifdef PD_PLATFORM_WINDOWS
		auto color = ConsoleColorToColorAttribute(consoleColor, true);

		HANDLE hOutput = GetStdHandle(STD_OUTPUT_HANDLE);

		CONSOLE_SCREEN_BUFFER_INFO csbi;
		if(!hOutput || !GetConsoleScreenBufferInfo(hOutput, &csbi)) return;

		if(!_S_DEFAULT_COLORS_INITIALIZED)
		{
			_S_DEFAULT_COLORS = csbi.wAttributes;
			_S_DEFAULT_COLORS_INITIALIZED = true;
		}

		auto attributes = csbi.wAttributes;
		attributes &= ~BACKGROUND_MASK;
		attributes = attributes | color;
		SetConsoleTextAttribute(hOutput, attributes);
#elif defined(PD_PLATFORM_LINUX)
		_S_BACKGROUND_COLOR = consoleColor;
		std::cout << _S_CONSOLE_COLOR_TO_BACKGROUND_ANSI_CODE[static_cast<u32>(consoleColor)];
#else
#error "Unsupported operating system"
#endif
	}

	void ResetColors()
	{
#ifdef PD_PLATFORM_WINDOWS
		if(_S_DEFAULT_COLORS_INITIALIZED)
        {
            HANDLE hOutput = GetStdHandle(STD_OUTPUT_HANDLE);
            if(hOutput) SetConsoleTextAttribute(hOutput, _S_DEFAULT_COLORS);
        }
#elif defined(PD_PLATFORM_LINUX)
		std::cout << "\u001b[0m";
		_S_FOREGROUND_COLOR = EConsoleColor::Gray;
		_S_BACKGROUND_COLOR = EConsoleColor::Black;
#else
#error "Unsupported operating system"
#endif
	}
}