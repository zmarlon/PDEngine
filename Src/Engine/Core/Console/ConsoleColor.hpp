//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 13.04.2020.
//

#ifndef PD_ENGINE_CORE_CONSOLE_CONSOLECOLOR_HPP
#define PD_ENGINE_CORE_CONSOLE_CONSOLECOLOR_HPP

#pragma once

namespace PD
{
	enum class EConsoleColor
	{
		Black,
		DarkBlue,
		DarkGreen,
		DarkCyan,
		DarkRed,
		DarkMagenta,
		DarkYellow,
		Gray,
		DarkGray,
		Blue,
		Green,
		Cyan,
		Red,
		Magenta,
		Yellow,
		White
	};
}

#endif //PD_ENGINE_CORE_CONSOLE_CONSOLECOLOR_HPP
