
//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 04.05.2020.
//

#include "GameConsole.hpp"
#include "../Event/EventHandler.hpp"
#include "../../Client/Renderer/Renderer.hpp"
#include "../Pak/PakLocation.hpp"
#include "../Pak/PakManager.hpp"
#include "../Window/Window.hpp"
#include "../../Game.hpp"
#include "../../Client/Renderer/Common/SpriteBatch.hpp"
#include "../Window/MessageBox.hpp"

namespace PD
{
	PD_POSTDEFINE_SINGLETON(CGameConsole)

	void CGameConsole::OnTextInput(wchar_t c)
	{
		m_inputText += c;
	}

	void CGameConsole::OnKeyDown(EKeys key)
	{
		if(m_opened)
		{
			if(key == EKeys::Key_BACKSPACE)
			{
				m_inputText = m_inputText.substr(0, m_inputText.size() - 1);
			}
		}
	}

	void CGameConsole::OnKeyPress(EKeys key)
	{
		if(key == EKeys::Key_GRAVE)
		{
			Toggle();
		}

		if(m_opened)
		{
			if(key == EKeys::Key_TAB)
			{
				//TODO: implement for functions

				STL::CWString hasToStartWith;
				int foundIndex = -1;

				for(int ix = m_inputText.size(); ix > 0; ix--)
				{
					if(m_inputText[ix] == L' ')
					{
						foundIndex = ix;
						break;
					}
				}
				if(foundIndex == -1)
				{
					hasToStartWith = m_inputText;
				}
				else
				{
					hasToStartWith = m_inputText.substr(foundIndex + 1);
				}

				u32 possibilities = 0;
				const wchar_t* pLastPossibility;

				for(auto& pair : m_variables)
				{
					if(hasToStartWith.size() > 0 && pair.first.starts_with(hasToStartWith))
					{
						WriteToNextLine(L"    ", Color4f::White);
						Write(pair.first, Color4f::Green);
						Write(L" = ", Color4f::Cyan);
						STL::CWString variableValue;
						pair.second.ToString(variableValue);
						Write(variableValue, Color4f::Yellow);

						possibilities++;
						pLastPossibility = pair.first.c_str();
					}
				}

				if(possibilities == 1)
				{
					u32 offset = foundIndex == -1 ? 0 : foundIndex;
					m_inputText.erase(m_inputText.begin() +  offset, m_inputText.end());
					if(foundIndex != -1) m_inputText += L" ";
					m_inputText += pLastPossibility;
				}
			}
			else if(key == EKeys::Key_RETURN)
			{
				Invoke();
			}
			else if(key == EKeys::Key_UP)
			{
				if(m_tabIndex < m_tabElements)
				{
					m_inputText = m_lastInputs[m_tabIndex];
					m_tabIndex++;
				}
			}
			else if(key == EKeys::Key_DOWN)
			{
				if(m_tabIndex != 0)
				{
					m_tabIndex--;
					m_inputText = m_lastInputs[m_tabIndex];
				}
			}
		}
	}

	void CGameConsole::OnWindowResize(u32 newWidth, u32 newHeight)
	{
		m_font.Dispose(CRenderer::GetInstance()->GetDevice());
		m_currentFontSize = GetFontSize(newHeight);
		m_font.Load(*m_pDevice, SPakLocation(CPakManager::GetInstance()->GetUserInterface(), "VeraMono.ttf"), m_currentFontSize);

		m_glyphSize = m_font.MeasureString(L">");
		m_textOrigin = glm::vec2(0.0f, static_cast<float>(newHeight / 2.0f) - m_glyphSize.y);
		m_otherTextOrigin = glm::vec2(m_textOrigin.x + m_glyphSize.x, m_textOrigin.y);
		m_consoleLines = (int)(m_otherTextOrigin.y / m_glyphSize.y);
		float offset = (m_otherTextOrigin.y - m_consoleLines * m_glyphSize.y) / 2;
		m_beginPoint = glm::vec2(m_otherTextOrigin.x, m_otherTextOrigin.y - m_glyphSize.y);
		m_beginPoint.y -= offset;

		ResizeMeshes();
	}

	void CGameConsole::ResizeMeshes()
	{
		auto& texture = m_font.GetTexture();
		float textureWidth = static_cast<float>(texture.GetWidth()), textureHeight = static_cast<float>(texture.GetHeight());
		u32 fontSize = m_font.GetSize();

		float offset = 0.0f;
		bool firstGlyphOfLine = true;

		for(STextInfo& textInfo : m_consoleChunks)
		{
			const usize size = textInfo.Text.size();

			for(usize ix = 0; ix < size; ix++)
			{
				const wchar_t c = textInfo.Text[ix];

				const CFontAtlas::SGlyphInfo* pGlyphInfo = m_font.GetGlyph(c);
				if(!pGlyphInfo) PD_ASSERT(0);

				float xCharOffset = pGlyphInfo->OffsetX;
				if(firstGlyphOfLine)
				{
					if(pGlyphInfo->OffsetX < 0.0f)
						xCharOffset = 0.0f;
					else
						xCharOffset = pGlyphInfo->OffsetX;

					firstGlyphOfLine = false;
				}

				SSpriteInfo& spriteInfo = textInfo.Mesh.m_spriteInfos[ix];
				spriteInfo.TexCoordTL = glm::vec2(pGlyphInfo->TexCoordX / textureWidth,
												  pGlyphInfo->TexCoordY / textureHeight);
				spriteInfo.TexCoordBR = glm::vec2((pGlyphInfo->TexCoordX + pGlyphInfo->Width) / textureWidth,
												  (pGlyphInfo->TexCoordY + pGlyphInfo->Height) / textureHeight);
				spriteInfo.Destination = SRectangleF(offset + xCharOffset,
													 (fontSize - pGlyphInfo->OffsetY),
													 pGlyphInfo->Width, pGlyphInfo->Height);
				spriteInfo.pImageView = &texture.GetImageView();

				offset += pGlyphInfo->XAdvance;
			}
			offset = 0.0f;
		}
	}

	void CGameConsole::Invoke()
	{
	    if(m_inputText.empty()) return;

	    m_tabIndex = 0;

	    if(m_tabElements < 8) m_tabElements++;
	    auto lastInputsCopy = m_lastInputs;

	    for(u32 ix = 0; ix < m_tabElements; ix++)
		{
	    	if(ix != 7) m_lastInputs[ix + 1] = lastInputsCopy[ix]; //TODO:
		}
		m_lastInputs[0] = m_inputText;

		STL::CVector<STL::CWString> splittedStrings;
		StringHelper::Split(m_inputText, L" ", splittedStrings);

		auto& submittedCommand = splittedStrings[0];

		if(m_functions.find(submittedCommand) == m_functions.end())
		{
			if(m_variables.find(submittedCommand) != m_variables.end())
			{
				auto& variable = m_variables[submittedCommand];
				if(splittedStrings.size() == 1 || splittedStrings.size() == 2 && splittedStrings[1] == L"=")
				{
					WriteToNextLine(submittedCommand, Color4f::Green);
					Write(L" = ", Color4f::Cyan);
					STL::CWString valueString;
					variable.ToString(valueString);
					Write(valueString, Color4f::Yellow);
				}
				else
				{
					u32 valueIndex = 1;
					if(splittedStrings[1] == L"=") valueIndex++;

					switch(variable.Type)
					{
						case SVariable::EType::I32:
							SetIntVariable(splittedStrings, valueIndex, variable);
							break;
						case SVariable::EType::I64:
							SetInt64Variable(splittedStrings, valueIndex, variable);
							break;
						case SVariable::EType::U32:
							SetUintVariable(splittedStrings, valueIndex, variable);
							break;
						case SVariable::EType::U64:
							SetUint64Variable(splittedStrings, valueIndex, variable);
							break;
						case SVariable::EType::Float:
							SetFloatVariable(splittedStrings, valueIndex, variable);
							break;
						case SVariable::EType::Double:
							SetDoubleVariable(splittedStrings, valueIndex, variable);
							break;
					}
				}
			}
			else
			{
				WriteToNextLine(L"[Error] Unknown command", Color4f::Red);
			}
		}
		else
		{
			//TODO: Handle commands
		}

		m_inputText.clear();
	}

	void CGameConsole::SetIntVariable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, CGameConsole::SVariable& variable)
	{
		i32 value;
		STL::CWString& submittedCommand = splittedStrings[0];
		STL::CWString& valueString = splittedStrings[valueIndex];
		try
		{
			value = std::stoi(valueString.c_str());
		}
		catch(std::invalid_argument& argument)
		{
			if(m_variables.find(valueString) != m_variables.end())
			{
				SVariable& variableRight = m_variables[valueString];
				switch(variableRight.Type)
				{
					case SVariable::EType::I32: *(i32*)variable.pData = (i32)*(i32*)variableRight.pData; break;
					case SVariable::EType::I64: *(i32*)variable.pData = (i32)*(i64*)variableRight.pData; break;
					case SVariable::EType::U32: *(i32*)variable.pData = (i32)*(u32*)variableRight.pData; break;
					case SVariable::EType::U64: *(i32*)variable.pData = (i32)*(u64*)variableRight.pData; break;
					case SVariable::EType::Float: *(i32*)variable.pData = (i32)*(float*)variableRight.pData; break;
					case SVariable::EType::Double: *(i32*)variable.pData = (i32)*(double*)variableRight.pData; break;
				}

				WriteToNextLine(submittedCommand, Color4f::Green);
				Write(L" = ", Color4f::Cyan);
				STL::CWString variableString;
				variable.ToString(variableString);
				Write(variableString, Color4f::Yellow);

				if(variable.pCallback) variable.pCallback();

				return;
			}

			WriteToNextLine(L"The variable is from type i32. Failed to convert", Color4f::Red);
			return;
		}
		catch(std::out_of_range& outOfRange)
		{
			WriteToNextLine(L"The variable is from type i32. This value is out of range", Color4f::Red);
			return;
		}
		*(i32*)variable.pData = value;

		WriteToNextLine(submittedCommand, Color4f::Green);
		Write(L" = ", Color4f::Cyan);
		Write(valueString, Color4f::Yellow);

		if(variable.pCallback) variable.pCallback();
	}

	void CGameConsole::SetInt64Variable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, CGameConsole::SVariable& variable)
	{
		i64 value;
		STL::CWString& submittedCommand = splittedStrings[0];
		STL::CWString& valueString = splittedStrings[valueIndex];
		try
		{
			value = std::stoll(valueString.c_str());
		}
		catch(std::invalid_argument& argument)
		{
			if(m_variables.find(valueString) != m_variables.end())
			{
				SVariable& variableRight = m_variables[valueString];
				switch(variableRight.Type)
				{
					case SVariable::EType::I32: *(i64*)variable.pData = (i64)*(i32*)variableRight.pData; break;
					case SVariable::EType::I64: *(i64*)variable.pData = (i64)*(i64*)variableRight.pData; break;
					case SVariable::EType::U32: *(i64*)variable.pData = (i64)*(u32*)variableRight.pData; break;
					case SVariable::EType::U64: *(i64*)variable.pData = (i64)*(u64*)variableRight.pData; break;
					case SVariable::EType::Float: *(i64*)variable.pData = (i64)*(float*)variableRight.pData; break;
					case SVariable::EType::Double: *(i64*)variable.pData = (i64)*(double*)variableRight.pData; break;
				}

				WriteToNextLine(submittedCommand, Color4f::Green);
				Write(L" = ", Color4f::Cyan);
				STL::CWString variableString;
				variable.ToString(variableString);
				Write(variableString, Color4f::Yellow);

				if(variable.pCallback) variable.pCallback();

				return;
			}

			WriteToNextLine(L"The variable is from type i64. Failed to convert", Color4f::Red);
			return;
		}
		catch(std::out_of_range& outOfRange)
		{
			WriteToNextLine(L"The variable is from type i64. This value is out of range", Color4f::Red);
			return;
		}
		*(i64*)variable.pData = value;

		WriteToNextLine(submittedCommand, Color4f::Green);
		Write(L" = ", Color4f::Cyan);
		Write(valueString, Color4f::Yellow);

		if(variable.pCallback) variable.pCallback();
	}

	void CGameConsole::SetUintVariable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, CGameConsole::SVariable& variable)
	{
		u32 value;
		STL::CWString& submittedCommand = splittedStrings[0];
		STL::CWString& valueString = splittedStrings[valueIndex];
		try
		{
			value = std::stoul(valueString.c_str());
		}
		catch(std::invalid_argument& argument)
		{
			if(m_variables.find(valueString) != m_variables.end())
			{
				SVariable& variableRight = m_variables[valueString];
				switch(variableRight.Type)
				{
					case SVariable::EType::I32: *(u32*)variable.pData = (u32)*(i32*)variableRight.pData; break;
					case SVariable::EType::I64: *(u32*)variable.pData = (u32)*(i64*)variableRight.pData; break;
					case SVariable::EType::U32: *(u32*)variable.pData = (u32)*(u32*)variableRight.pData; break;
					case SVariable::EType::U64: *(u32*)variable.pData = (u32)*(u64*)variableRight.pData; break;
					case SVariable::EType::Float: *(u32*)variable.pData = (u32)*(float*)variableRight.pData; break;
					case SVariable::EType::Double: *(u32*)variable.pData = (u32)*(double*)variableRight.pData; break;
				}

				WriteToNextLine(submittedCommand, Color4f::Green);
				Write(L" = ", Color4f::Cyan);
				STL::CWString variableString;
				variable.ToString(variableString);
				Write(variableString, Color4f::Yellow);

				if(variable.pCallback) variable.pCallback();

				return;
			}

			WriteToNextLine(L"The variable is from type u32. Failed to convert", Color4f::Red);
			return;
		}
		catch(std::out_of_range& outOfRange)
		{
			WriteToNextLine(L"The variable is from type u32. This value is out of range", Color4f::Red);
			return;
		}
		*(u32*)variable.pData = value;

		WriteToNextLine(submittedCommand, Color4f::Green);
		Write(L" = ", Color4f::Cyan);
		Write(valueString, Color4f::Yellow);

		if(variable.pCallback) variable.pCallback();
	}

	void CGameConsole::SetUint64Variable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, CGameConsole::SVariable& variable)
	{
		u64 value;
		STL::CWString& submittedCommand = splittedStrings[0];
		STL::CWString& valueString = splittedStrings[valueIndex];
		try
		{
			value = std::stoull(valueString.c_str());
		}
		catch(std::invalid_argument& argument)
		{
			if(m_variables.find(valueString) != m_variables.end())
			{
				SVariable& variableRight = m_variables[valueString];
				switch(variableRight.Type)
				{
					case SVariable::EType::I32: *(u64*)variable.pData = (u64)*(i32*)variableRight.pData; break;
					case SVariable::EType::I64: *(u64*)variable.pData = (u64)*(i64*)variableRight.pData; break;
					case SVariable::EType::U32: *(u64*)variable.pData = (u64)*(u32*)variableRight.pData; break;
					case SVariable::EType::U64: *(u64*)variable.pData = (u64)*(u64*)variableRight.pData; break;
					case SVariable::EType::Float: *(u64*)variable.pData = (u64)*(float*)variableRight.pData; break;
					case SVariable::EType::Double: *(u64*)variable.pData = (u64)*(double*)variableRight.pData; break;
				}

				WriteToNextLine(submittedCommand, Color4f::Green);
				Write(L" = ", Color4f::Cyan);
				STL::CWString variableString;
				variable.ToString(variableString);
				Write(variableString, Color4f::Yellow);

				if(variable.pCallback) variable.pCallback();

				return;
			}

			WriteToNextLine(L"The variable is from type u64. Failed to convert", Color4f::Red);
			return;
		}
		catch(std::out_of_range& outOfRange)
		{
			WriteToNextLine(L"The variable is from type u64. This value is out of range", Color4f::Red);
			return;
		}
		*(u64*)variable.pData = value;

		WriteToNextLine(submittedCommand, Color4f::Green);
		Write(L" = ", Color4f::Cyan);
		Write(valueString, Color4f::Yellow);

		if(variable.pCallback) variable.pCallback();
	}

	void CGameConsole::SetFloatVariable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, CGameConsole::SVariable& variable)
	{
		float value;
		STL::CWString& submittedCommand = splittedStrings[0];
		STL::CWString& valueString = splittedStrings[valueIndex];
		try
		{
			value = std::stof(valueString.c_str());
		}
		catch(std::invalid_argument& argument)
		{
			if(m_variables.find(valueString) != m_variables.end())
			{
				SVariable& variableRight = m_variables[valueString];
				switch(variableRight.Type)
				{
					case SVariable::EType::I32: *(float*)variable.pData = (float)*(i32*)variableRight.pData; break;
					case SVariable::EType::I64: *(float*)variable.pData = (float)*(i64*)variableRight.pData; break;
					case SVariable::EType::U32: *(float*)variable.pData = (float)*(u32*)variableRight.pData; break;
					case SVariable::EType::U64: *(float*)variable.pData = (float)*(u64*)variableRight.pData; break;
					case SVariable::EType::Float: *(float*)variable.pData = (float)*(float*)variableRight.pData; break;
					case SVariable::EType::Double: *(float*)variable.pData = (float)*(double*)variableRight.pData; break;
				}

				WriteToNextLine(submittedCommand, Color4f::Green);
				Write(L" = ", Color4f::Cyan);
				STL::CWString variableString;
				variable.ToString(variableString);
				Write(variableString, Color4f::Yellow);

				if(variable.pCallback) variable.pCallback();

				return;
			}

			WriteToNextLine(L"The variable is from type float. Failed to convert", Color4f::Red);
			return;
		}
		catch(std::out_of_range& outOfRange)
		{
			WriteToNextLine(L"The variable is from type float. This value is out of range", Color4f::Red);
			return;
		}
		*(float*)variable.pData = value;

		WriteToNextLine(submittedCommand, Color4f::Green);
		Write(L" = ", Color4f::Cyan);
		Write(valueString, Color4f::Yellow);

		if(variable.pCallback) variable.pCallback();
	}

	void CGameConsole::SetDoubleVariable(STL::CVector<STL::CWString>& splittedStrings, u32 valueIndex, CGameConsole::SVariable& variable)
	{
		double value;
		STL::CWString& submittedCommand = splittedStrings[0];
		STL::CWString& valueString = splittedStrings[valueIndex];
		try
		{
			value = std::stod(valueString.c_str());
		}
		catch(std::invalid_argument& argument)
		{
			if(m_variables.find(valueString) != m_variables.end())
			{
				SVariable& variableRight = m_variables[valueString];
				switch(variableRight.Type)
				{
					case SVariable::EType::I32: *(double*)variable.pData = (double)*(i32*)variableRight.pData; break;
					case SVariable::EType::I64: *(double*)variable.pData = (double)*(i64*)variableRight.pData; break;
					case SVariable::EType::U32: *(double*)variable.pData = (double)*(u32*)variableRight.pData; break;
					case SVariable::EType::U64: *(double*)variable.pData = (double)*(u64*)variableRight.pData; break;
					case SVariable::EType::Float: *(double*)variable.pData = (double)*(float*)variableRight.pData; break;
					case SVariable::EType::Double: *(double*)variable.pData = (double)*(double*)variableRight.pData; break;
				}

				WriteToNextLine(submittedCommand, Color4f::Green);
				Write(L" = ", Color4f::Cyan);
				STL::CWString variableString;
				variable.ToString(variableString);
				Write(variableString, Color4f::Yellow);

				if(variable.pCallback) variable.pCallback();

				return;
			}

			WriteToNextLine(L"The variable is from type double. Failed to convert", Color4f::Red);
			return;
		}
		catch(std::out_of_range& outOfRange)
		{
			WriteToNextLine(L"The variable is from type double. This value is out of range", Color4f::Red);
			return;
		}
		*(double*)variable.pData = value;

		WriteToNextLine(submittedCommand, Color4f::Green);
		Write(L" = ", Color4f::Cyan);
		Write(valueString, Color4f::Yellow);

		if(variable.pCallback) variable.pCallback();
	}

	void CGameConsole::OnDisposing()
	{
		DetachSingleton();

		CEventHandler* pEventHandler = CEventHandler::GetInstance();
		pEventHandler->RemoveKeyPressListener(this);
		pEventHandler->RemoveKeyDownListener(this);
		pEventHandler->RemoveTextInputListener(this);

		m_font.Dispose(CRenderer::GetInstance()->GetDevice());
	}

	void CGameConsole::Initialize(CVulkanDevice& device)
	{
		m_pDevice = &device;

		m_pWhitePixel = &CRenderer::GetInstance()->GetWhitePixel();

		CWindow* pWindow = CWindow::GetInstance();

		m_currentFontSize = GetFontSize(pWindow->GetHeight());
		m_font.Load(device, SPakLocation(CPakManager::GetInstance()->GetUserInterface(), "VeraMono.ttf"), m_currentFontSize);

		m_glyphSize = m_font.MeasureString(L">");
		m_textOrigin = glm::vec2(0.0f, static_cast<float>(pWindow->GetHeight() / 2.0f) - m_glyphSize.y);
		m_otherTextOrigin = glm::vec2(m_textOrigin.x + m_glyphSize.x, m_textOrigin.y);
		m_consoleLines = (int)(m_otherTextOrigin.y / m_glyphSize.y);
		float offset = (m_otherTextOrigin.y - m_consoleLines * m_glyphSize.y) / 2;
		m_beginPoint = glm::vec2(m_otherTextOrigin.x, m_otherTextOrigin.y - m_glyphSize.y);
		m_beginPoint.y -= offset;

		CEventHandler* pEventHandler = CEventHandler::GetInstance();
		pEventHandler->AddTextInputListener(this);
		pEventHandler->AddKeyDownListener(this);
		pEventHandler->AddKeyPressListener(this);
		pEventHandler->AddWindowResizeListener(this);

		varA.Type = SVariable::EType::I32;
		varA.pData = &a;
		varB.Type = SVariable::EType::I32;
		varB.pData = &b;
		a = 90;
		b = 8;
		varC.Type = SVariable::EType::Float;
		varC.pData = &c;
		varC.pCallback = []() {CGameConsole::GetInstance()->WriteToNextLine(L"Haoao ist ein Schinotz", Color4f::Cyan); };
		c = 30.01f;

		RegisterVariable(L"fov", varA);
		RegisterVariable(L"maxPlayers", varB);
		RegisterVariable(L"fladi", varC);

		AttachSingleton();

		PD_END_INITIALIZE;
	}

	void CGameConsole::Toggle()
	{
		m_opened = !m_opened;
		CGame* pGame = CGame::GetInstance();
		pGame->GetGameState().IsPaused = !pGame->GetGameState().IsPaused;

		if(m_opened) CEventHandler::GetInstance()->BeginTextInput();
		else CEventHandler::GetInstance()->EndTextInput();
	}

	void CGameConsole::Write(const wchar_t* pText, usize length, const SColor4f& color)
	{
		auto& texture = m_font.GetTexture();
		float textureWidth = static_cast<float>(texture.GetWidth()), textureHeight = static_cast<float>(texture.GetHeight());
		u32 fontSize = m_font.GetSize();

		if(m_consoleChunks.empty())
		{
			m_consoleChunks.emplace_back();
		}

		CStaticFontMesh* pCurrentMesh = &(m_consoleChunks.back().Mesh);
		bool firstGlyphOfLine = pCurrentMesh->m_size.x == 0.0f;

		float offset = pCurrentMesh->m_size.x;

		for(u32 ix = 0; ix < length; ix++)
		{
			const wchar_t c = pText[ix];
			if(c == L'\r')
			{
				continue;
			}
			if(c == L'\n')
			{
				m_consoleChunks.emplace_back();
				m_consoleChunks.back().Text.reserve(64);

				pCurrentMesh->m_size.x = offset;
				pCurrentMesh->m_size.y = static_cast<float>(m_font.GetLineSpacing());

				pCurrentMesh = &(m_consoleChunks.back().Mesh);

				offset = 0.0f;
				firstGlyphOfLine = true;
			}

			const CFontAtlas::SGlyphInfo* pGlyphInfo = m_font.GetGlyph(c);
			if(!pGlyphInfo) continue;

			float xCharOffset = pGlyphInfo->OffsetX;
			if(firstGlyphOfLine)
			{
				if(pGlyphInfo->OffsetX < 0.0f)
					xCharOffset = 0.0f;
				else
					xCharOffset = pGlyphInfo->OffsetX;

				firstGlyphOfLine = false;
			}

			SSpriteInfo spriteInfo;
			spriteInfo.TexCoordTL = glm::vec2(pGlyphInfo->TexCoordX / textureWidth,
											  pGlyphInfo->TexCoordY / textureHeight);
			spriteInfo.TexCoordBR = glm::vec2((pGlyphInfo->TexCoordX + pGlyphInfo->Width) / textureWidth,
											  (pGlyphInfo->TexCoordY + pGlyphInfo->Height) / textureHeight);
			spriteInfo.Destination = SRectangleF(offset + xCharOffset,
												 (fontSize - pGlyphInfo->OffsetY),
												 pGlyphInfo->Width, pGlyphInfo->Height);
			spriteInfo.Color = color;
			spriteInfo.Origin = glm::vec2();
			spriteInfo.Rotation = 0.0f;
			spriteInfo.Depth = 0.0f;
			spriteInfo.pImageView = &texture.GetImageView();

			pCurrentMesh->m_spriteInfos.push_back(spriteInfo);
			m_consoleChunks.back().Text += c;

			offset += pGlyphInfo->XAdvance;
		}

		pCurrentMesh->m_size.x = offset;
		pCurrentMesh->m_size.y = m_font.GetLineSpacing();
	}

	void CGameConsole::WriteToNextLine(const wchar_t* pText, usize length, const SColor4f& color)
	{
		if(IsAtStartOfLine())
		{
			Write(pText, length, color);
		}
		else
		{
			STL::CWString text;
			text.reserve(static_cast<usize>(length + 1));
			text += L'\n';
			text += pText;
			Write(text, color);
		}
	}

	void CGameConsole::Render(const SFrameEventArgs& frameEventArgs, CSpriteBatch& spriteBatch)
	{
		if(m_opened)
		{
			CWindow* pWindow = CWindow::GetInstance();
			float windowWidth = static_cast<float>(pWindow->GetWidth());
			float windowHeight = static_cast<float>(pWindow->GetHeight());
			float windowHeightHalf = windowHeight / 2.0f;

			spriteBatch.Draw(*m_pWhitePixel, SRectangleF(0.0f, 0.0f, windowWidth, windowHeightHalf + m_font.GetSize() / 4.0f), m_colorConsoleBackground);

			spriteBatch.DrawString(m_font, L">", m_textOrigin + m_shadowOffset, Color4f::Black);
			spriteBatch.DrawString(m_font, m_inputText.c_str(), m_otherTextOrigin + m_shadowOffset, Color4f::Black);

			spriteBatch.DrawString(m_font, L">", m_textOrigin, Color4f::White);
			spriteBatch.DrawString(m_font, m_inputText.c_str(), m_otherTextOrigin, Color4f::White);

			for(int ix = 0; ix < m_consoleLines; ix++)
			{
				if(ix < m_consoleChunks.size())
				{
					CStaticFontMesh& chunkMesh = m_consoleChunks[m_consoleChunks.size() - ix - 1 - m_selectedIndex].Mesh;
					glm::vec2 pos = glm::vec2(m_beginPoint.x, m_beginPoint.y - ix * m_glyphSize.y);
					spriteBatch.DrawStringWithColorOverride(chunkMesh,glm::vec2(pos + m_shadowOffset), Color4f::Black);
					spriteBatch.DrawString(chunkMesh, pos);

				}
			}
		}
	}
}