//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//
#ifndef PD_ENGINE_CORE_CONFIG_CONFIGCLIENT_HPP
#define PD_ENGINE_CORE_CONFIG_CONFIGCLIENT_HPP

#pragma once

#include "../Types.hpp"

namespace PD
{
    struct SConfigClient
    {
        u32 WindowWidth;
        u32 WindowHeight;
        u32 FrameLimit;
        u32 DebugLevel;
        bool Fullscreen;
        bool VerticalSync;
    };
}

#endif //PD_ENGINE_CORE_CONFIG_CONFIGCLIENT_HPP
