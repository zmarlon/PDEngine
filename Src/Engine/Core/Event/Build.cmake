set(PD_ENGINE_CORE_EVENT_DIRECTORY Src/Engine/Core/Event)

set(PD_ENGINE_CORE_EVENT_SOURCE_FILES
        ${PD_ENGINE_CORE_EVENT_DIRECTORY}/EventHandler.cpp
        ${PD_ENGINE_CORE_EVENT_DIRECTORY}/EventHandler.hpp
        ${PD_ENGINE_CORE_EVENT_DIRECTORY}/Listeners.hpp)
