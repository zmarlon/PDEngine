//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#include "EventHandler.hpp"
#include "Listeners.hpp"
#include "../Platform.hpp"
#include <algorithm>
#include <SDL2/SDL.h>
#include <codecvt>
#include <locale>

#include "../Managed/CommonLanguageRuntime.hpp"

namespace PD
{
    PD_POSTDEFINE_SINGLETON(CEventHandler)

    void CEventHandler::OnDisposing()
    {
        DetachSingleton();

        m_windowMaximizeListeners.clear();
        m_windowMinimizeListeners.clear();
        m_windowMoveListeners.clear();
        m_windowResizeListeners.clear();
    }

    void CEventHandler::InvokeWindowResizeListeners(u32 newWidth, u32 newHeight)
    {
        for(auto* pListener : m_windowResizeListeners) pListener->OnWindowResize(newWidth, newHeight);
        m_pfn_ManagedEventHandler_NInvokeWindowResizeListeners(newWidth, newHeight);
    }

    void CEventHandler::InvokeWindowMoveListeners(i32 newX, i32 newY)
    {
        for(auto* pListener : m_windowMoveListeners) pListener->OnWindowMove(newX, newY);
        m_pfn_ManagedEventHandler_NInvokeWindowMoveListeners(newX, newY);
    }

    void CEventHandler::InvokeWindowMinimizeListeners()
    {
        for(auto* pListener : m_windowMinimizeListeners) pListener->OnWindowMinimize();
        m_pfn_ManagedEventHandler_NInvokeWindowMinimizeListeners();
    }

    void CEventHandler::InvokeWindowMaximizeListeners()
    {
        for(auto* pListener : m_windowMaximizeListeners) pListener->OnWindowMaximize();
        m_pfn_ManagedEventHandler_NInvokeWindowMaximizeListeners();
    }

    void CEventHandler::InvokeGamePauseListeners()
    {
        for(auto* pListener : m_gamePauseListeners) pListener->OnGamePause();
        m_pfn_ManagedEventHandler_NInvokeGamePauseListeners();
    }

    void CEventHandler::InvokeGameResumeListeners()
    {
        for(auto* pListener : m_gameResumeListeners) pListener->OnGameResume();
        m_pfn_ManagedEventHandler_NInvokeGameResumeListeners();
    }

	void CEventHandler::InvokeTextInput(const SDL_Event& event)
	{
		std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> utf16conv;
		std::u16string a = utf16conv.from_bytes(event.text.text);

		wchar_t c = a.c_str()[0];

        for(auto* pListener : m_textInputListeners)
        {
            pListener->OnTextInput(c);
        }

        m_pfn_ManagedEventHandler_NInvokeTextInputListeners(c);
	}

	void CEventHandler::InvokeKeyPress(const SDL_Event& event)
	{
		auto key = static_cast<EKeys>(event.key.keysym.scancode);
		if(m_pressedKeys.find(key) == m_pressedKeys.end())
		{
			for(auto* pListener : m_keyPressListeners)
			{
				pListener->OnKeyPress(key);
			}
			m_pfn_ManagedEventHandler_NInvokeKeyPressListeners(key);
			m_pressedKeys.insert(key);
		}
	}

	void CEventHandler::InvokeKeyRelease(const SDL_Event& event)
	{
        auto key = static_cast<EKeys>(event.key.keysym.scancode);
		if(m_pressedKeys.find(key) != m_pressedKeys.end())
		{
			for(auto* pListener : m_keyReleaseListeners)
			{
				pListener->OnKeyRelease(key);
			}
			m_pfn_ManagedEventHandler_NInvokeKeyReleaseListeners(key);
			m_pressedKeys.erase(key);
		}
	}

	void CEventHandler::InvokeKeyDown(const SDL_Event& event)
	{
        auto key = static_cast<EKeys>(event.key.keysym.scancode);

		for(auto* pListener : m_keyDownListeners)
		{
			pListener->OnKeyDown(key);
		}
		m_pfn_ManagedEventHandler_NInvokeKeyDownListeners(key);
	}

	void CEventHandler::InvokeKeyUp(const SDL_Event& event)
	{
        auto key = static_cast<EKeys>(event.key.keysym.scancode);

		for(auto* pListener : m_keyUpListeners)
		{
			pListener->OnKeyUp(key);
		}
		m_pfn_ManagedEventHandler_NInvokeKeyUpListeners(key);
	}

	void CEventHandler::Initialize()
    {
        AttachSingleton();

        auto* pRuntime = CCommonLanguageRuntime::GetInstance();
        m_pfn_ManagedEventHandler_NInvokeWindowResizeListeners = pRuntime->GetDelegate<void(u32, u32)>("PDEngine.Managed.Core.Event.ManagedEventHandler", "NInvokeWindowResizeListeners");
        m_pfn_ManagedEventHandler_NInvokeWindowMoveListeners = pRuntime->GetDelegate<void(i32, i32)>("PDEngine.Managed.Core.Event.ManagedEventHandler", "NInvokeWindowMoveListeners");
        m_pfn_ManagedEventHandler_NInvokeWindowMinimizeListeners = pRuntime->GetDelegate<void()>("PDEngine.Managed.Core.Event.ManagedEventHandler", "NInvokeWindowMinimizeListeners");
        m_pfn_ManagedEventHandler_NInvokeWindowMaximizeListeners = pRuntime->GetDelegate<void()>("PDEngine.Managed.Core.Event.ManagedEventHandler", "NInvokeWindowMaximizeListeners");
        m_pfn_ManagedEventHandler_NInvokeGamePauseListeners = pRuntime->GetDelegate<void()>("PDEngine.Managed.Core.Event.ManagedEventHandler", "NInvokeGamePauseListeners");
        m_pfn_ManagedEventHandler_NInvokeGameResumeListeners = pRuntime->GetDelegate<void()>("PDEngine.Managed.Core.Event.ManagedEventHandler", "NInvokeGameResumeListeners");
        m_pfn_ManagedEventHandler_NInvokeTextInputListeners = pRuntime->GetDelegate<void(wchar_t)>("PDEngine.Managed.Core.Event.ManagedEventHandler", "NInvokeTextInputListeners");
        m_pfn_ManagedEventHandler_NInvokeKeyDownListeners = pRuntime->GetDelegate<void(EKeys)>("PDEngine.Managed.Core.Event.ManagedEventHandler", "NInvokeKeyDownListeners");
        m_pfn_ManagedEventHandler_NInvokeKeyUpListeners = pRuntime->GetDelegate<void(EKeys)>("PDEngine.Managed.Core.Event.ManagedEventHandler", "NInvokeKeyUpListeners");
        m_pfn_ManagedEventHandler_NInvokeKeyPressListeners = pRuntime->GetDelegate<void(EKeys)>("PDEngine.Managed.Core.Event.ManagedEventHandler", "NInvokeKeyPressListeners");
        m_pfn_ManagedEventHandler_NInvokeKeyReleaseListeners = pRuntime->GetDelegate<void(EKeys)>("PDEngine.Managed.Core.Event.ManagedEventHandler", "NInvokeKeyReleaseListeners");

        PD_END_INITIALIZE;
    }

	void CEventHandler::BeginTextInput()
	{
		SDL_StartTextInput();
	}

	void CEventHandler::EndTextInput()
	{
		SDL_StopTextInput();
	}

	//Add
    void CEventHandler::AddWindowResizeListener(IWindowResizeListener* pListener)
    {
        PD_ASSERT(pListener);
        m_windowResizeListeners.push_back(pListener);
    }

    void CEventHandler::AddWindowMoveListener(IWindowMoveListener* pListener)
    {
        PD_ASSERT(pListener);
        m_windowMoveListeners.push_back(pListener);
    }

    void CEventHandler::AddWindowMinimizeListener(IWindowMinimizeListener* pListener)
    {
        PD_ASSERT(pListener);
        m_windowMinimizeListeners.push_back(pListener);
    }

    void CEventHandler::AddWindowMaximizeListener(IWindowMaximizeListener* pListener)
    {
        PD_ASSERT(pListener);
        m_windowMaximizeListeners.push_back(pListener);
    }


    void CEventHandler::AddGamePauseListener(IGamePauseListener* pListener)
    {
        PD_ASSERT(pListener);
        m_gamePauseListeners.push_back(pListener);
    }

    void CEventHandler::AddGameResumeListener(IGameResumeListener* pListener)
    {
        PD_ASSERT(pListener);
        m_gameResumeListeners.push_back(pListener);
    }

    void CEventHandler::AddTextInputListener(ITextInputListener* pListener)
    {
        m_textInputListeners.push_back(pListener);
    }

    void CEventHandler::AddKeyDownListener(IKeyDownListener* pListener)
    {
        m_keyDownListeners.push_back(pListener);
    }

    void CEventHandler::AddKeyUpListener(IKeyUpListener* pListener)
    {
        m_keyUpListeners.push_back(pListener);
    }

    void CEventHandler::AddKeyPressListener(IKeyPressListener* pListener)
    {
        m_keyPressListeners.push_back(pListener);
    }

    void CEventHandler::AddKeyReleaseListener(IKeyReleaseListener* pListener)
    {
        m_keyReleaseListeners.push_back(pListener);
    }

    //Remove
    void CEventHandler::RemoveWindowResizeListener(IWindowResizeListener* pListener)
    {
        PD_ASSERT(pListener);
        std::remove(m_windowResizeListeners.begin(), m_windowResizeListeners.end(), pListener);
    }

    void CEventHandler::RemoveWindowMoveListener(IWindowMoveListener* pListener)
    {
        PD_ASSERT(pListener);
        std::remove(m_windowMoveListeners.begin(), m_windowMoveListeners.end(), pListener);
    }

    void CEventHandler::RemoveWindowMinimizeListener(IWindowMinimizeListener* pListener)
    {
        PD_ASSERT(pListener);
        std::remove(m_windowMinimizeListeners.begin(), m_windowMinimizeListeners.end(), pListener);
    }

    void CEventHandler::RemoveWindowMaximizeListener(IWindowMaximizeListener* pListener)
    {
        PD_ASSERT(pListener);
        std::remove(m_windowMaximizeListeners.begin(), m_windowMaximizeListeners.end(), pListener);
    }

    void CEventHandler::RemoveGamePauseListener(IGamePauseListener* pListener)
    {
        PD_ASSERT(pListener);
        std::remove(m_gamePauseListeners.begin(), m_gamePauseListeners.end(), pListener);
    }

    void CEventHandler::RemoveGameResumeListener(IGameResumeListener* pListener)
    {
        PD_ASSERT(pListener);
        std::remove(m_gameResumeListeners.begin(), m_gameResumeListeners.end(), pListener); //TODO: look with std::remove
    }

	void CEventHandler::RemoveTextInputListener(ITextInputListener* pListener)
	{
		PD_ASSERT(pListener);
		std::remove(m_textInputListeners.begin(), m_textInputListeners.end(), pListener);
	}

	void CEventHandler::RemoveKeyDownListener(IKeyDownListener* pListener)
	{
		PD_ASSERT(pListener);
		std::remove(m_keyDownListeners.begin(), m_keyDownListeners.end(), pListener);
	}

	void CEventHandler::RemoveKeyUpListener(IKeyUpListener* pListener)
	{
		PD_ASSERT(pListener);
		std::remove(m_keyUpListeners.begin(), m_keyUpListeners.end(), pListener);
	}

	void CEventHandler::RemoveKeyPressListener(IKeyPressListener* pListener)
	{
		PD_ASSERT(pListener);
		std::remove(m_keyPressListeners.begin(), m_keyPressListeners.end(), pListener);
	}

	void CEventHandler::RemoveKeyReleaseListener(IKeyReleaseListener* pListener)
	{
		PD_ASSERT(pListener);
		std::remove(m_keyReleaseListeners.begin(), m_keyReleaseListeners.end(), pListener);
	}
}
