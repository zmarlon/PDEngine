 //
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#ifndef PD_ENGINE_CORE_EVENT_EVENTHANDLER_HPP
#define PD_ENGINE_CORE_EVENT_EVENTHANDLER_HPP

#pragma once

#include "../Disposable.hpp"
#include "../Types.hpp"
#include "../Singleton.hpp"
#include "../STL/Vector.hpp"
#include "../STL/UnorderedSet.hpp"
#include "../Input/Keys.hpp"

 union SDL_Event;
namespace PD
{
	class ITextInputListener;
	class IKeyDownListener;
	class IKeyUpListener;
	class IKeyPressListener;
	class IKeyReleaseListener;

    class IWindowResizeListener;
    class IWindowMoveListener;
    class IWindowMinimizeListener;
    class IWindowMaximizeListener;
    class IGamePauseListener;
    class IGameResumeListener;

    class CEventHandler
    {
        friend class CWindow;

        PD_MAKE_CLASS_DISPOSABLE(CEventHandler)
        PD_MAKE_CLASS_SINGLETON(CEventHandler);
    private:
        STL::CVector<IWindowResizeListener*> m_windowResizeListeners;
        STL::CVector<IWindowMoveListener*> m_windowMoveListeners;
        STL::CVector<IWindowMinimizeListener*> m_windowMinimizeListeners;
        STL::CVector<IWindowMaximizeListener*> m_windowMaximizeListeners;
        STL::CVector<IGamePauseListener*> m_gamePauseListeners;
        STL::CVector<IGameResumeListener*> m_gameResumeListeners;

        STL::CVector<ITextInputListener*> m_textInputListeners;
        STL::CVector<IKeyDownListener*> m_keyDownListeners;
        STL::CVector<IKeyUpListener*> m_keyUpListeners;
        STL::CVector<IKeyPressListener*> m_keyPressListeners;
        STL::CVector<IKeyReleaseListener*> m_keyReleaseListeners;
        STL::CUnorderedSet<EKeys> m_pressedKeys;

        //BEGIN-MANAGED-EVENT-METHODS
        void(*m_pfn_ManagedEventHandler_NInvokeWindowResizeListeners)(u32, u32);
        void(*m_pfn_ManagedEventHandler_NInvokeWindowMoveListeners)(i32, i32);
        void(*m_pfn_ManagedEventHandler_NInvokeWindowMinimizeListeners)();
        void(*m_pfn_ManagedEventHandler_NInvokeWindowMaximizeListeners)();
        void(*m_pfn_ManagedEventHandler_NInvokeGamePauseListeners)();
        void(*m_pfn_ManagedEventHandler_NInvokeGameResumeListeners)();
        void(*m_pfn_ManagedEventHandler_NInvokeTextInputListeners)(wchar_t);
        void(*m_pfn_ManagedEventHandler_NInvokeKeyDownListeners)(EKeys);
        void(*m_pfn_ManagedEventHandler_NInvokeKeyUpListeners)(EKeys);
        void(*m_pfn_ManagedEventHandler_NInvokeKeyPressListeners)(EKeys);
        void(*m_pfn_ManagedEventHandler_NInvokeKeyReleaseListeners)(EKeys);
        //END-MANAGED-EVENT-METHODS

        void InvokeWindowResizeListeners(u32 newWidth, u32 newHeight);
        void InvokeWindowMoveListeners(i32 newX, i32 newY);
        void InvokeWindowMinimizeListeners();
        void InvokeWindowMaximizeListeners();
        void InvokeGamePauseListeners();
        void InvokeGameResumeListeners();

		void InvokeTextInput(const SDL_Event& event);
		void InvokeKeyPress(const SDL_Event& event);
		void InvokeKeyRelease(const SDL_Event& event);
		void InvokeKeyDown(const SDL_Event& event);
		void InvokeKeyUp(const SDL_Event& event);

        void OnDisposing();
    public:
        void Initialize();

		void BeginTextInput();
		void EndTextInput();

        void AddWindowResizeListener(IWindowResizeListener* pListener);
        void AddWindowMoveListener(IWindowMoveListener* pListener);
        void AddWindowMinimizeListener(IWindowMinimizeListener* pListener);
        void AddWindowMaximizeListener(IWindowMaximizeListener* pListener);
        void AddGamePauseListener(IGamePauseListener* pListener);
        void AddGameResumeListener(IGameResumeListener* pListener);
        void AddTextInputListener(ITextInputListener* pListener);
        void AddKeyDownListener(IKeyDownListener* pListener);
        void AddKeyUpListener(IKeyUpListener* pListener);
        void AddKeyPressListener(IKeyPressListener* pListener);
        void AddKeyReleaseListener(IKeyReleaseListener* pListener);

        void RemoveWindowResizeListener(IWindowResizeListener* pListener);
        void RemoveWindowMoveListener(IWindowMoveListener* pListener);
        void RemoveWindowMinimizeListener(IWindowMinimizeListener* pListener);
        void RemoveWindowMaximizeListener(IWindowMaximizeListener* pListener);
        void RemoveGamePauseListener(IGamePauseListener* pListener);
        void RemoveGameResumeListener(IGameResumeListener* pListener);
		void RemoveTextInputListener(ITextInputListener* pListener);
		void RemoveKeyDownListener(IKeyDownListener* pListener);
		void RemoveKeyUpListener(IKeyUpListener* pListener);
		void RemoveKeyPressListener(IKeyPressListener* pListener);
		void RemoveKeyReleaseListener(IKeyReleaseListener* pListener);
    };
}

#endif //PD_ENGINE_CORE_EVENT_EVENTHANDLER_HPP
