//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#ifndef PD_ENGINE_CORE_EVENT_LISTENERS_HPP
#define PD_ENGINE_CORE_EVENT_LISTENERS_HPP

#pragma once

#include "../Types.hpp"
#include "../Input/Keys.hpp"

namespace PD
{
    class IWindowResizeListener
    {
        friend class CEventHandler;
    protected:
        virtual void OnWindowResize(u32 newWidth, u32 newHeight) = 0;
    };

    class IWindowMoveListener
    {
        friend class CEventHandler;
    protected:
        virtual void OnWindowMove(i32 newX, i32 newY) = 0;
    };

    class IWindowMinimizeListener
    {
        friend class CEventHandler;
    protected:
        virtual void OnWindowMinimize() = 0;
    };

    class IWindowMaximizeListener
    {
        friend class CEventHandler;
    protected:
        virtual void OnWindowMaximize() = 0;
    };

    class IGamePauseListener
    {
        friend class CEventHandler;
    protected:
        virtual void OnGamePause() = 0;
    };

    class IGameResumeListener
    {
        friend class CEventHandler;
    protected:
        virtual void OnGameResume() = 0;
    };

	class ITextInputListener
	{
		friend class CEventHandler;
	protected:
		virtual void OnTextInput(wchar_t c) = 0;
	};

	class IKeyDownListener
	{
		friend class CEventHandler;
	protected:
		virtual void OnKeyDown(EKeys key) = 0;
	};

	class IKeyUpListener
	{
		friend class CEventHandler;
	protected:
		virtual void OnKeyUp(EKeys key) = 0;
	};

	class IKeyPressListener
	{
		friend class CEventHandler;
	protected:
		virtual void OnKeyPress(EKeys key) = 0;
	};

	class IKeyReleaseListener
	{
		friend class CEventHandler;
	protected:
		virtual void OnKeyRelease(EKeys key) = 0;
	};
}


#endif //PD_ENGINE_CORE_EVENT_LISTENERS_HPP
