//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 05.04.2020.
//
#ifndef PD_ENGINE_CORE_HINTS_FORCEINLINE_HPP
#define PD_ENGINE_CORE_HINTS_FORCEINLINE_HPP

#pragma once

#include "../Platform.hpp"

#ifdef PD_COMPILER_MSVC
#define PD_FORCEINLINE __forceinline
#elif defined(PD_COMPILER_GCC) || defined(PD_COMPILER_CLANG)
#define PD_FORCEINLINE inline __attribute__((always_inline))
#endif

#endif //PD_ENGINE_CORE_HINTS_FORCEINLINE_HPP
