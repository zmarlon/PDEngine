//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 05.04.2020.
//
#ifndef PD_ENGINE_CORE_HINTS_LIBRARYEXPORT_HPP
#define PD_ENGINE_CORE_HINTS_LIBRARYEXPORT_HPP

#pragma once

#include "../Platform.hpp"

#ifdef PD_PLATFORM_WINDOWS
#ifdef PD_COMPILER_MSVC
#define PD_LIBRARY_EXPORT __declspec(dllexport)
#elif defined(PD_COMPILER_GCC) || defined(PD_COMPILER_CLANG)
#define PD_LIBRARY_EXPORT __attribute__ ((dllexport))
#endif
#elif defined(PD_PLATFORM_LINUX)
#if defined(PD_COMPILER_GCC) || defined(PD_COMPILER_CLANG)
#define PD_LIBRARY_EXPORT __attribute__ ((visibility ("default")))
#endif
#endif

#endif //PD_ENGINE_CORE_HINTS_LIBRARYEXPORT_HPP
