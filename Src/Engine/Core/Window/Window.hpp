//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_WINDOW_WINDOW_HPP
#define PD_ENGINE_CORE_WINDOW_WINDOW_HPP

#pragma once

#include "../Disposable.hpp"
#include "../Singleton.hpp"
#include "../Types.hpp"
#include <SDL2/SDL.h>

namespace PD
{
    struct SGameInfo;
    class CEventHandler;
    class CWindow
    {
    PD_MAKE_CLASS_DISPOSABLE(CWindow)
    PD_MAKE_CLASS_SINGLETON(CWindow)
    public:
        struct SData
        {
            i32 X, Y;
            u32 Width, Height;
            bool Fullscreen;
        };

    private:
        SDL_Window* m_pWindow;
        bool m_shouldClose;
        SDL_Event m_inputEvent;
        void* m_pNativeHandle;

        SData m_data;

        void OnDisposing();
    public:
        void Initialize(u32 width, u32 height, bool fullscreen, const SGameInfo& gameInfo);
        void ProcessEvents(CEventHandler& eventHandler);

        PD_FORCEINLINE void SetX(i32 x)
        {
            m_data.X = x;
            SDL_SetWindowPosition(m_pWindow, x, m_data.Y);
        }

        PD_FORCEINLINE void SetY(i32 y)
        {
            m_data.Y = y;
            SDL_SetWindowPosition(m_pWindow, m_data.X, y);
        }

        PD_FORCEINLINE void SetPosition(i32 x, i32 y)
        {
            m_data.X = x;
            m_data.Y = y;
            SDL_SetWindowPosition(m_pWindow, x, y);
        }

        PD_FORCEINLINE void SetWidth(u32 width)
        {
            m_data.Width = width;
            SDL_SetWindowSize(m_pWindow, width, m_data.Height);
        }

        PD_FORCEINLINE void SetHeight(u32 height)
        {
            m_data.Height = height;
            SDL_SetWindowSize(m_pWindow, m_data.Width, height);
        }

        PD_FORCEINLINE void SetSize(u32 width, u32 height)
        {
            m_data.Width = width;
            m_data.Height = height;
            SDL_SetWindowSize(m_pWindow, width, height);
        }

        PD_FORCEINLINE i32 GetX() const {return m_data.X; }
        PD_FORCEINLINE i32 GetY() const {return m_data.Y; }
        PD_FORCEINLINE u32 GetWidth() const {return m_data.Width; }
        PD_FORCEINLINE u32 GetHeight() const {return m_data.Height; }
        PD_FORCEINLINE bool IsFullscreen() const {return m_data.Fullscreen; }
        PD_FORCEINLINE bool IsCloseRequested() const {return m_shouldClose; }
        PD_FORCEINLINE SData* GetData() {return &m_data; }

        PD_FORCEINLINE bool IsMinimized() const
        {
            return (SDL_GetWindowFlags(m_pWindow) & SDL_WINDOW_MINIMIZED) == SDL_WINDOW_MINIMIZED;
        }

        PD_FORCEINLINE void* GetNativeHandle() {return m_pNativeHandle; }
        PD_FORCEINLINE SDL_Window* GetHandle() {return m_pWindow; }
    };
}

#endif //PD_ENGINE_CORE_WINDOW_WINDOW_HPP
