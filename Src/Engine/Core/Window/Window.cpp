//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#include "Window.hpp"
#include "../Logging/Logger.hpp"
#include "../../GameInfo.hpp"
#include "../Event/EventHandler.hpp"
#include <SDL2/SDL_syswm.h>

namespace PD
{
    PD_POSTDEFINE_SINGLETON(CWindow)
    void CWindow::OnDisposing()
    {
        SDL_DestroyWindow(m_pWindow);
        SDL_Quit();
        DetachSingleton();
    }

    void CWindow::Initialize(u32 width, u32 height, bool fullscreen, const SGameInfo& gameInfo)
    {
        AttachSingleton();

        if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, L"Window") << L"SDL_Init failed: " << SDL_GetError() << CLogger::GetInstance()->Flush();
        }

        u32 windowFlags = SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_VULKAN;
        if(fullscreen)
        {
            windowFlags |= SDL_WINDOW_FULLSCREEN;
        }

        m_pWindow = SDL_CreateWindow(gameInfo.pName, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, windowFlags);

        if(!m_pWindow)
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, L"Window") << L"SDL_CreateWindow failed: " << SDL_GetError() << CLogger::GetInstance()->Flush();
        }

        SDL_GetWindowPosition(m_pWindow, &m_data.X, &m_data.Y);
        SDL_GetWindowSize(m_pWindow, (int*)&m_data.Width, (int*)&m_data.Height);
        m_data.Fullscreen = (SDL_GetWindowFlags(m_pWindow) & SDL_WINDOW_FULLSCREEN) == SDL_WINDOW_FULLSCREEN;

        auto* pSurface = SDL_GetWindowSurface(m_pWindow);
        SDL_Rect rect;
        rect.x = 0;
        rect.y = 0;
        rect.w = m_data.Width;
        rect.h = m_data.Height;

        SDL_FillRect(pSurface, &rect, 0xFFFFFFFF);
        SDL_UpdateWindowSurface(m_pWindow);

        m_shouldClose = false;

        SDL_SysWMinfo sysWMInfo;
        SDL_VERSION(&sysWMInfo.version)
        if(!SDL_GetWindowWMInfo(m_pWindow, &sysWMInfo))
        {
            CLogger::GetInstance()->Log(ELogLevel::Error, L"Window") << L"SDL_GetWindowWMInfo failed: " << SDL_GetError() << CLogger::GetInstance()->Flush();
        }

#ifdef PD_PLATFORM_WINDOWS
        m_pNativeHandle = sysWMInfo.info.win.window;
#elif defined(PD_PLATFORM_LINUX)
        m_pNativeHandle = nullptr;
#else
#error "Unsupported operating system"
#endif

        PD_END_INITIALIZE;
    }

    void CWindow::ProcessEvents(CEventHandler& eventHandler)
    {
        while(SDL_PollEvent(&m_inputEvent))
        {
            switch(m_inputEvent.type)
            {
                case SDL_QUIT:
                {
                    m_shouldClose = true;
                }

				case SDL_TEXTINPUT:
					eventHandler.InvokeTextInput(m_inputEvent);
					break;
				case SDL_KEYDOWN:
					eventHandler.InvokeKeyDown(m_inputEvent);
					eventHandler.InvokeKeyPress(m_inputEvent);
					break;
				case SDL_KEYUP:
					eventHandler.InvokeKeyUp(m_inputEvent);
					eventHandler.InvokeKeyRelease(m_inputEvent);
					break;
                case SDL_WINDOWEVENT:
                {
                    switch(m_inputEvent.window.event)
                    {
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            m_data.Width = static_cast<u32>(m_inputEvent.window.data1);
                            m_data.Height = static_cast<u32>(m_inputEvent.window.data2);

                            eventHandler.InvokeWindowResizeListeners(m_data.Width, m_data.Height);
                            break;
                        case SDL_WINDOWEVENT_MOVED:
                            m_data.X = m_inputEvent.window.data1;
                            m_data.Y = m_inputEvent.window.data2;

                            eventHandler.InvokeWindowMoveListeners(m_data.X, m_data.Y);
                            break;
                        case SDL_WINDOWEVENT_MINIMIZED:
                            eventHandler.InvokeWindowMinimizeListeners();
                            break;
                        case SDL_WINDOWEVENT_MAXIMIZED:
                            eventHandler.InvokeWindowMaximizeListeners();
                            break;
                    }
                }
                    break;
            }
        }
    }
}