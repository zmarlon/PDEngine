//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 27.04.2020.
//

#include "Cursor.hpp"
#include "../Pak/PakLocation.hpp"
#include "../Memory/ProxyAllocator.hpp"
#include "../Logging/Logger.hpp"

namespace PD
{
	void CCursor::OnDisposing()
	{
		SDL_FreeCursor(m_pCursor);
	}

	void CCursor::Load(const SPakLocation& location, int hotX, int hotY)
	{
		CArray<byte> buffer;
		location.Load(buffer, CProxyAllocator::GetInstance());

		SDL_Surface* pSurface = SDL_LoadBMP_RW(SDL_RWFromMem(buffer.GetData(), static_cast<int>(buffer.GetSize())), 1);
		if(!pSurface)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to load cursor from location: " << location << CLogger::GetInstance()->Flush();
		}

		m_pCursor = SDL_CreateColorCursor(pSurface, hotX, hotY);
		SDL_FreeSurface(pSurface);

		if(!m_pCursor)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to create cursor from location: " << location << CLogger::GetInstance()->Flush();
		}

		PD_END_INITIALIZE;
	}

	void CCursor::SetCurrent()
	{
		SDL_SetCursor(m_pCursor);
	}

	void CCursor::Enable()
	{
		SDL_ShowCursor(SDL_ENABLE);
	}

	void CCursor::Disable()
	{
		SDL_ShowCursor(SDL_DISABLE);
	}
}