//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#include "MessageBox.hpp"
#include "Window.hpp"

#ifdef PD_PLATFORM_WINDOWS
#include <Windows.h>
#undef MessageBox
#elif defined(PD_PLATFORM_LINUX)
#include <SDL2/SDL_messagebox.h>
#include <cwchar>
#endif

namespace PD
{
    namespace MessageBoxStyle
    {
        EMessageBoxStyle FromLogLevel(ELogLevel logLevel)
        {
            switch(logLevel)
            {
                case ELogLevel::Information:
                case ELogLevel::Success: return EMessageBoxStyle::Information;
                case ELogLevel::Warning: return EMessageBoxStyle::Warning;
                case ELogLevel::Error:
                case ELogLevel::Fatal: return EMessageBoxStyle::Error;
                default:
                    return EMessageBoxStyle::Information;
            }
        }

        const wchar_t* GetName(EMessageBoxStyle messageBoxStyle)
        {
            switch(messageBoxStyle)
            {
                case EMessageBoxStyle::Information: return L"Information";
                case EMessageBoxStyle::Warning: return L"Warning";
                case EMessageBoxStyle::Error: return L"Error";
                default: return L"Unknown EMessageBoxStyle";
            }
        }
    }

    namespace MessageBox
    {
        namespace INTERNAL
        {
            u32 OsMessageFromStyle(EMessageBoxStyle style)
            {
#ifdef PD_PLATFORM_WINDOWS
                switch(style)
                {
                    case EMessageBoxStyle::Information:
                        return MB_OK | MB_ICONINFORMATION;
                    case EMessageBoxStyle::Warning:
                        return MB_OK | MB_ICONWARNING;
                    case EMessageBoxStyle::Error:
                        return MB_OK | MB_ICONERROR;
                    default:
                        return MB_OK;
                }
#elif defined PD_PLATFORM_LINUX
                switch(style)
				{
					case EMessageBoxStyle::Information:
						return SDL_MESSAGEBOX_INFORMATION;
					case EMessageBoxStyle::Warning:
						return SDL_MESSAGEBOX_WARNING;
					case EMessageBoxStyle::Error:
						return SDL_MESSAGEBOX_ERROR;
					default:
						return SDL_MESSAGEBOX_INFORMATION;
				}
#else
#error "Unsupported operating system"
#endif
            }
        }

        void Show(const char* pMessage, const char* pCaption, EMessageBoxStyle style)
        {
#ifdef PD_PLATFORM_WINDOWS
            ::MessageBoxA(nullptr, pMessage, pCaption, INTERNAL::OsMessageFromStyle(style));
#elif defined(PD_PLATFORM_LINUX)
            SDL_ShowSimpleMessageBox(INTERNAL::OsMessageFromStyle(style), pCaption, pMessage, nullptr);
#else
#error "Unsupported operating system"
#endif
        }

        void Show(CWindow& window, const char* pMessage, const char* pCaption, EMessageBoxStyle style)
        {
#ifdef PD_PLATFORM_WINDOWS
            ::MessageBoxA((HWND)window.GetNativeHandle(), pMessage, pCaption, INTERNAL::OsMessageFromStyle(style));
#elif defined(PD_PLATFORM_LINUX)
            SDL_ShowSimpleMessageBox(INTERNAL::OsMessageFromStyle(style), pCaption, pMessage, window.GetHandle());
#else
#error "Unsupported operating system"
#endif
        }

        void Show(const wchar_t* pMessage, const wchar_t* pCaption, EMessageBoxStyle style)
        {
#ifdef PD_PLATFORM_WINDOWS
            ::MessageBoxW(nullptr, pMessage, pCaption, INTERNAL::OsMessageFromStyle(style));
#elif defined(PD_PLATFORM_LINUX)
            usize messageLength = wcslen(pMessage);
            char* pMessageCstr = new char[messageLength];
			usize length = wcstombs(pMessageCstr, pMessage, messageLength);
			PD_ASSERT(length == messageLength);

            usize captionLength = wcslen(pCaption);
            char* pCaptionCstr = new char[captionLength];
            usize length2 = wcstombs(pCaptionCstr, pCaption, captionLength);
			PD_ASSERT(length2 == captionLength);

			SDL_ShowSimpleMessageBox(INTERNAL::OsMessageFromStyle(style), pCaptionCstr, pMessageCstr, nullptr);

			delete[] pMessageCstr;
			delete[] pCaptionCstr;
#else
#error "Unsupported operating system"
#endif
        }

        void Show(CWindow& window, const wchar_t* pMessage, const wchar_t* pCaption, EMessageBoxStyle style)
        {
#ifdef PD_PLATFORM_WINDOWS
            ::MessageBoxW((HWND)window.GetNativeHandle(), pMessage, pCaption, INTERNAL::OsMessageFromStyle(style));
#elif defined(PD_PLATFORM_LINUX)
            usize messageLength = wcslen(pMessage);
			char* pMessageCstr = new char[messageLength];
			usize length = wcstombs(pMessageCstr, pMessage, messageLength);
			PD_ASSERT(length == messageLength);

			usize captionLength = wcslen(pCaption);
			char* pCaptionCstr = new char[captionLength];
			usize length2 = wcstombs(pCaptionCstr, pCaption, captionLength);
			PD_ASSERT(length2 == captionLength);

			SDL_ShowSimpleMessageBox(INTERNAL::OsMessageFromStyle(style), pCaptionCstr, pMessageCstr, window.GetHandle());

			delete[] pMessageCstr;
			delete[] pCaptionCstr;
#else
#error "Unsupported operating system"
#endif
        }
    }
}
