//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 27.04.2020.
//

#ifndef PD_ENGINE_CORE_WINDOW_CURSOR_HPP
#define PD_ENGINE_CORE_WINDOW_CURSOR_HPP

#include "../Disposable.hpp"
#include <SDL2/SDL.h>

namespace PD
{
	struct SPakLocation;
	class CCursor
	{
		PD_MAKE_CLASS_DISPOSABLE(CCursor)
	private:
		SDL_Cursor* m_pCursor;

		void OnDisposing();
	public:
		void Load(const SPakLocation& location, int hotX, int hotY);
		void SetCurrent();

		PD_FORCEINLINE SDL_Cursor* GetHandle() {return m_pCursor; }

		static void Enable();
		static void Disable();
	};
}

#endif //PD_ENGINE_CORE_WINDOW_CURSOR_HPP
