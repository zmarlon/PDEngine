//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 06.04.2020.
//
#ifndef PD_ENGINE_CORE_WINDOW_MESSAGEBOX_HPP
#define PD_ENGINE_CORE_WINDOW_MESSAGEBOX_HPP

#pragma once

#include "../Logging/LogLevel.hpp"

namespace PD
{
    enum class EMessageBoxStyle
    {
        Information = 0,
        Warning = 1,
        Error = 2
    };

    namespace MessageBoxStyle
    {
        EMessageBoxStyle FromLogLevel(ELogLevel logLevel);
        const wchar_t* GetName(EMessageBoxStyle messageBoxStyle);
    }

    class CWindow;
    namespace MessageBox
    {
        namespace INTERNAL
        {
            u32 OsMessageFromStyle(EMessageBoxStyle style);
        }

        void Show(const char* pMessage, const char* pCaption, EMessageBoxStyle style);
        void Show(CWindow& window, const char* pMessage, const char* pCaption, EMessageBoxStyle style);
        void Show(const wchar_t* pMessage, const wchar_t* pCaption, EMessageBoxStyle style);
        void Show(CWindow& window, const wchar_t* pMessage, const wchar_t* pCaption, EMessageBoxStyle style);
    };
}


#endif //PD_ENGINE_CORE_WINDOW_MESSAGEBOX_HPP
