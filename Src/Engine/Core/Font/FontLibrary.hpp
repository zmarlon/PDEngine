//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_FONT_FONTLIBRARY_HPP
#define PD_ENGINE_CORE_FONT_FONTLIBRARY_HPP

#pragma once

#include "../Disposable.hpp"
#include "../Singleton.hpp"
#include "../Platform.hpp"
#include <ft2build.h>
#include FT_FREETYPE_H

#ifdef PD_PLATFORM_LINUX
const char* FT_Error_String(FT_Error errorCode);
#endif

namespace PD
{
	class CFontLibrary
	{
		PD_MAKE_CLASS_DISPOSABLE(CFontLibrary)
		PD_MAKE_CLASS_SINGLETON(CFontLibrary)
	private:
		FT_Library m_library;

		void OnDisposing();

	public:
		void Initialize();

		PD_FORCEINLINE FT_Library GetHandle() { return m_library; }
	};

}

#endif //PD_ENGINE_CORE_FONT_FONTLIBRARY_HPP
