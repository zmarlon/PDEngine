//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "FontAtlas.hpp"
#include "../Logging/Logger.hpp"
#include "../Pak/PakLocation.hpp"
#include "../Pak/PakFile.hpp"

#define PD_FONT_ATLAS_LOG_PREFIX L"Font Atlas"

#define PD_FONT_ATLAS_PADDING_X 1
#define PD_FONT_ATLAS_PADDING_Y 1

namespace PD
{
	void CFontAtlas::OnDisposing()
	{
		m_glyphMap.clear();
		if(m_bitmap.IsValid()) m_bitmap.Dispose();

		FT_Error error = FT_Done_Face(m_pFace);
		if(error)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_FONT_ATLAS_LOG_PREFIX) << L"Failed to dispose font face: "
																					<< FT_Error_String(error) << CLogger::GetInstance()->Flush();
		}
	}

	void CFontAtlas::PreLoadGlyph(FT_ULong ch, u32& currentAtlasWidth, u32& maxAtlasWidth, u32& maxGlyphHeight, STL::CVector<glm::ivec2>& yData, u32& currentYHeight, u32& currentYOffset, FT_GlyphSlot pGlyphSlot)
	{
		FT_Error error = FT_Load_Char(m_pFace, ch, FT_LOAD_DEFAULT);
		if(error)
		{
			CLogger::GetInstance()->Log(ELogLevel::Information) << L"Generating font atlas: Skipped char " << ch
																<< CLogger::GetInstance()->Flush();
			return;
		}

		error = FT_Render_Glyph(m_pFace->glyph, FT_RENDER_MODE_NORMAL);
		if(error)
		{
			CLogger::GetInstance()->Log(ELogLevel::Information) << L"Generating font atlas: Skipped char " << ch
																<< CLogger::GetInstance()->Flush();
			return;
		}

		currentAtlasWidth += pGlyphSlot->bitmap.width + PD_FONT_ATLAS_PADDING_X;
		u32 currentHeight = pGlyphSlot->bitmap.rows;

		if(currentHeight > currentYHeight) currentYHeight = currentHeight;
		if(currentAtlasWidth > m_preferredWidth)
		{
			if(currentAtlasWidth > maxAtlasWidth) maxAtlasWidth = currentAtlasWidth;
			currentAtlasWidth = 0;
			currentYHeight += PD_FONT_ATLAS_PADDING_Y;

			yData.emplace_back(currentYHeight, currentYOffset);
			currentYOffset += currentYHeight;
			currentYHeight = 0;
		}

		if(currentHeight > maxGlyphHeight)
		{
			maxGlyphHeight = currentHeight;
		}
	}

	void CFontAtlas::AddAllGlyphs(const STL::CVector<FT_ULong>& chars, u32 maxGlyphHeight, const STL::CVector<glm::ivec2>& yData, FT_GlyphSlot pGlyphSlot)
	{
		u32 offsetX = 0, indexY = 0;

		for(size_t ix = 0; ix < chars.size(); ix++)
		{
			const FT_ULong ch = chars[ix];

			FT_Error error = FT_Load_Char(m_pFace, ch, FT_LOAD_DEFAULT);
			if(error) return;

			error = FT_Render_Glyph(m_pFace->glyph, FT_RENDER_MODE_NORMAL);
			if(error) return;

			u32 glyphWidth = pGlyphSlot->bitmap.width;
			u32 glyphHeight = pGlyphSlot->bitmap.rows;

			const auto offsetY = yData[indexY].y;

			for(u32 iz = 0; iz < glyphWidth; iz++)
			{
				for(u32 iy = 0; iy < glyphHeight; iy++)
				{
                    u8 value = static_cast<u8>(pGlyphSlot->bitmap.buffer[iy * glyphWidth + iz]);

					m_bitmap.SetPixel(offsetX + iz, offsetY + iy, value);
				}
			}

			m_glyphMap.emplace(std::piecewise_construct, std::make_tuple(ch), std::make_tuple(
					ch, static_cast<float>(pGlyphSlot->advance.x >> 6), static_cast<float>(pGlyphSlot->advance.y >> 6),
					static_cast<float>(glyphWidth), static_cast<float>(glyphHeight),
					static_cast<float>(pGlyphSlot->bitmap_left), static_cast<float>(pGlyphSlot->bitmap_top), offsetX,
					offsetY));

			offsetX += glyphWidth + PD_FONT_ATLAS_PADDING_X;
			if(offsetX > m_preferredWidth)
			{
				offsetX = 0;
				indexY++;
			}
		}
	}

	void CFontAtlas::Load(CFontLibrary& library, const SPakLocation& location, u32 fontSize, u32 faceIndex)
	{
		CArray<byte> data;
		location.Load(data);

		FT_Error error = FT_New_Memory_Face(library.GetHandle(), data.GetData(), static_cast<FT_Long>(data.GetSize()),
											faceIndex, &m_pFace);
		if(error)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_FONT_ATLAS_LOG_PREFIX)
					<< L"Failed to load font face from location: "
					<< location << L" : " << FT_Error_String(error)
					<< CLogger::GetInstance()->Flush();
		}

		error = FT_Select_Charmap(m_pFace, FT_ENCODING_UNICODE);
		if(error)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_FONT_ATLAS_LOG_PREFIX) << L"Failed to select unicode char map: "
																					<< FT_Error_String(error) << CLogger::GetInstance()->Flush();
		}

		error = FT_Set_Pixel_Sizes(m_pFace, 0, fontSize);
		if(error)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_FONT_ATLAS_LOG_PREFIX) << L"Failed to set font pixel sizes: "
																					<< FT_Error_String(error) << CLogger::GetInstance()->Flush();
		}

		u32 currentAtlasWidth = 0, maxAtlasWidth = 0;
		m_glyphHeight = 0;
		FT_GlyphSlot pGlyphSlot = m_pFace->glyph;

		m_preferredWidth = fontSize * 16;

		FT_UInt gIndex;

		STL::CVector<FT_ULong> charCodes;
		charCodes.reserve(256);

		FT_ULong charCode = FT_Get_First_Char(m_pFace, &gIndex);
		if(gIndex == 0)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_FONT_ATLAS_LOG_PREFIX) << L"Failed to create font atlas, first char is invalid!"
																					<< CLogger::GetInstance()->Flush();
		}

		STL::CVector<glm::ivec2> yData;
		yData.reserve(32);
		u32 tempYOffset = 0;
		u32 tempYHeight = 0;

		PreLoadGlyph(charCode, currentAtlasWidth, maxAtlasWidth, m_glyphHeight, yData, tempYHeight, tempYOffset, pGlyphSlot);
		charCodes.push_back(charCode);

		while(gIndex != 0)
		{
			charCode = FT_Get_Next_Char(m_pFace, charCode, &gIndex);
			PreLoadGlyph(charCode, currentAtlasWidth, maxAtlasWidth, m_glyphHeight, yData, tempYHeight, tempYOffset, pGlyphSlot);

			charCodes.push_back(charCode);
		}
		yData.emplace_back(tempYHeight, tempYOffset);

		u32 atlasWidth = maxAtlasWidth;

		const auto& lastYElement = yData.back();
		u32 atlasHeight = lastYElement.x + lastYElement.y;

		m_bitmap.Create(atlasWidth, atlasHeight, true);

		AddAllGlyphs(charCodes, m_glyphHeight, yData, pGlyphSlot);

		m_data.Size = fontSize;
		m_data.LineSpacing = m_glyphHeight;
		m_data.GlyphCount = static_cast<u32>(m_glyphMap.size());
		m_data.pGlyphInfos = new SGlyphInfo[m_data.GlyphCount];

		u32 counter = 0;
		for(const auto& glyph : m_glyphMap)
		{
			m_data.pGlyphInfos[counter++] = glyph.second;
		}

		m_size = fontSize;

		PD_END_INITIALIZE;
	}

    void CFontAtlas::UnloadBitmap()
    {
        PD_ASSERT(m_bitmap.IsValid());
        m_bitmap.Dispose();
    }
}