//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_FONT_FONTATLAS_HPP
#define PD_ENGINE_CORE_FONT_FONTATLAS_HPP

#pragma once

#include "FontLibrary.hpp"
#include "../Types.hpp"
#include "../Drawing/Bitmap.hpp"
#include "../STL/UnorderedMap.hpp"
#include "../STL/Vector.hpp"
#include <glm/vec2.hpp>

namespace PD
{
	struct SPakLocation;

	class CFontAtlas
	{
		PD_MAKE_CLASS_DISPOSABLE(CFontAtlas)
	public:
		struct SGlyphInfo
		{
			wchar_t Value;
			float XAdvance;
			float YAdvance;
			float Width;
			float Height;
			float OffsetX;
			float OffsetY;
			float TexCoordX;
			float TexCoordY;

			PD_FORCEINLINE SGlyphInfo() = default;

			PD_FORCEINLINE SGlyphInfo(wchar_t value, float xAdvance, float yAdvance, float width, float height,
									  float offsetX,
									  float offsetY, float texCoordX, float texCoordY) :
					Value(value), XAdvance(xAdvance), YAdvance(yAdvance), Width(width), Height(height),
					OffsetX(offsetX),
					OffsetY(offsetY), TexCoordX(texCoordX), TexCoordY(texCoordY) {}
		};

		struct SData
		{
			float Size;
			float LineSpacing;
			SGlyphInfo* pGlyphInfos;
			u32 GlyphCount;
		};
	private:
		FT_Face m_pFace;
		u32 m_atlasWidth;
		u32 m_atlasHeight;
		u32 m_glyphHeight;
		CBitmap<u8> m_bitmap;
		u32 m_size;

		u32 m_preferredWidth;

		STL::CUnorderedMap<wchar_t, SGlyphInfo> m_glyphMap;
		SData m_data;

		void PreLoadGlyph(FT_ULong ch, u32& currentAtlasWidth, u32& maxAtlasWidth, u32& maxGlyphHeight, STL::CVector<glm::ivec2>& yData, u32& currentYHeight, u32& currentYOffset,
						  FT_GlyphSlot pGlyphSlot);

		void AddAllGlyphs(const STL::CVector<FT_ULong>& chars, u32 maxGlyphHeight, const STL::CVector<glm::ivec2>& yData,
						  FT_GlyphSlot pGlyphSlot);

		void OnDisposing();

	public:
		void Load(CFontLibrary& library, const SPakLocation& location, u32 fontSize, u32 faceIndex = 0);
		void UnloadBitmap();

		PD_FORCEINLINE const SGlyphInfo* GetGlyph(wchar_t value) const
		{
			auto iterator = m_glyphMap.find(value);
			if(iterator != m_glyphMap.end()) return &iterator->second;
			return nullptr;
		}

		PD_FORCEINLINE const CBitmap<u8>& GetBitmap() const { return m_bitmap; }
		PD_FORCEINLINE const STL::CUnorderedMap<wchar_t, SGlyphInfo>& GetGlyphMap() const { return m_glyphMap; }
		PD_FORCEINLINE u32 GetSize() const { return m_size; }
		PD_FORCEINLINE u32 GetGlyphHeight() const { return m_glyphHeight; }
		PD_FORCEINLINE const SData* GetData() const { return &m_data; }
		PD_FORCEINLINE SData* GetData() {return &m_data; }
		PD_FORCEINLINE FT_Face GetHandle() { return m_pFace; }
	};

}

#endif //PD_ENGINE_CORE_FONT_FONTATLAS_HPP
