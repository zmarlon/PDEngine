//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "FontLibrary.hpp"
#include "../Logging/Logger.hpp"

#ifdef PD_PLATFORM_LINUX
const char* FT_Error_String(FT_Error errorCode)
{
	return "Unknown Error"; //TODO: maybe output error strings
}
#endif

namespace PD
{
	PD_POSTDEFINE_SINGLETON(CFontLibrary)

	void CFontLibrary::OnDisposing()
	{
		FT_Error error = FT_Done_FreeType(m_library);
		if(error)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"FT_Done_FreeType failed: "
														  << FT_Error_String(error) << CLogger::GetInstance()->Flush();
		}

		DetachSingleton();
	}

	void CFontLibrary::Initialize()
	{
		AttachSingleton();

		FT_Error error = FT_Init_FreeType(&m_library);
		if(error)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"FT_Init_FreeType failed: "
														  << FT_Error_String(error) << CLogger::GetInstance()->Flush();
		}

		PD_END_INITIALIZE;
	}
}