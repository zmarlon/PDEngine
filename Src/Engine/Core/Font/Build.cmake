set(PD_ENGINE_CORE_FONT_DIRECTORY Src/Engine/Core/Font)

set(PD_ENGINE_CORE_FONT_SOURCE_FILES
        ${PD_ENGINE_CORE_FONT_DIRECTORY}/FontAtlas.cpp
        ${PD_ENGINE_CORE_FONT_DIRECTORY}/FontAtlas.hpp
        ${PD_ENGINE_CORE_FONT_DIRECTORY}/FontLibrary.cpp
        ${PD_ENGINE_CORE_FONT_DIRECTORY}/FontLibrary.hpp)
