//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CORE_DRAWING_BITMAP_HPP
#define PD_ENGINE_CORE_DRAWING_BITMAP_HPP

#include "../Disposable.hpp"
#include "../Types.hpp"
#include "../Memory/ProxyAllocator.hpp"

namespace PD
{
	template<typename T>
	class CBitmap
	{
		PD_MAKE_CLASS_DISPOSABLE(CBitmap)
	private:
		IAllocator* m_pAllocator;

		T* m_pBuffer = nullptr;

		u32 m_width;
		u32 m_height;

		void OnDisposing()
		{
			m_pAllocator->Deallocate(m_pBuffer);
		    m_pBuffer = nullptr;
		}

	public:
		PD_FORCEINLINE void Create(u32 width, u32 height, bool zeroMemory = false, IAllocator* pAllocator = CProxyAllocator::GetInstance())
		{
			PD_ASSERT(pAllocator);

			m_pAllocator = pAllocator;

			m_width = width;
			m_height = height;

			usize size = width * height * sizeof(T);
			m_pBuffer = reinterpret_cast<T*>(pAllocator->Allocate(size));

			if(zeroMemory) MemoryHelper::Zero(m_pBuffer, size);

			PD_END_INITIALIZE;
		}

		PD_FORCEINLINE const T GetPixel(u32 x, u32 y) const
		{
			return m_pBuffer[y * m_width + x];
		}

		PD_FORCEINLINE void SetPixel(u32 x, u32 y, T pixel)
		{
			m_pBuffer[y * m_width + x] = pixel;
		}

		PD_FORCEINLINE const u32 GetWidth() const { return m_width; }

		PD_FORCEINLINE const u32 GetHeight() const { return m_height; }

		PD_FORCEINLINE const u32 GetBytesPerPixel() const { return sizeof(T); }

		PD_FORCEINLINE bool IsValid() const {return m_pBuffer != nullptr; }

		PD_FORCEINLINE const T* GetData() const { return m_pBuffer; }
	};

}

#endif //PD_ENGINE_CORE_DRAWING_BITMAP_HPP
