//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 16.04.2020.
//

#include "GUID.hpp"
#include "../Assert.hpp"
#include "../String/StringHelper.hpp"

namespace PD
{
    SGUID::SGUID(const char* pGUID)
    {
        PD_ASSERT(pGUID);
        PD_ASSERT(StringHelper::GetLength(pGUID) == 38);
        PD_ASSERT(pGUID[0] == '{' && pGUID[37] == '}');
        PD_ASSERT(pGUID[9] == '-' && pGUID[14] == '-' && pGUID[19] == '-' && pGUID[24] == '-');

        Bytes.B[0] = StringHelper::UpperHexCharToNumber(pGUID[1]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[2]);
        Bytes.B[1] = StringHelper::UpperHexCharToNumber(pGUID[3]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[4]);
        Bytes.B[2] = StringHelper::UpperHexCharToNumber(pGUID[5]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[6]);
        Bytes.B[3] = StringHelper::UpperHexCharToNumber(pGUID[7]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[8]);

        Bytes.B[4] = StringHelper::UpperHexCharToNumber(pGUID[10]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[11]);
        Bytes.B[5] = StringHelper::UpperHexCharToNumber(pGUID[12]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[13]);

        Bytes.B[6] = StringHelper::UpperHexCharToNumber(pGUID[15]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[16]);
        Bytes.B[7] = StringHelper::UpperHexCharToNumber(pGUID[17]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[18]);

        Bytes.B[8] = StringHelper::UpperHexCharToNumber(pGUID[20]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[21]);
        Bytes.B[9] = StringHelper::UpperHexCharToNumber(pGUID[22]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[23]);

        Bytes.B[10] = StringHelper::UpperHexCharToNumber(pGUID[25]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[26]);
        Bytes.B[11] = StringHelper::UpperHexCharToNumber(pGUID[27]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[28]);
        Bytes.B[12] = StringHelper::UpperHexCharToNumber(pGUID[29]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[30]);
        Bytes.B[13] = StringHelper::UpperHexCharToNumber(pGUID[31]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[32]);
        Bytes.B[14] = StringHelper::UpperHexCharToNumber(pGUID[33]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[34]);
        Bytes.B[15] = StringHelper::UpperHexCharToNumber(pGUID[35]) << 4u | StringHelper::UpperHexCharToNumber(pGUID[36]);
    }

    std::ostream& operator <<(std::ostream& stream, const SGUID& guid)
    {
        stream << '{';
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[0] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[0] & 0xFu);
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[1] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[1] & 0xFu);
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[2] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[2] & 0xFu);
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[3] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[3] & 0xFu);
        stream << '-';
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[4] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[4] & 0xFu);
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[5] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[5] & 0xFu);
        stream << '-';
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[6] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[6] & 0xFu);
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[7] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[7] & 0xFu);
        stream << '-';
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[8] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[8] & 0xFu);
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[9] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[9] & 0xFu);
        stream << '-';
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[10] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[10] & 0xFu);
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[11] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[11] & 0xFu);
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[12] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[12] & 0xFu);
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[13] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[13] & 0xFu);
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[14] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[14] & 0xFu);
        stream << StringHelper::HexNumberToUpperChar((guid.Bytes.B[15] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperChar(guid.Bytes.B[15] & 0xFu);
        stream << '}';
        return stream;
    }

    std::wostream& operator <<(std::wostream& stream, const SGUID& guid)
    {
        stream << L'{';
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[0] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[0] & 0xFu);
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[1] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[1] & 0xFu);
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[2] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[2] & 0xFu);
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[3] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[3] & 0xFu);
        stream << L'-';
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[4] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[4] & 0xFu);
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[5] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[5] & 0xFu);
        stream << L'-';
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[6] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[6] & 0xFu);
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[7] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[7] & 0xFu);
        stream << L'-';
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[8] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[8] & 0xFu);
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[9] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[9] & 0xFu);
        stream << L'-';
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[10] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[10] & 0xFu);
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[11] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[11] & 0xFu);
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[12] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[12] & 0xFu);
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[13] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[13] & 0xFu);
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[14] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[14] & 0xFu);
        stream << StringHelper::HexNumberToUpperWideChar((guid.Bytes.B[15] & 0xF0u) >> 4u) << StringHelper::HexNumberToUpperWideChar(guid.Bytes.B[15] & 0xFu);
        stream << L'}';
        return stream;
    }
}
