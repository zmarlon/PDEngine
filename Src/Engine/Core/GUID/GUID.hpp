//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 16.04.2020.
//
#ifndef PDENGINE_GUID_HPP
#define PDENGINE_GUID_HPP

#pragma once

#include "../Types.hpp"
#include "../Hints/ForceInline.hpp"
#include <iostream>

namespace PD
{
    union SGUID
    {
        struct
        {
            u32 Data1;
            u16 Data2;
            u16 Data3;
            u8 Data4, Data5, Data6, Data7, Data8, Data9, Data10, Data11;
        };
        struct
        {
            byte B[16];
        } Bytes;
        struct
        {
            u64 L1;
            u64 L2;
        } Longs;

        PD_FORCEINLINE SGUID()
        {
            Longs.L1 = 0;
            Longs.L2 = 0;
        }
        PD_FORCEINLINE SGUID(u32 data1, u16 data2, u16 data3, u8 data4, u8 data5, u8 data6, u8 data7, u8 data8, u8 data9, u8 data10, u8 data11)
            : Data1(data1), Data2(data2), Data3(data3), Data4(data4), Data5(data5), Data6(data6), Data7(data7), Data8(data8), Data9(data9), Data10(data10), Data11(data11) {}

        SGUID(const char* pGUID);

        PD_FORCEINLINE bool operator ==(const SGUID& other) const
        {
            return Longs.L1 == other.Longs.L1 && Longs.L2 == other.Longs.L2;
        }

        PD_FORCEINLINE bool operator !=(const SGUID& other) const
        {
            return Longs.L1 != other.Longs.L1 || Longs.L2 != other.Longs.L2;
        }

        friend std::ostream& operator <<(std::ostream& stream, const SGUID& guid);
        friend std::wostream& operator <<(std::wostream& stream, const SGUID& guid);
    };
}

#endif //PDENGINE_GUID_HPP
