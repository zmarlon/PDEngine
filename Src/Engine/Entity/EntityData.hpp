//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PD_ENGINE_ENTITY_ENTITYDATA_HPP
#define PD_ENGINE_ENTITY_ENTITYDATA_HPP

#pragma once

#include <glm/glm.hpp>

namespace PD
{
	struct SEntityData
	{
		glm::vec3 Position;
		glm::vec3 Rotation;
		bool Active;
	};
}

#endif //PD_ENGINE_ENTITY_ENTITYDATA_HPP
