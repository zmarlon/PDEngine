//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 26.04.2020.
//
#ifndef PD_ENGINE_GAMELAUNCHER_HPP
#define PD_ENGINE_GAMELAUNCHER_HPP

#pragma once

#include "Core/Hints/LibraryExport.hpp"

namespace PD::GameLauncher
{
	PD_LIBRARY_EXPORT int Launch(int argc, char** ppArgs);
}

#endif //PD_ENGINE_GAMELAUNCHER_HPP
