//
// Created by marlon on 26.04.20.
//

#include "GameLauncher.hpp"
#include "Game.hpp"
#include "Server/DedicatedGameServer.hpp"
#include <cstring>
#include <cstdio>

#define TYPE_CLIENT 0
#define TYPE_SERVER 1
#define TYPE_UNDEFINED 2

#ifdef PD_DEFAULT_RUN_SERVER
#define TYPE_DEFAULT TYPE_SERVER
#else
#define TYPE_DEFAULT TYPE_CLIENT
#endif

namespace PD
{
	int GetType(int argc, char** ppArgs)
	{
		if(argc == 1) return TYPE_DEFAULT;
		if(argc == 2)
		{
			if(strcmp(ppArgs[1], "server") == 0) return TYPE_SERVER;
			if(strcmp(ppArgs[1], "client") == 0) return TYPE_CLIENT;
		}
		return TYPE_UNDEFINED;
	}

	int GameLauncher::Launch(int argc, char** ppArgs)
	{
		auto type = GetType(argc, ppArgs);
		if(type == TYPE_CLIENT)
		{
			auto* pClient = new CGame;
			pClient->Initialize();
			pClient->Dispose();
			delete pClient;
			return 0;
		}
		if(type == TYPE_SERVER)
		{
			auto* pServer = new CDedicatedGameServer;
			pServer->Initialize();
			pServer->Dispose();
			delete pServer;
			return 0;
		}

		puts("[PDEngine] Invalid arguments!\nUsage:\n * PDEngine : Starts the client\n * PDEngine client : Starts the client\n * PDEngine server : Starts the server\n");

		return 1;
	}
}