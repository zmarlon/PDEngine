//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 05.04.2020.
//
#include "Game.hpp"
#include "Core/Memory/MemoryStack.hpp"
#include "Core/Threading/ThreadExtensions.hpp"
#include "Core/Logging/Logger.hpp"
#include "Core/Logging/Layout/BasicLayout.hpp"
#include "Core/Logging/Layout/SimpleLayout.hpp"
#include "Core/Logging/Appender/OstreamAppender.hpp"
#include "Core/Logging/Appender/MessageBoxAppender.hpp"
#include "Core/Logging/Appender/FileAppender.hpp"
#include "Core/Window/Window.hpp"
#include "Core/Event/EventHandler.hpp"
#include "Core/Font/FontLibrary.hpp"
#include "Core/Pak/PakManager.hpp"
#include "Core/Managed/CommonLanguageRuntime.hpp"
#include "Client/Renderer/Renderer.hpp"
#include "World/StaticEntity/StaticEntityRegistry.hpp"
#include "Core/Console/GameConsole.hpp"

#include <iostream>

namespace PD
{
    PD_POSTDEFINE_SINGLETON(CGame)
    void CGame::OnDisposing()
    {
        DetachSingleton();

        m_pfn_Game_DisposeInstance();

        m_pStaticEntityRegistry->Dispose();
        delete m_pStaticEntityRegistry;

        m_pGameConsole->Dispose();
        delete m_pGameConsole;

        m_pRenderer->Dispose();
        delete m_pRenderer;

        m_pPakManager->Dispose();
        delete m_pPakManager;

        m_pFontLibrary->Dispose();
        delete m_pFontLibrary;

        m_pWindow->Dispose();
        delete m_pWindow;

        DestroyEventHandler();

        m_pCLRuntime->Dispose();
        delete m_pCLRuntime;

        DestroyLogger();

        m_pMemoryStack->Dispose();
        delete m_pMemoryStack;

        m_pProxyAllocator->Dispose();
        delete m_pProxyAllocator;
    }

    void CGame::CreateLogger()
    {
        m_pLogger = new CLogger;
        m_pLogger->Initialize();

        auto basicLayout = std::make_shared<CBasicLayout>(L"%Y-%m-%d %X");
        auto simpleLayout = std::make_shared<CSimpleLayout>();

        auto consoleAppender = std::make_shared<COstreamAppender>(L"console", &std::wcout);
        consoleAppender->SetLayout(basicLayout);
        m_pLogger->AddAppender(consoleAppender);

        auto messageBoxAppender = std::make_shared<CMessageBoxAppender>(L"message_box");
        messageBoxAppender->SetLayout(simpleLayout);
        messageBoxAppender->SetMinLevel(ELogLevel::Error);
        m_pLogger->AddAppender(messageBoxAppender);

        auto fileAppender = std::make_shared<CFileAppender>(L"console", "PDEngine_Log_", "log", true, L"%Y-%m-%d_%H-%M-%S");
        fileAppender->SetLayout(basicLayout);
        m_pLogger->AddAppender(fileAppender);
    }

    void CGame::DestroyLogger()
    {
        m_pLogger->Dispose();
        delete m_pLogger;
    }

    void CGame::CreateEventHandler()
    {
        m_pEventHandler = new CEventHandler;
        m_pEventHandler->Initialize();
        m_pEventHandler->AddWindowResizeListener(this);
        m_pEventHandler->AddGamePauseListener(this);
        m_pEventHandler->AddGameResumeListener(this);
    }

    void CGame::DestroyEventHandler()
    {
        m_pEventHandler->RemoveGamePauseListener(this);
        m_pEventHandler->RemoveGameResumeListener(this);
        m_pEventHandler->RemoveWindowResizeListener(this);
        m_pEventHandler->Dispose();
        delete m_pEventHandler;
    }

    void CGame::CreateManagedDelegates()
    {
        m_pfn_Game_CreateInstance = m_pCLRuntime->GetDelegate<void(void*)>("PDEngine.Managed.Game", "NCreateInstance");
        m_pfn_Game_PreInit = m_pCLRuntime->GetDelegate<void()>("PDEngine.Managed.Game", "NPreInit");
        m_pfn_Game_Init = m_pCLRuntime->GetDelegate<void()>("PDEngine.Managed.Game", "NInit");
        m_pfn_Game_PostInit = m_pCLRuntime->GetDelegate<void()>("PDEngine.Managed.Game", "NPostInit");
        m_pfn_Game_DisposeInstance = m_pCLRuntime->GetDelegate<void()>("PDEngine.Managed.Game", "NDisposeInstance");
        m_pfn_Game_TickInstance = m_pCLRuntime->GetDelegate<void(float, float)>("PDEngine.Managed.Game", "NTickInstance");
    }

    //EVENTS-BEGIN
    void CGame::OnWindowResize(u32 newWidth, u32 newHeight)
    {
        m_pRenderer->Resize(newWidth, newHeight);
    }

    void CGame::OnGamePause()
    {
        m_gameState.IsPaused = true;
        //TODO:
    }

    void CGame::OnGameResume()
    {
        m_gameState.IsPaused = false;
        //TODO:
    }
    //EVENTS-END

    //INIT-BEGIN
    void CGame::PreInit()
    {
        m_gameState.IsPaused = false; //TODO TUDUUUH

        ThreadExtensions::SetCurrentThreadName(L"MAIN");

        m_pProxyAllocator = new CProxyAllocator;
        m_pProxyAllocator->Initialize();

        m_pMemoryStack = new CMemoryStack;
        m_pMemoryStack->Create();
        m_pMemoryStack->AttachCurrentThread();

        CreateLogger();

        m_pCLRuntime = new CCommonLanguageRuntime;
        m_pCLRuntime->Create();
        CreateManagedDelegates();
        m_pfn_Game_CreateInstance(this);

        CreateEventHandler();

        m_pfn_Game_PreInit();
    }

    void CGame::Init()
    {
        m_pWindow = new CWindow;
        m_pWindow->Initialize(m_config.WindowWidth, m_config.WindowHeight, m_config.Fullscreen, m_gameInfo);

        m_pFontLibrary = new CFontLibrary;
        m_pFontLibrary->Initialize();

        m_pPakManager = new CPakManager;
        m_pPakManager->Create();

        m_pRenderer = new CRenderer;
        m_pRenderer->Initialize(*m_pWindow, m_gameInfo, m_config.DebugLevel);

		m_pGameConsole = new CGameConsole;
		m_pGameConsole->Initialize(m_pRenderer->GetDevice());

        m_pfn_Game_Init();
    }

    void CGame::PostInit()
    {
        m_pStaticEntityRegistry = new CStaticEntityRegistry;
        m_pStaticEntityRegistry->Initialize(true);

        m_pfn_Game_PostInit();
    }
    //INIT-END

    void CGame::Initialize()
    {
        AttachSingleton();

        PreInit();
        Init();
        PostInit();

        m_loopTimer.SetFrameLimit(165);

        while(!m_pWindow->IsCloseRequested())
        {
            m_loopTimer.Tick([this](const SFrameEventArgs& frameEventArgs)
            {
                m_pWindow->ProcessEvents(*m_pEventHandler);
                if(!m_pWindow->IsMinimized())
                {
                    m_pRenderer->Render(frameEventArgs);

                    m_pfn_Game_TickInstance(frameEventArgs.DeltaTime, frameEventArgs.ElapsedTime);
                }
            });
        }

        PD_END_INITIALIZE;
    }

    void CGame::Crash()
    {

    }
}
