//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PDENGINE_CAMERA_HPP
#define PDENGINE_CAMERA_HPP

#include "../Entity/EntityData.hpp"
#include "../Core/Singleton.hpp"
#include "../Core/Time/FrameEventArgs.hpp"
#include "../Core/Types.hpp"

namespace PD
{
	class ICamera
	{
	private:
		static ICamera* _INSTANCE;

		void RefreshProjectionMatrix();
		void RefreshViewMatrix();
	protected:
		SEntityData* m_pEntityData;

		float m_degFieldOfView;
		float m_nearPlane;
		float m_farPlane;

		glm::mat4 m_projectionMatrix;
		glm::vec3 m_direction;
		glm::vec3 m_lookAt;
		glm::mat4 m_viewMatrix;

		virtual void OnInitialize() = 0;
		virtual void OnUpdate(const SFrameEventArgs& frameEventArgs) = 0;
	public:
		ICamera() = default;

		void Initialize(SEntityData* pEntityData);

		void Update(const SFrameEventArgs& frameEventArgs);
		void Resize();

		void SetCurrent();
	public:
		PD_FORCEINLINE float GetDegFieldOfView() const {return m_degFieldOfView; }
		PD_FORCEINLINE float GetNearPlane() const {return m_nearPlane; }
		PD_FORCEINLINE float GetFarPlane() const {return m_farPlane; }
		PD_FORCEINLINE const glm::mat4& GetProjectionMatrix() const {return m_projectionMatrix; }
		PD_FORCEINLINE const glm::vec3& GetDirection() const {return m_direction; }
		PD_FORCEINLINE const glm::vec3& GetLookAt() const {return m_lookAt; }
		PD_FORCEINLINE const glm::mat4& GetViewMatrix() const {return m_viewMatrix; }

		PD_FORCEINLINE static ICamera* GetCurrent()
		{
			PD_ASSERT(_INSTANCE);
			return _INSTANCE;
		}
	};
}

#endif //PDENGINE_CAMERA_HPP
