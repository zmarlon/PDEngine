//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PD_ENGINE_CLIENT_PLAYERCOMPONENT_HPP
#define PD_ENGINE_CLIENT_PLAYERCOMPONENT_HPP

#pragma once

#include "../Entity/EntityData.hpp"
#include "../Core/Disposable.hpp"
#include "../Core/Singleton.hpp"
#include "../Core/Time/FrameEventArgs.hpp"
#include "PlayerFreeCam.hpp"

namespace PD
{
	class CPlayerComponent
	{
		PD_MAKE_CLASS_DISPOSABLE(CPlayerComponent)
		PD_MAKE_CLASS_SINGLETON(CPlayerComponent)
	private:
		SEntityData* m_pData;

		CPlayerFreeCam m_playerFreeCam;

		void OnDisposing();
	public:
		void Initialize(SEntityData* pData);

		void Update(const SFrameEventArgs& frameEventArgs);

		PD_FORCEINLINE SEntityData* GetEntityData() {return m_pData; }
	};
}

#endif //PD_ENGINE_CLIENT_PLAYERCOMPONENT_HPP
