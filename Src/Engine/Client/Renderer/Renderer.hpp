//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#ifndef PD_ENGINE_CLIENT_RENDERER_RENDERER_HPP
#define PD_ENGINE_CLIENT_RENDERER_RENDERER_HPP

#pragma once

#include "../../Core/Types.hpp"
#include "../../Core/Disposable.hpp"
#include "Vulkan/VulkanInstance.hpp"
#include "Vulkan/VulkanSurface.hpp"
#include "Vulkan/VulkanPhysicalDevice.hpp"
#include "Vulkan/VulkanDevice.hpp"
#include "Vulkan/VulkanSwapchain.hpp"
#include "Settings/RendererProperties.hpp"
#include "Settings/GraphicsSettings.hpp"
#include "Common/SpriteBatch.hpp"
#include "Common/SpriteBatchFactory.hpp"
#include "Vulkan/Image/VulkanTexture2D.hpp"
#include "Common/SpriteFont.hpp"
#include "Deferred/DeferredRenderer.hpp"

namespace PD
{
    struct SFrameEventArgs;
    class CWindow;
    struct SGameInfo;
    class CRenderer
    {
    PD_MAKE_CLASS_DISPOSABLE(CRenderer)
    PD_MAKE_CLASS_SINGLETON(CRenderer)
    private:
        CVulkanInstance m_instance;
        CVulkanSurface m_surface;
        CVulkanPhysicalDevice m_physicalDevice;
        CVulkanDevice m_device;
        CVulkanSwapchain m_swapchain;

        CDeferredRenderer m_deferredRenderer;

        CVulkanPrimaryCommandBuffer m_commandBuffer;

        u32 m_debugLevel;
        SRendererProperties m_properties;
        SGraphicsSettings m_graphicsSettings;

        CSpriteBatch m_spriteBatch;
        CSpriteBatchFactory m_spriteBatchFactory;

        CVulkanTexture2D m_whitePixel;

        void(*m_pfn_Renderer_RenderSprites)(SFrameEventArgs);

        inline void CreateDeviceAndSwapchain(CWindow& window, const SGameInfo& gameInfo);
        inline void DestroyDeviceAndSwapchain();

        void RenderSprites(CVulkanPrimaryCommandBuffer& commandBuffer, const SFrameEventArgs& frameEventArgs);

        void OnDisposing();
    public:
        void Initialize(CWindow& window, const SGameInfo& gameInfo, u32 debugLevel);
        void ChangeSettings(const SGraphicsSettings& oldSettings, const SGraphicsSettings& newSettings);
        void Resize(u32 newWidth, u32 newHeight);

        void Render(const SFrameEventArgs& frameEventArgs);

        PD_FORCEINLINE u32 GetDebugLevel() const {return m_debugLevel; }
        PD_FORCEINLINE const SRendererProperties& GetProperties() const {return m_properties; }
        PD_FORCEINLINE const SGraphicsSettings& GetGraphicsSettings() const {return m_graphicsSettings; }

        PD_FORCEINLINE CVulkanInstance& GetVkInstance() {return m_instance; }
        PD_FORCEINLINE const CVulkanInstance& GetVkInstance() const {return m_instance; }

        PD_FORCEINLINE CVulkanPhysicalDevice& GetPhysicalDevice() {return m_physicalDevice; }
        PD_FORCEINLINE const CVulkanPhysicalDevice& GetPhysicalDevice() const {return m_physicalDevice; }

        PD_FORCEINLINE CVulkanDevice& GetDevice() {return m_device; }
        PD_FORCEINLINE const CVulkanDevice& GetDevice() const {return m_device; }

        PD_FORCEINLINE CVulkanSwapchain& GetSwapchain() {return m_swapchain; }
        PD_FORCEINLINE const CVulkanSwapchain& GetSwapchain() const {return m_swapchain; }

        PD_FORCEINLINE CSpriteBatch& GetSpriteBatch() {return m_spriteBatch; }
        PD_FORCEINLINE const CSpriteBatch& GetSpriteBatch() const {return m_spriteBatch; }

        PD_FORCEINLINE CVulkanTexture2D& GetWhitePixel() {return m_whitePixel; }
        PD_FORCEINLINE const CVulkanTexture2D& GetWhitePixel() const {return m_whitePixel; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_RENDERER_HPP
