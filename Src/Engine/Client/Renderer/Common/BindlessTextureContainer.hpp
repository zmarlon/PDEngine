//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#ifndef PD_ENGINE_CLIENT_RENDERER_COMMON_BINDLESSTEXTURECONTAINER_HPP
#define PD_ENGINE_CLIENT_RENDERER_COMMON_BINDLESSTEXTURECONTAINER_HPP

#pragma once

#include "../../../Core/STL/Vector.hpp"
#include "../Vulkan/Descriptor/VulkanDescriptorSetLayout.hpp"
#include "../Vulkan/Descriptor/VulkanDescriptorSet.hpp"

namespace PD
{
    class CPakFile;

    class CVulkanDevice;
    class CVulkanSampler;
    class CVulkanTexture2D;

    class CBindlessTextureTemplate
    {
    private:
        STL::CVector<const char*> m_suffixes;
        const char* m_pFormat = nullptr;
    public:
        void SetFormat(const char* pFormat)
        {
            PD_ASSERT(pFormat);
            m_pFormat = pFormat;
        }

        PD_FORCEINLINE void AddSuffix(const char* pSuffix)
        {
            PD_ASSERT(pSuffix);
            m_suffixes.push_back(pSuffix);
        }

        PD_FORCEINLINE const STL::CVector<const char*>& GetSuffixes() const {return m_suffixes; }
        PD_FORCEINLINE const char* GetFormat() const {return m_pFormat; }
    };

    class CBindlessTextureContainer
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CBindlessTextureContainer, CVulkanDevice&)
    protected:
        STL::CVector<CVulkanTexture2D*> m_textures;
        CVulkanDescriptorSetLayout m_descriptorSetLayout;
        CVulkanDescriptorSet m_descriptorSet;

        CVulkanDevice* m_pDevice;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Create(CVulkanDevice& device, u32 initialSize);
        u32 RegisterTexture(CPakFile& pakFile, const char* pName, const CBindlessTextureTemplate& textureTemplate);

        void Build(CVulkanSampler& sampler);
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_COMMON_BINDLESSTEXTURECONTAINER_HPP
