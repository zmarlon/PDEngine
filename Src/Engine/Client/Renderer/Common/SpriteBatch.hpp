//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 12.04.2020.
//
#ifndef PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEBATCH_HPP
#define PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEBATCH_HPP

#pragma once

#include "SpriteBatchTypes.hpp"
#include "../../../Core/STL/Vector.hpp"
#include "../../../Core/STL/String.hpp"
#include "../../../Core/Collections/Array.hpp"

#include "../Vulkan/Buffer/VulkanIndexBuffer.hpp"
#include "../Vulkan/Buffer/VulkanVertexBuffer.hpp"
#include "../Vulkan/Buffer/VulkanUniformBuffer.hpp"
#include "../Vulkan/Pipeline/VulkanShaderModule.hpp"
#include "../Vulkan/Pipeline/VulkanInputLayout.hpp"
#include "../Vulkan/Pipeline/VulkanPipelineLayout.hpp"
#include "../Vulkan/Pipeline/VulkanGraphicsPipeline.hpp"
#include "../Vulkan/Image/VulkanSampler.hpp"
#include "../Vulkan/Descriptor/VulkanDescriptorSet.hpp"
#include "../../../Core/String/StringHelper.hpp"

namespace PD
{
	//TODO: mit dieser SpriteBatch Implementation ist es nicht möglich, mehrfach mit begin und End in das gleiche RenderTarget reinzurendern.
	//Muss gefixt werden, da mit einem DrawCall nicht unendlich Sachen gebatcht werden können

	class CStaticFontMesh;
    class CVulkanTexture2D;
    class CSpriteFont;
    class CVulkanDevice;
    class CVulkanRenderPass;
    class CVulkanPrimaryCommandBuffer;
    class CSpriteBatch
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CSpriteBatch, CVulkanDevice&)
    private:
        static constexpr glm::vec2 _TEXCOORD_TL = glm::vec2(0.0f, 0.0f);
        static constexpr glm::vec2 _TEXCOORD_BR = glm::vec2(1.0f, 1.0f);

        bool m_drawing = false;

        static const usize _MAX_BATCH_SIZE = 2048;
        static const usize _MIN_BATCH_SIZE = 128;
        static const usize _INITIAL_QUEUE_SIZE = 64;
        static const usize _VERTICES_PER_SPRITE = 4;
        static const usize _INDICES_PER_SPRITE = 6;

        SSpriteBatchData m_data;
        glm::mat4 m_viewportMatrix;
        glm::mat4 m_totalMatrix;
        STL::CVector<SSpriteInfo*> m_sortedSprites;

        u32 m_numSpriteInfos;
        SSpriteInfo* m_pSpriteInfos;
        STL::CVector<SSpriteInfo> m_spriteInfos;

        CVulkanPrimaryCommandBuffer* m_pCommandBuffer;
        CVulkanRenderPass* m_pRenderPass;

        CVulkanIndexBuffer m_indexBuffer;
        CVulkanVertexBuffer m_vertexBuffer;
        CVulkanUniformBuffer m_uniformBuffer;

        CVulkanPipelineLayout m_pipelineLayout;
        std::array<CVulkanGraphicsPipeline, PD_SPRITEBATCH_NUM_PIPELINES> m_pipelines;
        std::array<CVulkanSampler, PD_SPRITEBATCH_NUM_SAMPLER_STATES> m_samplerStates;
        std::array<CVulkanDescriptorSet, PD_SPRITEBATCH_NUM_SAMPLER_STATES> m_descriptorSets;
        u32 m_vertexBufferPos = 0;
        CVulkanDevice* m_pDevice;

        void GrowSortedSprites();
        void SortSprites();
        void RenderSprite(const SSpriteInfo* pSprite, SSpriteBatchVertex* pVertices);
        void GenerateIndices(CArray<u16>& indices);

        void CreateIndexBuffer();
        void CreateVertexBuffer();
        void CreatePipelineLayout();
        void CreatePipeline(u32 blendStateIndex, u32 depthStencilStateIndex, u32 rasterizerStateIndex, CVulkanShaderModule& vertexShader, CVulkanShaderModule& fragmentShader,
                CVulkanInputLayoutBuilder& inputLayoutBuilder);
        void PrepareBatch(SSpriteInfo* pSpriteInfos, int numSpriteInfos);

        PD_FORCEINLINE usize
        GetPipelineIndex(u32 blendStateIndex, u32 depthStencilStateIndex, u32 rasterizerStateIndex) const
        {
            return blendStateIndex * PD_SPRITEBATCH_NUM_DEPTH_STENCIL_STATES * PD_SPRITEBATCH_NUM_RASTERIZER_STATES
                   + depthStencilStateIndex * PD_SPRITEBATCH_NUM_RASTERIZER_STATES + rasterizerStateIndex;
        }

        PD_FORCEINLINE usize GetCurrentPipelineIndex() const
        {
            return GetPipelineIndex(static_cast<u32>(m_data.BlendState), static_cast<u32>(m_data.DepthStencilState),
                                    static_cast<u32>(m_data.RasterizerState));
        }

        void RenderBatch(CVulkanPrimaryCommandBuffer& commandBuffer, CVulkanImageView* pTexture,
                         const SSpriteInfo* const* ppSpriteInfos, usize count);
        inline void CreateDescriptorSets();
        inline void PrepareForRendering();
        inline void FlushBatch(CVulkanPrimaryCommandBuffer& commandBuffer);

        void OnDisposing(CVulkanDevice& device);
    public:
        void Create(CVulkanDevice& device, CVulkanRenderPass& renderPass);

        PD_FORCEINLINE void SetCommandBuffer(CVulkanPrimaryCommandBuffer& commandBuffer)
        {
            m_pCommandBuffer = &commandBuffer;
        }

        void Flush(SSpriteInfo* pSpriteInfos, int numSpriteInfos);
        void Resize(u32 newWindowWidth, u32 newWindowHeight);

        void Begin(ESpriteSortMode spriteSortMode = ESpriteSortMode::Deferred, EBlendState blendState = EBlendState::Blended,
                           ESamplerState samplerState = ESamplerState::LinearClamp,
                           EDepthStencilState depthStencilState = EDepthStencilState::None,
                           ERasterizerState rasterizerState = ERasterizerState::None,
                           glm::mat4 transformationMatrix = glm::mat4(1.0f));
        void End();

        void Draw(CVulkanTexture2D& texture, const SRectangleF& destinationRectangle, const SColor4f& color,
                  glm::vec2 origin = glm::vec2(), float rotation = 0.0f, float depth = 0.0f);

        void Draw(CVulkanImageView& imageView, u32 width, u32 height, const SRectangleF& destinationRectangle, const SColor4f& color,
                glm::vec2 origin = glm::vec2(), float rotation = 0.0f, float depth = 0.0f);

        void Draw(CVulkanTexture2D& texture, const SRectangleF& destinationRectangle, const SRectangleF& sourceRectangle,
                  const SColor4f& color, glm::vec2 origin = glm::vec2(), float rotation = 0.0f, float depth = 0.0f);

        void Draw(CVulkanImageView& imageView, u32 width, u32 height, const SRectangleF& destinationRectangle, const SRectangleF& sourceRectangle,
                const SColor4f& color, glm::vec2 origin = glm::vec2(), float rotation = 0.0f, float depth = 0.0f);

        PD_FORCEINLINE void DrawString(CSpriteFont& spriteFont, const STL::CWString& message, glm::vec2 position, const SColor4f& color,
                                       glm::vec2 origin = glm::vec2(), float rotation = 0.0f, float depth = 0.0f)
        {
            DrawString(spriteFont, message.c_str(), message.length(), position, color, origin, rotation, depth);
        }

        PD_FORCEINLINE void DrawString(CSpriteFont& spriteFont, const wchar_t* pMessage, glm::vec2 position, const SColor4f& color,
                                       glm::vec2 origin = glm::vec2(), float rotation = 0.0f, float depth = 0.0f)
        {
            DrawString(spriteFont, pMessage, StringHelper::GetLength(pMessage), position, color, origin, rotation, depth);
        }

        void DrawString(CSpriteFont& spriteFont, const wchar_t* pMessage, usize length, glm::vec2 position, const SColor4f& color,
                        glm::vec2 origin = glm::vec2(), float rotation = 0.0f, float depth = 0.0f);

		void DrawString(CStaticFontMesh& fontMesh, glm::vec2 position, glm::vec2 origin = glm::vec2(), float rotation = 0.0f, float depth = 0.0f);
		void DrawStringWithColorOverride(CStaticFontMesh& fontMesh, glm::vec2 position, const SColor4f& color, glm::vec2 origin = glm::vec2(), float rotation = 0.0f, float depth = 0.0f);

		PD_FORCEINLINE SSpriteBatchData& GetData() {return m_data; }
		PD_FORCEINLINE const SSpriteBatchData& GetData() const {return m_data; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEBATCH_HPP
