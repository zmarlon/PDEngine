//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 17.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_COMMON_MODELMESH_HPP
#define PD_ENGINE_CLIENT_RENDERER_COMMON_MODELMESH_HPP

#pragma once

#include "../../../Core/Types.hpp"
#include "../../../Core/Memory/MemoryHelper.hpp"
#include "../../../Core/Disposable.hpp"
#include "../../../Core/Collections/Array.hpp"
#include "Vertices.hpp"
#include <filesystem>

namespace PD
{
	struct SPDModelHeader
	{
		char pSignature[8];
		usize VertexStride;
		usize VertexCount;
		usize IndexStride;
		usize IndexCount;

		SPDModelHeader(usize vertexStride, usize vertexCount, usize indexStride, usize indexCount) : VertexStride(vertexStride), VertexCount(vertexCount),
																									 IndexStride(indexStride), IndexCount(indexCount)
		{
			MemoryHelper::Copy(pSignature, "PDMODEL", 8);
		}
	};

	struct SPakLocation;

	class CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT
	{
		PD_MAKE_CLASS_DISPOSABLE(CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT)
	private:
		CArray<SVertex_POS_TEX_NORMAL_TANGENT_BITANGENT> m_vertices;
		CArray<u32> m_indices;

		void OnDisposing();
	public:
		void LoadFromPDModel(const SPakLocation& location);
		void LoadFromPDModel(const std::filesystem::path& filePath);
		void LoadFromObj(const SPakLocation& location);
		void LoadFromObjCached(const SPakLocation& location);

		void Save(const std::filesystem::path& filePath);

		PD_FORCEINLINE const CArray<SVertex_POS_TEX_NORMAL_TANGENT_BITANGENT>& GetVertices() const { return m_vertices; }

		PD_FORCEINLINE const CArray<u32>& GetIndices() const { return m_indices; }
	};

	typedef CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT CVertexModel;
}

#endif //PD_ENGINE_CLIENT_RENDERER_COMMON_MODELMESH_HPP
