//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 12.04.2020.
//
#ifndef PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEFONT_HPP
#define PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEFONT_HPP

#pragma once

#include "../Vulkan/Image/VulkanTexture2D.hpp"
#include "../../../Core/Font/FontAtlas.hpp"
#include "../../../Core/String/StringHelper.hpp"

namespace PD
{
    class CVulkanDevice;
    struct SPakLocation;
    class CSpriteFont
    {
    private:
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CSpriteFont, CVulkanDevice&)

        CVulkanTexture2D m_texture;
        CFontAtlas m_fontAtlas;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Load(CVulkanDevice& device, const SPakLocation& location, u32 fontSize);

        PD_FORCEINLINE CVulkanTexture2D& GetTexture() {return m_texture; }
        PD_FORCEINLINE float GetSize() const { return m_fontAtlas.GetData()->Size; }
        PD_FORCEINLINE float GetLineSpacing() const {return m_fontAtlas.GetData()->LineSpacing; }
        PD_FORCEINLINE const CFontAtlas::SGlyphInfo* GetGlyph(wchar_t value) const
        {
            return m_fontAtlas.GetGlyph(value);
        }

        PD_FORCEINLINE CFontAtlas& GetFontAtlas()
		{
        	return m_fontAtlas;
		}

        glm::vec2 MeasureString(const wchar_t* pMessage, usize length);

        PD_FORCEINLINE glm::vec2 MeasureString(const wchar_t* pMessage)
        {
            return MeasureString(pMessage, StringHelper::GetLength(pMessage));
        }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEFONT_HPP
