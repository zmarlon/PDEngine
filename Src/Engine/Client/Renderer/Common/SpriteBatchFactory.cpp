//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 01.05.2020.
//
#include "SpriteBatchFactory.hpp"
#include "SpriteBatch.hpp"
#include "../Renderer.hpp"

namespace PD
{
    PD_POSTDEFINE_SINGLETON(CSpriteBatchFactory)
    void CSpriteBatchFactory::OnDisposing()
    {
        DetachSingleton();
    }

    void CSpriteBatchFactory::Create()
    {
        AttachSingleton();

        PD_END_INITIALIZE;
    }

    CSpriteBatch* CSpriteBatchFactory::CreateBatch()
    {
        auto* pSpriteBatch = CProxyAllocator::GetInstance()->AllocateObject<CSpriteBatch>();
        pSpriteBatch->Create(CRenderer::GetInstance()->GetDevice(), CRenderer::GetInstance()->GetSwapchain().GetRenderPass());
        m_spriteBatches.push_back(pSpriteBatch);

        return pSpriteBatch;
    }

    void CSpriteBatchFactory::DestroyBatch(CVulkanDevice& device, CSpriteBatch* pSpriteBatch)
    {
        PD_ASSERT(pSpriteBatch);

        pSpriteBatch->Dispose(device);
        CProxyAllocator::GetInstance()->DeallocateObject(pSpriteBatch);

        std::remove(m_spriteBatches.begin(), m_spriteBatches.end(), pSpriteBatch);
    }

    void CSpriteBatchFactory::Resize(u32 newWidth, u32 newHeight)
    {
        for(auto* pSpriteBatch : m_spriteBatches) pSpriteBatch->Resize(newWidth, newHeight);
    }

    void CSpriteBatchFactory::SetCommandBuffer(CVulkanPrimaryCommandBuffer& commandBuffer)
    {
        for(auto* pSpriteBatch : m_spriteBatches) pSpriteBatch->SetCommandBuffer(commandBuffer);
    }
}