//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 01.05.2020.
//
#ifndef PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEBATCHFACTORY_HPP
#define PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEBATCHFACTORY_HPP

#pragma once

#include "../../../Core/STL/Vector.hpp"

namespace PD
{
    class CVulkanPrimaryCommandBuffer;
    class CSpriteBatch;
    class CVulkanDevice;
    class CSpriteBatchFactory
    {
        PD_MAKE_CLASS_DISPOSABLE(CSpriteBatchFactory);
        PD_MAKE_CLASS_SINGLETON(CSpriteBatchFactory);
    private:
        STL::CVector<CSpriteBatch*> m_spriteBatches;

        void OnDisposing();
    public:
        void Create();

        CSpriteBatch* CreateBatch();
        void DestroyBatch(CVulkanDevice& device, CSpriteBatch* pSpriteBatch);

        PD_FORCEINLINE void AddExisting(CSpriteBatch& spriteBatch)
        {
            m_spriteBatches.push_back(&spriteBatch);
        }

        void Resize(u32 newWidth, u32 newHeight);
        void SetCommandBuffer(CVulkanPrimaryCommandBuffer& commandBuffer);
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEBATCHFACTORY_HPP
