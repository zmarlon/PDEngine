//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_COMMON_STATICFONTMESH_HPP
#define PD_ENGINE_CLIENT_RENDERER_COMMON_STATICFONTMESH_HPP

#pragma once

#include "../../../Core/STL/Vector.hpp"
#include "../../../Core/STL/String.hpp"
#include "SpriteBatch.hpp"

namespace PD
{
	class CStaticFontMesh
	{
		friend class CGameConsole;
	private:
		STL::CVector<SSpriteInfo> m_spriteInfos;

		STL::CWString m_text;
		glm::vec2 m_size;

		//This method should only be used when the mesh does not contain a \n! falls du das hier ließt beiß mich nicht, ist das performanteste und unkomplizierteste <3
		void Append(CSpriteFont& spriteFont, const wchar_t* pString, const SColor4f& color);

		void Generate(CSpriteFont& spriteFont, const wchar_t* pString, const SColor4f& color);
		void GenerateScaled(CSpriteFont& spriteFont, const wchar_t* pString, const SColor4f& color, float scale);
	public:
		CStaticFontMesh() = default;
		CStaticFontMesh(CSpriteFont& spriteFont, const wchar_t* pString, const SColor4f& color);

		void Reset();
		void Reset(CSpriteFont& spriteFont, const wchar_t* pString, const SColor4f& color);

		PD_FORCEINLINE const STL::CWString& GetText() const { return m_text; }

		PD_FORCEINLINE const STL::CVector<SSpriteInfo>& GetSpriteInfos() const { return m_spriteInfos; }

		PD_FORCEINLINE glm::vec2& GetSize() { return m_size; }
	};
}

#endif //PD_ENGINE_CLIENT_RENDERER_COMMON_STATICFONTMESH_HPP
