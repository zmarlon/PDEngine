//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 17.04.2020.
//

#include "ObjParser.hpp"
#include "../../../Core/Pak/PakLocation.hpp"
#include "../../../Core/STL/String.hpp"

namespace PD
{
	const char* CObjParser::ParsePosition(const char* pCurrent)
	{
		glm::vec3 position;
		pCurrent = CharacterUtilities::ParseFloat(pCurrent, position.x);
		pCurrent = CharacterUtilities::ParseFloat(pCurrent, position.y);
		pCurrent = CharacterUtilities::ParseFloat(pCurrent, position.z);

		m_positions.push_back(position);

		return pCurrent;
	}

	const char* CObjParser::ParseTexCoord(const char* pCurrent)
	{
		glm::vec2 texCoord;
		pCurrent = CharacterUtilities::ParseFloat(pCurrent, texCoord.x);
		pCurrent = CharacterUtilities::ParseFloat(pCurrent, texCoord.y);

		m_texCoords.push_back(texCoord);

		return pCurrent;
	}

	const char* CObjParser::ParseNormal(const char* pCurrent)
	{
		glm::vec3 normal;
		pCurrent = CharacterUtilities::ParseFloat(pCurrent, normal.x);
		pCurrent = CharacterUtilities::ParseFloat(pCurrent, normal.y);
		pCurrent = CharacterUtilities::ParseFloat(pCurrent, normal.z);

		m_normals.push_back(normal);

		return pCurrent;
	}

	const char* CObjParser::ParseFace(const char* pCurrent)
	{
        u32 count;
        int positionIndex, texCoordIndex, normalIndex;
        SIndex index;

        pCurrent = SkipWhitespace(pCurrent);
        count = 0;

        while(!CharacterUtilities::IsNewLine(*pCurrent))
        {
            positionIndex = 0;
            texCoordIndex = 0;
            normalIndex = 0;

            pCurrent = CharacterUtilities::ParseInt(pCurrent, positionIndex);
            if(*pCurrent == '/')
            {
                pCurrent++;
                if(*pCurrent != '/') pCurrent = CharacterUtilities::ParseInt(pCurrent, texCoordIndex);
                if(*pCurrent == '/')
                {
                    pCurrent++;
                    pCurrent = CharacterUtilities::ParseInt(pCurrent, normalIndex);
                }
            }

            if(positionIndex < 0)
                index.PositionIndex = static_cast<u32>(m_positions.size()) - static_cast<u32>(-positionIndex);
            else
                index.PositionIndex = static_cast<u32>(positionIndex);

            if(texCoordIndex < 0)
                index.TexCoordIndex = static_cast<u32>(m_texCoords.size()) - static_cast<u32>(-texCoordIndex);
            else if(texCoordIndex > 0)
                index.TexCoordIndex = static_cast<u32>(texCoordIndex);
            else
                index.TexCoordIndex = 0;

            if(normalIndex < 0)
                index.NormalIndex = static_cast<u32>(m_normals.size()) - static_cast<u32>(-normalIndex);
            else if(normalIndex > 0)
                index.NormalIndex = static_cast<u32>(normalIndex);
            else
                index.NormalIndex = 0;

            m_indices.push_back(index);
            count++;

            pCurrent = SkipWhitespace(pCurrent);
        }

        m_faceVertexCounts.push_back(count);

        return pCurrent;
    }

	void CObjParser::Parse(const SPakLocation& location)
	{
		STL::CString buffer;
		location.LoadText(buffer);

		Parse(buffer.c_str(), buffer.size());
	}

	void CObjParser::Parse(const char* pData, usize length)
	{
		m_positions.reserve(_PREALLOCATION_SIZE);
		m_texCoords.reserve(_PREALLOCATION_SIZE);
		m_normals.reserve(_PREALLOCATION_SIZE);
		m_faceVertexCounts.reserve(_PREALLOCATION_SIZE);
		m_indices.reserve(_PREALLOCATION_SIZE * 3);

		const char* pCurrent = pData;
		const char* pEnd = pData + length;

		m_positions.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
		m_texCoords.push_back(glm::vec2(0.0f, 0.0f));
		m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));

		while(pCurrent != pEnd)
		{
			pCurrent = SkipWhitespace(pCurrent);

			switch(*pCurrent)
			{
				case 'v': pCurrent++;

					switch(*pCurrent++)
					{
						case ' ':
						case '\t': pCurrent = ParsePosition(pCurrent);
							break;
						case 't': pCurrent = ParseTexCoord(pCurrent);
							break;
						case 'n': pCurrent = ParseNormal(pCurrent);
							break;
						default: pCurrent--;
					}
					break;
				case 'f': pCurrent++;

					switch(*pCurrent++)
					{
						case ' ':
						case '\t': pCurrent = ParseFace(pCurrent);
							break;
						default: pCurrent--;
					}
					break;
				case '#': break;
			}

			pCurrent = SkipLine(pCurrent);
		}

		m_result = EParseResult::Ok;
	}
}