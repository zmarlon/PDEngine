//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 17.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_COMMON_VERTICES_HPP
#define PD_ENGINE_CLIENT_RENDERER_COMMON_VERTICES_HPP

#pragma once

#include <glm/glm.hpp>

namespace PD
{
	struct SVertex_POS_TEX_NORMAL_TANGENT_BITANGENT
	{
		glm::vec3 Position;
		glm::vec2 TexCoord;
		glm::vec3 Normal;
		glm::vec3 Tangent;
		glm::vec3 BiTangent;
	};
}

#endif //PD_ENGINE_CLIENT_RENDERER_COMMON_VERTICES_HPP
