//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//

#include "IndexedModel.hpp"
#include "ModelMesh.hpp"

namespace PD
{
	void CIndexedModel::OnDisposing()
	{
		m_indexBuffer.Dispose();
		m_vertexBuffer.Dispose();
	}

	void CIndexedModel::Finish(CVulkanDevice& device, const CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT& mesh)
	{
		m_vertexBuffer.Create(device, mesh.GetVertices().GetSize(), mesh.GetVertices().GetData());
		m_indexBuffer.Create(device, mesh.GetIndices().GetSize(), mesh.GetIndices().GetData());

		PD_END_INITIALIZE;
	}

	void CIndexedModel::LoadFromPDModel(CVulkanDevice& device, const SPakLocation& location)
	{
		CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT mesh;
		mesh.LoadFromPDModel(location);
		Finish(device, mesh);
		mesh.Dispose();
	}

	void CIndexedModel::LoadFromPDModel(CVulkanDevice& device, const std::filesystem::path& filePath)
	{
		CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT mesh;
		mesh.LoadFromPDModel(filePath);
		Finish(device, mesh);
		mesh.Dispose();
	}

	void CIndexedModel::LoadFromObj(CVulkanDevice& device, const SPakLocation& location)
	{
		CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT mesh;
		mesh.LoadFromObj(location);
		Finish(device, mesh);
		mesh.Dispose();
	}

	void CIndexedModel::LoadFromObjCached(CVulkanDevice& device, const SPakLocation& location)
	{
		CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT mesh;
		mesh.LoadFromObjCached(location);
		Finish(device, mesh);
		mesh.Dispose();
	}
}