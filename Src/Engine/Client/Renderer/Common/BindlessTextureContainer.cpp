//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#include "BindlessTextureContainer.hpp"
#include "../../../Core/STL/String.hpp"
#include "../../../Core/Pak/PakLocation.hpp"
#include "../Vulkan/VulkanDevice.hpp"
#include "../Vulkan/Image/VulkanSampler.hpp"
#include "../Vulkan/Image/VulkanTexture2D.hpp"
#include <array>

namespace PD
{
    void CBindlessTextureContainer::OnDisposing(CVulkanDevice& device)
    {
        m_descriptorSet.Dispose();
        m_descriptorSetLayout.Dispose(device);

        auto* pAllocator = CProxyAllocator::GetInstance();

        for(auto* pTexture : m_textures)
        {
            pTexture->Dispose(device);
            pAllocator->DeallocateObject(pTexture);
        }

        m_textures.clear();
    }

    void CBindlessTextureContainer::Create(CVulkanDevice& device, u32 initialSize)
    {
        PD_ASSERT(initialSize);

        m_pDevice = &device;

        m_textures.reserve(initialSize);
    }

    u32 CBindlessTextureContainer::RegisterTexture(CPakFile& pakFile, const char* pName, const CBindlessTextureTemplate& textureTemplate)
    {
        PD_ASSERT(pName);

        u32 currentIndex = static_cast<u32>(m_textures.size());

        const auto& suffixes = textureTemplate.GetSuffixes();
        for(const auto* pSuffix : suffixes)
        {
            STL::CString buffer;
            buffer.reserve(64);
            buffer += pName;
            buffer += '_';
            buffer += pSuffix;
            buffer += '.';
            buffer += textureTemplate.GetFormat();

            auto* pTexture = CProxyAllocator::GetInstance()->AllocateObject<CVulkanTexture2D>(); //TODO: use pool allocator
            pTexture->Load(*m_pDevice, SPakLocation(pakFile, buffer.c_str()));
            m_textures.push_back(pTexture);
        }

        return currentIndex;
    }

    void CBindlessTextureContainer::Build(CVulkanSampler& sampler)
    {
        u32 numTextures = static_cast<u32>(m_textures.size());

        CVulkanDescriptorSetLayoutBuilder dslBuilder;
        dslBuilder.AddBinding(0, VK_DESCRIPTOR_TYPE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
        dslBuilder.AddBinding(1, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT, numTextures);
        m_descriptorSetLayout.Create(*m_pDevice, dslBuilder, false);

        m_descriptorSet.Allocate(*m_pDevice, m_pDevice->GetDescriptorPool(), m_descriptorSetLayout);

        auto* pAllocator = CProxyAllocator::GetInstance();
        auto* pDescriptorImageInfos = pAllocator->AllocateObjects<VkDescriptorImageInfo>(numTextures);

        std::array<VkWriteDescriptorSet, 2> writeDescriptorSets;

        VkDescriptorImageInfo samplerDescriptorImageInfo;
        samplerDescriptorImageInfo.sampler = sampler.GetHandle();
        samplerDescriptorImageInfo.imageView = VK_NULL_HANDLE;
        samplerDescriptorImageInfo.imageLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        auto& samplerWriteDescriptorSet = writeDescriptorSets[0];
        samplerWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        samplerWriteDescriptorSet.pNext = nullptr;
        samplerWriteDescriptorSet.dstSet = m_descriptorSet.GetHandle();
        samplerWriteDescriptorSet.dstBinding = 0;
        samplerWriteDescriptorSet.dstArrayElement = 0;
        samplerWriteDescriptorSet.descriptorCount = 1;
        samplerWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
        samplerWriteDescriptorSet.pImageInfo = &samplerDescriptorImageInfo;
        samplerWriteDescriptorSet.pBufferInfo = nullptr;
        samplerWriteDescriptorSet.pTexelBufferView = nullptr;

        auto& texturesWriteDescriptorSet = writeDescriptorSets[1];
        texturesWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        texturesWriteDescriptorSet.pNext = nullptr;
        texturesWriteDescriptorSet.dstSet = m_descriptorSet.GetHandle();
        texturesWriteDescriptorSet.dstBinding = 1;
        texturesWriteDescriptorSet.dstArrayElement = 0;
        texturesWriteDescriptorSet.descriptorCount = numTextures;
        texturesWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
        texturesWriteDescriptorSet.pImageInfo = pDescriptorImageInfos;
        texturesWriteDescriptorSet.pBufferInfo = nullptr;
        texturesWriteDescriptorSet.pTexelBufferView = nullptr;

        for(u32 ix = 0; ix < numTextures; ix++)
        {
            auto& descriptorImageInfo = pDescriptorImageInfos[ix];
            descriptorImageInfo.sampler = VK_NULL_HANDLE;
            descriptorImageInfo.imageView = m_textures[ix]->GetImageView().GetHandle();
            descriptorImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        }

        m_descriptorSet.Update(writeDescriptorSets.data(), static_cast<u32>(writeDescriptorSets.size()));

        pAllocator->DeallocateObjects(pDescriptorImageInfos, numTextures);

        PD_END_INITIALIZE;
    }
}