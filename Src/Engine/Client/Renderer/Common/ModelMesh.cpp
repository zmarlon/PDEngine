//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 17.04.2020.
//

#include "ModelMesh.hpp"
#include "../../../Core/Logging/Logger.hpp"
#include "../../../Core/Pak/PakLocation.hpp"
#include "../../../Core/File/FileUtilities.hpp"
#include "../../../Core/String/StringHelper.hpp"
#include "ObjParser.hpp"
#include <meshoptimizer/meshoptimizer.h>

#define PDMODEL_HEADER_NAME "PDMODEL"

namespace PD
{
	void CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT::OnDisposing()
	{

	}

	void CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT::LoadFromPDModel(const SPakLocation& location)
	{
		CArray<byte> buffer;
		location.Load(buffer);

		auto* pHeader = reinterpret_cast<SPDModelHeader*>(buffer.GetData());
		if(strcmp(pHeader->pSignature, PDMODEL_HEADER_NAME) != 0)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed loading pd model from location "
														  << location << L" : Invalid signature" << CLogger::GetInstance()->Flush();
		}

		if(pHeader->VertexStride != sizeof(SVertex_POS_TEX_NORMAL_TANGENT_BITANGENT))
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed loading pd model from location "
														  << location << L" : Invalid vertex stride" << CLogger::GetInstance()->Flush();
		}
		if(pHeader->IndexStride != sizeof(u32))
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed loading pd model from location "
														  << location << L" : Invalid index stride" << CLogger::GetInstance()->Flush();
		}

		m_vertices.Allocate(pHeader->VertexCount, false);

		usize verticesOffset = sizeof(SPDModelHeader);
		usize verticesStride = sizeof(SVertex_POS_TEX_NORMAL_TANGENT_BITANGENT) * m_vertices.GetSize();
		MemoryHelper::Copy(m_vertices.GetData(), buffer.GetData() + verticesOffset, verticesStride);

		m_indices.Allocate(pHeader->IndexCount, false);
		usize indicesOffset = verticesOffset + verticesStride;
		usize indicesStride = sizeof(u32) * m_indices.GetSize();
		MemoryHelper::Copy(m_indices.GetData(), buffer.GetData() + indicesOffset, indicesStride);

		PD_END_INITIALIZE;
	}

	void CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT::LoadFromPDModel(const std::filesystem::path& filePath)
	{
		CArray<byte> buffer;
		FileUtilities::ReadBinaryFile(filePath.string().c_str(), buffer);

		auto* pHeader = reinterpret_cast<SPDModelHeader*>(buffer.GetData());
		if(strcmp(pHeader->pSignature, PDMODEL_HEADER_NAME) != 0)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed loading pd model from file "
														  << filePath << L" : Invalid signature" << CLogger::GetInstance()->Flush();
		}

		if(pHeader->VertexStride != sizeof(SVertex_POS_TEX_NORMAL_TANGENT_BITANGENT))
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed loading pd model from file "
														  << filePath << L" : Invalid vertex stride" << CLogger::GetInstance()->Flush();
		}
		if(pHeader->IndexStride != sizeof(u32))
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed loading pd model from file "
														  << filePath << L" : Invalid index stride" << CLogger::GetInstance()->Flush();
		}

		m_vertices.Allocate(pHeader->VertexCount, false);

		usize verticesOffset = sizeof(SPDModelHeader);
		usize verticesStride = sizeof(SVertex_POS_TEX_NORMAL_TANGENT_BITANGENT) * m_vertices.GetSize();
		MemoryHelper::Copy(m_vertices.GetData(), buffer.GetData() + verticesOffset, verticesStride);

		m_indices.Allocate(pHeader->IndexCount, false);
		usize indicesOffset = verticesOffset + verticesStride;
		usize indicesStride = sizeof(u32) * m_indices.GetSize();
		MemoryHelper::Copy(m_indices.GetData(), buffer.GetData() + indicesOffset, indicesStride);

		PD_END_INITIALIZE;
	}

	void CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT::LoadFromObj(const SPakLocation& location)
	{
		CObjParser objParser(location);
		if(objParser.GetResult() != CObjParser::EParseResult::Ok)
		{
			CLogger::GetInstance()->Log(ELogLevel::Fatal) << L"Failed to parse obj file at location " << location << CLogger::GetInstance()->Flush();
		}

		const auto& positions = objParser.GetPositions();
		const auto& texCoords = objParser.GetTexCoords();
		const auto& normals = objParser.GetNormals();
		const auto& faceVertexCounts = objParser.GetFaceVertexCounts();
		const auto& faceIndices = objParser.GetIndices();

		usize indexCount = 0;
		for(usize ix = 0; ix < faceVertexCounts.size(); ix++)
		{
			indexCount += faceVertexCounts[ix];
		}

		using V = SVertex_POS_TEX_NORMAL_TANGENT_BITANGENT;

		STL::CVector<V> objVertices(indexCount);

		usize vertexOffset = 0, indexOffset = 0;

		for(usize ix = 0; ix < faceVertexCounts.size(); ix++)
		{
			PD_ASSERT(faceVertexCounts[ix]);
			for(usize iy = 0; iy < faceVertexCounts[ix]; iy++)
			{
				auto index = faceIndices[indexOffset + iy];
				auto& vertex = objVertices[vertexOffset++];

				vertex.Position = positions[index.PositionIndex];
				vertex.TexCoord = texCoords[index.TexCoordIndex];
				vertex.Normal = normals[index.NormalIndex];
				vertex.Tangent = {};
				vertex.BiTangent = {}; //TODO:
			}
			indexOffset += faceVertexCounts[ix];
		}

		PD_ASSERT(vertexOffset == indexCount);

		STL::CVector<u32> remap(indexCount);
		usize vertexCount = meshopt_generateVertexRemap(remap.data(), nullptr, indexCount, objVertices.data(), indexCount, sizeof(V));

		m_vertices.Allocate(vertexCount, false);
		m_indices.Allocate(indexCount, false);

		meshopt_remapVertexBuffer(m_vertices.GetData(), objVertices.data(), indexCount, sizeof(V), remap.data());
		meshopt_remapIndexBuffer(m_indices.GetData(), nullptr, indexCount, remap.data());

		meshopt_optimizeVertexCache(m_indices.GetData(), m_indices.GetData(), indexCount, vertexCount);
		meshopt_optimizeVertexFetch(m_vertices.GetData(), m_indices.GetData(), indexCount, m_vertices.GetData(), vertexCount, sizeof(V));

		PD_END_INITIALIZE;
	}

	void CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT::LoadFromObjCached(const SPakLocation& location)
	{
		std::filesystem::path gameDataDirectory;
		FileUtilities::GetGameDataDirectory(gameDataDirectory);

		std::filesystem::path modelCachePath = gameDataDirectory / L"ModelCache";

		if(!std::filesystem::exists(modelCachePath) || !std::filesystem::is_directory(modelCachePath))
		{
			std::filesystem::create_directories(modelCachePath);
		}

		usize offset = StringHelper::GetLength(location.pEntryName) - 1;
		while(location.pEntryName[offset] != '.') { offset--; }

		char* pNameBuffer = (char*)CProxyAllocator::GetInstance()->Allocate(offset + 9);
		MemoryHelper::Copy(pNameBuffer, location.pEntryName, offset + 1);
		MemoryHelper::Copy(pNameBuffer + offset + 1, "pdmodel", 8);

		std::filesystem::path cachedModelPath = modelCachePath / pNameBuffer;
		if(std::filesystem::exists(cachedModelPath))
		{
			LoadFromPDModel(cachedModelPath);
		}
		else
		{
			LoadFromObj(location);
			Save(cachedModelPath);
		}
	}

	void CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT::Save(const std::filesystem::path& filePath)
	{
		usize verticesOffset = sizeof(SPDModelHeader);
		usize verticesStride = m_vertices.GetSize() * sizeof(SVertex_POS_TEX_NORMAL_TANGENT_BITANGENT);

		usize indicesOffset = verticesOffset + verticesStride;
		usize indicesStride = m_indices.GetSize() * sizeof(u32);

		usize totalSize = sizeof(SPDModelHeader) + verticesStride + indicesStride;

		SPDModelHeader header(sizeof(SVertex_POS_TEX_NORMAL_TANGENT_BITANGENT), m_vertices.GetSize(), sizeof(u32), m_indices.GetSize());

		STL::CVector<byte> buffer(totalSize);

		MemoryHelper::Copy(buffer.data(), &header, sizeof(SPDModelHeader));

		MemoryHelper::Copy(((u8*)buffer.data()) + verticesOffset, m_vertices.GetData(), verticesStride);
		MemoryHelper::Copy(((u8*)buffer.data()) + indicesOffset, m_indices.GetData(), indicesStride);

		CLogger::GetInstance()->Log(ELogLevel::Warning) << filePath.string().c_str() << CLogger::GetInstance()->Flush();

		if(!FileUtilities::WriteBinaryFile(filePath.string().c_str(), buffer))
		{
			CLogger::GetInstance()->Log(ELogLevel::Warning) << L"Failed to write pd model to file: " << filePath.c_str() << CLogger::GetInstance()->Flush();
		}
	}
}