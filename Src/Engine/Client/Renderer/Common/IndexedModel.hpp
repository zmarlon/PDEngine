//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_COMMON_INDEXEDMODEL_HPP
#define PD_ENGINE_CLIENT_RENDERER_COMMON_INDEXEDMODEL_HPP

#pragma once

#include "../Vulkan/Buffer/VulkanVertexBuffer.hpp"
#include "../Vulkan/Buffer/VulkanIndexBuffer.hpp"
#include <filesystem>

namespace PD
{
	struct SPakLocation;
	class CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT;
	class CVulkanDevice;
	class CIndexedModel
	{
		PD_MAKE_CLASS_DISPOSABLE(CIndexedModel)
	private:
		CVulkanVertexBuffer m_vertexBuffer;
		CVulkanIndexBuffer m_indexBuffer;

		void OnDisposing();

		inline void Finish(CVulkanDevice& device, const CVertexModel_POS_TEX_NORMAL_TANGENT_BITANGENT& mesh);
	public:
		void LoadFromPDModel(CVulkanDevice& device, const SPakLocation& location);
		void LoadFromPDModel(CVulkanDevice& device, const std::filesystem::path& filePath);
		void LoadFromObj(CVulkanDevice& device, const SPakLocation& location);
		void LoadFromObjCached(CVulkanDevice& device, const SPakLocation& location);

		PD_FORCEINLINE CVulkanVertexBuffer& GetVertexBuffer() {return m_vertexBuffer; }
		PD_FORCEINLINE CVulkanIndexBuffer& GetIndexBuffer() {return m_indexBuffer; }
	};
}

#endif //PD_ENGINE_CLIENT_RENDERER_COMMON_INDEXEDMODEL_HPP