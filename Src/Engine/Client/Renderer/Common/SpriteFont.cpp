//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 12.04.2020.
//
#include "SpriteFont.hpp"

namespace PD
{
    void CSpriteFont::OnDisposing(CVulkanDevice& device)
    {
        m_texture.Dispose(device);
        m_fontAtlas.Dispose();
    }

    void CSpriteFont::Load(CVulkanDevice& device, const SPakLocation& location, u32 fontSize)
    {
        m_fontAtlas.Load(*CFontLibrary::GetInstance(), location, fontSize);
        const auto& bitmap = m_fontAtlas.GetBitmap();

        m_texture.Load(device, bitmap.GetWidth(), bitmap.GetHeight(), VK_FORMAT_R8_UNORM,
                sizeof(u8), bitmap.GetData(), VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
                VK_COMPONENT_SWIZZLE_ONE, VK_COMPONENT_SWIZZLE_ONE, VK_COMPONENT_SWIZZLE_ONE, VK_COMPONENT_SWIZZLE_R);
        m_fontAtlas.UnloadBitmap();

        PD_END_INITIALIZE;
    }

    glm::vec2 CSpriteFont::MeasureString(const wchar_t* pMessage, usize length)
    {
        glm::vec2 offset(0.0f, GetLineSpacing());
        bool firstGlyphOfLine = true;

        float tempOffsetX = 0.0f;
        for(usize ix = 0; ix < length; ix++)
        {
            const wchar_t c = pMessage[ix];
            if(c == L'\r') continue;
            if(c == L'\n')
            {
                if(tempOffsetX > offset.x) offset.x = tempOffsetX;
                tempOffsetX = 0.0f;
                offset.y += GetLineSpacing();
                continue;
            }

            const auto* pGlyphInfo = m_fontAtlas.GetGlyph(c);
            if(!pGlyphInfo) continue;

            tempOffsetX += pGlyphInfo->XAdvance;
        }

        if(tempOffsetX > offset.x) offset.x = tempOffsetX;

        return offset;

    }
}