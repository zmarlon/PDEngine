//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//

#include "StaticFontMesh.hpp"
#include "SpriteFont.hpp"

namespace PD
{
	void CStaticFontMesh::Append(CSpriteFont& spriteFont, const wchar_t* pString, const SColor4f& color)
	{
		m_text += pString;

		bool firstGlyphOfLine = false;

		auto& texture = spriteFont.GetTexture();
		auto textureWidth = static_cast<float>(texture.GetWidth()), textureHeight = static_cast<float>(texture.GetHeight());
		u32 fontSize = spriteFont.GetSize();

		glm::vec2 offset = glm::vec2(m_size.x, 0.0f);
		for(int ix = 0; ix < wcslen(pString); ix++)
		{
			wchar_t c = pString[ix];
			if(c == '\r') continue;
			if(c == '\n')
			{
				if(offset.x > m_size.x) m_size.x = offset.x;
				offset.x = 0.0f;
				offset.y += spriteFont.GetLineSpacing();
				m_size.y += spriteFont.GetLineSpacing();
				firstGlyphOfLine = true;
				continue;
			}

			const CFontAtlas::SGlyphInfo* pGlyphInfo = spriteFont.GetGlyph(c);
			if(!pGlyphInfo) continue;

			float xCharOffset = pGlyphInfo->OffsetX;
			if(firstGlyphOfLine)
			{
				if(pGlyphInfo->OffsetX < 0.0f)
					xCharOffset = 0.0f;
				else
					xCharOffset = pGlyphInfo->OffsetX;

				firstGlyphOfLine = false;
			}

			SSpriteInfo spriteInfo;
			spriteInfo.TexCoordTL = glm::vec2(pGlyphInfo->TexCoordX / textureWidth,
											  pGlyphInfo->TexCoordY / textureHeight);
			spriteInfo.TexCoordBR = glm::vec2((pGlyphInfo->TexCoordX + pGlyphInfo->Width) / textureWidth,
											  (pGlyphInfo->TexCoordY + pGlyphInfo->Height) / textureHeight);
			spriteInfo.Destination = SRectangleF(offset.x + xCharOffset,
												 offset.y + (fontSize - pGlyphInfo->OffsetY),
												 pGlyphInfo->Width, pGlyphInfo->Height);
			spriteInfo.Color = color;
			spriteInfo.Origin = glm::vec2();
			spriteInfo.Rotation = 0.0f;
			spriteInfo.Depth = 0.0f;
			spriteInfo.pImageView = &texture.GetImageView();

			m_spriteInfos.push_back(spriteInfo);

			offset.x += pGlyphInfo->XAdvance;
		}

		if(offset.x > m_size.x) m_size.x = offset.x;
		m_size.y += spriteFont.GetLineSpacing();
	}

	void CStaticFontMesh::Generate(CSpriteFont& spriteFont, const wchar_t* pString, const SColor4f& color)
	{
		m_text = pString;
		bool firstGlyphOfLine = true;

		auto& texture = spriteFont.GetTexture();
		float textureWidth = static_cast<float>(texture.GetWidth()), textureHeight = static_cast<float>(texture.GetHeight());
		u32 fontSize = spriteFont.GetSize();

		glm::vec2 offset = glm::vec2();
		for(int ix = 0; ix < wcslen(pString); ix++)
		{
			wchar_t c = pString[ix];
			if(c == '\r') continue;
			if(c == '\n')
			{
				if(offset.x > m_size.x) m_size.x = offset.x;
				offset.x = 0.0f;
				offset.y += spriteFont.GetLineSpacing();
				m_size.y += spriteFont.GetLineSpacing();
				firstGlyphOfLine = true;
				continue;
			}

			const CFontAtlas::SGlyphInfo* pGlyphInfo = spriteFont.GetGlyph(c);
			if(!pGlyphInfo) continue;

			float xCharOffset = pGlyphInfo->OffsetX;
			if(firstGlyphOfLine)
			{
				if(pGlyphInfo->OffsetX < 0.0f)
					xCharOffset = 0.0f;
				else
					xCharOffset = pGlyphInfo->OffsetX;

				firstGlyphOfLine = false;
			}

			SSpriteInfo spriteInfo;
			spriteInfo.TexCoordTL = glm::vec2(pGlyphInfo->TexCoordX / textureWidth,
											  pGlyphInfo->TexCoordY / textureHeight);
			spriteInfo.TexCoordBR = glm::vec2((pGlyphInfo->TexCoordX + pGlyphInfo->Width) / textureWidth,
											  (pGlyphInfo->TexCoordY + pGlyphInfo->Height) / textureHeight);
			spriteInfo.Destination = SRectangleF(offset.x + xCharOffset,
												 offset.y + (fontSize - pGlyphInfo->OffsetY),
												 pGlyphInfo->Width, pGlyphInfo->Height);
			spriteInfo.Color = color;
			spriteInfo.Origin = glm::vec2();
			spriteInfo.Rotation = 0.0f;
			spriteInfo.Depth = 0.0f;
			spriteInfo.pImageView = &texture.GetImageView();

			m_spriteInfos.push_back(spriteInfo);

			offset.x += pGlyphInfo->XAdvance;
		}

		if(offset.x > m_size.x) m_size.x = offset.x;
		m_size.y += spriteFont.GetLineSpacing();
	}

	void CStaticFontMesh::GenerateScaled(CSpriteFont& spriteFont, const wchar_t* pString, const SColor4f& color, float scale)
	{
		//TODO:
	}

	CStaticFontMesh::CStaticFontMesh(CSpriteFont& spriteFont, const wchar_t* pString, const SColor4f& color)
	{
		Generate(spriteFont, pString, color);
	}

	void CStaticFontMesh::Reset()
	{
		m_spriteInfos.clear();
		m_size = glm::vec2();
	}

	void CStaticFontMesh::Reset(CSpriteFont& spriteFont, const wchar_t* pString, const SColor4f& color)
	{
		Generate(spriteFont, pString, color);
	}
}