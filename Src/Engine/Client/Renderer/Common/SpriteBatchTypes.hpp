//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 12.04.2020.
//
#ifndef PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEBATCHTYPES_HPP
#define PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEBATCHTYPES_HPP

#pragma once

#include "../../../Core/Math/Rectangle.hpp"
#include "../../../Core/Math/Color4f.hpp"
#include "../../../Core/Types.hpp"
#include "../../../Core/Disposable.hpp"
#include <glm/glm.hpp>

namespace PD
{
    class CVulkanImageView;

#define PD_SPRITEBATCH_NUM_SPRITE_SORT_MODES 4
    enum class ESpriteSortMode : u32
    {
        Deferred = 0,
        Texture = 1,
        BackToFront = 2,
        FrontToBack = 3
    };

#define PD_SPRITEBATCH_NUM_BLEND_STATES 3
    enum class EBlendState : u32
    {
        Opaque = 0,
        Blended = 1,
        Additive = 2
    };

#define PD_SPRITEBATCH_NUM_DEPTH_STENCIL_STATES 3
    enum class EDepthStencilState : u32
    {
        None = 0,
        Default = 1,
        ReadOnly = 2
    };

#define PD_SPRITEBATCH_NUM_RASTERIZER_STATES 4
    enum class ERasterizerState : u32
    {
        None = 0,
        Clockwise = 1,
        CounterClockwise = 2,
        Wireframe = 3
    };

#define PD_SPRITEBATCH_NUM_SAMPLER_STATES 6
    enum class ESamplerState : u32
    {
        PointWrap = 0,
        PointClamp = 1,
        LinearWrap = 2,
        LinearClamp = 3,
        AnisotropicWrap = 4,
        AnisotropicClamp = 5
    };

#define PD_SPRITEBATCH_NUM_PIPELINES (PD_SPRITEBATCH_NUM_BLEND_STATES * PD_SPRITEBATCH_NUM_DEPTH_STENCIL_STATES * PD_SPRITEBATCH_NUM_RASTERIZER_STATES)

    struct SSpriteInfo
    {
        SRectangleF Destination;
        glm::vec2 TexCoordTL;
        glm::vec2 TexCoordBR;
        SColor4f Color;
        glm::vec2 Origin;
        float Rotation;
        float Depth;
        CVulkanImageView* pImageView;
    };

    struct SSpriteBatchVertex
    {
        glm::vec3 Position;
        SColor4f Color;
        glm::vec2 TexCoord;
    };

    struct SSpriteBatchData
    {
        ESpriteSortMode SpriteSortMode;
        EBlendState BlendState;
        ESamplerState SamplerState;
        EDepthStencilState DepthStencilState;
        ERasterizerState RasterizerState;
        glm::mat4 TransformationMatrix;
    };

}

#endif //PD_ENGINE_CLIENT_RENDERER_COMMON_SPRITEBATCHTYPES_HPP
