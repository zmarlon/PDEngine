//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 17.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_COMMON_OBJPARSER_HPP
#define PD_ENGINE_CLIENT_RENDERER_COMMON_OBJPARSER_HPP

#pragma once

#include "../../../Core/Types.hpp"
#include "../../../Core/Singleton.hpp"
#include "../../../Core/STL/Vector.hpp"
#include "../../../Core/String/CharacterUtilities.hpp"
#include <glm/glm.hpp>

namespace PD
{
	struct SIndex
	{
		u32 PositionIndex;
		u32 TexCoordIndex;
		u32 NormalIndex;
	};

	struct SPakLocation;
	class CObjParser
	{
	public:
		enum class EParseResult
		{
			Idle,
			Ok,
			Failed
		};
	private:
		STL::CVector<u32> m_faceVertexCounts;
		STL::CVector<SIndex> m_indices;
		STL::CVector<glm::vec3> m_positions;
		STL::CVector<glm::vec2> m_texCoords;
		STL::CVector<glm::vec3> m_normals;
		EParseResult m_result = EParseResult::Idle;

		static const u32 _PREALLOCATION_SIZE = 65536;

        PD_FORCEINLINE const char* SkipWhitespace(const char* pCurrent)
        {
            while(CharacterUtilities::IsWhitespace(*pCurrent)) pCurrent++;
            return pCurrent;
        }

        PD_FORCEINLINE const char* SkipLine(const char* pCurrent)
        {
            while(!CharacterUtilities::IsNewLine(*pCurrent++)) {}
            return pCurrent;
        }

        const char* ParsePosition(const char* pCurrent);
		const char* ParseTexCoord(const char* pCurrent);
		const char* ParseNormal(const char* pCurrent);
		const char* ParseFace(const char* pCurrent);
	public:
		CObjParser() = default;
		PD_FORCEINLINE explicit CObjParser(const SPakLocation& location)
		{
			Parse(location);
		}

		PD_FORCEINLINE explicit CObjParser(const char* pData, usize length)
		{
			Parse(pData, length);
		}

		void Parse(const SPakLocation& location);
		void Parse(const char* pData, usize length);

		const STL::CVector<u32>& GetFaceVertexCounts() const { return m_faceVertexCounts; }
		const STL::CVector<SIndex>& GetIndices() const { return m_indices; }
		const STL::CVector<glm::vec3>& GetPositions() const { return m_positions; }
		const STL::CVector<glm::vec2>& GetTexCoords() const { return m_texCoords; }
		const STL::CVector<glm::vec3>& GetNormals() const { return m_normals; }

		PD_FORCEINLINE EParseResult GetResult() const { return m_result; }
	};
}

#endif //PD_ENGINE_CLIENT_RENDERER_COMMON_OBJPARSER_HPP
