//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 12.04.2020.
//
#include "SpriteBatch.hpp"
#include "SpriteFont.hpp"
#include "../Vulkan/VulkanDevice.hpp"
#include "../../../Core/Window/Window.hpp"
#include "../../../Core/Logging/Logger.hpp"
#include "../../../Core/Math/MatrixMath.hpp"
#include "../../../Core/Pak/PakManager.hpp"
#include "../../../Core/Pak/PakLocation.hpp"
#include "StaticFontMesh.hpp"

namespace PD
{
    void CSpriteBatch::OnDisposing(CVulkanDevice& device)
    {
        for(auto& pipeline : m_pipelines) pipeline.Dispose(device);

        m_pipelineLayout.Dispose(device);

        for(u32 ix = 0; ix < PD_SPRITEBATCH_NUM_SAMPLER_STATES; ix++) m_descriptorSets[ix].Dispose();
        for(u32 ix = 0; ix < PD_SPRITEBATCH_NUM_SAMPLER_STATES; ix++) m_samplerStates[ix].Dispose(device);

        m_uniformBuffer.Dispose();
        m_vertexBuffer.Dispose();
        m_indexBuffer.Dispose();
    }

    void CSpriteBatch::GrowSortedSprites()
    {
        usize previousSize = m_sortedSprites.size();
        m_sortedSprites.resize(m_numSpriteInfos);

        for(usize ix = previousSize; ix < m_numSpriteInfos; ix++) m_sortedSprites[ix] = m_pSpriteInfos + ix;
    }

    void CSpriteBatch::SortSprites()
    {
        if(m_sortedSprites.size() < m_numSpriteInfos) GrowSortedSprites();
        for(usize ix = 0; ix < m_numSpriteInfos; ix++) m_sortedSprites[ix] = m_pSpriteInfos + ix;

        switch(m_data.SpriteSortMode)
        {
            case ESpriteSortMode::Texture:
                std::sort(m_sortedSprites.begin(), m_sortedSprites.begin() + static_cast<int>(m_numSpriteInfos),
                          [](SSpriteInfo const* x, SSpriteInfo const* y) -> bool
                          {
                              return x->pImageView < y->pImageView;
                          });
                break;

            case ESpriteSortMode::BackToFront:
                std::sort(m_sortedSprites.begin(), m_sortedSprites.begin() + static_cast<int>(m_numSpriteInfos),
                          [](SSpriteInfo const* x, SSpriteInfo const* y) -> bool
                          {
                              return x->Depth > y->Depth;
                          });
                break;

            case ESpriteSortMode::FrontToBack:
                std::sort(m_sortedSprites.begin(), m_sortedSprites.begin() + static_cast<int>(m_numSpriteInfos),
                          [](SSpriteInfo const* x, SSpriteInfo const* y) -> bool
                          {
                              return x->Depth < y->Depth;
                          });
                break;
            default:break;
        }
    }

    void CSpriteBatch::RenderSprite(const SSpriteInfo* pSprite, SSpriteBatchVertex* pVertices)
    {
        const auto& color = pSprite->Color;

        const auto& texCoordTL = pSprite->TexCoordTL;
        const auto& texCoordBR = pSprite->TexCoordBR;

        float rotation = pSprite->Rotation;
        if(rotation != 0.0f)
        {
            float sin = glm::sin(rotation);
            float cos = glm::cos(rotation);
            float x = pSprite->Destination.X;
            float y = pSprite->Destination.Y;
            float dx = -pSprite->Origin.x;
            float dy = -pSprite->Origin.y;
            float w = pSprite->Destination.Width;
            float h = pSprite->Destination.Height;

            pVertices[0].Position.x = x + dx * cos - dy * sin;
            pVertices[0].Position.y = y + dx * sin + dy * cos;
            pVertices[0].Position.z = rotation;
            pVertices[0].Color = color;
            pVertices[0].TexCoord = texCoordTL;

            pVertices[1].Position.x = x + (dx + w) * cos - dy * sin;
            pVertices[1].Position.y = y + (dx + w) * sin + dy * cos;
            pVertices[1].Position.z = rotation;
            pVertices[1].Color = color;
            pVertices[1].TexCoord.x = texCoordBR.x;
            pVertices[1].TexCoord.y = texCoordTL.y;

            pVertices[2].Position.x = x + dx * cos - (dy + h) * sin;
            pVertices[2].Position.y = y + dx * sin + (dy + h) * cos;
            pVertices[2].Position.z = rotation;
            pVertices[2].Color = color;
            pVertices[2].TexCoord.x = texCoordTL.x;
            pVertices[2].TexCoord.y = texCoordBR.y;

            pVertices[3].Position.x = x + (dx + w) * cos - (dy + h) * sin;
            pVertices[3].Position.y = y + (dx + w) * sin + (dy + h) * cos;
            pVertices[3].Position.z = rotation;
            pVertices[3].Color = color;
            pVertices[3].TexCoord = texCoordBR;
        } else
        {
            float x = pSprite->Destination.X;
            float y = pSprite->Destination.Y;
            float xw = x + pSprite->Destination.Width;
            float yh = y + pSprite->Destination.Height;

            pVertices[0].Position.x = x;
            pVertices[0].Position.y = y;
            pVertices[0].Position.z = rotation;
            pVertices[0].Color = pSprite->Color;
            pVertices[0].TexCoord = texCoordTL;

            pVertices[1].Position.x = xw;
            pVertices[1].Position.y = y;
            pVertices[1].Position.z = rotation;
            pVertices[1].Color = color;
            pVertices[1].TexCoord.x = texCoordBR.x;
            pVertices[1].TexCoord.y = texCoordTL.y;

            pVertices[2].Position.x = x;
            pVertices[2].Position.y = yh;
            pVertices[2].Position.z = rotation;
            pVertices[2].Color = color;
            pVertices[2].TexCoord.x = texCoordTL.x;
            pVertices[2].TexCoord.y = texCoordBR.y;

            pVertices[3].Position.x = xw;
            pVertices[3].Position.y = yh;
            pVertices[3].Position.z = rotation;
            pVertices[3].Color = color;
            pVertices[3].TexCoord = texCoordBR;
        }
    }

    void CSpriteBatch::GenerateIndices(CArray<u16>& indices)
    {
        const usize indexCount = _MAX_BATCH_SIZE * _INDICES_PER_SPRITE;
        static_assert(_MAX_BATCH_SIZE * _INDICES_PER_SPRITE < std::numeric_limits<u16>::max(),
                      "Batch size must fit into UINT16T");

        indices.Allocate(indexCount, false);

        usize offset = 0;
        u16 currentIndex = 0;
        for(int ix = 0; ix < _MAX_BATCH_SIZE; ix++, currentIndex += 4)
        {
            indices[offset++]= currentIndex;
            indices[offset++]= currentIndex + 1;
            indices[offset++]= currentIndex + 2;
            indices[offset++]= currentIndex + 1;
            indices[offset++]= currentIndex + 3;
            indices[offset++]= currentIndex + 2;
        }
    }

    void CSpriteBatch::CreateIndexBuffer()
    {
        CArray<u16> indices;
        GenerateIndices(indices);

        m_indexBuffer.Create(*m_pDevice, static_cast<u32>(indices.GetSize()), indices.GetData());
    }

    void CSpriteBatch::CreateVertexBuffer()
    {
        usize vertexCount = _MAX_BATCH_SIZE * _VERTICES_PER_SPRITE;
        m_vertexBuffer.Create<SSpriteBatchVertex>(*m_pDevice, static_cast<u32>(vertexCount), nullptr, 0, VMA_MEMORY_USAGE_CPU_TO_GPU);
    }

    void CSpriteBatch::CreatePipelineLayout()
    {
        auto& templates = m_pDevice->GetDSLTemplates();

        CVulkanPipelineLayoutBuilder builder;
        builder.ReserveLayouts(2);
        builder.AddDescriptorSetLayout(templates.GetLayout_S_FS_U_VS());
        builder.AddDescriptorSetLayout(templates.GetLayoutPD_SI_FS()); 
        m_pipelineLayout.Create(*m_pDevice, builder);
    }

    void CSpriteBatch::CreatePipeline(u32 blendStateIndex, u32 depthStencilStateIndex, u32 rasterizerStateIndex, CVulkanShaderModule& vertexShader, CVulkanShaderModule& fragmentShader,
            CVulkanInputLayoutBuilder& inputLayoutBuilder)
    {
        CVulkanGraphicsPipelineBuilder builder;

        auto& pipeline = m_pipelines[GetPipelineIndex(blendStateIndex, depthStencilStateIndex, rasterizerStateIndex)];

        builder.SetViewport(0.0f, 0.0f, 1.0f, 1.0f);
        builder.SetScissorRect(0, 0, 1, 1);
        builder.SetPrimitiveTopology(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);

        builder.ReserveShader(2);
        builder.AddShader(vertexShader);
        builder.AddShader(fragmentShader);
        builder.SetInputLayout(inputLayoutBuilder);

        builder.ReserveDynamicStates(2);
        builder.AddDynamicState(VK_DYNAMIC_STATE_VIEWPORT);
        builder.AddDynamicState(VK_DYNAMIC_STATE_SCISSOR);

        builder.SetMultisampleState(1);

        pipeline.SetLayout(m_pipelineLayout);

        switch(blendStateIndex)
        {
            case 0:builder.AddColorBlendAttachment(VK_FALSE);
                break;
            case 1:
                builder.AddColorBlendAttachment(VK_TRUE, VK_BLEND_FACTOR_SRC_ALPHA,
                                                 VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
                                                 VK_BLEND_OP_ADD, VK_BLEND_FACTOR_SRC_ALPHA,
                                                 VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA, VK_BLEND_OP_ADD);
                break;
            case 2:
                builder.AddColorBlendAttachment(VK_TRUE, VK_BLEND_FACTOR_SRC_ALPHA, VK_BLEND_FACTOR_ONE,
                                                 VK_BLEND_OP_ADD,
                                                 VK_BLEND_FACTOR_SRC_ALPHA, VK_BLEND_FACTOR_ONE, VK_BLEND_OP_ADD);
                break;
        }
        builder.SetColorBlendState();

        switch(rasterizerStateIndex)
        {
            case 0:builder.SetRasterizerState(VK_POLYGON_MODE_FILL, VK_CULL_MODE_NONE, VK_FRONT_FACE_CLOCKWISE);
                break;
            case 1:builder.SetRasterizerState(VK_POLYGON_MODE_FILL, VK_CULL_MODE_BACK_BIT, VK_FRONT_FACE_CLOCKWISE);
                break;
            case 2:
                builder.SetRasterizerState(VK_POLYGON_MODE_FILL, VK_CULL_MODE_BACK_BIT,
                                            VK_FRONT_FACE_COUNTER_CLOCKWISE);
                break;
            case 3:
                builder.SetRasterizerState(VK_POLYGON_MODE_LINE, VK_CULL_MODE_BACK_BIT,
                                            VK_FRONT_FACE_COUNTER_CLOCKWISE);
                break;
        }

        switch(depthStencilStateIndex)
        {
            case 0:builder.SetDepthStencilState(VK_FALSE, VK_FALSE, VK_COMPARE_OP_NEVER);
                break;
            case 1:builder.SetDepthStencilState(VK_TRUE, VK_TRUE, VK_COMPARE_OP_LESS_OR_EQUAL);
                break;
            case 2:builder.SetDepthStencilState(VK_TRUE, VK_FALSE, VK_COMPARE_OP_LESS_OR_EQUAL);
                break;
        }
        pipeline.Create(*m_pDevice, builder, *m_pRenderPass);
    }

    void CSpriteBatch::PrepareBatch(SSpriteInfo* pSpriteInfos, int numSpriteInfos)
    {
        m_vertexBufferPos = 0;
        m_pSpriteInfos = pSpriteInfos;
        m_numSpriteInfos = numSpriteInfos;

        m_totalMatrix = m_viewportMatrix * m_data.TransformationMatrix;
    }

    void CSpriteBatch::RenderBatch(CVulkanPrimaryCommandBuffer& commandBuffer, CVulkanImageView* pTexture, const SSpriteInfo* const* ppSpriteInfos, usize count)
    {
        PD_ASSERT(pTexture);

        commandBuffer.PushSampledImageDescriptorKHR(1, 0, *pTexture);

        PD_ASSERT(_MAX_BATCH_SIZE >= m_vertexBufferPos + count * _VERTICES_PER_SPRITE);

        SSpriteBatchVertex* pMappedData =
                m_vertexBuffer.Map<SSpriteBatchVertex>() + m_vertexBufferPos * _VERTICES_PER_SPRITE;

        for(size_t ix = 0; ix < count; ix++)
        {
            PD_ASSERT(ix < count);
            RenderSprite(ppSpriteInfos[ix], pMappedData);
            pMappedData += _VERTICES_PER_SPRITE;
        }

        m_vertexBuffer.Unmap();

        u32 startIndex = static_cast<u32>(m_vertexBufferPos * _INDICES_PER_SPRITE);
        u32 indexCount = static_cast<u32>(count * _INDICES_PER_SPRITE);

        commandBuffer.DrawIndexed(indexCount, 1, startIndex, 0, 0);

        m_vertexBufferPos += static_cast<u32>(count * _VERTICES_PER_SPRITE);

    }

    void CSpriteBatch::CreateDescriptorSets()
    {
        VkDescriptorImageInfo descriptorImageInfo;
        descriptorImageInfo.sampler = VK_NULL_HANDLE;
        descriptorImageInfo.imageView = VK_NULL_HANDLE;
        descriptorImageInfo.imageLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        VkDescriptorBufferInfo descriptorBufferInfo;
        descriptorBufferInfo.buffer = m_uniformBuffer.GetHandle();
        descriptorBufferInfo.offset = 0;
        descriptorBufferInfo.range = m_uniformBuffer.GetSize();

        std::array<VkWriteDescriptorSet, 2> writeDescriptorSets;

        auto& imageWriteDescriptorSet = writeDescriptorSets[0];
        imageWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        imageWriteDescriptorSet.pNext = nullptr;
        imageWriteDescriptorSet.dstSet = VK_NULL_HANDLE;
        imageWriteDescriptorSet.dstBinding = 0;
        imageWriteDescriptorSet.dstArrayElement = 0;
        imageWriteDescriptorSet.descriptorCount = 1;
        imageWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
        imageWriteDescriptorSet.pImageInfo = &descriptorImageInfo;
        imageWriteDescriptorSet.pBufferInfo = nullptr;
        imageWriteDescriptorSet.pTexelBufferView = nullptr;

        auto& bufferWriteDescriptorSet = writeDescriptorSets[1];
        bufferWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        bufferWriteDescriptorSet.pNext = nullptr;
        bufferWriteDescriptorSet.dstSet = VK_NULL_HANDLE;
        bufferWriteDescriptorSet.dstBinding = 1;
        bufferWriteDescriptorSet.dstArrayElement = 0;
        bufferWriteDescriptorSet.descriptorCount = 1;
        bufferWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        bufferWriteDescriptorSet.pImageInfo = nullptr;
        bufferWriteDescriptorSet.pBufferInfo = &descriptorBufferInfo;
        bufferWriteDescriptorSet.pTexelBufferView = nullptr;

        for(int ix = 0; ix < PD_SPRITEBATCH_NUM_SAMPLER_STATES; ix++)
        {
            descriptorImageInfo.sampler = m_samplerStates[ix].GetHandle();

            auto& descriptorSet = m_descriptorSets[ix];
            descriptorSet.Allocate(*m_pDevice, m_pDevice->GetDescriptorPool(),
                                   m_pDevice->GetDSLTemplates().GetLayout_S_FS_U_VS());
            VkDescriptorSet handleSet = descriptorSet.GetHandle();

            imageWriteDescriptorSet.dstSet = handleSet;
            bufferWriteDescriptorSet.dstSet = handleSet;

            descriptorSet.Update(writeDescriptorSets.data(), static_cast<u32>(writeDescriptorSets.size()));
        }
    }

    void CSpriteBatch::PrepareForRendering()
    {
        auto* pMappedUniformBuffer = m_uniformBuffer.Map<glm::mat4>();
        *pMappedUniformBuffer = m_totalMatrix;
        m_uniformBuffer.Unmap();

        auto& descriptorSet = m_descriptorSets[static_cast<int>(m_data.SamplerState)];

        auto& pipeline = m_pipelines[GetCurrentPipelineIndex()];

        m_pCommandBuffer->BindPipeline(pipeline);

        auto* pWindow = CWindow::GetInstance();
        u32 width = pWindow->GetWidth(), height = pWindow->GetHeight();

        VkViewport viewport;
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = static_cast<float>(width);
        viewport.height = static_cast<float>(height);
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        m_pCommandBuffer->SetViewport(0, 1, &viewport);

        VkRect2D scissorRect;
        scissorRect.offset.x = 0;
        scissorRect.offset.y = 0;
        scissorRect.extent.width = static_cast<i32>(width);
        scissorRect.extent.height = static_cast<i32>(height);
        m_pCommandBuffer->SetScissor(0, 1, &scissorRect);

        m_pCommandBuffer->BindIndexBuffer(m_indexBuffer);
        m_pCommandBuffer->BindVertexBuffer(m_vertexBuffer);
        m_pCommandBuffer->BindDescriptorSet(m_descriptorSets[(int)m_data.SamplerState]);
    }

    void CSpriteBatch::FlushBatch(CVulkanPrimaryCommandBuffer& commandBuffer)
    {
        SortSprites();

        CVulkanImageView* pBatchTexture = nullptr;
        usize batchStart = 0;

        for(usize pos = 0; pos < m_numSpriteInfos; pos++)
        {
            auto* pTexture = reinterpret_cast<CVulkanImageView*>(m_sortedSprites[pos]->pImageView);
            PD_ASSERT(pTexture != nullptr);

            if(pTexture != pBatchTexture)
            {
                if(pos > batchStart)
                {
                    RenderBatch(commandBuffer, pBatchTexture, &m_sortedSprites[batchStart], pos - batchStart);
                }
                pBatchTexture = pTexture;
                batchStart = pos;
            }
        }

        RenderBatch(commandBuffer, pBatchTexture, &m_sortedSprites[batchStart], m_numSpriteInfos - batchStart);
        m_spriteInfos.clear();

        if(m_data.SpriteSortMode != ESpriteSortMode::Deferred) m_sortedSprites.clear();
    }

    void CSpriteBatch::Create(CVulkanDevice& device, CVulkanRenderPass& renderPass)
    {
        m_pDevice = &device;
        m_pRenderPass = &renderPass;

        auto& shadersFile = CPakManager::GetInstance()->GetShaders();

        SPakLocation vertexLocation(shadersFile, "sprite_batch.vert.spv");
        SPakLocation fragmentLocation(shadersFile, "sprite_batch.frag.spv");

        CVulkanShaderModule vertexShader, fragmentShader;
        vertexShader.LoadBinary(device, vertexLocation, "main", VK_SHADER_STAGE_VERTEX_BIT);
        fragmentShader.LoadBinary(device, fragmentLocation, "main", VK_SHADER_STAGE_FRAGMENT_BIT);

        CreateIndexBuffer();
        CreateVertexBuffer();

        CVulkanInputLayoutBuilder inputLayoutBuilder;
        inputLayoutBuilder.AddAttribute(0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0);
        inputLayoutBuilder.AddAttribute(0, 1, VK_FORMAT_R32G32B32A32_SFLOAT, sizeof(float) * 3);
        inputLayoutBuilder.AddAttribute(0, 2, VK_FORMAT_R32G32_SFLOAT, sizeof(float) * 7);
        inputLayoutBuilder.AddBinding(0, sizeof(SSpriteBatchVertex), VK_VERTEX_INPUT_RATE_VERTEX);
        inputLayoutBuilder.Create();

        m_uniformBuffer.Create<glm::mat4, 1>(device);

        m_samplerStates[0].Create(device, VK_FILTER_NEAREST, VK_FILTER_NEAREST, VK_SAMPLER_MIPMAP_MODE_NEAREST,
                                  VK_SAMPLER_ADDRESS_MODE_REPEAT);
        m_samplerStates[1].Create(device, VK_FILTER_NEAREST, VK_FILTER_NEAREST, VK_SAMPLER_MIPMAP_MODE_NEAREST,
                                  VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE);
        m_samplerStates[2].Create(device, VK_FILTER_LINEAR, VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_LINEAR,
                                  VK_SAMPLER_ADDRESS_MODE_REPEAT);
        m_samplerStates[3].Create(device, VK_FILTER_LINEAR, VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_LINEAR,
                                  VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE);
        m_samplerStates[4].Create(device, VK_FILTER_LINEAR, VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_LINEAR,
                                  VK_SAMPLER_ADDRESS_MODE_REPEAT, 0.0f, VK_TRUE);
        m_samplerStates[5].Create(device, VK_FILTER_LINEAR, VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_LINEAR,
                                  VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE, 0.0f, VK_TRUE);

        CreateDescriptorSets();
        CreatePipelineLayout();

        for(u32 ix = 0; ix < PD_SPRITEBATCH_NUM_BLEND_STATES; ix++)
        {
            for(u32 iy = 0; iy < PD_SPRITEBATCH_NUM_DEPTH_STENCIL_STATES; iy++)
            {
                for(u32 iz = 0; iz < PD_SPRITEBATCH_NUM_RASTERIZER_STATES; iz++)
                {
                    CreatePipeline(ix, iy, iz, vertexShader, fragmentShader, inputLayoutBuilder);
                }
            }
        }

        auto* pWindow = CWindow::GetInstance();
        m_viewportMatrix = MatrixMath::Ortho(0.0f, static_cast<float>(pWindow->GetWidth()), static_cast<float>(pWindow->GetHeight()),
                                      0.0f);

        fragmentShader.Dispose(device);
        vertexShader.Dispose(device);

        PD_END_INITIALIZE;

    }

    void CSpriteBatch::Flush(SSpriteInfo* pSpriteInfos, int numSpriteInfos)
    {
        PrepareBatch(pSpriteInfos, numSpriteInfos);
        PrepareForRendering();
        FlushBatch(*m_pCommandBuffer);
    }

    void CSpriteBatch::Resize(u32 newWindowWidth, u32 newWindowHeight)
    {
        m_viewportMatrix = MatrixMath::Ortho(0.0f, static_cast<float>(newWindowWidth), static_cast<float>(newWindowHeight),0.0f);
    }

    void CSpriteBatch::Begin(ESpriteSortMode spriteSortMode, EBlendState blendState, ESamplerState samplerState, EDepthStencilState depthStencilState, ERasterizerState rasterizerState, glm::mat4 transformationMatrix)
    {
        if(m_drawing)
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"SpriteBatch is already drawing" << CLogger::GetInstance()->Flush();
        }

        m_data.SpriteSortMode = spriteSortMode;
        m_data.BlendState = blendState;
        m_data.SamplerState = samplerState;
        m_data.DepthStencilState = depthStencilState;
        m_data.RasterizerState = rasterizerState;
        m_data.TransformationMatrix = transformationMatrix;

        m_drawing = true;
    }

    void CSpriteBatch::End()
    {
        if(!m_drawing)
        {
            CLogger::GetInstance()->Log(ELogLevel::Error, PD_VK_RENDERER_PREFIX) << L"SpriteBatch is not drawing" << CLogger::GetInstance()->Flush();
        }

        if(!m_spriteInfos.empty()) Flush(m_spriteInfos.data(), static_cast<int>(m_spriteInfos.size()));

        m_drawing = false;
    }

    void CSpriteBatch::Draw(CVulkanTexture2D& texture, const SRectangleF& destinationRectangle, const SColor4f& color, glm::vec2 origin, float rotation, float depth)
    {
        SSpriteInfo spriteInfo;
        spriteInfo.TexCoordTL = _TEXCOORD_TL;
        spriteInfo.TexCoordBR = _TEXCOORD_BR;
        spriteInfo.Destination = destinationRectangle;
        spriteInfo.Color = color;
        spriteInfo.Origin = origin;
        spriteInfo.Rotation = rotation;
        spriteInfo.Depth = depth;
        spriteInfo.pImageView = &texture.GetImageView();

        m_spriteInfos.push_back(spriteInfo);
    }

    void CSpriteBatch::Draw(CVulkanImageView& imageView, u32 width, u32 height, const SRectangleF& destinationRectangle, const SColor4f& color, glm::vec2 origin, float rotation, float depth)
    {
        SSpriteInfo spriteInfo;
        spriteInfo.TexCoordTL = _TEXCOORD_TL;
        spriteInfo.TexCoordBR = _TEXCOORD_BR;
        spriteInfo.Destination = destinationRectangle;
        spriteInfo.Color = color;
        spriteInfo.Origin = origin;
        spriteInfo.Rotation = rotation;
        spriteInfo.Depth = depth;
        spriteInfo.pImageView = &imageView;

        m_spriteInfos.push_back(spriteInfo);
    }

    void CSpriteBatch::Draw(CVulkanTexture2D& texture, const SRectangleF& destinationRectangle, const SRectangleF& sourceRectangle, const SColor4f& color, glm::vec2 origin, float rotation, float depth)
    {
        auto textureWidth = (float)texture.GetWidth();
        auto textureHeight = (float)texture.GetHeight();

        SSpriteInfo spriteInfo;
        spriteInfo.TexCoordTL = glm::vec2(sourceRectangle.X / textureWidth, sourceRectangle.Y / textureHeight);
        spriteInfo.TexCoordBR = glm::vec2((sourceRectangle.X + sourceRectangle.Width) / textureWidth,
                                          (sourceRectangle.Y + sourceRectangle.Height) / textureHeight);
        spriteInfo.Destination = destinationRectangle;
        spriteInfo.Color = color;
        spriteInfo.Origin = origin;
        spriteInfo.Rotation = rotation;
        spriteInfo.Depth = depth;
        spriteInfo.pImageView = &texture.GetImageView();

        m_spriteInfos.push_back(spriteInfo);
    }

    void CSpriteBatch::Draw(CVulkanImageView& imageView, u32 width, u32 height, const SRectangleF& destinationRectangle, const SRectangleF& sourceRectangle, const SColor4f& color, glm::vec2 origin, float rotation, float depth)
    {
        //TODO:
    }

    void CSpriteBatch::DrawString(CSpriteFont& spriteFont, const wchar_t* pMessage, usize length, glm::vec2 position, const SColor4f& color, glm::vec2 origin, float rotation, float depth)
    {
        glm::vec2 offset = glm::vec2();
        bool firstGlyphOfLine = true;

        auto& texture = spriteFont.GetTexture();
        auto textureWidth = static_cast<float>(texture.GetWidth()), textureHeight = static_cast<float>(texture.GetHeight());
        u32 fontSize = spriteFont.GetSize();

        for(int ix = 0; ix < length; ix++)
        {
            wchar_t c = pMessage[ix];
            if(c == '\r') continue;
            if(c == '\n')
            {
                offset.x = 0.0f;
                offset.y += spriteFont.GetLineSpacing();
                firstGlyphOfLine = true;
                continue;
            }

            const auto* pGlyphInfo = spriteFont.GetGlyph(c);

            if(!pGlyphInfo) continue;

            float xCharOffset = pGlyphInfo->OffsetX;
            if(firstGlyphOfLine)
            {
                if(pGlyphInfo->OffsetX < 0.0f)
                    xCharOffset = 0.0f;
                else
                    xCharOffset = pGlyphInfo->OffsetX;

                firstGlyphOfLine = false;
            }

            SSpriteInfo spriteInfo;
            spriteInfo.TexCoordTL = glm::vec2(pGlyphInfo->TexCoordX / textureWidth,
                                              pGlyphInfo->TexCoordY / textureHeight);
            spriteInfo.TexCoordBR = glm::vec2((pGlyphInfo->TexCoordX + pGlyphInfo->Width) / textureWidth,
                                              (pGlyphInfo->TexCoordY + pGlyphInfo->Height) / textureHeight);
            spriteInfo.Destination = SRectangleF(position.x + offset.x + xCharOffset,
                                                 position.y + offset.y + (fontSize - pGlyphInfo->OffsetY),
                                                 pGlyphInfo->Width, pGlyphInfo->Height);
            spriteInfo.Color = color;
            spriteInfo.Origin = origin;
            spriteInfo.Rotation = rotation;
            spriteInfo.Depth = depth;
            spriteInfo.pImageView = &texture.GetImageView();

            m_spriteInfos.push_back(spriteInfo);
            offset.x += pGlyphInfo->XAdvance;
        }
    }

	void CSpriteBatch::DrawString(CStaticFontMesh& fontMesh, glm::vec2 position, glm::vec2 origin, float rotation, float depth)
	{
		const STL::CVector<SSpriteInfo>& spriteInfos = fontMesh.GetSpriteInfos();

		for(usize ix = 0; ix < spriteInfos.size(); ix++)
		{
			SSpriteInfo spriteInfo = spriteInfos[ix];
			spriteInfo.Destination.X += position.x;
			spriteInfo.Destination.Y += position.y;
			spriteInfo.Origin = origin;
			spriteInfo.Rotation = rotation;
			spriteInfo.Depth = depth;

			m_spriteInfos.push_back(spriteInfo);
		}
	}

	void CSpriteBatch::DrawStringWithColorOverride(CStaticFontMesh& fontMesh, glm::vec2 position, const SColor4f& color, glm::vec2 origin, float rotation, float depth)
	{
		const STL::CVector<SSpriteInfo>& spriteInfos = fontMesh.GetSpriteInfos();

		for(usize ix = 0; ix < spriteInfos.size(); ix++)
		{
			SSpriteInfo spriteInfo = spriteInfos[ix];
			spriteInfo.Destination.X += position.x;
			spriteInfo.Destination.Y += position.y;
			spriteInfo.Origin = origin;
			spriteInfo.Rotation = rotation;
			spriteInfo.Depth = depth;
			spriteInfo.Color = color;

			m_spriteInfos.push_back(spriteInfo);
		}
	}
}