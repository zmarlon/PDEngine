//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 11.04.2020.
//
#ifndef PD_ENGINE_CLIENT_RENDERER_SETTINGS_RENDERERPROPERTIES_HPP
#define PD_ENGINE_CLIENT_RENDERER_SETTINGS_RENDERERPROPERTIES_HPP

#pragma once

namespace PD
{
    struct SRendererProperties
    {
        bool UsePushDescriptors;
        bool DebugMarkerEnabled;
        bool UseHDR;
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_SETTINGS_RENDERERPROPERTIES_HPP
