//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 11.04.2020.
//
#ifndef PD_ENGINE_CLIENT_RENDERER_SETTINGS_GRAPHICSSETTINGS_HPP
#define PD_ENGINE_CLIENT_RENDERER_SETTINGS_GRAPHICSSETTINGS_HPP

#pragma once

#include "../../../Core/Hints/ForceInline.hpp"
#include "TextureQualityLevel.hpp"
#include <cstring>

namespace PD
{
    struct SGraphicsSettings
    {
        ETextureQualityLevel TextureQualityLevel;

        PD_FORCEINLINE bool operator ==(const SGraphicsSettings& other) const
        {
            return memcmp(this, &other, sizeof(SGraphicsSettings)) == 0;
        }

        PD_FORCEINLINE bool operator !=(const SGraphicsSettings& other) const
        {
            return memcmp(this, &other, sizeof(SGraphicsSettings)) != 0;
        }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_SETTINGS_GRAPHICSSETTINGS_HPP
