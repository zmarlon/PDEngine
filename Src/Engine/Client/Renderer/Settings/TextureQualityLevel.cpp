//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 11.04.2020.
//

#include "TextureQualityLevel.hpp"

namespace PD
{
    u32 TextureQualityLevel::GetBaseMipLevel(ETextureQualityLevel textureQualityLevel)
    {
        switch(textureQualityLevel)
        {
            case ETextureQualityLevel::Low:
                return 2;
            case ETextureQualityLevel::Medium:
            case ETextureQualityLevel::High:
                return 1;
            case ETextureQualityLevel::VeryHigh:
            case ETextureQualityLevel::UltraHigh:
                return 0;
        }
        return 0;
    }

    float TextureQualityLevel::GetAnisotropyLevel(ETextureQualityLevel textureQualityLevel)
    {
        switch(textureQualityLevel)
        {
            case ETextureQualityLevel::Low:
                return 2.0f;
            case ETextureQualityLevel::Medium:
                return 4.0f;
            case ETextureQualityLevel::High:
                return 8.0f;
            case ETextureQualityLevel::VeryHigh:
            case ETextureQualityLevel::UltraHigh:
                return 16.0f;
        }
    }
}