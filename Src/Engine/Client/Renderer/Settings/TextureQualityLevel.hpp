//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 11.04.2020.
//
#ifndef PD_ENGINE_CLIENT_RENDERER_SETTINGS_TEXTUREQUALITYLEVEL_HPP
#define PD_ENGINE_CLIENT_RENDERER_SETTINGS_TEXTUREQUALITYLEVEL_HPP

#pragma once

#include "../../../Core/Types.hpp"

namespace PD
{
    enum class ETextureQualityLevel
    {
        Low = 0,
        Medium = 1,
        High = 2,
        VeryHigh = 3,
        UltraHigh = 4
    };

    namespace TextureQualityLevel
    {
        u32 GetBaseMipLevel(ETextureQualityLevel textureQualityLevel);
        float GetAnisotropyLevel(ETextureQualityLevel textureQualityLevel);
    }
}

#endif //PD_ENGINE_CLIENT_RENDERER_SETTINGS_TEXTUREQUALITYLEVEL_HPP
