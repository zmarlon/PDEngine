//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_DEFERRED_DEBUGRENDERER_HPP
#define PD_ENGINE_CLIENT_RENDERER_DEFERRED_DEBUGRENDERER_HPP

#pragma once

#include "../../../Core/Disposable.hpp"
#include "../Vulkan/Buffer/VulkanUniformBuffer.hpp"
#include "../Vulkan/Pipeline/VulkanGraphicsPipeline.hpp"
#include "../Vulkan/Pipeline/VulkanPipelineLayout.hpp"
#include "../Vulkan/Descriptor/VulkanDescriptorSetLayout.hpp"
#include "../Vulkan/Buffer/VulkanVertexBuffer.hpp"
#include "../Vulkan/Buffer/VulkanIndexBuffer.hpp"

namespace PD
{
	class CVulkanPrimaryCommandBuffer;
	struct SFrameEventArgs;
	class CVulkanDevice;
	class CDeferredRenderer;
	class CDebugRenderer
	{
		PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CDebugRenderer, CVulkanDevice&)
	private:
		CVulkanUniformBuffer m_ubo;
		CVulkanGraphicsPipeline m_pipeline;
		CVulkanPipelineLayout m_pipelineLayout;

		CVulkanDescriptorSetLayout m_descriptorSetLayout;

		CVulkanVertexBuffer m_vertexBuffer;
		CVulkanIndexBuffer m_indexBuffer;

		void OnDisposing(CVulkanDevice& device);
	public:
		void Initialize(CVulkanDevice& device, CDeferredRenderer& deferredRenderer);
		void Render(CVulkanPrimaryCommandBuffer& commandBuffer, const SFrameEventArgs& frameEventArgs);
	};
}

#endif //PD_ENGINE_CLIENT_RENDERER_DEFERRED_DEBUGRENDERER_HPP
