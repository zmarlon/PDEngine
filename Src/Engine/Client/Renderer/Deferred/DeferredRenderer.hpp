//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_DEFERRED_DEFERREDRENDERER_HPP
#define PD_ENGINE_CLIENT_RENDERER_DEFERRED_DEFERREDRENDERER_HPP

#include "../../../Core/Disposable.hpp"
#include "../Vulkan/Image/VulkanRenderTargetImage.hpp"
#include "../Vulkan/Image/VulkanDepthStencilImage.hpp"
#include "../Vulkan/Image/VulkanRenderPass.hpp"
#include "../Vulkan/Image/VulkanFramebuffer.hpp"
#include "../Vulkan/Command/VulkanPrimaryCommandBuffer.hpp"
#include "../Vulkan/VulkanSemaphore.hpp"
#include "../Vulkan/Command/VulkanSubmitInfo.hpp"
#include "../../../Core/Time/FrameEventArgs.hpp"
#include "DebugRenderer.hpp"
#include <array>

namespace PD
{
	class CVulkanDevice;
	class CDeferredRenderer
	{
		PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CDeferredRenderer, CVulkanDevice&)
	private:
		CVulkanDevice* m_pDevice;

		CVulkanRenderTargetImage m_gBufferDiffuseImage;
		CVulkanRenderTargetImage m_gBufferNormalImage;
		CVulkanRenderTargetImage m_gBufferSpecularImage;
		CVulkanDepthStencilImage m_gBufferDepthImage;

		CVulkanRenderPass m_gBufferRenderPass;
		CVulkanFramebuffer m_gBufferFramebuffer;

		CVulkanPrimaryCommandBuffer m_gBufferCommandBuffer;
		CVulkanSemaphore m_gBufferSignalSemaphore;
		CVulkanSubmitInfo<1, 0, 1> m_gBufferSubmitInfo;
		std::array<VkClearValue, 4> m_gBufferClearValues;

		CDebugRenderer m_debugRenderer;

		inline void DestroyGBuffer(CVulkanDevice& device);
		inline void CreateGBuffer(CVulkanDevice& device, u32 width, u32 height);

		void OnDisposing(CVulkanDevice& device);
	public:
		void Initialize(CVulkanDevice& device);

		void Render(const SFrameEventArgs& frameEventArgs);

		void Resize(CVulkanDevice& device, u32 newWindowWidth, u32 newWindowHeight);

		PD_FORCEINLINE CVulkanRenderTargetImage& GetGBufferDiffuseImage() {return m_gBufferDiffuseImage; }
		PD_FORCEINLINE CVulkanRenderTargetImage& GetGBufferNormalImage() {return m_gBufferNormalImage; }
		PD_FORCEINLINE CVulkanRenderTargetImage& GetGBufferSpecularImage() {return m_gBufferSpecularImage; }
		PD_FORCEINLINE CVulkanDepthStencilImage& GetGBufferDepthImage() {return m_gBufferDepthImage; }

		PD_FORCEINLINE CVulkanRenderPass& GetGBufferRenderPass() {return m_gBufferRenderPass; }
		PD_FORCEINLINE CVulkanFramebuffer& GetGBufferFramebuffer() {return m_gBufferFramebuffer; }

		PD_FORCEINLINE CVulkanSemaphore& GetGBufferSignalSemaphore() {return m_gBufferSignalSemaphore; }

	};
}

#endif //PD_ENGINE_CLIENT_RENDERER_DEFERRED_DEFERREDRENDERER_HPP
