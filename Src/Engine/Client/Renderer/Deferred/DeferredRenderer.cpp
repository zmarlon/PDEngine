//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#include "DeferredRenderer.hpp"
#include "../../../Core/Window/Window.hpp"
#include "../Vulkan/VulkanDevice.hpp"
#include "../../../Core/Math/Color4f.hpp"

namespace PD
{
	void CDeferredRenderer::OnDisposing(CVulkanDevice& device)
	{
	    m_debugRenderer.Dispose(device);

		m_gBufferSignalSemaphore.Dispose(*m_pDevice);
		m_gBufferCommandBuffer.Dispose();

		DestroyGBuffer(device);
	}

	void CDeferredRenderer::DestroyGBuffer(CVulkanDevice& device)
	{
		m_gBufferFramebuffer.Dispose(device);
		m_gBufferRenderPass.Dispose(device);

		m_gBufferDepthImage.Dispose(device);
		m_gBufferSpecularImage.Dispose(device);
		m_gBufferNormalImage.Dispose(device);
		m_gBufferDiffuseImage.Dispose(device);
	}

	void CDeferredRenderer::CreateGBuffer(CVulkanDevice& device, u32 width, u32 height)
	{
		m_gBufferDiffuseImage.Create(device, width, height, VK_FORMAT_R8G8B8A8_UNORM, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
		m_gBufferNormalImage.Create(device, width, height, VK_FORMAT_R8G8B8A8_SNORM, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
		m_gBufferSpecularImage.Create(device, width, height, VK_FORMAT_R8G8B8A8_UNORM, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
		m_gBufferDepthImage.Create(device, width, height, VK_FORMAT_D32_SFLOAT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);

		CVulkanRenderPassBuilder renderPassBuilder;
        renderPassBuilder.AddAttachment(VK_FORMAT_R8G8B8A8_UNORM, 1, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE,
										  VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        renderPassBuilder.AddAttachment(VK_FORMAT_R8G8B8A8_SNORM, 1, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE,
										  VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        renderPassBuilder.AddAttachment(VK_FORMAT_R8G8B8A8_UNORM, 1, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE,
										  VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        renderPassBuilder.AddAttachment(VK_FORMAT_D32_SFLOAT, 1, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE,
										  VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

		CVulkanSubpassBuilder subpassBuilder;
		subpassBuilder.ReserveColorAttachments(3);
		subpassBuilder.AddColorAttachment(0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
		subpassBuilder.AddColorAttachment(1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
		subpassBuilder.AddColorAttachment(2, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
		subpassBuilder.SetDepthAttachment(3, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
		subpassBuilder.SetBindPoint(VK_PIPELINE_BIND_POINT_GRAPHICS);
        renderPassBuilder.AddSubpass(subpassBuilder);
		m_gBufferRenderPass.Create(device, renderPassBuilder);

		CVulkanFramebufferBuilder framebufferBuilder;
		framebufferBuilder.AddView(m_gBufferDiffuseImage.GetImageView().GetHandle());
		framebufferBuilder.AddView(m_gBufferNormalImage.GetImageView().GetHandle());
		framebufferBuilder.AddView(m_gBufferSpecularImage.GetImageView().GetHandle());
		framebufferBuilder.AddView(m_gBufferDepthImage.GetImageView().GetHandle());
		m_gBufferFramebuffer.Initialize(device, framebufferBuilder, m_gBufferRenderPass, width, height);
	}

	void CDeferredRenderer::Initialize(CVulkanDevice& device)
	{
		m_pDevice = &device;

		auto* pWindow = CWindow::GetInstance();

		CreateGBuffer(device, pWindow->GetWidth(), pWindow->GetHeight());

		m_gBufferCommandBuffer.Initialize(device, device.GetThreadCollection().GetGraphicsPerFrameCommandPool());
		m_gBufferSignalSemaphore.Initialize(device);

		m_gBufferSubmitInfo.SetCommandBuffer(0, m_gBufferCommandBuffer);
		m_gBufferSubmitInfo.SetSignalSemaphore(0, m_gBufferSignalSemaphore);

		VkClearValue clearValue;
		clearValue.color.float32[0] = 100.0f / 255.0f;
		clearValue.color.float32[1] = 149.0f / 255.0f;
		clearValue.color.float32[2] = 237.0f / 255.0f;
		clearValue.color.float32[3] = 1.0f;

		m_gBufferClearValues[0] = clearValue;
		m_gBufferClearValues[1] = clearValue;
		m_gBufferClearValues[2] = clearValue;
		m_gBufferClearValues[3].depthStencil.depth = 0.0f;
		m_gBufferClearValues[3].depthStencil.stencil = 0;

		m_debugRenderer.Initialize(device, *this);

		PD_END_INITIALIZE;
	}

	void CDeferredRenderer::Render(const SFrameEventArgs& frameEventArgs)
	{
		m_gBufferCommandBuffer.Begin();
		m_gBufferCommandBuffer.BeginDebugLabel("GBuffer", SColor4f(0.022f, 0.372332f, 0.010f));
		m_gBufferCommandBuffer.BeginRenderPass(m_gBufferRenderPass, m_gBufferFramebuffer, static_cast<u32>(m_gBufferClearValues.size()), m_gBufferClearValues.data());

        m_debugRenderer.Render(m_gBufferCommandBuffer, frameEventArgs);

		m_gBufferCommandBuffer.EndRenderPass();
		m_gBufferCommandBuffer.EndDebugLabel();
		m_gBufferCommandBuffer.End();

		auto& graphicsQueue = m_pDevice->GetGraphicsQueue();
		graphicsQueue.Submit(m_gBufferSubmitInfo);
	}

	void CDeferredRenderer::Resize(CVulkanDevice& device, u32 newWindowWidth, u32 newWindowHeight)
	{
		DestroyGBuffer(device);
		CreateGBuffer(*m_pDevice, newWindowWidth, newWindowHeight);
	}
}