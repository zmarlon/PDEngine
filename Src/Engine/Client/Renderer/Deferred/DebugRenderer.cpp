//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#include "DebugRenderer.hpp"
#include "../Vulkan/Pipeline/VulkanShaderModule.hpp"
#include "../../../Core/Pak/PakManager.hpp"
#include "../../../Core/Pak/PakLocation.hpp"
#include "../Vulkan/Pipeline/VulkanInputLayout.hpp"
#include "../Common/Vertices.hpp"
#include "DeferredRenderer.hpp"
#include "../Common/ModelMesh.hpp"
#include "../../../Core/Window/Window.hpp"
#include "../../Camera.hpp"
#include "../../../Core/Math/Rectangle.hpp"
#include <glm/gtc/matrix_transform.hpp>

namespace PD
{
	void CDebugRenderer::OnDisposing(CVulkanDevice& device)
	{
		m_pipelineLayout.Dispose(device);
		m_pipeline.Dispose(device);

		m_indexBuffer.Dispose();
		m_vertexBuffer.Dispose();

		m_descriptorSetLayout.Dispose(device);

		m_ubo.Dispose();
	}

	void CDebugRenderer::Initialize(CVulkanDevice& device, CDeferredRenderer& deferredRenderer)
	{
	    auto& shadersPak = CPakManager::GetInstance()->GetShaders();

		CVulkanShaderModule vertexShader;
		vertexShader.LoadBinary(device, SPakLocation(shadersPak, "debug_renderer.vert.spv"), "main", VK_SHADER_STAGE_VERTEX_BIT);

		CVulkanShaderModule fragmentShader;
		fragmentShader.LoadBinary(device, SPakLocation(shadersPak, "debug_renderer.frag.spv"), "main", VK_SHADER_STAGE_FRAGMENT_BIT);

		CVulkanDescriptorSetLayoutBuilder dslBuilder;
		dslBuilder.AddBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT);
		m_descriptorSetLayout.Create(device, dslBuilder, true);

		CVulkanInputLayoutBuilder inputLayoutBuilder;
		inputLayoutBuilder.AddBinding(0, sizeof(SVertex_POS_TEX_NORMAL_TANGENT_BITANGENT), VK_VERTEX_INPUT_RATE_VERTEX);
		inputLayoutBuilder.AddAttribute(0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0);
		inputLayoutBuilder.AddAttribute(0, 1, VK_FORMAT_R32G32_SFLOAT, 3 * sizeof(float));
		inputLayoutBuilder.AddAttribute(0, 2, VK_FORMAT_R32G32B32_SFLOAT, 5 * sizeof(float));
		inputLayoutBuilder.AddAttribute(0, 3, VK_FORMAT_R32G32B32_SFLOAT, 8 * sizeof(float));
		inputLayoutBuilder.AddAttribute(0, 4, VK_FORMAT_R32G32B32_SFLOAT, 11 * sizeof(float));
		inputLayoutBuilder.Create();

		CVulkanPipelineLayoutBuilder pipelineLayoutBuilder;
		pipelineLayoutBuilder.AddDescriptorSetLayout(m_descriptorSetLayout);
		pipelineLayoutBuilder.AddPushConstantRange(VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::mat4));
		m_pipelineLayout.Create(device, pipelineLayoutBuilder);

		CVulkanGraphicsPipelineBuilder graphicsPipelineBuilder;
		graphicsPipelineBuilder.AddShader(vertexShader);
		graphicsPipelineBuilder.AddShader(fragmentShader);
		graphicsPipelineBuilder.SetInputLayout(inputLayoutBuilder);
		graphicsPipelineBuilder.SetPrimitiveTopology(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
		graphicsPipelineBuilder.SetRasterizerState(VK_POLYGON_MODE_FILL, VK_CULL_MODE_NONE, VK_FRONT_FACE_CLOCKWISE);
		graphicsPipelineBuilder.SetMultisampleState(1);
		graphicsPipelineBuilder.SetDepthStencilState(VK_TRUE, VK_TRUE, VK_COMPARE_OP_GREATER);
		graphicsPipelineBuilder.AddColorBlendAttachment(VK_FALSE);
		graphicsPipelineBuilder.AddColorBlendAttachment(VK_FALSE);
		graphicsPipelineBuilder.AddColorBlendAttachment(VK_FALSE);
		graphicsPipelineBuilder.SetColorBlendState();
		graphicsPipelineBuilder.SetViewport(0.0f, 0.0f, 1.0f, 1.0f);
		graphicsPipelineBuilder.AddDynamicState(VK_DYNAMIC_STATE_VIEWPORT);
		graphicsPipelineBuilder.AddDynamicState(VK_DYNAMIC_STATE_SCISSOR);
        m_pipeline.SetLayout(m_pipelineLayout);
		m_pipeline.Create(device, graphicsPipelineBuilder, deferredRenderer.GetGBufferRenderPass(), 0);

		m_ubo.Create(device, sizeof(glm::mat4));

		CVertexModel model;
		model.LoadFromObj(SPakLocation(CPakManager::GetInstance()->GetShaders(), "dragon.obj"));

		m_vertexBuffer.Create(device, model.GetVertices().GetSize(), model.GetVertices().GetData());
		m_indexBuffer.Create(device, model.GetIndices().GetSize(), model.GetIndices().GetData());

		model.Dispose();
		fragmentShader.Dispose(device);
		vertexShader.Dispose(device);

		PD_END_INITIALIZE;
	}

	void CDebugRenderer::Render(CVulkanPrimaryCommandBuffer& commandBuffer, const SFrameEventArgs& frameEventArgs)
	{
		auto* pWindow = CWindow::GetInstance();

		ICamera* pCamera = ICamera::GetCurrent();
		glm::mat4 viewProjectionMatrix = pCamera->GetProjectionMatrix() * pCamera->GetViewMatrix();

		glm::mat4 modelMatrix = glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(2.0f, 2.0f, 2.0f)), glm::vec3(5.0f, -5.0f, 10.0f));

		auto* pVPM = m_ubo.Map<glm::mat4>();
		pVPM[0] = viewProjectionMatrix;
		m_ubo.Unmap();

		commandBuffer.BindPipeline(m_pipeline);

		VkViewport viewport;
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = static_cast<float>(pWindow->GetWidth());
		viewport.height = static_cast<float>(pWindow->GetHeight());
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		commandBuffer.SetViewport(0, 1, &viewport);

		VkRect2D scissor;
		scissor.offset.x = 0;
		scissor.offset.y = 0;
		scissor.extent.width = pWindow->GetWidth();
		scissor.extent.height = pWindow->GetHeight();

		commandBuffer.SetScissor(0, 1, &scissor);

		commandBuffer.PushConstants(VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::mat4), &modelMatrix);
		commandBuffer.PushUniformBufferDescriptorKHR(0, 0, m_ubo);
		commandBuffer.BindVertexBuffer(m_vertexBuffer);
		commandBuffer.BindIndexBuffer(m_indexBuffer);
		commandBuffer.DrawIndexed(m_indexBuffer.GetIndexCount(), 1, 0, 0, 0);
	}
}