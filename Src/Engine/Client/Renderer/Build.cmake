set(PD_ENGINE_CLIENT_RENDERER_DIRECTORY Src/Engine/Client/Renderer)

include(Src/Engine/Client/Renderer/Common/Build.cmake)
include(Src/Engine/Client/Renderer/Deferred/Build.cmake)
include(Src/Engine/Client/Renderer/Settings/Build.cmake)
include(Src/Engine/Client/Renderer/Vulkan/Build.cmake)

set(PD_ENGINE_CLIENT_RENDERER_SOURCE_FILES
        ${PD_ENGINE_CLIENT_RENDERER_DIRECTORY}/Renderer.cpp
        ${PD_ENGINE_CLIENT_RENDERER_DIRECTORY}/Renderer.hpp
        ${PD_ENGINE_CLIENT_RENDERER_COMMON_SOURCE_FILES}
        ${PD_ENGINE_CLIENT_RENDERER_DEFERRED_SOURCE_FILES}
        ${PD_ENGINE_CLIENT_RENDERER_SETTINGS_SOURCE_FILES}
        ${PD_ENGINE_CLIENT_RENDERER_VULKAN_SOURCE_FILES})