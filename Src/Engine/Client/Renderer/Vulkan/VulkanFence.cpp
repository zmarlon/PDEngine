//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanFence.hpp"
#include "VulkanDevice.hpp"
#include <limits>

namespace PD
{
	void CVulkanFence::OnDisposing()
	{
		vkDestroyFence(m_pDevice->GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
	}

	void CVulkanFence::Initialize(CVulkanDevice& device)
	{
		m_pDevice = &device;

		m_fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		m_fenceCreateInfo.pNext = nullptr;
		m_fenceCreateInfo.flags = 0;

		PD_VK_THROW_IF_FAILED(vkCreateFence(m_pDevice->GetHandle(), &m_fenceCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));

		PD_END_INITIALIZE;
	}

	void CVulkanFence::Reset()
	{
		PD_VK_THROW_IF_FAILED(vkResetFences(m_pDevice->GetHandle(), 1, &m_handle));
	}

	void CVulkanFence::WaitFor()
	{
		PD_VK_THROW_IF_FAILED(
				vkWaitForFences(m_pDevice->GetHandle(), 1, &m_handle, VK_TRUE, std::numeric_limits<u64>::max()));
	}

	VkResult CVulkanFence::GetStatus()
	{
		return vkGetFenceStatus(m_pDevice->GetHandle(), m_handle);
	}
}