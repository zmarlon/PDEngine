//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanSwapchain.hpp"
#include "VulkanDevice.hpp"
#include "../../../Core/Logging/Logger.hpp"
#include <array>

namespace PD
{
    void CVulkanSwapchain::OnDisposing()
    {
        m_fence.Dispose();
        m_semaphoreRenderingDone.Dispose(*m_pDevice);
        m_semaphoreImageAvailable.Dispose(*m_pDevice);

        for(usize ix = 0; ix < m_images.GetSize(); ix++)
        {
            vkDestroyFramebuffer(m_pDevice->GetHandle(), m_framebuffers[ix], PD_VK_ALLOCATOR_FUTURE);
            vkDestroyImageView(m_pDevice->GetHandle(), m_imageViews[ix], PD_VK_ALLOCATOR_FUTURE);
        }
        m_framebuffers.Deallocate();

        m_renderPass.Dispose(*m_pDevice);
        m_depthImage.Dispose(*m_pDevice);

        m_imageViews.Deallocate();
        m_images.Deallocate();

        vkDestroySwapchainKHR(m_pDevice->GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanSwapchain::CreateImageViews()
    {
        m_imageViews.Allocate(m_numImages, false);

        VkImageViewCreateInfo imageViewCreateInfo;
        imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCreateInfo.pNext = nullptr;
        imageViewCreateInfo.flags = 0;
        imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        imageViewCreateInfo.format = m_surfaceFormat.format;
        imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
        imageViewCreateInfo.subresourceRange.levelCount = 1;
        imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
        imageViewCreateInfo.subresourceRange.layerCount = 1;

        for(usize ix = 0; ix < m_imageViews.GetSize(); ix++)
        {
            imageViewCreateInfo.image = m_images[ix];
            PD_VK_THROW_IF_FAILED(vkCreateImageView(m_pDevice->GetHandle(), &imageViewCreateInfo, PD_VK_ALLOCATOR_FUTURE, m_imageViews.GetData() + ix));
        }
    }

    void CVulkanSwapchain::CreateFramebuffers(u32 width, u32 height)
    {
        m_framebuffers.Allocate(m_numImages, false);

        std::array<VkImageView, 2> imageViewHandles = {VK_NULL_HANDLE, m_depthImage.GetImageView().GetHandle()};

        VkFramebufferCreateInfo framebufferCreateInfo;
        framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferCreateInfo.pNext = nullptr;
        framebufferCreateInfo.flags = 0;
        framebufferCreateInfo.renderPass = m_renderPass.GetHandle();
        framebufferCreateInfo.attachmentCount = static_cast<u32>(imageViewHandles.size());
        framebufferCreateInfo.pAttachments = imageViewHandles.data();
        framebufferCreateInfo.width = width;
        framebufferCreateInfo.height = height;
        framebufferCreateInfo.layers = 1;

        for(usize ix = 0; ix < m_images.GetSize(); ix++)
        {
            imageViewHandles[0] = m_imageViews[ix];

            VkFramebuffer fb;
            PD_VK_THROW_IF_FAILED(vkCreateFramebuffer(m_pDevice->GetHandle(), &framebufferCreateInfo, PD_VK_ALLOCATOR_FUTURE, m_framebuffers.GetData() + ix));
        }
    }

    void CVulkanSwapchain::Create()
    {
        const auto& surfaceCapabilities = m_pSurface->GetCapabilities();

        m_swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        m_swapchainCreateInfo.pNext = nullptr;
        m_swapchainCreateInfo.flags = 0;
        m_swapchainCreateInfo.surface = m_pSurface->GetHandle();
        m_swapchainCreateInfo.minImageCount = m_numImages;
        m_swapchainCreateInfo.imageFormat = m_surfaceFormat.format;
        m_swapchainCreateInfo.imageColorSpace = m_surfaceFormat.colorSpace;
        m_swapchainCreateInfo.imageExtent = surfaceCapabilities.currentExtent;
        m_swapchainCreateInfo.imageArrayLayers = surfaceCapabilities.maxImageArrayLayers;
        m_swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        m_swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        m_swapchainCreateInfo.queueFamilyIndexCount = 0;
        m_swapchainCreateInfo.pQueueFamilyIndices = nullptr;
        m_swapchainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
        m_swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        m_swapchainCreateInfo.presentMode = m_presentMode;
        m_swapchainCreateInfo.clipped = VK_TRUE;
        m_swapchainCreateInfo.oldSwapchain = m_handle;

        PD_VK_THROW_IF_FAILED(vkCreateSwapchainKHR(m_pDevice->GetHandle(), &m_swapchainCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));
    }

    void CVulkanSwapchain::CreateRenderPass()
    {
        CVulkanRenderPassBuilder renderPassBuilder;
        renderPassBuilder.AddAttachment(m_surfaceFormat.format, 1, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE,
                                   VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
        renderPassBuilder.AddAttachment(m_depthImage.GetImageView().GetFormat(), 1, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_DONT_CARE, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

        CVulkanSubpassBuilder builder;
        builder.SetBindPoint(VK_PIPELINE_BIND_POINT_GRAPHICS);
        builder.AddColorAttachment(0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
        builder.SetDepthAttachment(1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
        renderPassBuilder.AddSubpass(builder);

        VkSubpassDependency subpassDependency;
        subpassDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        subpassDependency.dstSubpass = 0;
        subpassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpassDependency.srcAccessMask = 0;
        subpassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        subpassDependency.dependencyFlags = 0;

        renderPassBuilder.AddSubpassDependency(subpassDependency);
        m_renderPass.Create(*m_pDevice, renderPassBuilder);
    }

    u32 CVulkanSwapchain::FindOptimalImageCount() const
    {
        const auto& surfaceCapabilities = m_pSurface->GetCapabilities();
        if(surfaceCapabilities.maxImageCount >= 3) return 3;
        if(surfaceCapabilities.maxImageCount == 2) return 2;
        CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Double buffering is not supported!" << CLogger::GetInstance()->Flush();
        return 0;
    }

    VkPresentModeKHR CVulkanSwapchain::FindOptimalPresentMode(bool verticalSync) const
    {
        if(verticalSync)
        {
            return VK_PRESENT_MODE_FIFO_KHR;
        }
        else
        {
            if(m_pSurface->IsPresentModeSupported(VK_PRESENT_MODE_IMMEDIATE_KHR)) return VK_PRESENT_MODE_IMMEDIATE_KHR;
            if(m_pSurface->IsPresentModeSupported(VK_PRESENT_MODE_MAILBOX_KHR)) return VK_PRESENT_MODE_MAILBOX_KHR;
            if(m_pSurface->IsPresentModeSupported(VK_PRESENT_MODE_FIFO_RELAXED_KHR)) return VK_PRESENT_MODE_FIFO_RELAXED_KHR;
        }
        CLogger::GetInstance()->Log(ELogLevel::Warning, PD_VK_RENDERER_PREFIX) << L"Failed to find non-vertical sync present mode!" << CLogger::GetInstance()->Flush();
        return VK_PRESENT_MODE_FIFO_KHR;
    }

    //@formatter:off
    static std::array<VkFormat, 4> _S_NON_HDR_SURFACE_FORMATS =
    {
            VK_FORMAT_B8G8R8A8_UNORM,
            VK_FORMAT_R8G8B8A8_UNORM,
            VK_FORMAT_B8G8R8A8_SRGB,
            VK_FORMAT_R8G8B8A8_SRGB,
    };

    static std::array<VkFormat, 2> _S_HDR_SURFACE_FORMATS =
    {
            VK_FORMAT_A2B10G10R10_UNORM_PACK32,
            VK_FORMAT_A2R10G10B10_UNORM_PACK32,
    };
    //@formatter:on

    VkSurfaceFormatKHR CVulkanSwapchain::FindOptimalSurfaceFormat(bool useHDR) const
    {
        VkColorSpaceKHR colorSpace;
        if(useHDR)
        {
            for(usize ix = 0; ix < _S_HDR_SURFACE_FORMATS.size(); ix++)
            {
                const auto format = _S_HDR_SURFACE_FORMATS[ix];
                if(m_pSurface->IsFormatSupported(format, colorSpace)) return VkSurfaceFormatKHR{format, colorSpace};
            }
        }
        for(usize ix = 0; ix < _S_NON_HDR_SURFACE_FORMATS.size(); ix++)
        {
            const auto format = _S_NON_HDR_SURFACE_FORMATS[ix];
            if(m_pSurface->IsFormatSupported(format, colorSpace)) return VkSurfaceFormatKHR{format, colorSpace};
        }
        CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Failed to find optimal surface format!" << CLogger::GetInstance()->Flush();
        return VkSurfaceFormatKHR{};
    }

    void CVulkanSwapchain::Initialize(CVulkanDevice& device, CVulkanSurface& surface, VkFormat depthFormat, bool verticalSync, bool useHDR)
    {
        m_pDevice = &device;
        m_pSurface = &surface;

        m_numImages = FindOptimalImageCount();
        m_presentMode = FindOptimalPresentMode(verticalSync);
        m_surfaceFormat = FindOptimalSurfaceFormat(useHDR);

        Create();

        PD_VK_THROW_IF_FAILED(vkGetSwapchainImagesKHR(device.GetHandle(), m_handle, &m_numImages, nullptr));
        m_images.Allocate(m_numImages, false);
        PD_VK_THROW_IF_FAILED(vkGetSwapchainImagesKHR(device.GetHandle(), m_handle, &m_numImages, m_images.GetData()));

        auto size = m_pSurface->GetCapabilities().currentExtent;

        CreateImageViews();
        m_depthImage.Create(device, size.width, size.height, depthFormat);

        CreateRenderPass();
        CreateFramebuffers(size.width, size.height);

        m_semaphoreImageAvailable.Initialize(device);
        m_semaphoreRenderingDone.Initialize(device);
        m_fence.Initialize(device);

        CLogger::GetInstance()->Log(ELogLevel::Success, PD_VK_RENDERER_PREFIX) << L"Swapchain created" << CLogger::GetInstance()->Flush();

        PD_END_INITIALIZE;
    }

    void CVulkanSwapchain::Resize(CVulkanDevice& device, u32 width, u32 height)
    {
        m_pDevice->WaitIdle();

        for(u32 ix = 0; ix < m_numImages; ix++)
        {
            vkDestroyFramebuffer(m_pDevice->GetHandle(), m_framebuffers[ix], PD_VK_ALLOCATOR_FUTURE);
            vkDestroyImageView(m_pDevice->GetHandle(), m_imageViews[ix], PD_VK_ALLOCATOR_FUTURE);
        }

        auto depthFormat = m_depthImage.GetImageView().GetFormat();
        m_depthImage.Dispose(device);

        VkSwapchainKHR handleOldSwapchain = m_handle;
        m_pSurface->RefreshCapabilities();

        Create();

        u32 newImageCount;
        PD_VK_THROW_IF_FAILED(vkGetSwapchainImagesKHR(m_pDevice->GetHandle(), m_handle, &newImageCount, nullptr));

        if(newImageCount != m_numImages)
        {
            m_numImages = newImageCount;

            m_images.Deallocate();
            m_imageViews.Deallocate();
            m_framebuffers.Deallocate();
        }

        PD_VK_THROW_IF_FAILED(vkGetSwapchainImagesKHR(m_pDevice->GetHandle(), m_handle, &m_numImages, m_images.GetData()));

        CreateImageViews();
        m_depthImage.Create(*m_pDevice, width, height, depthFormat);

        CreateFramebuffers(width, height);

        if(handleOldSwapchain != VK_NULL_HANDLE) vkDestroySwapchainKHR(m_pDevice->GetHandle(), handleOldSwapchain, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanSwapchain::Begin(CVulkanPrimaryCommandBuffer& commandBuffer, const SColor4f& color)
    {
        PD_VK_THROW_IF_FAILED(vkAcquireNextImageKHR(m_pDevice->GetHandle(), m_handle, std::numeric_limits<u64>::max(),
                                                    m_semaphoreImageAvailable.GetHandle(), VK_NULL_HANDLE, &m_currentImageIndex));

        std::array<VkClearValue, 2> clearValues;
        auto& colorValue = clearValues[0].color;
        colorValue.float32[0] = color.R;
        colorValue.float32[1] = color.G;
        colorValue.float32[2] = color.B;
        colorValue.float32[3] = color.A;

        auto& depthValue = clearValues[1].depthStencil;
        depthValue.depth = 0.0f;
        depthValue.stencil = 0;

        auto& surfaceCapabilities = m_pSurface->GetCapabilities();

        commandBuffer.BeginDebugLabel("Swap Chain", Color4f::Red);

        VkRenderPassBeginInfo renderPassBeginInfo;
        renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassBeginInfo.pNext = nullptr;
        renderPassBeginInfo.renderPass = m_renderPass.GetHandle();
        renderPassBeginInfo.framebuffer = m_framebuffers[m_currentImageIndex];
        renderPassBeginInfo.renderArea.offset.x = 0;
        renderPassBeginInfo.renderArea.offset.y = 0;
        renderPassBeginInfo.renderArea.extent = surfaceCapabilities.currentExtent;
        renderPassBeginInfo.clearValueCount = static_cast<u32>(clearValues.size());
        renderPassBeginInfo.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffer.GetHandle(), &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
    }

    void CVulkanSwapchain::End(CVulkanSemaphore& semaphore, VkPipelineStageFlags waitStageFlags, CVulkanPrimaryCommandBuffer& commandBuffer)
    {
        commandBuffer.EndRenderPass();
        commandBuffer.EndDebugLabel();
        commandBuffer.End();

        std::array<VkPipelineStageFlags, 2> pipelineStageFlags = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT};
        std::array<VkSemaphore, 2> waitSemaphores = {m_semaphoreImageAvailable.GetHandle(), semaphore.GetHandle()};

        VkSubmitInfo submitInfo;
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.pNext = nullptr;
        submitInfo.waitSemaphoreCount = static_cast<u32>(waitSemaphores.size());
        submitInfo.pWaitSemaphores = waitSemaphores.data();
        submitInfo.pWaitDstStageMask = pipelineStageFlags.data();
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer.GetHandle();
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = &m_semaphoreRenderingDone.GetHandle();

        auto handleGraphicsQueue = m_pDevice->GetGraphicsQueue().GetHandle();

        PD_VK_THROW_IF_FAILED(vkQueueSubmit(handleGraphicsQueue, 1, &submitInfo, m_fence.GetHandle()));

        VkPresentInfoKHR presentInfo;
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.pNext = nullptr;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = &m_semaphoreRenderingDone.GetHandle();
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = &m_handle;
        presentInfo.pImageIndices = &m_currentImageIndex;
        presentInfo.pResults = nullptr;

        VkResult presentResult = vkQueuePresentKHR(handleGraphicsQueue, &presentInfo);
        if(presentResult != VK_SUCCESS)
        {
            auto& surfaceCapabilities = m_pSurface->GetCapabilities();
            if(!(surfaceCapabilities.currentExtent.width == 0 || surfaceCapabilities.currentExtent.height == 0))
            {
                PD_VK_THROW_IF_FAILED(presentResult);
            }
        }

        m_fence.WaitFor();
        m_fence.Reset();
    }

    void CVulkanSwapchain::End(CVulkanPrimaryCommandBuffer& commandBuffer)
    {
        commandBuffer.EndRenderPass();
        commandBuffer.End();

        VkPipelineStageFlags pipelineStageFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

        VkSubmitInfo submitInfo;
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.pNext = nullptr;
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = &m_semaphoreImageAvailable.GetHandle();
        submitInfo.pWaitDstStageMask = &pipelineStageFlags;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer.GetHandle();
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = &m_semaphoreRenderingDone.GetHandle();

        auto handleGraphicsQueue = m_pDevice->GetGraphicsQueue().GetHandle();

        PD_VK_THROW_IF_FAILED(vkQueueSubmit(handleGraphicsQueue, 1, &submitInfo, m_fence.GetHandle()));

        VkPresentInfoKHR presentInfo;
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.pNext = nullptr;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = &m_semaphoreRenderingDone.GetHandle();
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = &m_handle;
        presentInfo.pImageIndices = &m_currentImageIndex;
        presentInfo.pResults = nullptr;

        VkResult presentResult = vkQueuePresentKHR(handleGraphicsQueue, &presentInfo);
        if(presentResult != VK_SUCCESS)
        {
            auto& surfaceCapabilities = m_pSurface->GetCapabilities();
            if(!(surfaceCapabilities.currentExtent.width == 0 || surfaceCapabilities.currentExtent.height == 0))
            {
                PD_VK_THROW_IF_FAILED(presentResult);
            }
        }

        m_fence.WaitFor();
        m_fence.Reset();
    }

    void CVulkanSwapchain::PrintProperties()
    {
        auto* pLogger = CLogger::GetInstance();
        pLogger->Log(ELogLevel::Information, PD_VK_RENDERER_PREFIX) << L"\nSwapchain Properties:\nMin Image Count        : " << m_numImages
                                                                    << L"\nSurface Format         : " << VulkanHelper::FormatToString(m_surfaceFormat.format) << L"\nSurface Color Space    : " << VulkanHelper::ColorSpaceToString(m_surfaceFormat.colorSpace)
                                                                    << L"\nSurface Present Mode   : " << VulkanHelper::PresentModeToString(m_presentMode);
        pLogger->Flush();
    }
}