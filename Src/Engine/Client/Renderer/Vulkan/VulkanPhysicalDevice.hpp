//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANPHYSICALDEVICE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANPHYSICALDEVICE_HPP

#pragma once

#include "../../../Core/Types.hpp"
#include "../../../Core/Disposable.hpp"
#include "../../../Core/Collections/Array.hpp"
#include "VulkanHelper.hpp"
#include <functional>

namespace PD
{
    class CVulkanSurface;
    class CVulkanPhysicalDevice
    {
        PD_MAKE_CLASS_DISPOSABLE(CVulkanPhysicalDevice)
    private:
        VkPhysicalDevice m_handle;

        CVulkanSurface* m_pSurface;

        u32 m_queueFamilyIndexGraphics;
        u32 m_queueFamilyIndexCompute;
        u32 m_queueFamilyIndexTransfer;

        VkPhysicalDeviceProperties2 m_properties;
        VkPhysicalDeviceMemoryProperties2 m_memoryProperties;
        VkPhysicalDeviceFeatures2 m_supportedFeatures;

        CArray<VkQueueFamilyProperties2> m_queueFamilyProperties;
        CArray<VkLayerProperties> m_supportedLayers;
        CArray<VkExtensionProperties> m_supportedExtensions;

        VkPhysicalDeviceFeatures m_enabledFeatures;

        inline u32 FindGraphicsQueueFamilyIndex();
        inline u32 FindComputeQueueFamilyIndex();
        inline u32 FindTransferQueueFamilyIndex();

        void OnDisposing();
    public:
        void Initialize(CVulkanSurface& surface, VkPhysicalDevice handlePhysicalDevice,
                        const std::function<void(const VkPhysicalDeviceFeatures&, VkPhysicalDeviceFeatures&)>& functionEnableFeatures);

        bool IsLayerSupported(const char* pLayerName) const;
        bool IsExtensionSupported(const char* pExtensionName) const;

        void PrintProperties();

        PD_FORCEINLINE u32 GetGraphicsQueueFamilyIndex() const {return m_queueFamilyIndexGraphics; };
        PD_FORCEINLINE u32 GetComputeQueueFamilyIndex() const {return m_queueFamilyIndexCompute; };
        PD_FORCEINLINE u32 GetTransferQueueFamilyIndex() const {return m_queueFamilyIndexTransfer; };

        PD_FORCEINLINE const VkPhysicalDeviceProperties& GetProperties() const {return m_properties.properties; }
        PD_FORCEINLINE const VkPhysicalDeviceMemoryProperties& GetMemoryProperties() const {return m_memoryProperties.memoryProperties; }

        PD_FORCEINLINE CArray<VkQueueFamilyProperties2>& GetQueueFamilyProperties() {return m_queueFamilyProperties; }
        PD_FORCEINLINE const CArray<VkQueueFamilyProperties2>& GetQueueFamilyProperties() const {return m_queueFamilyProperties; }
        PD_FORCEINLINE const CArray<VkLayerProperties>& GetSupportedLayers() const {return m_supportedLayers; }
        PD_FORCEINLINE const CArray<VkExtensionProperties>& GetExtensionProperies() const {return m_supportedExtensions; }

        PD_FORCEINLINE const VkPhysicalDeviceFeatures& GetSupportedFeatures() const { return m_supportedFeatures.features; }
        PD_FORCEINLINE const VkPhysicalDeviceFeatures& GetEnabledFeatures() { return m_enabledFeatures; }

        PD_FORCEINLINE VkPhysicalDevice& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkPhysicalDevice& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANPHYSICALDEVICE_HPP