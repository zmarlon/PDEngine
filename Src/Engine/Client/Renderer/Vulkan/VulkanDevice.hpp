//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDEVICE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDEVICE_HPP

#pragma once

#include "../../../Core/Disposable.hpp"
#include "VulkanHelper.hpp"
#include "VulkanQueue.hpp"
#include "VulkanThreadCollection.hpp"
#include "Descriptor/VulkanDescriptorPool.hpp"
#include "Descriptor/VulkanDescriptorSetLayoutTemplates.hpp"
#include "../../../Core/STL/Map.hpp"
#include "../../../Core/STL/Vector.hpp"
#include <vk_mem_alloc.h>
#include <thread>

namespace PD
{
    class CRenderer;
    class CVulkanPhysicalDevice;
    class CVulkanDevice
    {
        PD_MAKE_CLASS_DISPOSABLE(CVulkanDevice)
    private:
        VkDevice m_handle;

        CRenderer* m_pRenderer;
        CVulkanPhysicalDevice* m_pPhysicalDevice;

        VkDeviceCreateInfo m_deviceCreateInfo;
        STL::CVector<VkDeviceQueueCreateInfo> m_deviceQueueCreateInfos;
        STL::CVector<const char*> m_enabledLayers;
        STL::CVector<const char*> m_enabledExtensions;

        CVulkanQueue m_queueGraphics;
        CVulkanQueue m_queueCompute;
        CVulkanQueue m_queueTransfer;

        VmaAllocator m_allocator;
        VmaAllocatorCreateInfo m_allocatorCreateInfo;
        VmaDeviceMemoryCallbacks m_deviceMemoryCallbacks;

        CVulkanDescriptorPool m_descriptorPool;
        CVulkanDescriptorSetLayoutTemplates m_descriptorSetLayoutTemplates;

        STL::CMap<std::thread::id, CVulkanThreadCollection> m_threadCollections;

        void CreateMemoryAllocator();
        void CreateDescriptorPool();

        void OnDisposing();
    public:
        void Initialize(CRenderer& renderer, CVulkanPhysicalDevice& physicalDevice);
        void Create();

        bool AddLayer(const char* pLayerName);
        bool AddExtension(const char* pExtensionName);

        PD_FORCEINLINE void EndFrame()
        {
            for(auto& iterator : m_threadCollections) iterator.second.EndFrame();
        }

        PD_FORCEINLINE void* MapMemory(VmaAllocation handleAllocation)
        {
            void* pReturnValue;
            PD_VK_THROW_IF_FAILED(vmaMapMemory(m_allocator, handleAllocation, &pReturnValue));
            return pReturnValue;
        }

        PD_FORCEINLINE void UnmapMemory(VmaAllocation handleAllocation)
        {
            vmaUnmapMemory(m_allocator, handleAllocation);
        }

        PD_FORCEINLINE void WaitIdle()
        {
            PD_VK_THROW_IF_FAILED(vkDeviceWaitIdle(m_handle));
        }

        void AttachThread(std::thread::id threadId);

        PD_FORCEINLINE CVulkanThreadCollection& GetThreadCollection()
        {
            std::thread::id threadId = std::this_thread::get_id();
            PD_ASSERT(m_threadCollections.find(threadId) != m_threadCollections.end());
            return m_threadCollections[threadId];
        }

        PD_FORCEINLINE CVulkanPhysicalDevice& GetPhysicalDevice()
        {
            return *m_pPhysicalDevice;
        }

        PD_FORCEINLINE CVulkanDescriptorPool& GetDescriptorPool() {return m_descriptorPool; }
        PD_FORCEINLINE CVulkanDescriptorSetLayoutTemplates& GetDSLTemplates() {return m_descriptorSetLayoutTemplates; }

        PD_FORCEINLINE CVulkanQueue& GetGraphicsQueue() { return m_queueGraphics; }
        PD_FORCEINLINE CVulkanQueue& GetComputeQueue() { return m_queueCompute; }
        PD_FORCEINLINE CVulkanQueue& GetTransferQueue() { return m_queueTransfer; }

        PD_FORCEINLINE const VmaAllocator& GetVmaAllocator() const {return m_allocator; }
        PD_FORCEINLINE VmaAllocator& GetVmaAllocator() {return m_allocator; }

        PD_FORCEINLINE VkDevice& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkDevice& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDEVICE_HPP