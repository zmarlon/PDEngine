//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanQueryPool.hpp"
#include "VulkanDevice.hpp"

namespace PD
{
	void CVulkanQueryPool::OnDisposing()
	{
		vkDestroyQueryPool(m_pDevice->GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
	}

	void CVulkanQueryPool::Initialize(CVulkanDevice& device, VkQueryPoolCreateFlags createFlags, VkQueryType queryType, u32 queryCount, VkQueryPipelineStatisticFlags pipelineStatisticFlags)
	{
	    m_pDevice = &device;

        VkQueryPoolCreateInfo queryPoolCreateInfo;
		queryPoolCreateInfo.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
		queryPoolCreateInfo.pNext = nullptr;
		queryPoolCreateInfo.flags = createFlags;
		queryPoolCreateInfo.queryType = queryType;
		queryPoolCreateInfo.queryCount = queryCount;
		queryPoolCreateInfo.pipelineStatistics = pipelineStatisticFlags;

		PD_VK_THROW_IF_FAILED(
				vkCreateQueryPool(device.GetHandle(), &queryPoolCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));

		PD_END_INITIALIZE;
	}

	void CVulkanQueryPool::GetResults(u32 firstQuery, u32 queryCount, usize dataSize, void* pData, VkDeviceSize stride, VkQueryResultFlags flags)
	{
		PD_VK_THROW_IF_FAILED(vkGetQueryPoolResults(m_pDevice->GetHandle(), m_handle, firstQuery, queryCount, dataSize, pData, stride, flags));
	}

	void CVulkanQueryPool::Reset(u32 firstQuery, u32 queryCount)
	{
		vkResetQueryPool(m_pDevice->GetHandle(), m_handle, firstQuery, queryCount);
	}
}