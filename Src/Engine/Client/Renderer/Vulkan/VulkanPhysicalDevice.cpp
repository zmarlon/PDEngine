//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanPhysicalDevice.hpp"
#include "VulkanSurface.hpp"
#include "../../../Core/Logging/Logger.hpp"
#include "../../../Core/String/StringHelper.hpp"

namespace PD
{
    void CVulkanPhysicalDevice::OnDisposing() {}

    u32 CVulkanPhysicalDevice::FindGraphicsQueueFamilyIndex()
    {
        u32 queueCount = 0;
        u32 queueFamily;

        VkResult result;

        for(usize ix = 0; ix < m_queueFamilyProperties.GetSize(); ix++)
        {
            const auto& properties = m_queueFamilyProperties[ix].queueFamilyProperties;
            if((properties.queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0)
            {
                if(properties.queueCount > queueCount)
                {
                    VkBool32 surfaceSupported = VK_FALSE;
                    result = vkGetPhysicalDeviceSurfaceSupportKHR(m_handle, ix, m_pSurface->GetHandle(),
                                                                  &surfaceSupported);
                    if(surfaceSupported)
                    {
                        queueCount = properties.queueCount;
                        queueFamily = ix;
                    }
                }
            }
        }
        if(queueCount == 0)
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Failed to find graphics queue family!" << CLogger::GetInstance()->Flush();
        }
        return queueFamily;
    }

    u32 CVulkanPhysicalDevice::FindComputeQueueFamilyIndex()
    {
        for(usize ix = 0; ix < m_queueFamilyProperties.GetSize(); ix++)
        {
            const auto& properties = m_queueFamilyProperties[ix].queueFamilyProperties;
            if((properties.queueFlags & VK_QUEUE_COMPUTE_BIT) == VK_QUEUE_COMPUTE_BIT) return ix;
        }

        for(usize ix = 0; ix < m_queueFamilyProperties.GetSize(); ix++)
        {
            const auto& properties = m_queueFamilyProperties[ix].queueFamilyProperties;
            if((properties.queueFlags & VK_QUEUE_COMPUTE_BIT) != 0 && ix != m_queueFamilyIndexGraphics) return ix;
        }

        if((m_queueFamilyProperties[m_queueFamilyIndexGraphics].queueFamilyProperties.queueFlags & VK_QUEUE_COMPUTE_BIT) == 0)
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Failed to find compute queue family!"
                                                                                 << CLogger::GetInstance()->Flush();
        }

        return m_queueFamilyIndexGraphics;
    }

    u32 CVulkanPhysicalDevice::FindTransferQueueFamilyIndex()
    {
        for(usize ix = 0; ix < m_queueFamilyProperties.GetSize(); ix++)
        {
            const auto& properties = m_queueFamilyProperties[ix].queueFamilyProperties;
            if((properties.queueFlags & VK_QUEUE_TRANSFER_BIT) == VK_QUEUE_TRANSFER_BIT) return ix;
        }

        for(u32 ix = 0; ix < m_queueFamilyProperties.GetSize(); ix++)
        {
            const auto& properties = m_queueFamilyProperties[ix].queueFamilyProperties;
            if((properties.queueFlags & VK_QUEUE_TRANSFER_BIT) != 0 && ix != m_queueFamilyIndexGraphics && ix != m_queueFamilyIndexCompute) return ix;
        }

        if((m_queueFamilyProperties[m_queueFamilyIndexGraphics].queueFamilyProperties.queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0)
        {
            return m_queueFamilyIndexGraphics;
        }

        if((m_queueFamilyProperties[m_queueFamilyIndexTransfer].queueFamilyProperties.queueFlags & VK_QUEUE_COMPUTE_BIT) == 0)
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Failed to find transfer queue family!"
                                                                                 << CLogger::GetInstance()->Flush();
        }

        return m_queueFamilyIndexTransfer;
    }

    void CVulkanPhysicalDevice::Initialize(CVulkanSurface& surface, VkPhysicalDevice handlePhysicalDevice,
                                           const std::function<void(const VkPhysicalDeviceFeatures&, VkPhysicalDeviceFeatures&)>& functionEnableFeatures)
    {
        m_pSurface = &surface;
        m_handle = handlePhysicalDevice;

        m_properties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
        m_properties.pNext = nullptr;
        vkGetPhysicalDeviceProperties2(handlePhysicalDevice, &m_properties);

        m_memoryProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2;
        m_memoryProperties.pNext = nullptr;
        vkGetPhysicalDeviceMemoryProperties2(handlePhysicalDevice, &m_memoryProperties);

        m_supportedFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
        m_supportedFeatures.pNext = nullptr;
        vkGetPhysicalDeviceFeatures2(handlePhysicalDevice, &m_supportedFeatures);

        u32 numQueueFamilies;
        vkGetPhysicalDeviceQueueFamilyProperties2(handlePhysicalDevice, &numQueueFamilies, nullptr);

        m_queueFamilyProperties.Allocate(numQueueFamilies, false);
        for(auto& familyProperties : m_queueFamilyProperties)
        {
            familyProperties.sType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2;
            familyProperties.pNext = nullptr;
        }
        vkGetPhysicalDeviceQueueFamilyProperties2(handlePhysicalDevice, &numQueueFamilies, m_queueFamilyProperties.GetData());

        u32 numLayerProperties;
        PD_VK_THROW_IF_FAILED(vkEnumerateDeviceLayerProperties(handlePhysicalDevice, &numLayerProperties, nullptr))
        m_supportedLayers.Allocate(numLayerProperties, false);
        PD_VK_THROW_IF_FAILED(vkEnumerateDeviceLayerProperties(handlePhysicalDevice, &numLayerProperties, m_supportedLayers.GetData()))

        u32 numExtensionProperties;
        PD_VK_THROW_IF_FAILED(vkEnumerateDeviceExtensionProperties(handlePhysicalDevice, nullptr, &numExtensionProperties, nullptr))
        m_supportedExtensions.Allocate(numExtensionProperties, false);
        PD_VK_THROW_IF_FAILED(vkEnumerateDeviceExtensionProperties(handlePhysicalDevice, nullptr, &numExtensionProperties, m_supportedExtensions.GetData()))

        if(!functionEnableFeatures)
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Failed to enable devices: function does not exist!" << CLogger::GetInstance()->Flush();
        }

        m_enabledFeatures = {};
        functionEnableFeatures(m_supportedFeatures.features, m_enabledFeatures);

        m_queueFamilyIndexGraphics = FindGraphicsQueueFamilyIndex();
        m_queueFamilyIndexCompute = FindComputeQueueFamilyIndex();
        m_queueFamilyIndexTransfer = FindTransferQueueFamilyIndex();

        PD_END_INITIALIZE;
    }

    bool CVulkanPhysicalDevice::IsLayerSupported(const char* pLayerName) const
    {
        for(const auto& currentLayer : m_supportedLayers)
        {
            if(StringHelper::AreEqual(pLayerName, currentLayer.layerName)) return true;
        }
        return false;
    }

    bool CVulkanPhysicalDevice::IsExtensionSupported(const char* pExtensionName) const
    {
        for(const auto& currentExtension : m_supportedExtensions)
        {
            if(StringHelper::AreEqual(pExtensionName, currentExtension.extensionName)) return true;
        }
        return false;
    }

    void CVulkanPhysicalDevice::PrintProperties()
    {
//@formatter:off
        const auto& properties = m_properties.properties;
        const auto& memoryProperties = m_memoryProperties.memoryProperties;

        auto* pLogger = CLogger::GetInstance();
        pLogger->Log(ELogLevel::Information, PD_VK_RENDERER_PREFIX) << L"Physical Device properties:\nCurrent Physical Device: \nDevice Name:                                           ";
        auto& stream = pLogger->Out();
        stream << properties.deviceName << L"\nApi Version:                                           ";
        VulkanHelper::AppendVersion(properties.apiVersion, stream);
        stream << L"\nDriver Version                                         " << properties.driverVersion
               << L"\nVendor ID:                                             " << properties.vendorID
               << L"\nDevice ID:                                             " << properties.deviceID
               << L"\nDevice Type:                                           ";

        switch(properties.deviceType)
        {
            case VK_PHYSICAL_DEVICE_TYPE_OTHER:
                stream << L"VK_PHYSICAL_DEVICE_TYPE_OTHER";
                break;
            case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
                stream << L"VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU";
                break;
            case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
                stream << L"VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU";
                break;
            case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
                stream << L"VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU";
                break;
            case VK_PHYSICAL_DEVICE_TYPE_CPU:
                stream << L"VK_PHYSICAL_DEVICE_TYPE_CPU";
                break;
            default:stream << L"Unknown GPU Type";
                break;
        }

        stream << L"\n\nPhysical Device Memory Properties: "
               << L"\nMemory Type Count:                                     " << memoryProperties.memoryTypeCount
               << L'\n';
        for(usize ix = 0; ix < memoryProperties.memoryTypeCount; ix++)
        {
            const auto propertyFlags = memoryProperties.memoryTypes[ix].propertyFlags;
            stream << L"Memory Type [" << ix << "]: "
                   << L"\nHeap Index:                                            "
                   << memoryProperties.memoryTypes[ix].heapIndex
                   << L"\nVkMemoryPropertyFlag: "
                   << L"\nVK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT                    "
                   << ((propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) != 0)
                   << L"\nVK_MEMORY_PROPERTY_HOST_VISIBLE_BIT                    "
                   << ((propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) != 0)
                   << L"\nVK_MEMORY_PROPERTY_HOST_COHERENT_BIT                   "
                   << ((propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) != 0)
                   << L"\nVK_MEMORY_PROPERTY_HOST_CACHED_BIT                     "
                   << ((propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) != 0)
                   << L"\nVK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT                "
                   << ((propertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT) != 0)
                   << L"\nVK_MEMORY_PROPERTY_PROTECTED_BIT                       "
                   << ((propertyFlags & VK_MEMORY_PROPERTY_PROTECTED_BIT) != 0)
                   << L'\n';
        }
        stream << L"\nMemory Heap Count:                                     " << memoryProperties.memoryHeapCount;
        for(usize ix = 0; ix < memoryProperties.memoryHeapCount; ix++)
        {
            const auto& currentHeap = memoryProperties.memoryHeaps[ix];
            stream << L"\nMemory Heap [" << ix << L"]: "
                   << L"\nVkDeviceSize:                                          "
                   << currentHeap.size
                   << L"\nVkMemoryHeapFlags: "
                   << L"\nVK_MEMORY_HEAP_DEVICE_LOCAL_BIT                        "
                   << ((currentHeap.flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) != 0)
                   << L"\nVK_MEMORY_HEAP_MULTI_INSTANCE_BIT                      "
                   << ((currentHeap.flags & VK_MEMORY_HEAP_MULTI_INSTANCE_BIT) != 0);
        }

        stream << L"\n\nPhysical Device Queue Family Properties: "
               << L"\nQueue Family Count: " << m_queueFamilyProperties.GetSize() << L'\n';
        for(usize ix = 0; ix < m_queueFamilyProperties.GetSize(); ix++)
        {
            const auto& currentProperties = m_queueFamilyProperties[ix].queueFamilyProperties;

            stream << L"Queue Family [" << ix << L"]: "
                   << L"\nQueue Count:                                           "
                   << currentProperties.queueCount
                   << L"\nQueue Flags:"
                   << L"\nVK_QUEUE_GRAPHICS_BIT                                  "
                   << ((currentProperties.queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0)
                   << L"\nVK_QUEUE_COMPUTE_BIT                                   "
                   << ((currentProperties.queueFlags & VK_QUEUE_COMPUTE_BIT) != 0)
                   << L"\nVK_QUEUE_TRANSFER_BIT                                  "
                   << ((currentProperties.queueFlags & VK_QUEUE_TRANSFER_BIT) != 0)
                   << L"\nVK_QUEUE_SPARSE_BINDING_BIT                            "
                   << ((currentProperties.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) != 0)
                   << L"\nVK_QUEUE_PROTECTED_BIT                                 "
                   << ((currentProperties.queueFlags & VK_QUEUE_PROTECTED_BIT) != 0)
                   << L"\nTimestamp valid bits:                                  "
                   << currentProperties.timestampValidBits
                   << L"\nMinImageTransferGranularity [width, height, depth]     ["
                   << currentProperties.minImageTransferGranularity.width << L", "
                   << currentProperties.minImageTransferGranularity.height << L", "
                   << currentProperties.minImageTransferGranularity.depth << L"]\n";
        }
        stream << L"\nAvailable device layers [" << m_supportedLayers.GetSize() << L"]\n";
        for(usize ix = 0; ix < m_supportedLayers.GetSize(); ix++)
        {
            const auto& currentLayer = m_supportedLayers[ix];
            stream << L"Layer [" << ix << L"]:\n" <<
                   L"Layer Name: " << currentLayer.layerName <<
                   L"\nLayer Implementation Version: " << currentLayer.implementationVersion <<
                   L"\nLayer Spec Version: " << VK_VERSION_MAJOR(currentLayer.specVersion) << L'.'
                   << VK_VERSION_MINOR(currentLayer.specVersion) << L'.'
                   << VK_VERSION_PATCH(currentLayer.specVersion) <<
                   L"\nLayer Description: " << currentLayer.description << L'\n';
        }
        stream << L"\nAvailable device extensions [" << m_supportedExtensions.GetSize()
               << L"]\n";
        for(usize ix = 0; ix < m_supportedExtensions.GetSize(); ix++)
        {
            const auto& currentExtension = m_supportedExtensions[ix];
            stream << L"Extension [" << ix << L"]:\n" <<
                   L"Extension Name: " << currentExtension.extensionName <<
                   L"\nExtension Spec Version: " << VK_VERSION_MAJOR(currentExtension.specVersion)
                   << L'.' << VK_VERSION_MINOR(currentExtension.specVersion) << L'.'
                   << VK_VERSION_PATCH(currentExtension.specVersion) << L'\n';
        }
        pLogger->Flush();
        //@formatter:on
    }
}