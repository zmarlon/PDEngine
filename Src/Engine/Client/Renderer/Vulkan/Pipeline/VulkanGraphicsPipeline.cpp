//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanGraphicsPipeline.hpp"
#include "VulkanPipelineLayout.hpp"
#include "VulkanShaderModule.hpp"
#include "VulkanInputLayout.hpp"
#include "../Image/VulkanRenderPass.hpp"
#include "../VulkanDevice.hpp"

namespace PD
{
    void CVulkanGraphicsPipelineBuilder::AddShader(CVulkanShaderModule& shaderModule)
    {
        m_shaderStageCreateInfos.push_back(shaderModule.GetPipelineShaderStageCreateInfo());
    }

    void CVulkanGraphicsPipelineBuilder::SetInputLayout(CVulkanInputLayoutBuilder& builder)
    {
        m_pipelineVertexInputStateCreateInfo = builder.GetCreateInfo();
        m_vertexInputStateUsed = true;
    }

    void CVulkanGraphicsPipeline::Create(CVulkanDevice& device, CVulkanGraphicsPipelineBuilder& builder, CVulkanRenderPass& renderPass, u32 subpass)
    {
        m_bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

        builder.m_pipelineDynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        builder.m_pipelineDynamicStateCreateInfo.pNext = nullptr;
        builder.m_pipelineDynamicStateCreateInfo.flags = 0;
        builder.m_pipelineDynamicStateCreateInfo.dynamicStateCount = static_cast<u32>(builder.m_dynamicStates.size());
        builder.m_pipelineDynamicStateCreateInfo.pDynamicStates = builder.m_dynamicStates.data();

        if(!builder.m_vertexInputStateUsed)
        {
            builder.m_pipelineVertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
            builder.m_pipelineVertexInputStateCreateInfo.pNext = nullptr;
            builder.m_pipelineVertexInputStateCreateInfo.flags = 0;
            builder.m_pipelineVertexInputStateCreateInfo.vertexBindingDescriptionCount = 0;
            builder.m_pipelineVertexInputStateCreateInfo.pVertexBindingDescriptions = nullptr;
            builder.m_pipelineVertexInputStateCreateInfo.vertexAttributeDescriptionCount = 0;
            builder.m_pipelineVertexInputStateCreateInfo.pVertexAttributeDescriptions = nullptr;
        }

        builder.m_pipelineViewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        builder.m_pipelineViewportStateCreateInfo.pNext = nullptr;
        builder.m_pipelineViewportStateCreateInfo.flags = 0;
        builder.m_pipelineViewportStateCreateInfo.viewportCount = 1;
        builder.m_pipelineViewportStateCreateInfo.pViewports = &builder.m_viewport;
        builder.m_pipelineViewportStateCreateInfo.scissorCount = 1;
        builder.m_pipelineViewportStateCreateInfo.pScissors = &builder.m_scissor;

        VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo;
        graphicsPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        graphicsPipelineCreateInfo.pNext = nullptr;
        graphicsPipelineCreateInfo.flags = 0;
        graphicsPipelineCreateInfo.stageCount = static_cast<u32>(builder.m_shaderStageCreateInfos.size());
        graphicsPipelineCreateInfo.pStages = builder.m_shaderStageCreateInfos.data();
        graphicsPipelineCreateInfo.pVertexInputState = builder.m_vertexInputStateUsed ? &builder.m_pipelineVertexInputStateCreateInfo : nullptr;
        graphicsPipelineCreateInfo.pInputAssemblyState = &builder.m_pipelineInputAssemblyStateCreateInfo;
        graphicsPipelineCreateInfo.pTessellationState = builder.m_tessellationStateUsed
                                                          ? &builder.m_pipelineTessellationStateCreateInfo : nullptr;
        graphicsPipelineCreateInfo.pViewportState = &builder.m_pipelineViewportStateCreateInfo;
        graphicsPipelineCreateInfo.pRasterizationState = &builder.m_pipelineRasterizationStateCreateInfo;
        graphicsPipelineCreateInfo.pMultisampleState = &builder.m_pipelineMultisampleStateCreateInfo;
        graphicsPipelineCreateInfo.pDepthStencilState = builder.m_depthStencilStateUsed
                                                          ? &builder.m_pipelineDepthStencilStateCreateInfo : nullptr;
        graphicsPipelineCreateInfo.pColorBlendState = &builder.m_pipelineColorBlendStateCreateInfo;
        graphicsPipelineCreateInfo.pDynamicState = &builder.m_pipelineDynamicStateCreateInfo;
        graphicsPipelineCreateInfo.layout = m_pLayout->GetHandle();
        graphicsPipelineCreateInfo.renderPass = renderPass.GetHandle();
        graphicsPipelineCreateInfo.subpass = subpass;
        graphicsPipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
        graphicsPipelineCreateInfo.basePipelineIndex = -1;

        PD_VK_THROW_IF_FAILED(vkCreateGraphicsPipelines(device.GetHandle(), VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));

        PD_END_INITIALIZE;
    }
}