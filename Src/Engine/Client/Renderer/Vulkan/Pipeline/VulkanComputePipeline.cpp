//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanComputePipeline.hpp"
#include "VulkanPipelineLayout.hpp"
#include "VulkanShaderModule.hpp"

namespace PD
{
    void CVulkanComputePipelineBuilder::SetShader(CVulkanShaderModule& shaderModule)
    {
        m_pipelineShaderStageCreateInfo = shaderModule.GetPipelineShaderStageCreateInfo();
    }

    void CVulkanComputePipeline::Create(CVulkanDevice& device, const CVulkanComputePipelineBuilder& builder)
    {
        m_bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

        VkComputePipelineCreateInfo computePipelineCreateInfo;
        computePipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
        computePipelineCreateInfo.pNext = nullptr;
        computePipelineCreateInfo.flags = 0;
        computePipelineCreateInfo.stage = builder.m_pipelineShaderStageCreateInfo;
        computePipelineCreateInfo.layout = m_pLayout->GetHandle();
        computePipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
        computePipelineCreateInfo.basePipelineIndex = -1;

        PD_END_INITIALIZE;
    }
}