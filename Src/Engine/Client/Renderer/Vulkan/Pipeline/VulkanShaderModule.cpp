//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanShaderModule.hpp"
#include "../VulkanDevice.hpp"
#include "../../../../Core/Logging/Logger.hpp"
#include "../../../../Core/Pak/PakLocation.hpp"

namespace PD
{
    void CVulkanShaderModule::OnDisposing(CVulkanDevice& device)
    {
        vkDestroyShaderModule(device.GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanShaderModule::LoadBinary(CVulkanDevice& device, const SPakLocation& location, const char* pEntryPoint, VkShaderStageFlagBits shaderStage, VkSpecializationInfo* pSpecializationInfo)
    {
        CArray<byte> buffer;
        location.Load(buffer);

        if((buffer.GetSize() & 3ul) != 0ul)
        {
            CLogger::GetInstance()->Log(ELogLevel::Error, PD_VK_RENDERER_PREFIX)
                    << L"Failed to load shader module from location: " << location << CLogger::GetInstance()->Flush();
        }

        VkShaderModuleCreateInfo shaderModuleCreateInfo;
        shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        shaderModuleCreateInfo.pNext = nullptr;
        shaderModuleCreateInfo.flags = 0;
        shaderModuleCreateInfo.codeSize = static_cast<u32>(buffer.GetSize());
        shaderModuleCreateInfo.pCode = (u32*)buffer.GetData();

        PD_VK_THROW_IF_FAILED(
                vkCreateShaderModule(device.GetHandle(), &shaderModuleCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));

        m_pipelineShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        m_pipelineShaderStageCreateInfo.pNext = nullptr;
        m_pipelineShaderStageCreateInfo.flags = 0;
        m_pipelineShaderStageCreateInfo.stage = shaderStage;
        m_pipelineShaderStageCreateInfo.module = m_handle;
        m_pipelineShaderStageCreateInfo.pName = pEntryPoint;
        m_pipelineShaderStageCreateInfo.pSpecializationInfo = pSpecializationInfo;

        PD_END_INITIALIZE;
    }
}