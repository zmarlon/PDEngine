//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANINPUTLAYOUT_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANINPUTLAYOUT_HPP

#pragma once

#include "../../../../Core/Types.hpp"
#include "../../../../Core/STL/Vector.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanInputLayoutBuilder
    {
    private:
        VkPipelineVertexInputStateCreateInfo m_pipelineVertexInputStateCreateInfo;
        STL::CVector<VkVertexInputBindingDescription> m_bindings;
        STL::CVector<VkVertexInputAttributeDescription> m_attributes;
    public:
        PD_FORCEINLINE void ReserveBindings(usize size)
        {
            m_bindings.reserve(size);
        }

        PD_FORCEINLINE void AddBinding(u32 binding, u32 stride, VkVertexInputRate vertexInputRate)
        {
            VkVertexInputBindingDescription inputBindingDescription;
            inputBindingDescription.binding = binding;
            inputBindingDescription.stride = stride;
            inputBindingDescription.inputRate = vertexInputRate;

            m_bindings.push_back(inputBindingDescription);
        }

        PD_FORCEINLINE void ReserveAttributes(usize size)
        {
            m_attributes.reserve(size);
        }

        PD_FORCEINLINE void AddAttribute(u32 binding, u32 location, VkFormat format, u32 offset)
        {
            VkVertexInputAttributeDescription inputAttributeDescription;
            inputAttributeDescription.location = location;
            inputAttributeDescription.binding = binding;
            inputAttributeDescription.format = format;
            inputAttributeDescription.offset = offset;

            m_attributes.push_back(inputAttributeDescription);
        }

        void Create()
        {
            m_pipelineVertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
            m_pipelineVertexInputStateCreateInfo.pNext = nullptr;
            m_pipelineVertexInputStateCreateInfo.flags = 0;
            m_pipelineVertexInputStateCreateInfo.vertexBindingDescriptionCount = static_cast<u32>(m_bindings.size());
            m_pipelineVertexInputStateCreateInfo.pVertexBindingDescriptions = m_bindings.data();
            m_pipelineVertexInputStateCreateInfo.vertexAttributeDescriptionCount = static_cast<u32>(m_attributes.size());
            m_pipelineVertexInputStateCreateInfo.pVertexAttributeDescriptions = m_attributes.data();
        }

        PD_FORCEINLINE const VkPipelineVertexInputStateCreateInfo& GetCreateInfo() const { return m_pipelineVertexInputStateCreateInfo; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANINPUTLAYOUT_HPP