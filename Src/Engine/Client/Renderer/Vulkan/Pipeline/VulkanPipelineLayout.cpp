//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanPipelineLayout.hpp"
#include "../Descriptor/VulkanDescriptorSetLayout.hpp"
#include "../VulkanDevice.hpp"

namespace PD
{
    void CVulkanPipelineLayoutBuilder::AddDescriptorSetLayout(CVulkanDescriptorSetLayout& descriptorSetLayout)
    {
        m_descriptorSetLayouts.push_back(descriptorSetLayout.GetHandle());
    }

    void CVulkanPipelineLayoutBuilder::AddPushConstantRange(VkShaderStageFlags shaderStage, u32 offset, u32 size)
    {
        VkPushConstantRange pushConstantRange;
        pushConstantRange.stageFlags = shaderStage;
        pushConstantRange.offset = offset;
        pushConstantRange.size = size;

        m_pushConstantRanges.push_back(pushConstantRange);
    }

    void CVulkanPipelineLayout::OnDisposing(CVulkanDevice& device)
    {
        vkDestroyPipelineLayout(device.GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanPipelineLayout::Create(CVulkanDevice& device, const CVulkanPipelineLayoutBuilder& builder)
    {
        VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo;
        pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutCreateInfo.pNext = nullptr;
        pipelineLayoutCreateInfo.flags = 0;
        pipelineLayoutCreateInfo.setLayoutCount = static_cast<u32>(builder.m_descriptorSetLayouts.size());
        pipelineLayoutCreateInfo.pSetLayouts = builder.m_descriptorSetLayouts.data();
        pipelineLayoutCreateInfo.pushConstantRangeCount = static_cast<u32>(builder.m_pushConstantRanges.size());
        pipelineLayoutCreateInfo.pPushConstantRanges = builder.m_pushConstantRanges.data();

        PD_VK_THROW_IF_FAILED(vkCreatePipelineLayout(device.GetHandle(), &pipelineLayoutCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));
        PD_END_INITIALIZE;
    }
}