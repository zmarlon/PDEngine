//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANGRAPHICSPIPELINE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANGRAPHICSPIPELINE_HPP

#pragma once

#include "VulkanPipeline.hpp"
#include "../../../../Core/Types.hpp"
#include "../../../../Core/Memory/SystemAllocator.hpp"
#include "../../../../Core/Math/Color4f.hpp"
#include "../../../../Core/STL/Vector.hpp"

namespace PD
{
    class CVulkanShaderModule;
    class CVulkanInputLayoutBuilder;
    class CVulkanRenderPass;

    class CVulkanGraphicsPipelineBuilder
    {
        friend class CVulkanGraphicsPipeline;
    private:
        VkPipelineRasterizationStateCreateInfo m_pipelineRasterizationStateCreateInfo;
        STL::CVector<VkPipelineShaderStageCreateInfo> m_shaderStageCreateInfos;

        VkPipelineInputAssemblyStateCreateInfo m_pipelineInputAssemblyStateCreateInfo;

        VkPipelineViewportStateCreateInfo m_pipelineViewportStateCreateInfo;
        VkViewport m_viewport;
        VkRect2D m_scissor;

        VkPipelineDynamicStateCreateInfo m_pipelineDynamicStateCreateInfo;
        STL::CVector<VkDynamicState> m_dynamicStates;

        VkPipelineTessellationStateCreateInfo m_pipelineTessellationStateCreateInfo;
        bool m_tessellationStateUsed = false;

        VkPipelineDepthStencilStateCreateInfo m_pipelineDepthStencilStateCreateInfo;
        bool m_depthStencilStateUsed = false;

        VkPipelineMultisampleStateCreateInfo m_pipelineMultisampleStateCreateInfo;

        VkPipelineColorBlendStateCreateInfo m_pipelineColorBlendStateCreateInfo;
        STL::CVector<VkPipelineColorBlendAttachmentState> m_colorBlendAttachments;

        VkPipelineVertexInputStateCreateInfo m_pipelineVertexInputStateCreateInfo;
        bool m_vertexInputStateUsed = false;
    public:
        PD_FORCEINLINE void SetViewport(float x, float y, float width, float height, float minDepth = 0.0f, float maxDepth = 1.0f)
        {
            m_viewport.x = x;
            m_viewport.y = y;
            m_viewport.width = width;
            m_viewport.height = height;
            m_viewport.minDepth = minDepth;
            m_viewport.maxDepth = maxDepth;
        }

        PD_FORCEINLINE void SetScissorRect(int x, int y, u32 width, u32 height)
        {
            m_scissor.offset.x = x;
            m_scissor.offset.y = y;
            m_scissor.extent.width = width;
            m_scissor.extent.height = height;
        }

        PD_FORCEINLINE void SetPrimitiveTopology(VkPrimitiveTopology primitiveTopology, VkBool32 primitiveRestartEnable = VK_FALSE)
        {
            m_pipelineInputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
            m_pipelineInputAssemblyStateCreateInfo.pNext = nullptr;
            m_pipelineInputAssemblyStateCreateInfo.flags = 0;
            m_pipelineInputAssemblyStateCreateInfo.topology = primitiveTopology;
            m_pipelineInputAssemblyStateCreateInfo.primitiveRestartEnable = primitiveRestartEnable;
        }

        PD_FORCEINLINE void SetRasterizerState(VkPolygonMode polygonMode, VkCullModeFlags cullMode, VkFrontFace frontFace,
                                               VkBool32 depthClampEnable = VK_FALSE,
                                               VkBool32 rasterizerDiscardEnable = VK_FALSE, VkBool32 depthBiasEnable = VK_FALSE,
                                               float depthBiasConstantFactor = 0.0f,
                                               float depthBiasClamp = 0.0f, float depthBiasSlopeFactor = 0.0f, float lineWidth = 1.0f)
        {
            m_pipelineRasterizationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
            m_pipelineRasterizationStateCreateInfo.pNext = nullptr;
            m_pipelineRasterizationStateCreateInfo.flags = 0;
            m_pipelineRasterizationStateCreateInfo.depthClampEnable = depthClampEnable;
            m_pipelineRasterizationStateCreateInfo.rasterizerDiscardEnable = rasterizerDiscardEnable;
            m_pipelineRasterizationStateCreateInfo.polygonMode = polygonMode;
            m_pipelineRasterizationStateCreateInfo.cullMode = cullMode;
            m_pipelineRasterizationStateCreateInfo.frontFace = frontFace;
            m_pipelineRasterizationStateCreateInfo.depthBiasEnable = depthBiasEnable;
            m_pipelineRasterizationStateCreateInfo.depthBiasConstantFactor = depthBiasConstantFactor;
            m_pipelineRasterizationStateCreateInfo.depthBiasClamp = depthBiasClamp;
            m_pipelineRasterizationStateCreateInfo.depthBiasSlopeFactor = depthBiasSlopeFactor;
            m_pipelineRasterizationStateCreateInfo.lineWidth = lineWidth;
        }

        PD_FORCEINLINE void ReserveShader(usize size)
        {
            m_shaderStageCreateInfos.reserve(size);
        }

        void AddShader(CVulkanShaderModule& shaderModule);
        void SetInputLayout(CVulkanInputLayoutBuilder& builder);

        PD_FORCEINLINE void ReserveDynamicStates(usize size)
        {
            m_dynamicStates.reserve(size);
        }

        PD_FORCEINLINE void AddDynamicState(VkDynamicState dynamicState)
        {
            m_dynamicStates.reserve(dynamicState);
        }

        PD_FORCEINLINE void SetMultisampleState(u32 samples, VkBool32 sampleShadingEnable = VK_FALSE, float minSampleShading = 0.0f,
                                                const VkSampleMask* pSampleMask = nullptr, VkBool32 alphaToCoverageEnable = VK_FALSE,
                                                VkBool32 alphaToOneEnable = VK_FALSE)
        {
            m_pipelineMultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
            m_pipelineMultisampleStateCreateInfo.pNext = nullptr;
            m_pipelineMultisampleStateCreateInfo.flags = 0;
            m_pipelineMultisampleStateCreateInfo.rasterizationSamples = VulkanHelper::GetSampleCount(samples);
            m_pipelineMultisampleStateCreateInfo.sampleShadingEnable = sampleShadingEnable;
            m_pipelineMultisampleStateCreateInfo.minSampleShading = minSampleShading;
            m_pipelineMultisampleStateCreateInfo.pSampleMask = pSampleMask;
            m_pipelineMultisampleStateCreateInfo.alphaToCoverageEnable = alphaToCoverageEnable;
            m_pipelineMultisampleStateCreateInfo.alphaToOneEnable = alphaToOneEnable;
        }

        PD_FORCEINLINE void ReserveColorBlendAttachments(usize size)
        {
            m_colorBlendAttachments.reserve(size);
        }

        PD_FORCEINLINE void
        AddColorBlendAttachment(VkBool32 blendEnable, VkBlendFactor srcColorBlendFactor = VK_BLEND_FACTOR_ONE,
                                VkBlendFactor dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
                                VkBlendOp colorBlendOp = VK_BLEND_OP_ADD,
                                VkBlendFactor srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
                                VkBlendFactor dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
                                VkBlendOp alphaBlendOp = VK_BLEND_OP_ADD, VkColorComponentFlags colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT)
        {
            VkPipelineColorBlendAttachmentState pipelineColorBlendAttachmentState;
            pipelineColorBlendAttachmentState.blendEnable = blendEnable;
            pipelineColorBlendAttachmentState.srcColorBlendFactor = srcColorBlendFactor;
            pipelineColorBlendAttachmentState.dstColorBlendFactor = dstColorBlendFactor;
            pipelineColorBlendAttachmentState.colorBlendOp = colorBlendOp;
            pipelineColorBlendAttachmentState.srcAlphaBlendFactor = srcAlphaBlendFactor;
            pipelineColorBlendAttachmentState.dstAlphaBlendFactor = dstAlphaBlendFactor;
            pipelineColorBlendAttachmentState.alphaBlendOp = alphaBlendOp;
            pipelineColorBlendAttachmentState.colorWriteMask = colorWriteMask;

            m_colorBlendAttachments.push_back(pipelineColorBlendAttachmentState);
        }

        PD_FORCEINLINE void SetColorBlendState(VkBool32 logicOpEnable = VK_FALSE, VkLogicOp logicOp = VK_LOGIC_OP_NO_OP, const SColor4f& color = Color4f::Black)
        {
            m_pipelineColorBlendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
            m_pipelineColorBlendStateCreateInfo.pNext = nullptr;
            m_pipelineColorBlendStateCreateInfo.flags = 0;
            m_pipelineColorBlendStateCreateInfo.logicOpEnable = logicOpEnable;
            m_pipelineColorBlendStateCreateInfo.logicOp = logicOp;
            m_pipelineColorBlendStateCreateInfo.attachmentCount = static_cast<u32>(m_colorBlendAttachments.size());
            m_pipelineColorBlendStateCreateInfo.pAttachments = m_colorBlendAttachments.data();
            m_pipelineColorBlendStateCreateInfo.blendConstants[0] = color.R;
            m_pipelineColorBlendStateCreateInfo.blendConstants[1] = color.G;
            m_pipelineColorBlendStateCreateInfo.blendConstants[2] = color.B;
            m_pipelineColorBlendStateCreateInfo.blendConstants[3] = color.A;
        }

        PD_FORCEINLINE void AddTessellationState(u32 patchControlPoints)
        {
            m_pipelineTessellationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
            m_pipelineTessellationStateCreateInfo.pNext = nullptr;
            m_pipelineTessellationStateCreateInfo.flags = 0;
            m_pipelineTessellationStateCreateInfo.patchControlPoints = patchControlPoints;
            m_tessellationStateUsed = true;
        }

        PD_FORCEINLINE void SetDepthStencilState(VkBool32 depthTestEnable, VkBool32 depthWriteEnable, VkCompareOp depthCompareOp,
                                                 VkBool32 stencilTestEnable = VK_FALSE, const VkStencilOpState& front = {},
                                                 const VkStencilOpState& back = {},
                                                 VkBool32 depthBoundsTestEnable = VK_FALSE, float minDepthBounds = 0.0f,
                                                 float maxDepthBounds = 1.0f)
        {
            m_pipelineDepthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
            m_pipelineDepthStencilStateCreateInfo.pNext = nullptr;
            m_pipelineDepthStencilStateCreateInfo.flags = 0;
            m_pipelineDepthStencilStateCreateInfo.depthTestEnable = depthTestEnable;
            m_pipelineDepthStencilStateCreateInfo.depthWriteEnable = depthWriteEnable;
            m_pipelineDepthStencilStateCreateInfo.depthCompareOp = depthCompareOp;
            m_pipelineDepthStencilStateCreateInfo.depthBoundsTestEnable = depthBoundsTestEnable;
            m_pipelineDepthStencilStateCreateInfo.stencilTestEnable = stencilTestEnable;
            m_pipelineDepthStencilStateCreateInfo.front = front;
            m_pipelineDepthStencilStateCreateInfo.back = back;
            m_pipelineDepthStencilStateCreateInfo.minDepthBounds = minDepthBounds;
            m_pipelineDepthStencilStateCreateInfo.maxDepthBounds = maxDepthBounds;
            m_depthStencilStateUsed = true;
        }
    };

    class CVulkanGraphicsPipeline : public CVulkanPipeline
    {
    public:
        void Create(CVulkanDevice& device, CVulkanGraphicsPipelineBuilder& builder, CVulkanRenderPass& renderPass, u32 subpass = 0);
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANGRAPHICSPIPELINE_HPP