//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANSHADERMODULE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANSHADERMODULE_HPP

#pragma once

#include "../../../../Core/Disposable.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    struct SPakLocation;
    class CVulkanDevice;
    class CVulkanShaderModule
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanShaderModule, CVulkanDevice&)
    private:
        VkShaderModule m_handle;
        VkPipelineShaderStageCreateInfo m_pipelineShaderStageCreateInfo;

        void OnDisposing(CVulkanDevice& device);
    public:
        void LoadBinary(CVulkanDevice& device, const SPakLocation& location, const char* pEntryPoint,
                        VkShaderStageFlagBits shaderStage, VkSpecializationInfo* pSpecializationInfo = nullptr);

        PD_FORCEINLINE const VkPipelineShaderStageCreateInfo& GetPipelineShaderStageCreateInfo() const { return m_pipelineShaderStageCreateInfo; }

        PD_FORCEINLINE VkShaderModule& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkShaderModule& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANSHADERMODULE_HPP