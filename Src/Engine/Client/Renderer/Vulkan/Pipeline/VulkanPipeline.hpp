//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANPIPELINE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANPIPELINE_HPP

#pragma once

#include "../../../../Core/Disposable.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanDevice;
    class CVulkanPipelineLayout;
    class CVulkanPipeline
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanPipeline, CVulkanDevice&)
    protected:
        VkPipeline m_handle;
        CVulkanPipelineLayout* m_pLayout = nullptr;

        VkPipelineBindPoint m_bindPoint;

        void OnDisposing(CVulkanDevice& device);
    public:
        void SetLayout(CVulkanPipelineLayout& layout);

        PD_FORCEINLINE CVulkanPipelineLayout& GetLayout()
        {
            PD_ASSERT(m_pLayout);
            return *m_pLayout;
        }

        PD_FORCEINLINE VkPipelineBindPoint GetBindPoint() const
        {
            return m_bindPoint;
        }

        PD_FORCEINLINE VkPipeline GetHandle() { return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANPIPELINE_HPP