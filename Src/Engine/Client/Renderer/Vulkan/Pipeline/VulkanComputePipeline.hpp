//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANCOMPUTEPIPELINE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANCOMPUTEPIPELINE_HPP

#pragma once

#include "VulkanPipeline.hpp"

namespace PD
{
    class CVulkanShaderModule;

    class CVulkanComputePipelineBuilder
    {
        friend class CVulkanComputePipeline;
    private:
        VkPipelineShaderStageCreateInfo m_pipelineShaderStageCreateInfo;
    public:
        void SetShader(CVulkanShaderModule& shaderModule);
    };

    class CVulkanComputePipeline : public CVulkanPipeline
    {
    public:
        void Create(CVulkanDevice& device, const CVulkanComputePipelineBuilder& builder);
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANCOMPUTEPIPELINE_HPP