//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanPipeline.hpp"
#include "../VulkanDevice.hpp"

namespace PD
{
    void CVulkanPipeline::OnDisposing(CVulkanDevice& device)
    {
        vkDestroyPipeline(device.GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanPipeline::SetLayout(CVulkanPipelineLayout& layout)
    {
        m_pLayout = &layout;
    }
}