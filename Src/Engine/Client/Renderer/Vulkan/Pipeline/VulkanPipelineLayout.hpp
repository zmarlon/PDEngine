//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANPIPELINELAYOUT_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANPIPELINELAYOUT_HPP

#pragma once

#include "../../../../Core/Types.hpp"
#include "../../../../Core/Disposable.hpp"
#include "../../../../Core/Memory/SystemAllocator.hpp"
#include "../../../../Core/STL/Vector.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanDevice;
    class CVulkanDescriptorSetLayout;

    class CVulkanPipelineLayoutBuilder
    {
        friend class CVulkanPipelineLayout;
    private:
        STL::CVector<VkDescriptorSetLayout> m_descriptorSetLayouts;
        STL::CVector<VkPushConstantRange> m_pushConstantRanges;

    public:
        PD_FORCEINLINE void ReserveLayouts(usize size)
        {
            m_descriptorSetLayouts.reserve(size);
        }

        PD_FORCEINLINE void ReserveConstants(usize size)
        {
            m_pushConstantRanges.reserve(size);
        }

        void AddDescriptorSetLayout(CVulkanDescriptorSetLayout& descriptorSetLayout);
        void AddPushConstantRange(VkShaderStageFlags shaderStage, u32 offset, u32 size);
    };

    class CVulkanPipelineLayout
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanPipelineLayout, CVulkanDevice&)
    private:
        VkPipelineLayout m_handle;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Create(CVulkanDevice& device, const CVulkanPipelineLayoutBuilder& builder);

        PD_FORCEINLINE VkPipelineLayout& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkPipelineLayout& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_PIPELINE_VULKANPIPELINELAYOUT_HPP