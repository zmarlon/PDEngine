//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSEMAPHORE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSEMAPHORE_HPP

#pragma once

#include "../../../Core/Disposable.hpp"
#include "VulkanHelper.hpp"

namespace PD
{
	class CVulkanDevice;

	class CVulkanSemaphore
	{
		PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanSemaphore, CVulkanDevice&)
	private:
		VkSemaphore m_handle;

		void OnDisposing(CVulkanDevice& device);
	public:
		void Initialize(CVulkanDevice& device);

		PD_FORCEINLINE VkSemaphore& GetHandle() { return m_handle; }
		PD_FORCEINLINE const VkSemaphore& GetHandle() const {return m_handle; }
	};
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSEMAPHORE_HPP