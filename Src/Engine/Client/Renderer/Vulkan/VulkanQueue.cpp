//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanQueue.hpp"
#include "VulkanFence.hpp"
#include "VulkanDevice.hpp"
#include "Command/VulkanCommandBuffer.hpp"

namespace PD
{
    void CVulkanQueue::OnDisposing() {}
    void CVulkanQueue::Initialize(CVulkanDevice& device, u32 familyIndex, u32 index, VkQueueFamilyProperties2& familyProperties)
    {
        m_familyIndex = familyIndex;
        m_index = index;
        m_familyProperties = familyProperties;
        vkGetDeviceQueue(device.GetHandle(), familyIndex, index, &m_handle);
    }

    void CVulkanQueue::Submit(CVulkanPrimaryCommandBuffer& commandBuffer)
    {
        VkSubmitInfo submitInfo;
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.pNext = nullptr;
        submitInfo.waitSemaphoreCount = 0;
        submitInfo.pWaitSemaphores = nullptr;
        submitInfo.pWaitDstStageMask = nullptr;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer.GetHandle();
        submitInfo.signalSemaphoreCount = 0;
        submitInfo.pSignalSemaphores = nullptr;

        PD_VK_THROW_IF_FAILED(vkQueueSubmit(m_handle, 1, &submitInfo, VK_NULL_HANDLE));
    }

    void CVulkanQueue::Submit(CVulkanPrimaryCommandBuffer& commandBuffer, CVulkanFence& fence)
    {
        VkSubmitInfo submitInfo;
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.pNext = nullptr;
        submitInfo.waitSemaphoreCount = 0;
        submitInfo.pWaitSemaphores = nullptr;
        submitInfo.pWaitDstStageMask = nullptr;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer.GetHandle();
        submitInfo.signalSemaphoreCount = 0;
        submitInfo.pSignalSemaphores = nullptr;

        PD_VK_THROW_IF_FAILED(vkQueueSubmit(m_handle, 1, &submitInfo, fence.GetHandle()));
    }
}