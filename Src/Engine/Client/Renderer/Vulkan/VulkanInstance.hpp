//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANINSTANCE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANINSTANCE_HPP

#pragma once

#include "VulkanHelper.hpp"
#include "../../../Core/Disposable.hpp"
#include "../../../Core/Collections/Array.hpp"
#include "../../../Core/Types.hpp"
#include "../../../Core/STL/Vector.hpp"

namespace PD
{
    struct SGameInfo;
    class CWindow;
    class CVulkanInstance
    {
        PD_MAKE_CLASS_DISPOSABLE(CVulkanInstance)
    private:
        CWindow* m_pWindow;

        VkApplicationInfo m_applicationInfo;
        VkInstanceCreateInfo m_instanceCreateInfo;

        CArray<const char*> m_sdl2Extensions;

        CArray<VkLayerProperties> m_supportedLayers;
        CArray<VkExtensionProperties> m_supportedExtensions;

        STL::CVector<const char*> m_enabledLayers;
        STL::CVector<const char*> m_enabledExtensions;

        CArray<VkPhysicalDevice> m_physicalDevices;

        VkInstance m_handle;

        void OnDisposing();
    public:
        void Initialize(const SGameInfo& gameInfo, CWindow& window);
        void Create();

        VkPhysicalDevice FindOptimalPhysicalDevice() const;

        bool IsLayerSupported(const char* pLayerName) const;
        bool IsExtensionSupported(const char* pExtensionName) const;

        bool AddLayer(const char* pLayerName);
        bool AddExtension(const char* pExtensionName);

        void PrintProperties();

        PD_FORCEINLINE VkInstance& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkInstance& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANINSTANCE_HPP