//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanSemaphore.hpp"
#include "VulkanDevice.hpp"

namespace PD
{
	void CVulkanSemaphore::OnDisposing(CVulkanDevice& device)
	{
		vkDestroySemaphore(device.GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
	}

	void CVulkanSemaphore::Initialize(CVulkanDevice& device)
	{
		VkSemaphoreCreateInfo semaphoreCreateInfo;
		semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		semaphoreCreateInfo.pNext = nullptr;
		semaphoreCreateInfo.flags = 0;

		PD_VK_THROW_IF_FAILED(
				vkCreateSemaphore(device.GetHandle(), &semaphoreCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));

		PD_END_INITIALIZE;
	}
}