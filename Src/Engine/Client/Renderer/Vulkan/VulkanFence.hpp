//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANFENCE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANFENCE_HPP

#pragma once

#include "../../../Core/Disposable.hpp"
#include "VulkanHelper.hpp"

namespace PD
{
	class CVulkanDevice;
	class CVulkanFence
	{
		PD_MAKE_CLASS_DISPOSABLE(CVulkanFence)
	private:
		VkFence m_handle;
		CVulkanDevice* m_pDevice;

		VkFenceCreateInfo m_fenceCreateInfo;

		void OnDisposing();

	public:
		void Initialize(CVulkanDevice& device);

		void Reset();
		void WaitFor();
		VkResult GetStatus();

		PD_FORCEINLINE VkFence& GetHandle() { return m_handle; }
		PD_FORCEINLINE const VkFence& GetHandle() const {return m_handle; }
	};
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANFENCE_HPP