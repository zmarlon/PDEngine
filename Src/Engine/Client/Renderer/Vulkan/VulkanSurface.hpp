//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSURFACE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSURFACE_HPP

#pragma once

#include "../../../Core/Disposable.hpp"
#include "../../../Core/Collections/Array.hpp"
#include "VulkanHelper.hpp"

namespace PD
{
    class CWindow;
    class CVulkanInstance;
    class CVulkanSurface
    {
        PD_MAKE_CLASS_DISPOSABLE(CVulkanSurface)
    private:
        VkSurfaceKHR m_handle;

        CVulkanInstance* m_pInstance;
        VkPhysicalDevice m_handlePhysicalDevice;

        VkSurfaceCapabilitiesKHR m_surfaceCapabilities;
        CArray<VkSurfaceFormatKHR> m_supportedSurfaceFormats;
        CArray<VkPresentModeKHR> m_supportedPresentModes;

        inline void PrintTransform(VkSurfaceTransformFlagsKHR transformFlags, std::wostream& stream);

        void OnDisposing();
    public:
        void Initialize(CVulkanInstance& instance, VkPhysicalDevice handlePhysicalDevice, CWindow& window);

        void RefreshCapabilities();
        void PrintProperties();

        bool IsFormatSupported(VkFormat format, VkColorSpaceKHR& colorSpace) const;
        bool IsFormatSupported(const VkSurfaceFormatKHR& surfaceFormat) const;
        bool IsPresentModeSupported(VkPresentModeKHR presentMode) const;

        PD_FORCEINLINE const VkSurfaceCapabilitiesKHR& GetCapabilities() const {return m_surfaceCapabilities; }
        PD_FORCEINLINE const CArray<VkSurfaceFormatKHR>& GetSupportedSurfaceFormats() const {return m_supportedSurfaceFormats; }
        PD_FORCEINLINE const CArray<VkPresentModeKHR>& GetSupportedPresentModes() const {return m_supportedPresentModes; }

        PD_FORCEINLINE VkSurfaceKHR& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkSurfaceKHR& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSURFACE_HPP