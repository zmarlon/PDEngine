//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANEVENT_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANEVENT_HPP

#pragma once

#include "../../../Core/Disposable.hpp"
#include "VulkanHelper.hpp"

namespace PD
{
	class CVulkanDevice;

	class CVulkanEvent
	{
		PD_MAKE_CLASS_DISPOSABLE(CVulkanEvent)
	private:
		VkEvent m_handle;

		CVulkanDevice* m_pDevice;

		void OnDisposing();

	public:
		void Initialize(CVulkanDevice& device);

		void Set();
		void Reset();
		VkResult GetStatus();

		PD_FORCEINLINE const VkEvent GetHandle() const { return m_handle; }
	};
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANEVENT_HPP