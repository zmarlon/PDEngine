//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANTHREADCOLLECTION_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANTHREADCOLLECTION_HPP

#pragma once

#include "Command/VulkanCommandPool.hpp"
#include "Command/VulkanPrimaryCommandBuffer.hpp"
#include "VulkanFence.hpp"

namespace PD
{
    class CVulkanThreadCollection
    {
        PD_MAKE_CLASS_DISPOSABLE(CVulkanThreadCollection)
    private:
        CVulkanCommandPool m_graphicsPerFrameCommandPool;
        CVulkanCommandPool m_computePerFrameCommandPool;
        CVulkanCommandPool m_transferPerFrameCommandPool;

        CVulkanCommandPool m_graphicsImmediateCommandPool;
        CVulkanCommandPool m_computeImmediateCommandPool;
        CVulkanCommandPool m_transferImmediateCommandPool;

        CVulkanPrimaryCommandBuffer m_graphicsCommandBuffer;
        CVulkanPrimaryCommandBuffer m_computeCommandBuffer;
        CVulkanPrimaryCommandBuffer m_transferCommandBuffer;

        CVulkanFence m_fence;

        CVulkanDevice* m_pDevice;

        void OnDisposing();
    public:
        void Initialize(CVulkanDevice& device);
        void EndFrame();

        CVulkanPrimaryCommandBuffer& BeginImmediateGraphics();
        void EndImmediateGraphics();

        CVulkanPrimaryCommandBuffer& BeginImmediateCompute();
        void EndImmediateCompute();

        CVulkanPrimaryCommandBuffer& BeginImmediateTransfer();
        void EndImmediateTransfer();

        PD_FORCEINLINE CVulkanCommandPool& GetGraphicsPerFrameCommandPool() {return m_graphicsPerFrameCommandPool; };
        PD_FORCEINLINE CVulkanCommandPool& GetComputePerFrameCommandPool() {return m_computePerFrameCommandPool; };
        PD_FORCEINLINE CVulkanCommandPool& GetTransferPerFrameCommandPool() {return m_transferPerFrameCommandPool; };
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANTHREADCOLLECTION_HPP