//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANBUFFER_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANBUFFER_HPP

#pragma once

#include "../../../../Core/Disposable.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanDevice;
    class CVulkanBuffer
    {
        PD_MAKE_CLASS_DISPOSABLE(CVulkanBuffer)
    protected:
        VkBuffer m_handle;
        VmaAllocation m_handleAllocation;
        VmaAllocationInfo m_allocationInfo;
        VmaMemoryUsage m_memoryUsage;

        CVulkanDevice* m_pDevice;
        VkDeviceSize m_size;

        void OnDisposing();
    public:
        void Create(CVulkanDevice& device, VkDeviceSize size, VkBufferUsageFlags usage, VmaMemoryUsage memoryUsage);

        void* MapRaw();
        void Unmap();

        template<typename T>
        PD_FORCEINLINE T* Map() {return reinterpret_cast<T*>(MapRaw()); }

        PD_FORCEINLINE usize GetSize() const {return m_size; }

        PD_FORCEINLINE VmaAllocation& GetAllocationHandle() {return m_handleAllocation; }
        PD_FORCEINLINE const VmaAllocation& GetAllocationHandle() const {return m_handleAllocation; }
        PD_FORCEINLINE const VmaAllocationInfo& GetAllocationInfo() const {return m_allocationInfo; }

        PD_FORCEINLINE VkBuffer& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkBuffer& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANBUFFER_HPP