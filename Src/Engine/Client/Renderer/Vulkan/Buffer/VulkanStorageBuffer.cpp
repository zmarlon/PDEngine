//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanStorageBuffer.hpp"
#include "../../../../Core/Logging/Logger.hpp"
#include "../../../../Core/Memory/MemoryHelper.hpp"
#include "../VulkanDevice.hpp"

namespace PD
{
    void CVulkanStorageBuffer::Create(CVulkanDevice& device, VkDeviceSize size, const void* pData, VkBufferUsageFlags additionalFlags, VmaMemoryUsage memoryUsage)
    {
        if(memoryUsage != VMA_MEMORY_USAGE_GPU_ONLY && memoryUsage != VMA_MEMORY_USAGE_CPU_TO_GPU)
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Storage buffer memory usage must be VMA_MEMORY_USAGE_CPU_TO_GPU or VMA_MEMORY_USAGE_GPU_ONLY" << CLogger::GetInstance()->Flush();
        }

        VkBufferUsageFlags bufferUsageFlags = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | additionalFlags;
        if(pData && memoryUsage == VMA_MEMORY_USAGE_GPU_ONLY) bufferUsageFlags |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;
        CVulkanBuffer::Create(device, size, bufferUsageFlags, memoryUsage);

        if(pData)
        {
            if(memoryUsage == VMA_MEMORY_USAGE_GPU_ONLY)
            {
                CVulkanBuffer stagingBuffer;
                stagingBuffer.Create(device, size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);

                void* pMappedData = stagingBuffer.MapRaw();
                MemoryHelper::Copy(pMappedData, pData, size);
                stagingBuffer.Unmap();

                auto& threadCollection = device.GetThreadCollection();
                auto& commandBuffer = threadCollection.BeginImmediateTransfer();

                VkBufferCopy bufferCopy;
                bufferCopy.srcOffset = 0;
                bufferCopy.dstOffset = 0;
                bufferCopy.size = 0;

                commandBuffer.CopyBuffer(stagingBuffer, *this, 1, &bufferCopy);
                threadCollection.EndImmediateTransfer();

                stagingBuffer.Dispose();

            }
            else if(memoryUsage == VMA_MEMORY_USAGE_CPU_TO_GPU)
            {
                void* pMappedData = MapRaw();
                MemoryHelper::Copy(pMappedData, pData, size);
                Unmap();
            }
        }

        PD_END_INITIALIZE;
    }
}