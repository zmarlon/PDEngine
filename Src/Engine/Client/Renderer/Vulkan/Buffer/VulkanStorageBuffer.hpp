//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANSTORAGEBUFFER_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANSTORAGEBUFFER_HPP

#pragma once

#include "VulkanBuffer.hpp"

namespace PD
{
    class CVulkanStorageBuffer final : public CVulkanBuffer
    {
    public:
        void Create(CVulkanDevice& device, VkDeviceSize size, const void* pData = nullptr, VkBufferUsageFlags additionalFlags = 0, VmaMemoryUsage memoryUsage = VMA_MEMORY_USAGE_GPU_ONLY);

        template<typename T, VkDeviceSize TSize>
        PD_FORCEINLINE void Create(CVulkanDevice& device)
        {
            Create(device, sizeof(T) * TSize);
        }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANSTORAGEBUFFER_HPP