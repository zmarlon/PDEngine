//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanUniformBuffer.hpp"

namespace PD
{
    void CVulkanUniformBuffer::Create(CVulkanDevice& device, VkDeviceSize size, VkBufferUsageFlags additionalFlags)
    {
        CVulkanBuffer::Create(device, size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | additionalFlags, VMA_MEMORY_USAGE_CPU_TO_GPU);
    }
}