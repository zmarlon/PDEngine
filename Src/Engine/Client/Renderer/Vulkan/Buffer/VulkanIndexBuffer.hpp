//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANINDEXBUFFER_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANINDEXBUFFER_HPP

#pragma once

#include "../../../../Core/Types.hpp"
#include "VulkanBuffer.hpp"

namespace PD
{
    class CVulkanIndexBuffer final : public CVulkanBuffer
    {
    private:
        u32 m_indexCount;
        u32 m_stride;
        VkIndexType m_indexType;
    public:
        void Create(CVulkanDevice& device, u32 indexCount, u32 stride, const void* pData, VkBufferUsageFlags additionalFlags = 0, VmaMemoryUsage memoryUsage = VMA_MEMORY_USAGE_GPU_ONLY);

        template<typename T>
        PD_FORCEINLINE void Create(CVulkanDevice& device, u32 indexCount, const T* pData, VkBufferUsageFlags additionalFlags = 0, VmaMemoryUsage memoryUsage = VMA_MEMORY_USAGE_GPU_ONLY)
        {
            Create(device, indexCount, static_cast<u32>(sizeof(T)), pData, additionalFlags, memoryUsage);
        }

        PD_FORCEINLINE u32 GetIndexCount() const {return m_indexCount; }
        PD_FORCEINLINE u32 GetStride() const {return m_stride; }
        PD_FORCEINLINE VkIndexType GetIndexType() const {return m_indexType; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANINDEXBUFFER_HPP