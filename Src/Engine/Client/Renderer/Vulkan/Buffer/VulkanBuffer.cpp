//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanBuffer.hpp"
#include "../VulkanDevice.hpp"

namespace PD
{
    void CVulkanBuffer::OnDisposing()
    {
        vmaDestroyBuffer(m_pDevice->GetVmaAllocator(), m_handle, m_handleAllocation);
    }

    void CVulkanBuffer::Create(CVulkanDevice& device, VkDeviceSize size, VkBufferUsageFlags usage, VmaMemoryUsage memoryUsage)
    {
        m_pDevice = &device;

        VkBufferCreateInfo bufferCreateInfo;
        bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferCreateInfo.pNext = nullptr;
        bufferCreateInfo.flags = 0;
        bufferCreateInfo.size = size;
        bufferCreateInfo.usage = usage;
        bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        bufferCreateInfo.queueFamilyIndexCount = 0;
        bufferCreateInfo.pQueueFamilyIndices = nullptr;

        m_size = size;

        VmaAllocationCreateInfo allocationCreateInfo;
        allocationCreateInfo = {};
        allocationCreateInfo.usage = memoryUsage;

        m_memoryUsage = allocationCreateInfo.usage;

        PD_VK_THROW_IF_FAILED(vmaCreateBuffer(m_pDevice->GetVmaAllocator(), &bufferCreateInfo, &allocationCreateInfo, &m_handle, &m_handleAllocation, &m_allocationInfo))
        PD_END_INITIALIZE;
    }

    void* CVulkanBuffer::MapRaw()
    {
        PD_ASSERT(m_memoryUsage == VMA_MEMORY_USAGE_CPU_ONLY || m_memoryUsage == VMA_MEMORY_USAGE_CPU_TO_GPU);
        return m_pDevice->MapMemory(m_handleAllocation);
    }

    void CVulkanBuffer::Unmap()
    {
        PD_ASSERT(m_memoryUsage == VMA_MEMORY_USAGE_CPU_ONLY || m_memoryUsage == VMA_MEMORY_USAGE_CPU_TO_GPU);
        m_pDevice->UnmapMemory(m_handleAllocation);
    }
}