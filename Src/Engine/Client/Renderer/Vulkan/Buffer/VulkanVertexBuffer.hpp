//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANVERTEXBUFFER_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANVERTEXBUFFER_HPP

#pragma once

#include "../../../../Core/Types.hpp"
#include "VulkanBuffer.hpp"

namespace PD
{
    class CVulkanVertexBuffer final : public CVulkanBuffer
    {
    private:
        u32 m_stride;
        u32 m_vertexCount;
    public:
        void Create(CVulkanDevice& device, u32 vertexCount, u32 stride, const void* pData, VkBufferUsageFlags additionalFlags = 0, VmaMemoryUsage memoryUsage = VMA_MEMORY_USAGE_GPU_ONLY);

        template<typename T>
        PD_FORCEINLINE void Create(CVulkanDevice& device, u32 vertexCount, const T* pData, VkBufferUsageFlags additionalFlags = 0, VmaMemoryUsage memoryUsage = VMA_MEMORY_USAGE_GPU_ONLY)
        {
            Create(device, vertexCount, static_cast<u32>(sizeof(T)), pData, additionalFlags, memoryUsage);
        }

        PD_FORCEINLINE u32 GetStride() const {return m_stride; }
        PD_FORCEINLINE u32 GetVertexCount() const {return m_vertexCount; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_BUFFER_VULKANVERTEXBUFFER_HPP