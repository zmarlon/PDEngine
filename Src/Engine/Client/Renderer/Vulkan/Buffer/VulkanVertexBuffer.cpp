//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanVertexBuffer.hpp"
#include "../../../../Core/Logging/LogLevel.hpp"
#include "../../../../Core/Logging/Logger.hpp"
#include "../../../../Core/Memory/MemoryHelper.hpp"
#include "../VulkanDevice.hpp"

namespace PD
{
    void CVulkanVertexBuffer::Create(CVulkanDevice& device, u32 vertexCount, u32 stride, const void* pData, VkBufferUsageFlags additionalFlags, VmaMemoryUsage memoryUsage)
    {
        m_stride = stride;
        m_vertexCount = vertexCount;

        usize bufferSize = vertexCount * stride;
        if(memoryUsage == VMA_MEMORY_USAGE_CPU_TO_GPU)
        {
            CVulkanBuffer::Create(device, bufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | additionalFlags, memoryUsage);

            if(pData)
            {
                auto* pMappedData = MapRaw();
                MemoryHelper::Copy(pMappedData, pData, bufferSize);
                Unmap();
            }
        }
        else if(memoryUsage == VMA_MEMORY_USAGE_GPU_ONLY)
        {
            CVulkanBuffer::Create(device, bufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | additionalFlags, memoryUsage);

            if(pData)
            {
                CVulkanBuffer stagingBuffer;
                stagingBuffer.Create(device, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);

                auto* pMappedData = stagingBuffer.MapRaw();
                MemoryHelper::Copy(pMappedData, pData, bufferSize);
                stagingBuffer.Unmap();

                auto& threadCollection = device.GetThreadCollection();
                auto& commandBuffer = threadCollection.BeginImmediateTransfer();

                VkBufferCopy bufferCopy;
                bufferCopy.srcOffset = 0;
                bufferCopy.dstOffset = 0;
                bufferCopy.size = bufferSize;

                commandBuffer.CopyBuffer(stagingBuffer, *this, 1, &bufferCopy);
                threadCollection.EndImmediateTransfer();

                stagingBuffer.Dispose();
            }
        }
        else
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Vertex buffer memory usage must be VMA_MEMORY_USAGE_CPU_TO_GPU or VMA_MEMORY_USAGE_GPU_ONLY" << CLogger::GetInstance()->Flush();
        }
    }
}