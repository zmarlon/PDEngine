//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANCOMMANDBUFFER_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANCOMMANDBUFFER_HPP

#pragma once

#include "../../../../Core/Disposable.hpp"
#include "../../../../Core/Types.hpp"
#include "../../../../Core/Math/Color4f.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanDevice;
    class CVulkanCommandPool;
    class CVulkanPipeline;
    class CVulkanCommandBuffer
    {
        PD_MAKE_CLASS_DISPOSABLE(CVulkanCommandBuffer)
    protected:
        VkCommandBuffer m_handle;

        CVulkanDevice* m_pDevice;
        CVulkanCommandPool* m_pCommandPool;
        CVulkanPipeline* m_pPipeline = nullptr;

        void Initialize(CVulkanDevice& device, CVulkanCommandPool& commandPool, VkCommandBufferLevel commandBufferLevel);

        void OnDisposing();
    public:
        //Vulkan 1.0

        PD_FORCEINLINE void Reset(VkCommandBufferResetFlags resetFlags = 0)
        {
            PD_VK_THROW_IF_FAILED(vkResetCommandBuffer(m_handle, resetFlags))
        }

        PD_FORCEINLINE void End()
        {
            PD_VK_THROW_IF_FAILED(vkEndCommandBuffer(m_handle))
        }

        template<typename TVulkanQueryPool>
        PD_FORCEINLINE void BeginQuery(TVulkanQueryPool& queryPool, u32 query, VkQueryControlFlags flags)
        {
            vkCmdBeginQuery(m_handle, queryPool.GetHandle(), query, flags);
        }

        template<typename TVulkanDescriptorSet>
        PD_FORCEINLINE void BindDescriptorSet(TVulkanDescriptorSet& descriptorSet, u32 set = 0)
        {
            BindDescriptorSets(set, 1, &descriptorSet.GetHandle(), 0, nullptr);
        }

        void BindDescriptorSets(u32 firstSet, u32 descriptorSetCount, VkDescriptorSet* const pDescriptorSets, u32 dynamicOffsetCount, const u32* pDynamicOffsets);

        template<typename TVulkanIndexBuffer>
        PD_FORCEINLINE void BindIndexBuffer(TVulkanIndexBuffer& indexBuffer, VkDeviceSize offset = 0)
        {
            vkCmdBindIndexBuffer(m_handle, indexBuffer.GetHandle(), offset, indexBuffer.GetIndexType());
        }

        template<typename TVulkanPipeline>
        PD_FORCEINLINE void BindPipeline(TVulkanPipeline& pipeline)
        {
            m_pPipeline = &pipeline;
            vkCmdBindPipeline(m_handle, pipeline.GetBindPoint(), pipeline.GetHandle());
        }

        template<typename TVulkanVertexBuffer>
        PD_FORCEINLINE void BindVertexBuffer(TVulkanVertexBuffer& vertexBuffer, u32 firstBinding = 0, VkDeviceSize offset = 0)
        {
            vkCmdBindVertexBuffers(m_handle, firstBinding, 1, &vertexBuffer.GetHandle(), &offset);
        }

        PD_FORCEINLINE void BindVertexBuffers(u32 firstBinding, u32 bindingCount, VkBuffer const* pBuffers, const VkDeviceSize* pOffsets)
        {
            vkCmdBindVertexBuffers(m_handle, firstBinding, bindingCount, pBuffers, pOffsets);
        }

        template<typename TVulkanImage, typename TVulkanImage2>
        PD_FORCEINLINE void BlitImage(TVulkanImage& srcImage, VkImageLayout srcImageLayout, TVulkanImage2& dstImage, VkImageLayout dstImageLayout, u32 regionCount, const VkImageBlit* pRegions, VkFilter filter)
        {
            BlitImage(srcImage.GetHandle(), srcImageLayout, dstImage.GetHandle(), dstImageLayout, regionCount, pRegions, filter);
        }

        PD_FORCEINLINE void BlitImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, u32 regionCount, const VkImageBlit* pRegions, VkFilter filter)
        {
            vkCmdBlitImage(m_handle, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions, filter);
        }

        PD_FORCEINLINE void ClearAttachments(u32 attachmentCount, const VkClearAttachment* pAttachments, u32 rectCount, const VkClearRect* pRects)
        {
            vkCmdClearAttachments(m_handle, attachmentCount, pAttachments, rectCount, pRects);
        }

        template<typename TVulkanImage>
        PD_FORCEINLINE void ClearColorImage(TVulkanImage& image, VkImageLayout imageLayout, const VkClearColorValue* pColor, u32 rangeCount, const VkImageSubresourceRange* pRanges)
        {
            ClearColorImage(image.GetHandle(), imageLayout, imageLayout, pColor, rangeCount, pRanges);
        }

        PD_FORCEINLINE void ClearColorImage(VkImage image, VkImageLayout imageLayout, const VkClearColorValue* pColor, u32 rangeCount, const VkImageSubresourceRange* pRanges)
        {
            vkCmdClearColorImage(m_handle, image, imageLayout, pColor, rangeCount, pRanges);
        }

        template<typename TVulkanImage>
        PD_FORCEINLINE void ClearDepthStencilImage(TVulkanImage& image, VkImageLayout imageLayout, const VkClearDepthStencilValue* pDepthStencil, u32 rangeCount, const VkImageSubresourceRange* pRanges)
        {
            ClearDepthStencilImage(image.GetHandle(), imageLayout, pDepthStencil, rangeCount, pRanges);
        }

        PD_FORCEINLINE void ClearDepthStencilImage(VkImage image, VkImageLayout imageLayout, const VkClearDepthStencilValue* pDepthStencil, u32 rangeCount, const VkImageSubresourceRange* pRanges)
        {
            vkCmdClearDepthStencilImage(m_handle, image, imageLayout, pDepthStencil, rangeCount, pRanges);
        }

        template<typename TVulkanBuffer, typename TVulkanBuffer2>
        PD_FORCEINLINE void CopyBuffer(TVulkanBuffer& srcBuffer, TVulkanBuffer2& dstBuffer, u32 regionCount, const VkBufferCopy* pRegions)
        {
            CopyBuffer(srcBuffer.GetHandle(), dstBuffer.GetHandle(), regionCount, pRegions);
        }

        PD_FORCEINLINE void CopyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, u32 regionCount, const VkBufferCopy* pRegions)
        {
            vkCmdCopyBuffer(m_handle, srcBuffer, dstBuffer, regionCount, pRegions);
        }

        template<typename TVulkanBuffer, typename TVulkanImage>
        PD_FORCEINLINE void CopyBufferToImage(TVulkanBuffer& srcBuffer, TVulkanImage& dstImage, VkImageLayout dstImageLayout, u32 regionCount, const VkBufferImageCopy* pRegions)
        {
            CopyBufferToImage(srcBuffer.GetHandle(), dstImage.GetHandle(), dstImageLayout, regionCount, pRegions);
        }

        PD_FORCEINLINE void CopyBufferToImage(VkBuffer srcBuffer, VkImage dstImage, VkImageLayout dstImageLayout, u32 regionCount, const VkBufferImageCopy* pRegions)
        {
            vkCmdCopyBufferToImage(m_handle, srcBuffer, dstImage, dstImageLayout, regionCount, pRegions);
        }

        template<typename TVulkanImage, typename TVulkanImage2>
        PD_FORCEINLINE void CopyImage(TVulkanImage& srcImage, VkImageLayout srcImageLayout, TVulkanImage2& dstImage, VkImageLayout dstImageLayout, u32 regionCount, const VkImageCopy* pRegions)
        {
            CopyImage(srcImage.GetHandle(), srcImageLayout, dstImage.GetHandle(), dstImageLayout, regionCount, pRegions);
        }

        PD_FORCEINLINE void CopyImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, u32 regionCount, const VkImageCopy* pRegions)
        {
            vkCmdCopyImage(m_handle, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
        }

        template<typename TVulkanImage, typename TVulkanBuffer>
        PD_FORCEINLINE void CopyImageToBuffer(TVulkanImage& srcImage, VkImageLayout srcImageLayout, TVulkanBuffer& dstBuffer, u32 regionCount, const VkBufferImageCopy* pRegions)
        {
            CopyImageToBuffer(srcImage.GetHandle(), srcImageLayout, dstBuffer.GetHandle(), regionCount, pRegions);
        }

        PD_FORCEINLINE void CopyImageToBuffer(VkImage srcImage, VkImageLayout srcImageLayout, VkBuffer dstBuffer, u32 regionCount, const VkBufferImageCopy* pRegions)
        {
            vkCmdCopyImageToBuffer(m_handle, srcImage, srcImageLayout, dstBuffer, regionCount, pRegions);
        }

        template<typename TVulkanQueryPool>
        PD_FORCEINLINE void CopyQueryPoolResults(TVulkanQueryPool& queryPool, u32 firstQuery, u32 queryCount, VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize stride, VkQueryResultFlags flags)
        {
            vkCmdCopyQueryPoolResults(m_handle, queryPool.GetHandle(), firstQuery, queryCount, dstBuffer, dstOffset, stride, flags);
        }

        PD_FORCEINLINE void Dispatch(u32 groupCountX, u32 groupCountY, u32 groupCountZ)
        {
            vkCmdDispatch(m_handle, groupCountX, groupCountY, groupCountZ);
        }

        template<typename TVulkanBuffer>
        PD_FORCEINLINE void DispatchIndirect(TVulkanBuffer& buffer, VkDeviceSize offset = 0)
        {
            DispatchIndirect(buffer.GetHandle(), offset);
        }

        PD_FORCEINLINE void DispatchIndirect(VkBuffer buffer, VkDeviceSize offset = 0)
        {
            vkCmdDispatchIndirect(m_handle, buffer, offset);
        }

        PD_FORCEINLINE void Draw(u32 vertexCount, u32 instanceCount, u32 firstVertex, u32 firstInstance)
        {
            vkCmdDraw(m_handle, vertexCount, instanceCount, firstVertex, firstInstance);
        }

        PD_FORCEINLINE void DrawIndexed(u32 indexCount, u32 instanceCount, u32 firstIndex, i32 vertexOffset, u32 firstInstance)
        {
            vkCmdDrawIndexed(m_handle, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
        }

        template<typename TVulkanBuffer>
        PD_FORCEINLINE void DrawIndexedIndirect(TVulkanBuffer& buffer, VkDeviceSize offset, u32 drawCount, u32 stride)
        {
            DrawIndexedIndirect(buffer.GetHandle(), offset, drawCount, stride);
        }

        PD_FORCEINLINE void DrawIndexedIndirect(VkBuffer buffer, VkDeviceSize offset, u32 drawCount, u32 stride)
        {
            vkCmdDrawIndexedIndirect(m_handle, buffer, offset, drawCount, stride);
        }

        template<typename TVulkanBuffer>
        PD_FORCEINLINE void DrawIndirect(TVulkanBuffer& buffer, VkDeviceSize offset, u32 drawCount, u32 stride)
        {
            DrawIndirect(buffer.GetHandle(), offset, drawCount, stride);
        }

        PD_FORCEINLINE void DrawIndirect(VkBuffer buffer, VkDeviceSize offset, u32 drawCount, u32 stride)
        {
            vkCmdDrawIndirect(m_handle, buffer, offset, drawCount, stride);
        }

        template<typename TVulkanQueryPool>
        PD_FORCEINLINE void EndQuery(TVulkanQueryPool& queryPool, u32 query)
        {
            vkCmdEndQuery(m_handle, queryPool.GetHandle(), query);
        }

        template<typename TVulkanBuffer>
        PD_FORCEINLINE void FillBuffer(TVulkanBuffer& dstBuffer, VkDeviceSize dstOffset, VkDeviceSize size, u32 data)
        {
            FillBuffer(dstBuffer.GetHandle(), dstOffset, size, data);
        }

        PD_FORCEINLINE void FillBuffer(VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize size, u32 data)
        {
            vkCmdFillBuffer(m_handle, dstBuffer, dstOffset, size, data);
        }

        PD_FORCEINLINE void NextSubpass(VkSubpassContents contents)
        {
            vkCmdNextSubpass(m_handle, contents);
        }

        PD_FORCEINLINE void PipelineBarrier(VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, VkDependencyFlags dependencyFlags, u32 memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers,
                u32 bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, u32 imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers)
        {
            vkCmdPipelineBarrier(m_handle, srcStageMask, dstStageMask, dependencyFlags, memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers);
        }

        void PushConstants(VkShaderStageFlags stageFlags, u32 offset, u32 size, const void* pValues);

        template<typename TVulkanEvent>
        PD_FORCEINLINE void ResetEvent(TVulkanEvent&& event, VkPipelineStageFlags stageMask)
        {
            vkCmdResetEvent(m_handle, event.GetHandle(), stageMask);
        }

        template<typename TVulkanQueryPool>
        PD_FORCEINLINE void ResetQueryPool(TVulkanQueryPool& queryPool, u32 firstQuery, u32 queryCount)
        {
            vkCmdResetQueryPool(m_handle, queryPool.GetHandle(), firstQuery, queryCount);
        }

        template<typename TVulkanImage, typename TVulkanImage2>
        PD_FORCEINLINE void ResolveImage(TVulkanImage& srcImage, VkImageLayout srcImageLayout, TVulkanImage& dstImage, VkImageLayout dstImageLayout, u32 regionCount, const VkImageResolve* pRegions)
        {
            ResolveImage(srcImage.GetHandle(), srcImageLayout, dstImage.GetHandle(), dstImageLayout, regionCount, pRegions);
        }

        PD_FORCEINLINE void ResolveImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, u32 regionCount, const VkImageResolve* pRegions)
        {
            vkCmdResolveImage(m_handle, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
        }

        PD_FORCEINLINE void SetBlendConstants(const float* pBlendConstants)
        {
            vkCmdSetBlendConstants(m_handle, pBlendConstants);
        }

        PD_FORCEINLINE void SetDepthBias(float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor)
        {
            vkCmdSetDepthBias(m_handle, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor);
        }

        PD_FORCEINLINE void SetDepthBounds(float minDepthBounds, float maxDepthBounds)
        {
            vkCmdSetDepthBounds(m_handle, minDepthBounds, maxDepthBounds);
        }

        template<typename TVulkanEvent>
        PD_FORCEINLINE void SetEvent(TVulkanEvent& event, VkPipelineStageFlags stageMask)
        {
            vkCmdSetEvent(m_handle, event.GetHandle(), stageMask);
        }

        PD_FORCEINLINE void SetLineWidth(float lineWidth)
        {
            vkCmdSetLineWidth(m_handle, lineWidth);
        }

        PD_FORCEINLINE void SetScissor(u32 firstScissor, u32 scissorCount, const VkRect2D* pScissors)
        {
            vkCmdSetScissor(m_handle, firstScissor, scissorCount, pScissors);
        }

        PD_FORCEINLINE void SetStencilCompareMask(VkStencilFaceFlags faceMask, u32 compareMask)
        {
            vkCmdSetStencilCompareMask(m_handle, faceMask, compareMask);
        }

        PD_FORCEINLINE void SetStencilReference(VkStencilFaceFlags faceMask, u32 reference)
        {
            vkCmdSetStencilReference(m_handle, faceMask, reference);
        }

        PD_FORCEINLINE void SetStencilWriteMask(VkStencilFaceFlags faceMask, u32 writeMask)
        {
            vkCmdSetStencilWriteMask(m_handle, faceMask, writeMask);
        }

        PD_FORCEINLINE void SetViewport(u32 firstViewport, u32 viewportCount, const VkViewport* pViewports)
        {
            vkCmdSetViewport(m_handle, firstViewport, viewportCount, pViewports);
        }

        template<typename TVulkanBuffer>
        PD_FORCEINLINE void UpdateBuffer(TVulkanBuffer& dstBuffer, VkDeviceSize dstOffset, VkDeviceSize dataSize, const void* pData)
        {
            UpdateBuffer(dstBuffer.GetHandle(), dstOffset, dataSize, pData);
        }

        PD_FORCEINLINE void UpdateBuffer(VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize dataSize, const void* pData)
        {
            vkCmdUpdateBuffer(m_handle, dstBuffer, dstOffset, dataSize, pData);
        }

        PD_FORCEINLINE void WaitEvents(u32 eventCount, const VkEvent* pEvents, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, u32 memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers,
                u32 bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, u32 imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers)
        {
            vkCmdWaitEvents(m_handle, eventCount, pEvents, srcStageMask, dstStageMask, memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers);
        }

        template<typename TVulkanQueryPool>
        PD_FORCEINLINE void WriteTimestamp(VkPipelineStageFlagBits pipelineStage, TVulkanQueryPool& queryPool, u32 query)
        {
            vkCmdWriteTimestamp(m_handle, pipelineStage, queryPool.GetHandle(), query);
        }

        //Vulkan 1.1
        PD_FORCEINLINE void DispatchBase(u32 baseGroupX, u32 baseGroupY, u32 baseGroupZ, u32 groupCountX, u32 groupCountY, u32 groupCountZ)
        {
            vkCmdDispatchBase(m_handle, baseGroupX, baseGroupY, baseGroupZ, groupCountX, groupCountY, groupCountZ);
        }

        PD_FORCEINLINE void SetDeviceMask(u32 deviceMask)
        {
            vkCmdSetDeviceMask(m_handle, deviceMask);
        }

        //Vulkan 1.2
        //TODDO: vkCmdBeginRenderPass2, vkCmdEndRenderPass2, vkCmdNextSubpass2

        PD_FORCEINLINE void DrawIndexedIndirectCount(VkBuffer buffer, VkDeviceSize offset, VkBuffer countBuffer, VkDeviceSize countBufferOffset, u32 maxDrawCount, u32 stride)
        {
            vkCmdDrawIndexedIndirectCount(m_handle, buffer, offset, countBuffer, countBufferOffset, maxDrawCount, stride);
        }

        PD_FORCEINLINE void DrawIndirectCount(VkBuffer buffer, VkDeviceSize offset, VkBuffer countBuffer, VkDeviceSize countBufferOffset, u32 maxDrawCount, u32 stride)
        {
            vkCmdDrawIndirectCount(m_handle, buffer, offset, countBuffer, countBufferOffset, maxDrawCount, stride);
        }

        //VK_KHR_push_descriptor
        void PushDescriptorSetKHR(u32 set, u32 descriptorWriteCount, const VkWriteDescriptorSet* pDescriptorWrites);

        template<typename TVulkanImageView>
        PD_FORCEINLINE void PushSampledImageDescriptorKHR(u32 setIndex, u32 bindingIndex, TVulkanImageView& imageView, VkImageLayout imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
        {
            VkDescriptorImageInfo descriptorImageInfo;
            descriptorImageInfo.sampler = VK_NULL_HANDLE;
            descriptorImageInfo.imageView = imageView.GetHandle();
            descriptorImageInfo.imageLayout = imageLayout;

            VkWriteDescriptorSet writeDescriptorSet;
            writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            writeDescriptorSet.pNext = nullptr;
            writeDescriptorSet.dstSet = VK_NULL_HANDLE;
            writeDescriptorSet.dstBinding = bindingIndex;
            writeDescriptorSet.dstArrayElement = 0;
            writeDescriptorSet.descriptorCount = 1;
            writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
            writeDescriptorSet.pImageInfo = &descriptorImageInfo;
            writeDescriptorSet.pBufferInfo = nullptr;
            writeDescriptorSet.pTexelBufferView = nullptr;

            PushDescriptorSetKHR(setIndex, 1, &writeDescriptorSet);
        }

        template<typename TVulkanUniformBuffer>
        PD_FORCEINLINE void PushUniformBufferDescriptorKHR(u32 setIndex, u32 bindingIndex, TVulkanUniformBuffer& uniformBuffer)
        {
            VkDescriptorBufferInfo descriptorBufferInfo;
            descriptorBufferInfo.buffer = uniformBuffer.GetHandle();
            descriptorBufferInfo.offset = 0;
            descriptorBufferInfo.range = uniformBuffer.GetSize();

            VkWriteDescriptorSet writeDescriptorSet;
            writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            writeDescriptorSet.pNext = nullptr;
            writeDescriptorSet.dstSet = VK_NULL_HANDLE;
            writeDescriptorSet.dstBinding = bindingIndex;
            writeDescriptorSet.dstArrayElement = 0;
            writeDescriptorSet.descriptorCount = 1;
            writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            writeDescriptorSet.pImageInfo = nullptr;
            writeDescriptorSet.pBufferInfo = &descriptorBufferInfo;
            writeDescriptorSet.pTexelBufferView = nullptr;

            PushDescriptorSetKHR(setIndex, 1, &writeDescriptorSet);
        }

        //VK_EXT_debug_utils
        void BeginDebugLabel(const char* pLabelName, const SColor4f& color);
        void EndDebugLabel();

        //Custom methods
        void ChangeImageLayout(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, VkFormat format,
                                              const VkImageSubresourceRange& subresourceRange,
                                              VkPipelineStageFlags srcStageMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                                              VkPipelineStageFlags dstStageMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);

        template<typename TVulkanImage>
        PD_FORCEINLINE void ChangeImageLayout(TVulkanImage& image, VkImageLayout oldLayout, VkImageLayout newLayout, VkFormat format,
                const VkImageSubresourceRange& subresourceRange, VkPipelineStageFlags srcStageMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VkPipelineStageFlags dstStageMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT)
        {
            ChangeImageLayout(image.GetHandle(), oldLayout, newLayout, format, subresourceRange, srcStageMask, dstStageMask);
        }


        PD_FORCEINLINE VkCommandBuffer& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkCommandBuffer& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANCOMMANDBUFFER_HPP