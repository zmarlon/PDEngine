//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanCommandPool.hpp"
#include "../VulkanDevice.hpp"
#include "../VulkanQueue.hpp"

namespace PD
{
    void CVulkanCommandPool::OnDisposing()
    {
        vkDestroyCommandPool(m_pDevice->GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanCommandPool::Initialize(CVulkanDevice& device, CVulkanQueue& queue, VkCommandPoolCreateFlags createFlags)
    {
        m_pDevice = &device;

        VkCommandPoolCreateInfo commandPoolCreateInfo;
        commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        commandPoolCreateInfo.pNext = nullptr;
        commandPoolCreateInfo.flags = createFlags;
        commandPoolCreateInfo.queueFamilyIndex = queue.GetFamilyIndex();

        PD_VK_THROW_IF_FAILED(
                vkCreateCommandPool(device.GetHandle(), &commandPoolCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));

        PD_END_INITIALIZE;
    }

    void CVulkanCommandPool::Reset(VkCommandPoolResetFlags resetFlags)
    {
        PD_VK_THROW_IF_FAILED(vkResetCommandPool(m_pDevice->GetHandle(), m_handle, resetFlags));
    }

    void CVulkanCommandPool::Trim()
    {
        vkTrimCommandPool(m_pDevice->GetHandle(), m_handle, 0);
    }
}