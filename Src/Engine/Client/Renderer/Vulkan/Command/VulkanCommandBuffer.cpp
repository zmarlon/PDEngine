//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanCommandBuffer.hpp"
#include "VulkanCommandPool.hpp"
#include "../VulkanDevice.hpp"
#include "../Pipeline/VulkanPipeline.hpp"
#include "../Pipeline/VulkanPipelineLayout.hpp"
#include "../../Renderer.hpp"
#include "../../../../Core/Logging/Logger.hpp"

namespace PD
{
    void CVulkanCommandBuffer::OnDisposing()
    {
        vkFreeCommandBuffers(m_pDevice->GetHandle(), m_pCommandPool->GetHandle(), 1, &m_handle);
    }

    void CVulkanCommandBuffer::Initialize(CVulkanDevice& device, CVulkanCommandPool& commandPool, VkCommandBufferLevel commandBufferLevel)
    {
        m_pDevice = &device;
        m_pCommandPool = &commandPool;

        VkCommandBufferAllocateInfo commandBufferAllocateInfo;
        commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAllocateInfo.pNext = nullptr;
        commandBufferAllocateInfo.commandPool = commandPool.GetHandle();
        commandBufferAllocateInfo.level = commandBufferLevel;
        commandBufferAllocateInfo.commandBufferCount = 1;

        PD_VK_THROW_IF_FAILED(vkAllocateCommandBuffers(device.GetHandle(), &commandBufferAllocateInfo, &m_handle));

        PD_END_INITIALIZE;
    }

    void CVulkanCommandBuffer::BindDescriptorSets(u32 firstSet, u32 descriptorSetCount, VkDescriptorSet* const pDescriptorSets, u32 dynamicOffsetCount, const u32* pDynamicOffsets)
	{
		vkCmdBindDescriptorSets(m_handle, m_pPipeline->GetBindPoint(), m_pPipeline->GetLayout().GetHandle(), firstSet, descriptorSetCount, pDescriptorSets, dynamicOffsetCount, pDynamicOffsets);
	}

	void CVulkanCommandBuffer::PushConstants(VkShaderStageFlags stageFlags, u32 offset, u32 size, const void* pValues)
	{
		vkCmdPushConstants(m_handle, m_pPipeline->GetLayout().GetHandle(), stageFlags, offset, size, pValues);
	}

	//VK_KHR_push_descriptor
	void CVulkanCommandBuffer::PushDescriptorSetKHR(u32 set, u32 descriptorWriteCount, const VkWriteDescriptorSet* pDescriptorWrites)
	{
		vkCmdPushDescriptorSetKHR(m_handle, m_pPipeline->GetBindPoint(), m_pPipeline->GetLayout().GetHandle(), set, descriptorWriteCount, pDescriptorWrites);
	}

    //VK_EXT_debug_utils
    void CVulkanCommandBuffer::BeginDebugLabel(const char* pLabelName, const SColor4f& color)
    {
        if(CRenderer::GetInstance()->GetProperties().DebugMarkerEnabled)
        {
            VkDebugUtilsLabelEXT debugUtilsLabelExt;
            debugUtilsLabelExt.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT;
            debugUtilsLabelExt.pNext = nullptr;
            debugUtilsLabelExt.pLabelName = pLabelName;
            debugUtilsLabelExt.color[0] = color.R;
            debugUtilsLabelExt.color[1] = color.G;
            debugUtilsLabelExt.color[2] = color.B;
            debugUtilsLabelExt.color[3] = color.A;

            vkCmdBeginDebugUtilsLabelEXT(m_handle, &debugUtilsLabelExt);
        }
    }

    void CVulkanCommandBuffer::EndDebugLabel()
    {
        if(CRenderer::GetInstance()->GetProperties().DebugMarkerEnabled)
        {
            vkCmdEndDebugUtilsLabelEXT(m_handle);
        }
    }

    void CVulkanCommandBuffer::ChangeImageLayout(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, VkFormat format, const VkImageSubresourceRange& subresourceRange, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask)
    {
        VkAccessFlags srcAccessMask = 0, dstAccessMask = 0;

        switch(oldLayout)
        {
            case VK_IMAGE_LAYOUT_UNDEFINED:srcAccessMask = 0;
                break;
            case VK_IMAGE_LAYOUT_PREINITIALIZED:srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
                break;
            case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
                break;
            case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
                break;
            case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
                break;
            case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                break;
            case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
                break;
            default:
                CLogger::GetInstance()->Log(ELogLevel::Error, PD_VK_RENDERER_PREFIX) << L"Old layout "
                                                                                     << VulkanHelper::ImageLayoutToString(oldLayout)
                                                                                     << L"is not supported!" << CLogger::GetInstance()->Flush();
                break;
        }

        switch(newLayout)
        {
            case VK_IMAGE_LAYOUT_GENERAL:
                dstAccessMask = 0;
                break;
            case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                break;
            case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
                break;
            case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
                break;
            case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:dstAccessMask = dstAccessMask | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
                break;
            case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
                if(srcAccessMask == 0)
                {
                    srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
                }
                dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
                break;
            default:
                CLogger::GetInstance()->Log(ELogLevel::Error, PD_VK_RENDERER_PREFIX) << L"New layout "
                                                                                     << VulkanHelper::ImageLayoutToString(newLayout)
                                                                                     << L" is not supported!" << CLogger::GetInstance()->Flush();
                break;
        }

        VkImageMemoryBarrier imageMemoryBarrier;
        imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        imageMemoryBarrier.pNext = nullptr;
        imageMemoryBarrier.srcAccessMask = srcAccessMask;
        imageMemoryBarrier.dstAccessMask = dstAccessMask;
        imageMemoryBarrier.oldLayout = oldLayout;
        imageMemoryBarrier.newLayout = newLayout;
        imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        imageMemoryBarrier.image = image;
        imageMemoryBarrier.subresourceRange = subresourceRange;

        PipelineBarrier(srcStageMask, dstStageMask, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);
    }
}