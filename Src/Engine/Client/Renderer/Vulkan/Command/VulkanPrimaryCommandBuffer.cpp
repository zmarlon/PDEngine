//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanPrimaryCommandBuffer.hpp"

namespace PD
{
    void CVulkanPrimaryCommandBuffer::Initialize(CVulkanDevice& device, CVulkanCommandPool& commandPool)
    {
        CVulkanCommandBuffer::Initialize(device, commandPool, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
    }
}