//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANCOMMANDPOOL_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANCOMMANDPOOL_HPP

#pragma once

#include "../../../../Core/Disposable.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanQueue;
    class CVulkanDevice;
    class CVulkanCommandPool
    {
        PD_MAKE_CLASS_DISPOSABLE(CVulkanCommandPool)
    private:
        VkCommandPool m_handle;
        CVulkanDevice* m_pDevice;

        void OnDisposing();
    public:
        void Initialize(CVulkanDevice& device, CVulkanQueue& queue, VkCommandPoolCreateFlags createFlags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);
        void Reset(VkCommandPoolResetFlags resetFlags = VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);
        void Trim();

        PD_FORCEINLINE VkCommandPool& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkCommandPool& GetHandle() const { return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANCOMMANDPOOL_HPP