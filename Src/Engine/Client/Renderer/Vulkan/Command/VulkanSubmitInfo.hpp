//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSUBMITINFO_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSUBMITINFO_HPP

#pragma once

#include "../VulkanHelper.hpp"
#include "../../../../Core/Types.hpp"
#include <array>

namespace PD
{
    template<u32 TNumCommandBuffers, u32 TNumWaitSemaphores, u32 TNumSignalSemaphores>
    class CVulkanSubmitInfo
    {
    private:
        std::array <VkCommandBuffer, TNumCommandBuffers> m_commandBuffers;
        std::array <VkSemaphore, TNumWaitSemaphores> m_waitSemaphores;
        std::array <VkPipelineStageFlags, TNumWaitSemaphores> m_pipelineStageFlags;
        std::array <VkSemaphore, TNumSignalSemaphores> m_signalSemaphores;

        VkSubmitInfo m_submitInfo;
    public:
        CVulkanSubmitInfo()
        {
            m_submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            m_submitInfo.pNext = nullptr;
            m_submitInfo.waitSemaphoreCount = TNumWaitSemaphores;
            m_submitInfo.pWaitSemaphores = m_waitSemaphores.data();
            m_submitInfo.pWaitDstStageMask = m_pipelineStageFlags.data();
            m_submitInfo.commandBufferCount = TNumCommandBuffers;
            m_submitInfo.pCommandBuffers = m_commandBuffers.data();
            m_submitInfo.signalSemaphoreCount = TNumSignalSemaphores;
            m_submitInfo.pSignalSemaphores = m_signalSemaphores.data();
        }

        template<typename TVulkanPrimaryCommandBuffer>
        PD_FORCEINLINE void SetCommandBuffer(u32 index, TVulkanPrimaryCommandBuffer& commandBuffer)
        {
            PD_ASSERT(index < TNumCommandBuffers);
            m_commandBuffers[index] = commandBuffer.GetHandle();
        }

        template<typename TVulkanSemaphore>
        PD_FORCEINLINE void SetWaitSemaphore(u32 index, TVulkanSemaphore& semaphore, VkPipelineStageFlags pipelineStageFlags)
        {
            PD_ASSERT(index < TNumWaitSemaphores);
            m_waitSemaphores[index] = semaphore.GetHandle();
            m_pipelineStageFlags[index] = pipelineStageFlags;
        }

        template<typename TVulkanSemaphore>
        PD_FORCEINLINE void SetSignalSemaphore(u32 index, TVulkanSemaphore& semaphore)
        {
            PD_ASSERT(index < TNumSignalSemaphores);
            m_signalSemaphores[index] = semaphore.GetHandle();
        }

        PD_FORCEINLINE const VkSubmitInfo& GetSubmitInfo() const { return m_submitInfo; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSUBMITINFO_HPP