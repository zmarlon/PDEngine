//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANPRIMARYCOMMANDBUFFER_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANPRIMARYCOMMANDBUFFER_HPP

#pragma once

#include "VulkanCommandBuffer.hpp"

namespace PD
{
    class CVulkanPrimaryCommandBuffer : public CVulkanCommandBuffer
    {
    public:
        void Initialize(CVulkanDevice& device, CVulkanCommandPool& commandPool);

        PD_FORCEINLINE void Begin(VkCommandBufferUsageFlags usageFlags = 0)
        {
            VkCommandBufferBeginInfo commandBufferBeginInfo;
            commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            commandBufferBeginInfo.pNext = nullptr;
            commandBufferBeginInfo.flags = usageFlags;
            commandBufferBeginInfo.pInheritanceInfo = nullptr;

            PD_VK_THROW_IF_FAILED(vkBeginCommandBuffer(m_handle, &commandBufferBeginInfo));
        }

        template<typename TVulkanRenderPass, typename TVulkanFramebuffer>
        PD_FORCEINLINE void BeginRenderPass(TVulkanRenderPass& renderPass, TVulkanFramebuffer& framebuffer, u32 clearValueCount,
                                            VkClearValue* pClearValues, VkSubpassContents subpassContents = VK_SUBPASS_CONTENTS_INLINE)
        {
            VkRenderPassBeginInfo renderPassBeginInfo;
            renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassBeginInfo.pNext = nullptr;
            renderPassBeginInfo.renderPass = renderPass.GetHandle();
            renderPassBeginInfo.framebuffer = framebuffer.GetHandle();
            renderPassBeginInfo.renderArea.offset.x = 0;
            renderPassBeginInfo.renderArea.offset.y = 0;
            renderPassBeginInfo.renderArea.extent.width = framebuffer.GetWidth();
            renderPassBeginInfo.renderArea.extent.height = framebuffer.GetHeight();
            renderPassBeginInfo.clearValueCount = clearValueCount;
            renderPassBeginInfo.pClearValues = pClearValues;

            vkCmdBeginRenderPass(m_handle, &renderPassBeginInfo, subpassContents);
        }

        PD_FORCEINLINE void EndRenderPass()
        {
            vkCmdEndRenderPass(m_handle);
        }

        template<typename TVulkanSecondaryCommandBuffer>
        PD_FORCEINLINE void ExecuteCommands(const TVulkanSecondaryCommandBuffer& secondaryCommandBuffer)
        {
            ExecuteCommands(1, &secondaryCommandBuffer.GetHandle());
        }

        PD_FORCEINLINE void ExecuteCommands(u32 commandBufferCount, VkCommandBuffer* const pCommandBuffers)
        {
            vkCmdExecuteCommands(m_handle, commandBufferCount, pCommandBuffers);
        }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANPRIMARYCOMMANDBUFFER_HPP