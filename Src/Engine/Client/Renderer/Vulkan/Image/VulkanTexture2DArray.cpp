//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 11.04.2020.
//
#include "VulkanTexture2DArray.hpp"
#include "../VulkanDevice.hpp"
#include "../../Renderer.hpp"
#include "../../../../Core/Pak/PakLocation.hpp"
#include "../../../../Core/Logging/Logger.hpp"
#include "TextureHelper.hpp"
#include <gli/gli.hpp>

namespace PD
{
    void CVulkanTexture2DArray::OnDisposing(CVulkanDevice& device)
    {
        m_imageView.Dispose(device);
        m_image.Dispose(device);
    }

    void CVulkanTexture2DArray::Load(CVulkanDevice& device, const SPakLocation& location, VkImageUsageFlags imageUsageFlags)
    {
        CArray<byte> buffer;
        location.Load(buffer);

        gli::texture2d_array gliTextureArray(gli::load(reinterpret_cast<char*>(buffer.GetData(), buffer.GetSize())));
        if(gliTextureArray.empty())
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Failed to load Texture2DArray from location: " << location << CLogger::GetInstance()->Flush();
        }

        auto baseMipLevel = TextureHelper::GetStartMipLevel(CRenderer::GetInstance()->GetGraphicsSettings(), static_cast<u32>(gliTextureArray.levels()));
        u32 numMipLevels = static_cast<u32>(gliTextureArray.levels() - baseMipLevel);

        m_image.Create(device, VK_IMAGE_TYPE_2D, static_cast<VkFormat>(gliTextureArray.format()), static_cast<u32>(gliTextureArray.extent().x), static_cast<u32>(gliTextureArray.extent().y), 1, numMipLevels, static_cast<u32>(gliTextureArray.layers()),
                       VK_SAMPLE_COUNT_1_BIT, imageUsageFlags, VK_IMAGE_LAYOUT_UNDEFINED, VMA_MEMORY_USAGE_GPU_ONLY);
        m_imageView.Create(device, m_image, VK_IMAGE_VIEW_TYPE_2D, m_image.GetFormat(), VK_IMAGE_ASPECT_COLOR_BIT);

        VkDeviceSize size = TextureHelper::GetImageArraySize(gliTextureArray, baseMipLevel, numMipLevels);

        //TODO:
    }
}