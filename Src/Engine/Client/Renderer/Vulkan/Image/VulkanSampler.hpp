//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSAMPLER_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSAMPLER_HPP

#pragma once

#include "../../../../Core/Disposable.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanDevice;
    class CVulkanSampler
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanSampler, CVulkanDevice&)
    private:
        VkSampler m_handle;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Create(CVulkanDevice& device, VkFilter magFilter, VkFilter minFilter, VkSamplerMipmapMode mipmapMode,
                    VkSamplerAddressMode addressMode, float mipLodBias = 0.0f, VkBool32 anisotropyEnable = VK_FALSE,
                    VkBool32 compareEnable = VK_FALSE,
                    VkCompareOp compareOp = VK_COMPARE_OP_NEVER, float minLod = 0.0f,
                    float maxLod = std::numeric_limits<float>::max());

        PD_FORCEINLINE VkSampler& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkSampler& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANSAMPLER_HPP