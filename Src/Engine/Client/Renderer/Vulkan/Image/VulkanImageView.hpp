//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANIMAGEVIEW_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANIMAGEVIEW_HPP

#pragma once

#include "../../../../Core/Disposable.hpp"
#include "../../../../Core/Types.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanDevice;
    class CVulkanImageView
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanImageView, CVulkanDevice&)
    private:
        VkImageView m_handle;

        VkFormat m_format;
        VkImageSubresourceRange m_subresourceRange;

        void OnDisposing(CVulkanDevice& device);
    public:
        template<typename TVulkanImage>
        PD_FORCEINLINE void Create(CVulkanDevice& device, TVulkanImage& image, VkImageViewType viewType, VkFormat format, VkImageAspectFlags aspectMask,
                u32 baseMipLevel = 0,
                u32 levelCount = 1,
                u32 baseArrayLayer = 0,
                u32 layerCount = 1,
                VkComponentSwizzle swizzleR = VK_COMPONENT_SWIZZLE_IDENTITY,
                VkComponentSwizzle swizzleG = VK_COMPONENT_SWIZZLE_IDENTITY,
                VkComponentSwizzle swizzleB = VK_COMPONENT_SWIZZLE_IDENTITY,
                VkComponentSwizzle swizzleA = VK_COMPONENT_SWIZZLE_IDENTITY)
        {
            Create(device, image.GetHandle(), viewType, format, aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount, swizzleR, swizzleG, swizzleB, swizzleA);
        }

        void Create(CVulkanDevice& device, VkImage handleImage, VkImageViewType viewType, VkFormat format, VkImageAspectFlags aspectMask,
                    u32 baseMipLevel = 0,
                    u32 levelCount = 1,
                    u32 baseArrayLayer = 0,
                    u32 layerCount = 1,
                    VkComponentSwizzle swizzleR = VK_COMPONENT_SWIZZLE_IDENTITY,
                    VkComponentSwizzle swizzleG = VK_COMPONENT_SWIZZLE_IDENTITY,
                    VkComponentSwizzle swizzleB = VK_COMPONENT_SWIZZLE_IDENTITY,
                    VkComponentSwizzle swizzleA = VK_COMPONENT_SWIZZLE_IDENTITY);

        PD_FORCEINLINE VkFormat GetFormat() const {return m_format; }
        PD_FORCEINLINE const VkImageSubresourceRange& GetSubresourceRange() const {return m_subresourceRange; }

        PD_FORCEINLINE VkImageView& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkImageView& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANIMAGEVIEW_HPP

