//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_VULKANTEXTURE2D_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_VULKANTEXTURE2D_HPP

#pragma once

#include "VulkanImage.hpp"
#include "VulkanImageView.hpp"

namespace PD
{
    struct SGraphicsSettings;
	struct SPakLocation;
    class CVulkanTexture2D final
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanTexture2D, CVulkanDevice&)
    private:
        CVulkanImage m_image;
        CVulkanImageView m_imageView;

        void OnDisposing(CVulkanDevice& device);
    public:
    	void Load(CVulkanDevice& device, const SPakLocation& location, VkImageUsageFlags imageUsageFlags = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);
    	void Load(CVulkanDevice& device, u32 width, u32 height, VkFormat format, u32 stride, const void* pData, VkImageUsageFlags imageUsageFlags = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
    	        VkComponentSwizzle componentSwizzleR = VK_COMPONENT_SWIZZLE_IDENTITY,
    	        VkComponentSwizzle componentSwizzleG = VK_COMPONENT_SWIZZLE_IDENTITY,
    	        VkComponentSwizzle componentSwizzleB = VK_COMPONENT_SWIZZLE_IDENTITY,
    	        VkComponentSwizzle componentSwizzleA = VK_COMPONENT_SWIZZLE_IDENTITY);

        PD_FORCEINLINE u32 GetWidth() const {return m_image.GetWidth(); }
        PD_FORCEINLINE u32 GetHeight() const {return m_image.GetHeight(); }
        PD_FORCEINLINE u32 GetMipLevels() const {return m_image.GetMipLevels(); }
        PD_FORCEINLINE VkFormat GetFormat() const {return m_imageView.GetFormat(); }

        PD_FORCEINLINE CVulkanImage& GetImage() {return m_image; }
        PD_FORCEINLINE const CVulkanImage& GetImage() const {return m_image; }

        PD_FORCEINLINE CVulkanImageView& GetImageView() {return m_imageView; }
        PD_FORCEINLINE const CVulkanImageView& GetImageView() const {return m_imageView; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_VULKANTEXTURE2D_HPP