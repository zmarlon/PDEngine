//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanSampler.hpp"
#include "../VulkanDevice.hpp"
#include "../../Renderer.hpp"

namespace PD
{
    void CVulkanSampler::OnDisposing(CVulkanDevice& device)
    {
        vkDestroySampler(device.GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanSampler::Create(CVulkanDevice& device, VkFilter magFilter, VkFilter minFilter, VkSamplerMipmapMode mipmapMode, VkSamplerAddressMode addressMode, float mipLodBias, VkBool32 anisotropyEnable, VkBool32 compareEnable, VkCompareOp compareOp, float minLod, float maxLod)
    {
        VkSamplerCreateInfo samplerCreateInfo;
        samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        samplerCreateInfo.pNext = nullptr;
        samplerCreateInfo.flags = 0;
        samplerCreateInfo.magFilter = magFilter;
        samplerCreateInfo.minFilter = minFilter;
        samplerCreateInfo.mipmapMode = mipmapMode;
        samplerCreateInfo.addressModeU = addressMode;
        samplerCreateInfo.addressModeV = addressMode;
        samplerCreateInfo.addressModeW = addressMode;
        samplerCreateInfo.mipLodBias = mipLodBias;
        samplerCreateInfo.anisotropyEnable = anisotropyEnable;
        samplerCreateInfo.maxAnisotropy = anisotropyEnable ? TextureQualityLevel::GetAnisotropyLevel(CRenderer::GetInstance()->GetGraphicsSettings().TextureQualityLevel) : 0.0f;
        samplerCreateInfo.compareEnable = compareEnable;
        samplerCreateInfo.compareOp = compareOp;
        samplerCreateInfo.minLod = minLod;
        samplerCreateInfo.maxLod = maxLod;
        samplerCreateInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
        samplerCreateInfo.unnormalizedCoordinates = VK_FALSE;

        PD_VK_THROW_IF_FAILED(
                vkCreateSampler(device.GetHandle(), &samplerCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));
        PD_END_INITIALIZE;
    }
}