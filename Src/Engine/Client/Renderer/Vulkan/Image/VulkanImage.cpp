//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanImage.hpp"
#include "../VulkanDevice.hpp"

namespace PD
{
    void CVulkanImage::OnDisposing(CVulkanDevice& device)
    {
        vmaDestroyImage(device.GetVmaAllocator(), m_handle, m_handleAllocation);
    }

    void CVulkanImage::Create(CVulkanDevice& device, VkImageType type, VkFormat format, u32 width, u32 height, u32 depth, u32 mipLevels, u32 arrayLayers, VkSampleCountFlagBits samples,
            VkImageUsageFlags usage, VkImageLayout initialLayout, VmaMemoryUsage memoryUsage)
    {
        VkImageCreateInfo imageCreateInfo;
        imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        imageCreateInfo.pNext = nullptr;
        imageCreateInfo.flags = 0;
        imageCreateInfo.imageType = type;
        imageCreateInfo.format = format;
        imageCreateInfo.extent.width = width;
        imageCreateInfo.extent.height = height;
        imageCreateInfo.extent.depth = depth;
        imageCreateInfo.mipLevels = mipLevels;
        imageCreateInfo.arrayLayers = arrayLayers;
        imageCreateInfo.samples = samples;
        imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
        imageCreateInfo.usage = usage;
        imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        imageCreateInfo.queueFamilyIndexCount = 0;
        imageCreateInfo.pQueueFamilyIndices = nullptr;
        imageCreateInfo.initialLayout = initialLayout;

        m_format = format;
        m_width = width;
        m_height = height;
        m_depth = depth;
        m_mipLevels = mipLevels;
        m_arrayLayers = arrayLayers;
        m_samples = samples;

        VmaAllocationCreateInfo allocationCreateInfo;
        allocationCreateInfo = {};
        allocationCreateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

        PD_VK_THROW_IF_FAILED(vmaCreateImage(device.GetVmaAllocator(), &imageCreateInfo, &allocationCreateInfo, &m_handle, &m_handleAllocation, &m_allocationInfo))
        PD_END_INITIALIZE;
    }
}