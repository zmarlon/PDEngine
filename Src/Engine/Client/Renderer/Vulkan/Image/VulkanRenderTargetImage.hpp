//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANRENDERTARGETIMAGE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANRENDERTARGETIMAGE_HPP

#pragma once

#include "VulkanImage.hpp"
#include "VulkanImageView.hpp"

namespace PD
{
    class CVulkanRenderTargetImage
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanRenderTargetImage, CVulkanDevice&)
    private:
        CVulkanImage m_image;
        CVulkanImageView m_imageView;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Create(CVulkanDevice& device, u32 width, u32 height, VkFormat format, VkSampleCountFlagBits samples, VkImageUsageFlags imageUsageFlags);

        PD_FORCEINLINE u32 GetWidth() const {return m_image.GetWidth(); }
        PD_FORCEINLINE u32 GetHeight() const {return m_image.GetHeight(); }
        PD_FORCEINLINE VkFormat GetFormat() const {return m_imageView.GetFormat(); }

        PD_FORCEINLINE CVulkanImage& GetImage() {return m_image; }
        PD_FORCEINLINE const CVulkanImage& GetImage() const {return m_image; }

        PD_FORCEINLINE CVulkanImageView& GetImageView() {return m_imageView; }
        PD_FORCEINLINE const CVulkanImageView& GetImageView() const {return m_imageView; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANRENDERTARGETIMAGE_HPP