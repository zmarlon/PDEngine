//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanTexture2D.hpp"
#include "../VulkanDevice.hpp"
#include "../../Renderer.hpp"
#include "../../../../Core/Pak/PakLocation.hpp"
#include "../../../../Core/Logging/Logger.hpp"
#include "../Buffer/VulkanBuffer.hpp"
#include "TextureHelper.hpp"
#include <gli/gli.hpp>

namespace PD
{
    void CVulkanTexture2D::OnDisposing(CVulkanDevice& device)
    {
        m_imageView.Dispose(device);
        m_image.Dispose(device);
    }

    void CVulkanTexture2D::Load(CVulkanDevice& device, const SPakLocation& location, VkImageUsageFlags imageUsageFlags)
    {
        CArray<byte> buffer;
        location.Load(buffer);

        gli::texture2d gliTexture(gli::load(reinterpret_cast<char*>(buffer.GetData()), buffer.GetSize()));
        if(gliTexture.empty())
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Failed to load Texture2D from location: " << location << CLogger::GetInstance()->Flush();
        }

        auto baseMipLevel = TextureHelper::GetStartMipLevel(CRenderer::GetInstance()->GetGraphicsSettings(), static_cast<u32>(gliTexture.levels()));
        u32 numMipLevels = static_cast<u32>(gliTexture.levels() - baseMipLevel);

        m_image.Create(device, VK_IMAGE_TYPE_2D, static_cast<VkFormat>(gliTexture.format()), static_cast<u32>(gliTexture.extent().x), static_cast<u32>(gliTexture.extent().y), 1, numMipLevels, 1,
                VK_SAMPLE_COUNT_1_BIT, imageUsageFlags, VK_IMAGE_LAYOUT_UNDEFINED, VMA_MEMORY_USAGE_GPU_ONLY);
        m_imageView.Create(device, m_image, VK_IMAGE_VIEW_TYPE_2D, m_image.GetFormat(), VK_IMAGE_ASPECT_COLOR_BIT);

        VkDeviceSize size = TextureHelper::GetImageSize(gliTexture, baseMipLevel, numMipLevels);

        CVulkanBuffer stagingBuffer;
        stagingBuffer.Create(device, size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
        void* pMappedBuffer = stagingBuffer.Map<void>();
        MemoryHelper::Copy(pMappedBuffer, gliTexture[baseMipLevel].data(), size);
        stagingBuffer.Unmap();

        auto* pBufferCopyRegions = new VkBufferImageCopy[numMipLevels]; //TODO:

        VkDeviceSize offset = 0;
        for(u32 ix = 0; ix < numMipLevels; ix++)
        {
            const auto& currentMipImage = gliTexture[baseMipLevel + ix];
            auto& bufferImageCopy = pBufferCopyRegions[ix];

            bufferImageCopy.bufferOffset = offset;
            bufferImageCopy.bufferRowLength = 0;
            bufferImageCopy.bufferImageHeight = 0;
            bufferImageCopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            bufferImageCopy.imageSubresource.mipLevel = ix;
            bufferImageCopy.imageSubresource.baseArrayLayer = 0;
            bufferImageCopy.imageSubresource.layerCount = 1;
            bufferImageCopy.imageOffset.x = 0;
            bufferImageCopy.imageOffset.y = 0;
            bufferImageCopy.imageOffset.z = 0;
            bufferImageCopy.imageExtent.width = static_cast<u32>(currentMipImage.extent().x);
            bufferImageCopy.imageExtent.height = static_cast<u32>(currentMipImage.extent().y);
            bufferImageCopy.imageExtent.depth = 1;

            offset += currentMipImage.size();
        }

        auto& threadCollection = device.GetThreadCollection();
        auto& commandBuffer = threadCollection.BeginImmediateTransfer();

        commandBuffer.ChangeImageLayout(m_image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, m_image.GetFormat(), m_imageView.GetSubresourceRange());
        commandBuffer.CopyBufferToImage(stagingBuffer, m_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, numMipLevels, pBufferCopyRegions);
        commandBuffer.ChangeImageLayout(m_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, m_image.GetFormat(), m_imageView.GetSubresourceRange());

        threadCollection.EndImmediateTransfer();

        delete[] pBufferCopyRegions;

        stagingBuffer.Dispose();

        PD_END_INITIALIZE;
    }

    void CVulkanTexture2D::Load(CVulkanDevice& device, u32 width, u32 height, VkFormat format, u32 stride, const void* pData, VkImageUsageFlags imageUsageFlags,
                                VkComponentSwizzle componentSwizzleR, VkComponentSwizzle componentSwizzleG, VkComponentSwizzle componentSwizzleB, VkComponentSwizzle componentSwizzleA)
    {
        m_image.Create(device, VK_IMAGE_TYPE_2D, format, width, height, 1, 1, 1,
                       VK_SAMPLE_COUNT_1_BIT, imageUsageFlags, VK_IMAGE_LAYOUT_UNDEFINED, VMA_MEMORY_USAGE_GPU_ONLY);
        m_imageView.Create(device, m_image, VK_IMAGE_VIEW_TYPE_2D, m_image.GetFormat(), VK_IMAGE_ASPECT_COLOR_BIT,
                0, 1, 0, 1, componentSwizzleR, componentSwizzleG, componentSwizzleB, componentSwizzleA);

        VkDeviceSize size = width * height * stride;

        CVulkanBuffer stagingBuffer;
        stagingBuffer.Create(device, size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
        void* pMappedBuffer = stagingBuffer.Map<void>();
        MemoryHelper::Copy(pMappedBuffer, pData, size);
        stagingBuffer.Unmap();

        VkBufferImageCopy bufferImageCopy;
        bufferImageCopy.bufferOffset = 0;
        bufferImageCopy.bufferRowLength = 0;
        bufferImageCopy.bufferImageHeight = 0;
        bufferImageCopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        bufferImageCopy.imageSubresource.mipLevel = 0;
        bufferImageCopy.imageSubresource.baseArrayLayer = 0;
        bufferImageCopy.imageSubresource.layerCount = 1;
        bufferImageCopy.imageOffset.x = 0;
        bufferImageCopy.imageOffset.y = 0;
        bufferImageCopy.imageOffset.z = 0;
        bufferImageCopy.imageExtent.width = width;
        bufferImageCopy.imageExtent.height = height;
        bufferImageCopy.imageExtent.depth = 1;

        auto& threadCollection = device.GetThreadCollection();
        auto& commandBuffer = threadCollection.BeginImmediateTransfer();

        commandBuffer.ChangeImageLayout(m_image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, m_image.GetFormat(), m_imageView.GetSubresourceRange());
        commandBuffer.CopyBufferToImage(stagingBuffer, m_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &bufferImageCopy);
        commandBuffer.ChangeImageLayout(m_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, m_image.GetFormat(), m_imageView.GetSubresourceRange());

        threadCollection.EndImmediateTransfer();

        stagingBuffer.Dispose();

        PD_END_INITIALIZE;
    }
}