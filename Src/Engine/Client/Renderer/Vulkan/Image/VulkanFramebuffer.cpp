//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanFramebuffer.hpp"
#include "../VulkanDevice.hpp"
#include "VulkanRenderPass.hpp"

namespace PD
{
    void CVulkanFramebuffer::OnDisposing(CVulkanDevice& device)
    {
        vkDestroyFramebuffer(device.GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanFramebuffer::Initialize(CVulkanDevice& device, const CVulkanFramebufferBuilder& builder, CVulkanRenderPass& renderPass, u32 width, u32 height)
    {
        m_pRenderPass = &renderPass;

        VkFramebufferCreateInfo framebufferCreateInfo;
        framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferCreateInfo.pNext = nullptr;
        framebufferCreateInfo.flags = 0;
        framebufferCreateInfo.renderPass = renderPass.GetHandle();
        framebufferCreateInfo.attachmentCount = static_cast<u32>(builder.m_imageViews.size());
        framebufferCreateInfo.pAttachments = builder.m_imageViews.data();
        framebufferCreateInfo.width = width;
        framebufferCreateInfo.height = height;
        framebufferCreateInfo.layers = 1;

        m_width = width;
        m_height = height;

        PD_VK_THROW_IF_FAILED(
                vkCreateFramebuffer(device.GetHandle(), &framebufferCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));

        PD_END_INITIALIZE;
    }
}