//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 11.04.2020.
//

#include "TextureHelper.hpp"
#include "../../../../Core/Types.hpp"
#include "../../Settings/GraphicsSettings.hpp"
#include <gli/gli.hpp>
#include <algorithm>

namespace PD
{
    u32 TextureHelper::GetStartMipLevel(const SGraphicsSettings& graphicsSettings, u32 numMipLevels)
    {
        return std::min(TextureQualityLevel::GetBaseMipLevel(graphicsSettings.TextureQualityLevel), numMipLevels - 1);
    }

    usize TextureHelper::GetImageSize(const gli::texture2d& texture, u32 baseMipLevel, u32 numMipLevels)
    {
        usize numBytes = 0;
        for(usize ix = baseMipLevel; ix < baseMipLevel + numMipLevels; ix++) numBytes += texture[ix].size();
        return numBytes;
    }

    usize TextureHelper::GetImageArraySize(const gli::texture2d_array& textureArray, u32 baseMipLevel, u32 numMipLevels)
    {
        usize numBytes = 0;
        for(usize ix = 0; ix < textureArray.layers(); ix++)
        {
            const auto& currentTexture = textureArray[ix];
            for(usize iy = baseMipLevel; iy < baseMipLevel + numMipLevels; iy++) numBytes += currentTexture[iy].size();
        }
        return numBytes;
    }
}