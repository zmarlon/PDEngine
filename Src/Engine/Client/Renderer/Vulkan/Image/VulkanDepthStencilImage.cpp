//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanDepthStencilImage.hpp"

namespace PD
{
    void CVulkanDepthStencilImage::OnDisposing(CVulkanDevice& device)
    {
        m_imageView.Dispose(device);
        m_image.Dispose(device);
    }

    void CVulkanDepthStencilImage::Create(CVulkanDevice& device, u32 width, u32 height, VkFormat format, VkImageUsageFlags imageUsageFlags)
    {
        m_image.Create(device, VK_IMAGE_TYPE_2D, format, width, height, 1, 1, 1, VK_SAMPLE_COUNT_1_BIT, imageUsageFlags, VK_IMAGE_LAYOUT_UNDEFINED,
                VMA_MEMORY_USAGE_GPU_ONLY);

        VkImageAspectFlags aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
        if(VulkanHelper::IsDepthStencilFormat(format)) aspectMask |= VK_IMAGE_ASPECT_DEPTH_BIT;

        m_imageView.Create(device, m_image, VK_IMAGE_VIEW_TYPE_2D, format, aspectMask);

        PD_END_INITIALIZE;
    }
}