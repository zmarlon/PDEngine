//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanImageView.hpp"
#include "../VulkanDevice.hpp"

namespace PD
{
    void CVulkanImageView::OnDisposing(CVulkanDevice& device)
    {
        vkDestroyImageView(device.GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanImageView::Create(CVulkanDevice& device, VkImage handleImage, VkImageViewType viewType, VkFormat format, VkImageAspectFlags aspectMask,
            u32 baseMipLevel, u32 levelCount, u32 baseArrayLayer, u32 layerCount, VkComponentSwizzle swizzleR, VkComponentSwizzle swizzleG, VkComponentSwizzle swizzleB, VkComponentSwizzle swizzleA)
    {
        VkImageViewCreateInfo imageViewCreateInfo;
        imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCreateInfo.pNext = nullptr;
        imageViewCreateInfo.flags = 0;
        imageViewCreateInfo.image = handleImage;
        imageViewCreateInfo.viewType = viewType;
        imageViewCreateInfo.format = format;
        imageViewCreateInfo.components.r = swizzleR;
        imageViewCreateInfo.components.g = swizzleG;
        imageViewCreateInfo.components.b = swizzleB;
        imageViewCreateInfo.components.a = swizzleA;
        imageViewCreateInfo.subresourceRange.aspectMask = aspectMask;
        imageViewCreateInfo.subresourceRange.baseMipLevel = baseMipLevel;
        imageViewCreateInfo.subresourceRange.levelCount = levelCount;
        imageViewCreateInfo.subresourceRange.baseArrayLayer = baseArrayLayer;
        imageViewCreateInfo.subresourceRange.layerCount = layerCount;

        m_format = format;
        m_subresourceRange = imageViewCreateInfo.subresourceRange;

        PD_VK_THROW_IF_FAILED(vkCreateImageView(device.GetHandle(), &imageViewCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle))

        PD_END_INITIALIZE;
    }
}