//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANFRAMEBUFFER_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANFRAMEBUFFER_HPP

#pragma once

#include "../../../../Core/Types.hpp"
#include "../../../../Core/Disposable.hpp"
#include "../../../../Core/STL/Vector.hpp"
#include "../VulkanHelper.hpp"
#include <type_traits>

namespace PD
{
    class CVulkanDevice;
    class CVulkanRenderPass;

    class CVulkanFramebufferBuilder
    {
        friend class CVulkanFramebuffer;
    private:
        STL::CVector<VkImageView> m_imageViews;
        bool m_hasDepthImage = false;

    public:
        PD_FORCEINLINE void Reserve(usize size)
        {
            m_imageViews.reserve(size);
        }

        PD_FORCEINLINE void AddView(VkImageView handleImageView)
        {
            m_imageViews.push_back(handleImageView);
        }

        template<typename TVulkanRenderTargetImage>
        PD_FORCEINLINE void AddRenderTargetImage(TVulkanRenderTargetImage& image)
        {
            m_imageViews.push_back(image.GetView().GetHandle());
        }

        template<typename TVulkanDepthStencilImage>
        PD_FORCEINLINE void AddDepthStencilImage(TVulkanDepthStencilImage& image)
        {
            PD_ASSERT(!m_hasDepthImage);
            m_imageViews.push_back(image.GetView().GetHandle());
            m_hasDepthImage = true;
        }
    };

    class CVulkanFramebuffer
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanFramebuffer, CVulkanDevice&)
    private:
        VkFramebuffer m_handle;
        CVulkanRenderPass* m_pRenderPass;

        u32 m_width;
        u32 m_height;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Initialize(CVulkanDevice& device, const CVulkanFramebufferBuilder& builder, CVulkanRenderPass& renderPass, u32 width, u32 height);

        PD_FORCEINLINE u32 GetWidth() const { return m_width; }
        PD_FORCEINLINE u32 GetHeight() const { return m_height; }

        PD_FORCEINLINE CVulkanRenderPass& GetRenderPass() {return *m_pRenderPass; }

        PD_FORCEINLINE VkFramebuffer& GetHandle() { return m_handle; }
        PD_FORCEINLINE const VkFramebuffer& GetHandle() const { return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANFRAMEBUFFER_HPP