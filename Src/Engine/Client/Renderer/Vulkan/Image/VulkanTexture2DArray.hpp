//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 11.04.2020.
//
#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_VULKANTEXTURE2DARRAY_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_VULKANTEXTURE2DARRAY_HPP

#include "VulkanImage.hpp"
#include "VulkanImageView.hpp"

namespace PD
{
    struct SGraphicsSettings;
    struct SPakLocation;
    class CVulkanTexture2DArray final
    {
    PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanTexture2DArray, CVulkanDevice&)
    private:
        CVulkanImage m_image;
        CVulkanImageView m_imageView;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Load(CVulkanDevice& device, const SPakLocation& location, VkImageUsageFlags imageUsageFlags = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);

        PD_FORCEINLINE u32 GetWidth() const {return m_image.GetWidth(); }
        PD_FORCEINLINE u32 GetHeight() const {return m_image.GetHeight(); }
        PD_FORCEINLINE u32 GetMipLevels() const {return m_image.GetMipLevels(); }
        PD_FORCEINLINE u32 GetArrayLayers() const {return m_image.GetArrayLayers(); }
        PD_FORCEINLINE VkFormat GetFormat() const {return m_imageView.GetFormat(); }

        PD_FORCEINLINE CVulkanImage& GetImage() {return m_image; }
        PD_FORCEINLINE const CVulkanImage& GetImage() const {return m_image; }

        PD_FORCEINLINE CVulkanImageView& GetImageView() {return m_imageView; }
        PD_FORCEINLINE const CVulkanImageView& GetImageView() const {return m_imageView; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_VULKANTEXTURE2DARRAY_HPP
