//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_VULKANRENDERPASS_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_VULKANRENDERPASS_HPP

#pragma once

#include "../../../../Core/Types.hpp"
#include "../../../../Core/Disposable.hpp"
#include "../../../../Core/STL/Vector.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanSubpassBuilder
    {
        friend class CVulkanRenderPassBuilder;
    private:
        STL::CVector<VkAttachmentReference> m_inputAttachments;
        STL::CVector<VkAttachmentReference> m_colorAttachments;
        STL::CVector<VkAttachmentReference> m_resolveAttachments;
        STL::CVector<u32> m_preserveAttachments;

        bool m_depthAttachmentUsed = false;
        VkAttachmentReference m_depthAttachment;

        VkAttachmentDescription m_subpassDescription;
        VkPipelineBindPoint m_bindPoint;
    public:
        PD_FORCEINLINE void ReserveInputAttachments(usize size)
        {
            m_inputAttachments.reserve(size);
        }

        PD_FORCEINLINE void AddInputAttachment(u32 attachment, VkImageLayout imageLayout)
        {
            VkAttachmentReference attachmentReference;
            attachmentReference.attachment = attachment;
            attachmentReference.layout = imageLayout;

            m_inputAttachments.push_back(attachmentReference);
        }

        PD_FORCEINLINE void ReserveColorAttachments(usize size)
        {
            m_colorAttachments.reserve(size);
        }

        PD_FORCEINLINE void AddColorAttachment(u32 attachment, VkImageLayout imageLayout)
        {
            VkAttachmentReference attachmentReference;
            attachmentReference.attachment = attachment;
            attachmentReference.layout = imageLayout;

            m_colorAttachments.push_back(attachmentReference);
        }

        PD_FORCEINLINE void ReserveResolveAttachments(usize size)
        {
            m_resolveAttachments.reserve(size);
        }

        PD_FORCEINLINE void AddResolveAttachment(u32 attachment, VkImageLayout imageLayout)
        {
            VkAttachmentReference attachmentReference;
            attachmentReference.attachment = attachment;
            attachmentReference.layout = imageLayout;

            m_resolveAttachments.push_back(attachmentReference);
        }

        PD_FORCEINLINE void ReservePreserveAttachments(usize size)
        {
            m_preserveAttachments.reserve(size);
        }

        PD_FORCEINLINE void AddPreserveAttachment(u32 attachment)
        {
            m_preserveAttachments.push_back(attachment);
        }

        PD_FORCEINLINE void SetDepthAttachment(u32 attachment, VkImageLayout layout)
        {
            m_depthAttachment.attachment = attachment;
            m_depthAttachment.layout = layout;
            m_depthAttachmentUsed = true;
        }

        PD_FORCEINLINE void SetBindPoint(VkPipelineBindPoint bindPoint)
        {
            m_bindPoint = bindPoint;
        }
    };

    class CVulkanDevice;
    class CVulkanRenderPassBuilder
    {
        friend class CVulkanRenderPass;
    private:
        STL::CVector<VkAttachmentDescription> m_attachmentDescriptions;
        STL::CVector<VkSubpassDescription> m_subpassDescriptions;
        STL::CVector<VkSubpassDependency> m_subpassDependencies;

    public:
        PD_FORCEINLINE void ReserveAttachments(usize size)
        {
            m_attachmentDescriptions.reserve(size);
        }

        void AddAttachment(VkFormat format, u32 samples, VkAttachmentLoadOp loadOp, VkAttachmentStoreOp storeOp,
                           VkImageLayout initialLayout, VkImageLayout finalLayout,
                           VkAttachmentLoadOp stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                           VkAttachmentStoreOp stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE);

        PD_FORCEINLINE void ReserveDependencies(usize size)
        {
            m_subpassDependencies.reserve(size);
        }

        void AddSubpassDependency(u32 srcSubpass, u32 dstSubpass, VkPipelineStageFlags srcStageMask,
                                  VkPipelineStageFlags dstStageMask, VkAccessFlags srcAccessMask,
                                  VkAccessFlags dstAccessMask, VkDependencyFlags dependencyFlags);

        PD_FORCEINLINE void AddSubpassDependency(const VkSubpassDependency& subpassDependency) {m_subpassDependencies.push_back(subpassDependency); }

        PD_FORCEINLINE void ReserveSubpasses(usize size)
        {
            m_subpassDependencies.reserve(size);
        }

        void AddSubpass(CVulkanSubpassBuilder& builder);
    };

    class CVulkanRenderPass
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanRenderPass, CVulkanDevice&)
    private:
        VkRenderPass m_handle;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Create(CVulkanDevice& device, const CVulkanRenderPassBuilder& builder);

        PD_FORCEINLINE VkRenderPass& GetHandle() { return m_handle; }
        PD_FORCEINLINE const VkRenderPass& GetHandle() const { return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_VULKANRENDERPASS_HPP