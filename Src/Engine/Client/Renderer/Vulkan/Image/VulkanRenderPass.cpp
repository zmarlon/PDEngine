//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanRenderPass.hpp"
#include "../VulkanDevice.hpp"

namespace PD
{
    void CVulkanRenderPassBuilder::AddAttachment(VkFormat format, u32 samples, VkAttachmentLoadOp loadOp, VkAttachmentStoreOp storeOp, VkImageLayout initialLayout, VkImageLayout finalLayout, VkAttachmentLoadOp stencilLoadOp, VkAttachmentStoreOp stencilStoreOp)
    {
        VkAttachmentDescription attachmentDescription;
        attachmentDescription.flags = 0;
        attachmentDescription.format = format;
        attachmentDescription.samples = VulkanHelper::GetSampleCount(samples);
        attachmentDescription.loadOp = loadOp;
        attachmentDescription.storeOp = storeOp;
        attachmentDescription.stencilLoadOp = stencilLoadOp;
        attachmentDescription.stencilStoreOp = stencilStoreOp;
        attachmentDescription.initialLayout = initialLayout;
        attachmentDescription.finalLayout = finalLayout;

        m_attachmentDescriptions.push_back(attachmentDescription);
    }

    void CVulkanRenderPassBuilder::AddSubpassDependency(u32 srcSubpass, u32 dstSubpass, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, VkAccessFlags srcAccessMask, VkAccessFlags dstAccessMask, VkDependencyFlags dependencyFlags)
    {
        VkSubpassDependency subpassDependency;
        subpassDependency.srcSubpass = srcSubpass;
        subpassDependency.dstSubpass = dstSubpass;
        subpassDependency.srcStageMask = srcStageMask;
        subpassDependency.dstStageMask = dstStageMask;
        subpassDependency.srcAccessMask = srcAccessMask;
        subpassDependency.dstAccessMask = dstAccessMask;
        subpassDependency.dependencyFlags = dependencyFlags;

        m_subpassDependencies.push_back(subpassDependency);
    }

    void CVulkanRenderPassBuilder::AddSubpass(CVulkanSubpassBuilder& builder)
    {
        VkSubpassDescription subpassDescription;
        subpassDescription.flags = 0;
        subpassDescription.pipelineBindPoint = builder.m_bindPoint;
        subpassDescription.inputAttachmentCount = static_cast<u32>(builder.m_inputAttachments.size());
        subpassDescription.pInputAttachments = builder.m_inputAttachments.data();
        subpassDescription.colorAttachmentCount = static_cast<u32>(builder.m_colorAttachments.size());
        subpassDescription.pColorAttachments = builder.m_colorAttachments.data();
        subpassDescription.pResolveAttachments = builder.m_resolveAttachments.data();
        subpassDescription.pDepthStencilAttachment = builder.m_depthAttachmentUsed ? &builder.m_depthAttachment : nullptr;
        subpassDescription.preserveAttachmentCount = static_cast<u32>(builder.m_preserveAttachments.size());
        subpassDescription.pPreserveAttachments = builder.m_preserveAttachments.data();

        m_subpassDescriptions.push_back(subpassDescription);
    }

    void CVulkanRenderPass::OnDisposing(CVulkanDevice& device)
    {
        vkDestroyRenderPass(device.GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanRenderPass::Create(CVulkanDevice& device, const CVulkanRenderPassBuilder& builder)
    {
        VkRenderPassCreateInfo renderPassCreateInfo;
        renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderPassCreateInfo.pNext = nullptr;
        renderPassCreateInfo.flags = 0;
        renderPassCreateInfo.attachmentCount = static_cast<u32>(builder.m_attachmentDescriptions.size());
        renderPassCreateInfo.pAttachments = builder.m_attachmentDescriptions.data();
        renderPassCreateInfo.subpassCount = static_cast<u32>(builder.m_subpassDescriptions.size());
        renderPassCreateInfo.pSubpasses = builder.m_subpassDescriptions.data();
        renderPassCreateInfo.dependencyCount = static_cast<u32>(builder.m_subpassDependencies.size());
        renderPassCreateInfo.pDependencies = builder.m_subpassDependencies.data();

        PD_VK_THROW_IF_FAILED(vkCreateRenderPass(device.GetHandle(), &renderPassCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle))
        PD_END_INITIALIZE;
    }
}