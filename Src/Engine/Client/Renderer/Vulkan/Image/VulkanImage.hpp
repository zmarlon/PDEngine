//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANIMAGE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANIMAGE_HPP

#pragma once

#include "../VulkanHelper.hpp"
#include "../../../../Core/Types.hpp"
#include "../../../../Core/Disposable.hpp"

namespace PD
{
    class CVulkanDevice;
    class CVulkanImage
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanImage, CVulkanDevice&)
    private:
        VkImage m_handle;
        VmaAllocation m_handleAllocation;
        VmaAllocationInfo m_allocationInfo;

        VkFormat m_format;
        u32 m_width;
        u32 m_height;
        u32 m_depth;
        u32 m_mipLevels;
        u32 m_arrayLayers;
        VkSampleCountFlagBits m_samples;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Create(CVulkanDevice& device, VkImageType type, VkFormat format, u32 width, u32 height, u32 depth, u32 mipLevels, u32 arrayLayers, VkSampleCountFlagBits samples, VkImageUsageFlags usage,
                VkImageLayout initialLayout, VmaMemoryUsage memoryUsage);

        PD_FORCEINLINE VmaAllocation& GetAllocationHandle() {return m_handleAllocation; }
        PD_FORCEINLINE const VmaAllocation& GetAllocationHandle() const {return m_handleAllocation; }
        PD_FORCEINLINE const VmaAllocationInfo& GetAllocationInfo() const {return m_allocationInfo; }

        PD_FORCEINLINE VkFormat GetFormat() const {return m_format; }
        PD_FORCEINLINE u32 GetWidth() const {return m_width; }
        PD_FORCEINLINE u32 GetHeight() const {return m_height; }
        PD_FORCEINLINE u32 GetDepth() const {return m_depth; }
        PD_FORCEINLINE u32 GetMipLevels() const {return m_mipLevels; }
        PD_FORCEINLINE u32 GetArrayLayers() const {return m_arrayLayers; }
        PD_FORCEINLINE VkSampleCountFlagBits GetSamples() const {return m_samples; }

        PD_FORCEINLINE VkImage& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkImage& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANIMAGE_HPP