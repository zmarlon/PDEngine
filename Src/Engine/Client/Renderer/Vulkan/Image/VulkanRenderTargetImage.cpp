//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanRenderTargetImage.hpp"

namespace PD
{
    void CVulkanRenderTargetImage::OnDisposing(CVulkanDevice& device)
    {
        m_imageView.Dispose(device);
        m_image.Dispose(device);
    }

    void CVulkanRenderTargetImage::Create(CVulkanDevice& device, u32 width, u32 height, VkFormat format, VkSampleCountFlagBits samples, VkImageUsageFlags imageUsageFlags)
    {
        m_image.Create(device, VK_IMAGE_TYPE_2D, format, width, height, 1, 1, 1, samples, imageUsageFlags, VK_IMAGE_LAYOUT_UNDEFINED, VMA_MEMORY_USAGE_GPU_ONLY);
        m_imageView.Create(device, m_image, VK_IMAGE_VIEW_TYPE_2D, format, VK_IMAGE_ASPECT_COLOR_BIT);

        PD_END_INITIALIZE;
    }
}