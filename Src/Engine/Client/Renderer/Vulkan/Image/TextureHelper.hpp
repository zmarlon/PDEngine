//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 11.04.2020.
//
#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_TEXTUREHELPER_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_TEXTUREHELPER_HPP

#include "../../../../Core/Types.hpp"
#include "../../Settings/GraphicsSettings.hpp"

namespace gli
{
    class texture2d;
    class texture2d_array;
}

namespace PD::TextureHelper
{
    u32 GetStartMipLevel(const SGraphicsSettings& graphicsSettings, u32 numMipLevels);
    usize GetImageSize(const gli::texture2d& texture, u32 baseMipLevel, u32 numMipLevels);
    usize GetImageArraySize(const gli::texture2d_array& textureArray, u32 baseMipLevel, u32 numMipLevels);
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_IMAGE_TEXTUREHELPER_HPP
