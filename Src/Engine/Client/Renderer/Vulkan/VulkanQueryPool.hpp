//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANQUERYPOOL_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANQUERYPOOL_HPP

#pragma once

#include "VulkanHelper.hpp"
#include "../../../Core/Types.hpp"
#include "../../../Core/Disposable.hpp"

namespace PD
{
	class CVulkanDevice;

	class CVulkanQueryPool
	{
		PD_MAKE_CLASS_DISPOSABLE(CVulkanQueryPool)
	private:
		VkQueryPool m_handle;
		CVulkanDevice* m_pDevice;

		void OnDisposing();
	public:
		void Initialize(CVulkanDevice& device, VkQueryPoolCreateFlags createFlags, VkQueryType queryType, u32 queryCount,
						VkQueryPipelineStatisticFlags pipelineStatisticFlags);

		void GetResults(u32 firstQuery, u32 queryCount, usize dataSize, void* pData, VkDeviceSize stride, VkQueryResultFlags flags);
		void Reset(u32 firstQuery, u32 queryCount);

		PD_FORCEINLINE const VkQueryPool GetHandle() const { return m_handle; }
	};
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANQUERYPOOL_HPP