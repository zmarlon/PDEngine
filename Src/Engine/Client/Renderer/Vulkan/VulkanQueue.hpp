//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANQUEUE_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANQUEUE_HPP

#pragma once

#include "../../../Core/Types.hpp"
#include "../../../Core/Disposable.hpp"
#include "VulkanHelper.hpp"

namespace PD
{
    class CVulkanFence;
    class CVulkanPrimaryCommandBuffer;
    class CVulkanDevice;
    class CVulkanQueue
    {
        PD_MAKE_CLASS_DISPOSABLE(CVulkanQueue)
    private:
        VkQueue m_handle;
        u32 m_familyIndex;
        u32 m_index;
        VkQueueFamilyProperties2 m_familyProperties;

        void OnDisposing();
    public:
        void Initialize(CVulkanDevice& device, u32 familyIndex, u32 index, VkQueueFamilyProperties2& familyProperties);

        void Submit(CVulkanPrimaryCommandBuffer& commandBuffer);
        void Submit(CVulkanPrimaryCommandBuffer& commandBuffer, CVulkanFence& fence);

        template<typename TVulkanSubmitInfo>
        PD_FORCEINLINE void Submit(TVulkanSubmitInfo& submitInfo)
        {
            PD_VK_THROW_IF_FAILED(vkQueueSubmit(m_handle, 1, &submitInfo.GetSubmitInfo(), VK_NULL_HANDLE));
        }

        template<typename TVulkanSubmitInfo, typename TVulkanFence>
        PD_FORCEINLINE void Submit(TVulkanSubmitInfo& submitInfo, TVulkanFence& fence)
        {
            PD_VK_THROW_IF_FAILED(vkQueueSubmit(m_handle, 1, &submitInfo.GetSubmitInfo(), fence.GetHandle()));
        }

        PD_FORCEINLINE void Submit(const VkSubmitInfo& submitInfo)
        {
            PD_VK_THROW_IF_FAILED(vkQueueSubmit(m_handle, 1, &submitInfo, VK_NULL_HANDLE));
        }

        template<typename TVulkanFence>
        PD_FORCEINLINE void Submit(const VkSubmitInfo& submitInfo, TVulkanFence& fence)
        {
            PD_VK_THROW_IF_FAILED(
                    vkQueueSubmit(m_handle, 1, &submitInfo, fence.GetHandle()));
        }

        PD_FORCEINLINE void WaitIdle()
        {
            PD_VK_THROW_IF_FAILED(vkQueueWaitIdle(m_handle));
        }

        PD_FORCEINLINE u32 GetFamilyIndex() const { return m_familyIndex; }
        PD_FORCEINLINE u32 GetIndex() const { return m_index; }
        PD_FORCEINLINE VkQueue& GetHandle() { return m_handle; }
        PD_FORCEINLINE const VkQueue& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANQUEUE_HPP