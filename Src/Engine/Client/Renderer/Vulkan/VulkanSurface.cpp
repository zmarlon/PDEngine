//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanSurface.hpp"
#include "VulkanInstance.hpp"
#include "../../../Core/Logging/Logger.hpp"
#include "../../../Core/Window/Window.hpp"
#include <SDL2/SDL_vulkan.h>

namespace PD
{
	void CVulkanSurface::OnDisposing()
	{
        vkDestroySurfaceKHR(m_pInstance->GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
	}

    void CVulkanSurface::PrintTransform(VkSurfaceTransformFlagsKHR transformFlags, std::wostream& stream)
    {
        //@formatter:off
        stream << L"\nVK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR                       "
               << ((transformFlags & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) != 0)
               << L"\nVK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR                      "
               << ((transformFlags & VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR) != 0)
               << L"\nVK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR                     "
               << ((transformFlags & VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR) != 0)
               << L"\nVK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR                     "
               << ((transformFlags & VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR) != 0)
               << L"\nVK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR              "
               << ((transformFlags & VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR) != 0)
               << L"\nVK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR    "
               << ((transformFlags & VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR) != 0)
               << L"\nVK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR   "
               << ((transformFlags & VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR) != 0)
               << L"\nVK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR   "
               << ((transformFlags & VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR) != 0)
               << L"\nVK_SURFACE_TRANSFORM_INHERIT_BIT_KHR                        "
               << ((transformFlags & VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR) != 0);
        //@formatter:on
    }

	void CVulkanSurface::Initialize(CVulkanInstance& instance, VkPhysicalDevice handlePhysicalDevice, CWindow& window)
	{
        m_pInstance = &instance;
        m_handlePhysicalDevice = handlePhysicalDevice;

        if(!SDL_Vulkan_CreateSurface(window.GetHandle(), instance.GetHandle(), &m_handle))
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Failed to create SDL2 surface: " << SDL_GetError() << CLogger::GetInstance()->Flush();
        }

        CLogger::GetInstance()->Log(ELogLevel::Success, PD_VK_RENDERER_PREFIX) << L"Surface created" << CLogger::GetInstance()->Flush();

        RefreshCapabilities();

        u32 numSupportedSurfaceFormats;
        PD_VK_THROW_IF_FAILED(vkGetPhysicalDeviceSurfaceFormatsKHR(m_handlePhysicalDevice, m_handle, &numSupportedSurfaceFormats, nullptr));
        m_supportedSurfaceFormats.Allocate(numSupportedSurfaceFormats,false);
        PD_VK_THROW_IF_FAILED(vkGetPhysicalDeviceSurfaceFormatsKHR(m_handlePhysicalDevice, m_handle, &numSupportedSurfaceFormats, m_supportedSurfaceFormats.GetData()));

        u32 numSupportedPresentModes;
        PD_VK_THROW_IF_FAILED(vkGetPhysicalDeviceSurfacePresentModesKHR(m_handlePhysicalDevice, m_handle, &numSupportedPresentModes, nullptr));
        m_supportedPresentModes.Allocate(numSupportedPresentModes, false);
        PD_VK_THROW_IF_FAILED(vkGetPhysicalDeviceSurfacePresentModesKHR(m_handlePhysicalDevice, m_handle, &numSupportedPresentModes, m_supportedPresentModes.GetData()));

        PD_END_INITIALIZE;
	}

	void CVulkanSurface::RefreshCapabilities()
	{
        PD_VK_THROW_IF_FAILED(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_handlePhysicalDevice, m_handle, &m_surfaceCapabilities));
	}

	void CVulkanSurface::PrintProperties()
	{
//@formatter:off
        auto* pLogger = CLogger::GetInstance();
        pLogger->Log(ELogLevel::Information, PD_VK_RENDERER_PREFIX) << L"Surface properties:\nSurfaceCapabilities" <<
                                                                    L"\nMin Image Count: " << m_surfaceCapabilities.minImageCount <<
                                                                    L"\nMax Image Count: " << m_surfaceCapabilities.maxImageCount <<
                                                                    L"\nCurrent Image Extent [Width, Height]: " << m_surfaceCapabilities.currentExtent.width << L':' << m_surfaceCapabilities.currentExtent.height <<
                                                                    L"\nMin Image Extent [Width, Height]: " << m_surfaceCapabilities.minImageExtent.width << L':' << m_surfaceCapabilities.minImageExtent.height <<
                                                                    L"\nMax Image Extent [Width, Height]: " << m_surfaceCapabilities.maxImageExtent.width << L':' << m_surfaceCapabilities.maxImageExtent.height <<
                                                                    L"\nMax Image Array Layers: " << m_surfaceCapabilities.maxImageArrayLayers <<
                                                                    L"\nCurrent Transform:";
        auto& stream = pLogger->Out();
        PrintTransform(m_surfaceCapabilities.supportedTransforms, stream);
        stream << L"\nCurrent Transform:";
        PrintTransform(m_surfaceCapabilities.currentTransform, stream);

        stream << L"\nSupported Composite Alpha:"
               << L"\nVK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR                           "
               << ((m_surfaceCapabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR) != 0)
               << L"\nVK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR                   "
               << ((m_surfaceCapabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR) != 0)
               << L"\nVK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR                  "
               << ((m_surfaceCapabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR) != 0)
               << L"\nVK_COMPOSITE_ALPHA_INHERIT_BIT_KHR                          "
               << ((m_surfaceCapabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR) != 0)
               << L"\nSupported Usage Flags:"
               << L"\nVK_IMAGE_USAGE_TRANSFER_SRC_BIT                             "
               << ((m_surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_SRC_BIT) != 0)
               << L"\nVK_IMAGE_USAGE_TRANSFER_DST_BIT                             "
               << ((m_surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_DST_BIT) != 0)
               << L"\nVK_IMAGE_USAGE_SAMPLED_BIT                                  "
               << ((m_surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_SAMPLED_BIT) != 0)
               << L"\nVK_IMAGE_USAGE_STORAGE_BIT                                  "
               << ((m_surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_STORAGE_BIT) != 0)
               << L"\nVK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT                         "
               << ((m_surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT) != 0)
               << L"\nVK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT                 "
               << ((m_surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT) != 0)
               << L"\nVK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT                     "
               << ((m_surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT) != 0)
               << L"\nVK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT                         "
               << ((m_surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT) != 0);

        stream << L"\n\n" << L"Amount of Physical Device Surface Formats: [" << m_supportedSurfaceFormats.GetSize() << L"]\n";
        for(usize ix = 0; ix < m_supportedSurfaceFormats.GetSize(); ix++)
        {
            if(ix != 0) stream << L'\n';
            const auto& currentFormat = m_supportedSurfaceFormats[ix];

            stream << L"Physical Device Surface Format [" << ix
                   << L"]:\nFormat:      " << VulkanHelper::FormatToString(currentFormat.format)
                   << L"\nColor Space: " << VulkanHelper::ColorSpaceToString(currentFormat.colorSpace);
        }
        stream << L'\n' << L"\nAmount of Physical Device Surface Present Modes: [" << m_supportedPresentModes.GetSize() << L"]\n";
        for(u32 ix = 0; ix < m_supportedPresentModes.GetSize(); ix++)
        {
            if(ix != 0) stream << L'\n';
            stream << L"Physical Device Surface Present Mode [" << ix
                   << L"]:\nPresent Mode: " << VulkanHelper::PresentModeToString(m_supportedPresentModes[ix]);
        }
        stream << L'\n';

        pLogger->Flush();
        //@formatter:on
	}

	bool CVulkanSurface::IsFormatSupported(VkFormat format, VkColorSpaceKHR& colorSpace) const
	{
		for(const auto& currentFormat : m_supportedSurfaceFormats)
        {
		    if(currentFormat.format == format)
            {
		        colorSpace = currentFormat.colorSpace;
		        return true;
            }
        }
		return false;
	}

	bool CVulkanSurface::IsFormatSupported(const VkSurfaceFormatKHR& surfaceFormat) const
	{
		for(const auto& currentFormat: m_supportedSurfaceFormats)
        {
		    if(currentFormat.format == surfaceFormat.format && currentFormat.colorSpace == surfaceFormat.colorSpace) return true;
        }
		return false;
	}

	bool CVulkanSurface::IsPresentModeSupported(VkPresentModeKHR presentMode) const
	{
		for(const auto currentMode : m_supportedPresentModes)
        {
		    if(currentMode == presentMode) return true;
        }
		return false;
	}
}