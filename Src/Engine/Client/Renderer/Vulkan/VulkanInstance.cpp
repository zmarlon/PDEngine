//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanInstance.hpp"
#include "../../../GameInfo.hpp"
#include "../../../Core/Logging/LogLevel.hpp"
#include "../../../Core/Logging/Logger.hpp"
#include "../../../Core/Window/Window.hpp"
#include "../../../Core/String/StringHelper.hpp"
#include <SDL2/SDL_vulkan.h>

namespace PD
{
    void CVulkanInstance::OnDisposing()
    {
        vkDestroyInstance(m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

	void CVulkanInstance::Initialize(const SGameInfo& gameInfo, CWindow& window)
    {
        PD_VK_THROW_IF_FAILED(volkInitialize())

        u32 supportedVersion;
        PD_VK_THROW_IF_FAILED(vkEnumerateInstanceVersion(&supportedVersion))

        u32 supportedMajor = VK_VERSION_MAJOR(supportedVersion), supportedMinor = VK_VERSION_MAJOR(supportedVersion);
        if(supportedMajor < 1 || supportedMinor < 1) //TODO: current version is 1.1 because drivers are still in beta
        {
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_ALLOCATOR_FUTURE) << L"Vulkan 1.2 is not supported" << CLogger::GetInstance()->Flush();
        }

        u32 numLayers;
        PD_VK_THROW_IF_FAILED(vkEnumerateInstanceLayerProperties(&numLayers, nullptr))
        m_supportedLayers.Allocate(numLayers, false);
        PD_VK_THROW_IF_FAILED(vkEnumerateInstanceLayerProperties(&numLayers, m_supportedLayers.GetData()))

        u32 numExtensions;
        PD_VK_THROW_IF_FAILED(vkEnumerateInstanceExtensionProperties(nullptr, &numExtensions, nullptr))
        m_supportedExtensions.Allocate(numExtensions, false);
        PD_VK_THROW_IF_FAILED(vkEnumerateInstanceExtensionProperties(nullptr, &numExtensions, m_supportedExtensions.GetData()))

        m_applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        m_applicationInfo.pNext = nullptr;
        m_applicationInfo.pApplicationName = gameInfo.pName;
        m_applicationInfo.applicationVersion = VK_MAKE_VERSION(gameInfo.Major, gameInfo.Minor, gameInfo.Major);
        m_applicationInfo.pEngineName = PD_ENGINE_NAME;
        m_applicationInfo.engineVersion = VK_MAKE_VERSION(PD_ENGINE_VERSION_MAJOR, PD_ENGINE_VERSION_MINOR, PD_ENGINE_VERSION_PATCH);
        m_applicationInfo.apiVersion = VK_API_VERSION_1_2;

        u32 numSDLExtensions;
        if(!SDL_Vulkan_GetInstanceExtensions(window.GetHandle(), &numSDLExtensions, nullptr))
		{
        	CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"SDL_Vulkan_GetInstanceExtensions [0] failed: " << SDL_GetError() << CLogger::GetInstance()->Flush();
		}

		m_sdl2Extensions.Allocate(numSDLExtensions, false);

		if(!SDL_Vulkan_GetInstanceExtensions(window.GetHandle(), &numSDLExtensions, m_sdl2Extensions.GetData()))
		{
            CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"SDL_Vulkan_GetInstanceExtensions [1] failed: " << SDL_GetError() << CLogger::GetInstance()->Flush();
		}

		for(const auto* pExtension : m_sdl2Extensions) 
		{
			if(!IsExtensionSupported(pExtension))
            {
			    CLogger::GetInstance()->Log(ELogLevel::Fatal, PD_VK_RENDERER_PREFIX) << L"Required extension is not supported: " << pExtension << CLogger::GetInstance()->Flush();
            }
            m_enabledExtensions.push_back(pExtension);
		}

        m_pWindow = &window;
    }

    void CVulkanInstance::Create()
    {
        m_instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        m_instanceCreateInfo.pNext = nullptr;
        m_instanceCreateInfo.flags = 0;
        m_instanceCreateInfo.pApplicationInfo = &m_applicationInfo;
        m_instanceCreateInfo.enabledLayerCount = static_cast<u32>(m_enabledLayers.size());
        m_instanceCreateInfo.ppEnabledLayerNames = m_enabledLayers.data();
        m_instanceCreateInfo.enabledExtensionCount = static_cast<u32>(m_enabledExtensions.size());
        m_instanceCreateInfo.ppEnabledExtensionNames = m_enabledExtensions.data();

        PD_VK_THROW_IF_FAILED(vkCreateInstance(&m_instanceCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle))
        volkLoadInstance(m_handle);

        u32 numPhysicalDevices;
        PD_VK_THROW_IF_FAILED(vkEnumeratePhysicalDevices(m_handle, &numPhysicalDevices, nullptr))
        m_physicalDevices.Allocate(numPhysicalDevices);
        PD_VK_THROW_IF_FAILED(vkEnumeratePhysicalDevices(m_handle, &numPhysicalDevices, m_physicalDevices.GetData()))

        PD_END_INITIALIZE;
    }

    VkPhysicalDevice CVulkanInstance::FindOptimalPhysicalDevice() const
    {
        VkDeviceSize highestHeapSize = 0;
        VkPhysicalDevice handlePhysicalDevice = VK_NULL_HANDLE;

        for(usize ix = 0; ix < m_physicalDevices.GetSize(); ix++)
        {
            auto currentPhysicalDevice = m_physicalDevices[ix];

            VkPhysicalDeviceProperties physicalDeviceProperties;
            vkGetPhysicalDeviceProperties(currentPhysicalDevice, &physicalDeviceProperties);

            if(physicalDeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
            {
                VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties;
                vkGetPhysicalDeviceMemoryProperties(currentPhysicalDevice, &physicalDeviceMemoryProperties);

                VkDeviceSize heapSize = 0;
                for(usize iy = 0; iy < physicalDeviceMemoryProperties.memoryHeapCount; iy++)
                {
                    const auto& currentHeap = physicalDeviceMemoryProperties.memoryHeaps[iy];
                    if((currentHeap.flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) == VK_MEMORY_HEAP_DEVICE_LOCAL_BIT)
                    {
                        heapSize += physicalDeviceMemoryProperties.memoryHeaps[iy].size;
                    }
                }

                if(heapSize > highestHeapSize)
                {
                    highestHeapSize = heapSize;
                    handlePhysicalDevice = currentPhysicalDevice;
                }
            }
        }

        if(handlePhysicalDevice == VK_NULL_HANDLE)
        {
            CLogger::GetInstance()->Log(ELogLevel::Warning, PD_VK_RENDERER_PREFIX) << L"No dedicated gpu found, trying to run on integrated one!" << CLogger::GetInstance()->Flush();
            handlePhysicalDevice = m_physicalDevices[0];
        }

        return handlePhysicalDevice;
    }

    bool CVulkanInstance::IsLayerSupported(const char* pLayerName) const
    {
    	for(const auto& currentLayer : m_supportedLayers)
		{
			if(StringHelper::AreEqual(pLayerName, currentLayer.layerName)) return true;
		}
        return false;
    }

    bool CVulkanInstance::IsExtensionSupported(const char* pExtensionName) const
    {
    	for(const auto& currentExtension : m_supportedExtensions)
		{
    		if(StringHelper::AreEqual(pExtensionName, currentExtension.extensionName)) return true;
		}
        return false;
    }

    bool CVulkanInstance::AddLayer(const char* pLayerName)
    {
        if(IsLayerSupported(pLayerName))
        {
            m_enabledLayers.push_back(pLayerName);
            return true;
        }
        return false;
    }

    bool CVulkanInstance::AddExtension(const char* pExtensionName)
    {
        if(IsExtensionSupported(pExtensionName))
        {
            m_enabledExtensions.push_back(pExtensionName);
            return true;
        }
        return false;
    }

    void CVulkanInstance::PrintProperties()
    {
        auto* pLogger = CLogger::GetInstance();
        pLogger->Log(ELogLevel::Information, PD_VK_RENDERER_PREFIX) << L"Available instance layers ["
                                                                    << m_supportedLayers.GetSize() << L"]\n";
        auto& stream = pLogger->Out();

        for(usize ix = 0; ix < m_supportedLayers.GetSize(); ix++)
        {
            const auto& layer = m_supportedLayers[ix];
            stream << L"Layer [" << ix << L"]:\nLayer Name: " << layer.layerName << L"\nLayer Implementation Version: ";
            VulkanHelper::AppendVersion(layer.implementationVersion, stream);
            stream << L"\nLayer Spec Version: ";
            VulkanHelper::AppendVersion(layer.specVersion, stream);
            stream << L"\nLayer Description: " << layer.description << L'\n';
        }
        pLogger->Flush();

        pLogger->Log(ELogLevel::Information, PD_VK_RENDERER_PREFIX) << L"Available instance extensions ["
                                                                    << m_supportedExtensions.GetSize() << L"]\n";
        for(usize ix = 0; ix < m_supportedExtensions.GetSize(); ix++)
        {
            const auto& extension = m_supportedExtensions[ix];
            stream << L"Extension [" << ix << L"]:\nExtension Name: " << extension.extensionName << L"\nExtension Spec Version: ";
            VulkanHelper::AppendVersion(extension.specVersion, stream);
            stream << L'\n';
        }
        pLogger->Flush();
    }
}