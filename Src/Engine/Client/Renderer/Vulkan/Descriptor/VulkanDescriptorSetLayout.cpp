//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanDescriptorSetLayout.hpp"
#include "../VulkanDevice.hpp"

namespace PD
{
    void CVulkanDescriptorSetLayout::OnDisposing(CVulkanDevice& device)
    {
        vkDestroyDescriptorSetLayout(device.GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanDescriptorSetLayoutBuilder::AddBinding(u32 binding, VkDescriptorType descriptorType, VkShaderStageFlags shaderStageFlags, u32 descriptorCount, const VkSampler* pImmutableSamplers)
    {
        VkDescriptorSetLayoutBinding descriptorSetLayoutBinding;
        descriptorSetLayoutBinding.binding = binding;
        descriptorSetLayoutBinding.descriptorType = descriptorType;
        descriptorSetLayoutBinding.descriptorCount = descriptorCount;
        descriptorSetLayoutBinding.stageFlags = shaderStageFlags;
        descriptorSetLayoutBinding.pImmutableSamplers = pImmutableSamplers;

        m_layoutBindings.push_back(descriptorSetLayoutBinding);
    }

    void CVulkanDescriptorSetLayout::Create(CVulkanDevice& device, const CVulkanDescriptorSetLayoutBuilder& builder, bool isPushDescriptorLayout)
    {
        VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo;
        descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        descriptorSetLayoutCreateInfo.pNext = nullptr;
        descriptorSetLayoutCreateInfo.flags = 0;
        descriptorSetLayoutCreateInfo.bindingCount = static_cast<u32>(builder.m_layoutBindings.size());
        descriptorSetLayoutCreateInfo.pBindings = builder.m_layoutBindings.data();

        if(isPushDescriptorLayout) descriptorSetLayoutCreateInfo.flags |= VK_DESCRIPTOR_SET_LAYOUT_CREATE_PUSH_DESCRIPTOR_BIT_KHR;

        PD_VK_THROW_IF_FAILED(vkCreateDescriptorSetLayout(device.GetHandle(), &descriptorSetLayoutCreateInfo,
                                                          PD_VK_ALLOCATOR_FUTURE, &m_handle));

        PD_END_INITIALIZE;
    }
}