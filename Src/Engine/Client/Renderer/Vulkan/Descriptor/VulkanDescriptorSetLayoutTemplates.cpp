//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanDescriptorSetLayoutTemplates.hpp"
#include "../../Renderer.hpp"

namespace PD
{
    void CVulkanDescriptorSetLayoutTemplates::OnDisposing(CVulkanDevice& device)
    {
        if(CRenderer::GetInstance()->GetProperties().UsePushDescriptors)
        {
            m_layoutPD_SI_FS.Dispose(device);
        }

        m_layout_S_FS_U_VS.Dispose(device);
        m_layout_SI_FS.Dispose(device);
    }

    void CVulkanDescriptorSetLayoutTemplates::Initialize(CVulkanDevice& device)
    {
        CVulkanDescriptorSetLayoutBuilder builder_SI_FS;
        builder_SI_FS.Reserve(1);
        builder_SI_FS.AddBinding(0, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT);
        m_layout_SI_FS.Create(device, builder_SI_FS, false);

        CVulkanDescriptorSetLayoutBuilder builder_S_FS_U_VS;
        builder_S_FS_U_VS.Reserve(2);
        builder_S_FS_U_VS.AddBinding(0, VK_DESCRIPTOR_TYPE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
        builder_S_FS_U_VS.AddBinding(1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT);
        m_layout_S_FS_U_VS.Create(device, builder_S_FS_U_VS, false);

        if(CRenderer::GetInstance()->GetProperties().UsePushDescriptors)
        {
            CVulkanDescriptorSetLayoutBuilder builder_PD_SI_FS;
            builder_PD_SI_FS.Reserve(1);
            builder_PD_SI_FS.AddBinding(0, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT);
            m_layoutPD_SI_FS.Create(device, builder_PD_SI_FS, true);
        }

        PD_END_INITIALIZE;
    }
}