//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORSETLAYOUT_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORSETLAYOUT_HPP

#pragma once

#include "../../../../Core/Types.hpp"
#include "../../../../Core/Disposable.hpp"
#include "../../../../Core/STL/Vector.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanDevice;

    class CVulkanDescriptorSetLayoutBuilder
    {
        friend class CVulkanDescriptorSetLayout;
    private:
        STL::CVector<VkDescriptorSetLayoutBinding> m_layoutBindings;
    public:
        void AddBinding(u32 binding, VkDescriptorType descriptorType, VkShaderStageFlags shaderStageFlags, u32 descriptorCount = 1, const VkSampler* pImmutableSamplers = nullptr);

        PD_FORCEINLINE void Reserve(usize size) {m_layoutBindings.reserve(size); }
    };

    class CVulkanDescriptorSetLayout
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanDescriptorSetLayout, CVulkanDevice&)
    private:
        VkDescriptorSetLayout m_handle;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Create(CVulkanDevice& device, const CVulkanDescriptorSetLayoutBuilder& builder, bool isPushDescriptorLayout);

        PD_FORCEINLINE VkDescriptorSetLayout& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkDescriptorSetLayout& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORSETLAYOUT_HPP