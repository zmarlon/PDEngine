//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORSETLAYOUTTEMPLATES_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORSETLAYOUTTEMPLATES_HPP

#pragma once

#include "../../../../Core/Disposable.hpp"
#include "VulkanDescriptorSetLayout.hpp"

namespace PD
{
    class CVulkanDescriptorSetLayoutTemplates
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanDescriptorSetLayoutTemplates, CVulkanDevice&)
    private:
        CVulkanDescriptorSetLayout m_layout_SI_FS;
        CVulkanDescriptorSetLayout m_layoutPD_SI_FS;
        CVulkanDescriptorSetLayout m_layout_S_FS_U_VS;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Initialize(CVulkanDevice& device);

        PD_FORCEINLINE CVulkanDescriptorSetLayout&GetLayout_SI_FS() { return m_layout_SI_FS; }
        PD_FORCEINLINE CVulkanDescriptorSetLayout& GetLayoutPD_SI_FS() { return m_layoutPD_SI_FS; }
        PD_FORCEINLINE CVulkanDescriptorSetLayout& GetLayout_S_FS_U_VS() { return m_layout_S_FS_U_VS; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORSETLAYOUTTEMPLATES_HPP