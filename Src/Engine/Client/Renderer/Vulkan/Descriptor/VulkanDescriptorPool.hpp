//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORPOOL_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORPOOL_HPP

#pragma once

#include "../../../../Core/Disposable.hpp"
#include "../../../../Core/Types.hpp"
#include "../../../../Core/STL/Vector.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanDevice;

    class CVulkanDescriptorPoolBuilder
    {
        friend class CVulkanDescriptorPool;
    private:
        u32 m_maxSize = 0;
        STL::CVector<VkDescriptorPoolSize> m_descriptorPoolSizes;

    public:
        PD_FORCEINLINE void AddPoolSize(VkDescriptorType descriptorType, u32 count)
        {
            VkDescriptorPoolSize poolSize;
            poolSize.type = descriptorType;
            poolSize.descriptorCount = count;

            m_descriptorPoolSizes.push_back(poolSize);
            m_maxSize += count;
        }

        PD_FORCEINLINE void Reserve(usize size) {m_descriptorPoolSizes.reserve(size); }
    };

    class CVulkanDescriptorPool
    {
        PD_MAKE_CLASS_DISPOSABLE_WITH_ARGUMENT(CVulkanDescriptorPool, CVulkanDevice&)
    private:
        VkDescriptorPool m_handle;

        void OnDisposing(CVulkanDevice& device);
    public:
        void Create(CVulkanDevice& device, const CVulkanDescriptorPoolBuilder& builder);

        PD_FORCEINLINE VkDescriptorPool& GetHandle() { return m_handle; }
        PD_FORCEINLINE const VkDescriptorPool& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORPOOL_HPP