//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanDescriptorSet.hpp"
#include "VulkanDescriptorPool.hpp"
#include "VulkanDescriptorSetLayout.hpp"
#include "../VulkanDevice.hpp"

namespace PD
{
    void CVulkanDescriptorSet::OnDisposing() {} //Does nothing

    void CVulkanDescriptorSet::Allocate(CVulkanDevice& device, CVulkanDescriptorPool& descriptorPool, CVulkanDescriptorSetLayout& descriptorSetLayout)
    {
        m_pDevice = &device;

        VkDescriptorSetAllocateInfo descriptorSetAllocateInfo;
        descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        descriptorSetAllocateInfo.pNext = nullptr;
        descriptorSetAllocateInfo.descriptorPool = descriptorPool.GetHandle();
        descriptorSetAllocateInfo.descriptorSetCount = 1;

        VkDescriptorSetLayout handleDescriptorSetLayout = descriptorSetLayout.GetHandle();
        descriptorSetAllocateInfo.pSetLayouts = &handleDescriptorSetLayout;

        PD_VK_THROW_IF_FAILED(vkAllocateDescriptorSets(device.GetHandle(), &descriptorSetAllocateInfo, &m_handle));
        PD_END_INITIALIZE;
    }

    void CVulkanDescriptorSet::Update(const VkWriteDescriptorSet* pWriteDescriptorSets, u32 numWriteDescriptorSets)
    {
        vkUpdateDescriptorSets(m_pDevice->GetHandle(), numWriteDescriptorSets, pWriteDescriptorSets, 0, nullptr);
    }
}