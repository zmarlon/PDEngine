//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORSET_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORSET_HPP

#pragma once

#include "../../../../Core/Types.hpp"
#include "../../../../Core/Disposable.hpp"
#include "../VulkanHelper.hpp"

namespace PD
{
    class CVulkanDevice;
    class CVulkanDescriptorPool;
    class CVulkanDescriptorSetLayout;
    class CVulkanDescriptorSet
    {
    PD_MAKE_CLASS_DISPOSABLE(CVulkanDescriptorSet)
    private:
        VkDescriptorSet m_handle;
        CVulkanDevice* m_pDevice;

        void OnDisposing();
    public:
        void Allocate(CVulkanDevice& device, CVulkanDescriptorPool& descriptorPool,
                      CVulkanDescriptorSetLayout& descriptorSetLayout);
        void Update(const VkWriteDescriptorSet* pWriteDescriptorSets, u32 numWriteDescriptorSets);

        PD_FORCEINLINE VkDescriptorSet& GetHandle() { return m_handle; }
        PD_FORCEINLINE const VkDescriptorSet& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANDESCRIPTORSET_HPP