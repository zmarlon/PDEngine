//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanDescriptorPool.hpp"
#include "../VulkanDevice.hpp"
#include "../../../../Core/Logging/Logger.hpp"

namespace PD
{
    void CVulkanDescriptorPool::OnDisposing(CVulkanDevice& device)
    {
        vkDestroyDescriptorPool(device.GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanDescriptorPool::Create(CVulkanDevice& device, const CVulkanDescriptorPoolBuilder& builder)
    {
        VkDescriptorPoolCreateInfo descriptorPoolCreateInfo;
        descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        descriptorPoolCreateInfo.pNext = nullptr;
        descriptorPoolCreateInfo.flags = 0;
        descriptorPoolCreateInfo.maxSets = builder.m_maxSize;
        descriptorPoolCreateInfo.poolSizeCount = static_cast<u32>(builder.m_descriptorPoolSizes.size());
        descriptorPoolCreateInfo.pPoolSizes = builder.m_descriptorPoolSizes.data();

        if(builder.m_descriptorPoolSizes.empty())
        {
            CLogger::GetInstance()->Log(ELogLevel::Error, PD_VK_RENDERER_PREFIX)
                    << L"Failed to create descriptor pool with empty pool size vector!"
                    << CLogger::GetInstance()->Flush();
        }

        PD_VK_THROW_IF_FAILED(vkCreateDescriptorPool(device.GetHandle(), &descriptorPoolCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));

        PD_END_INITIALIZE;
    }
}