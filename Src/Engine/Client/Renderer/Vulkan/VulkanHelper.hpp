//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANHELPER_HPP
#define PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANHELPER_HPP

#pragma once

#include "../../../Core/Types.hpp"
#include "../../../Core/Singleton.hpp"
#include <volk/volk.h>
#include <vk_mem_alloc.h>
#include <ostream>

#define PD_VK_RENDERER_PREFIX L"Vulkan Renderer"

#define PD_VK_THROW_IF_FAILED(x) VulkanHelper::ThrowIfFailed(x, PD_WIDE_STRINGIFY(x));
#define PD_VK_ALLOCATOR_FUTURE nullptr

namespace PD::VulkanHelper
{
    void ThrowIfFailed(VkResult result, const wchar_t* pMessage);

    const wchar_t* ResultToString(VkResult result);
    const wchar_t* FormatToString(VkFormat format);
    const wchar_t* ColorSpaceToString(VkColorSpaceKHR colorSpace);
    const wchar_t* PresentModeToString(VkPresentModeKHR presentMode);
    const wchar_t* ImageLayoutToString(VkImageLayout imageLayout);
    const wchar_t* GetDeviceFeatureString(u32 index);

    PD_FORCEINLINE VkSampleCountFlagBits GetSampleCount(u32 samples)
    {
        PD_ASSERT((samples & (samples - 1)) == 0);
        return static_cast<VkSampleCountFlagBits>(samples);
    }

    VkIndexType GetIndexType(u32 size);
    void AppendVersion(u32 version, std::wostream& stream);

    bool IsDepthFormat(VkFormat format);
    bool IsDepthStencilFormat(VkFormat format);
}

#endif //PD_ENGINE_CLIENT_RENDERER_VULKAN_VULKANHELPER_HPP