//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanThreadCollection.hpp"
#include "VulkanDevice.hpp"

namespace PD
{
    void CVulkanThreadCollection::OnDisposing()
    {
        m_fence.Dispose();


        m_transferCommandBuffer.Dispose();
        m_computeCommandBuffer.Dispose();
        m_graphicsCommandBuffer.Dispose();

        m_graphicsPerFrameCommandPool.Dispose();
        m_computePerFrameCommandPool.Dispose();
        m_transferPerFrameCommandPool.Dispose();

        m_transferImmediateCommandPool.Dispose();
        m_computeImmediateCommandPool.Dispose();
        m_graphicsImmediateCommandPool.Dispose();
    }

    void CVulkanThreadCollection::Initialize(CVulkanDevice& device)
    {
        m_pDevice = &device;

        m_graphicsImmediateCommandPool.Initialize(device, device.GetGraphicsQueue());
        m_computeImmediateCommandPool.Initialize(device, device.GetComputeQueue());
        m_transferImmediateCommandPool.Initialize(device, device.GetTransferQueue());

        m_graphicsPerFrameCommandPool.Initialize(device, device.GetGraphicsQueue());
        m_computePerFrameCommandPool.Initialize(device, device.GetComputeQueue());
        m_transferPerFrameCommandPool.Initialize(device, device.GetTransferQueue());

        m_graphicsCommandBuffer.Initialize(device, m_graphicsImmediateCommandPool);
        m_computeCommandBuffer.Initialize(device, m_computeImmediateCommandPool);
        m_transferCommandBuffer.Initialize(device, m_transferImmediateCommandPool);

        m_fence.Initialize(device);

        PD_END_INITIALIZE;
    }

    void CVulkanThreadCollection::EndFrame()
    {
        m_graphicsPerFrameCommandPool.Reset();
        m_computePerFrameCommandPool.Reset();
        m_transferPerFrameCommandPool.Reset();
    }

    CVulkanPrimaryCommandBuffer& CVulkanThreadCollection::BeginImmediateGraphics()
    {
        m_graphicsCommandBuffer.Begin();
        return m_graphicsCommandBuffer;
    }

    void CVulkanThreadCollection::EndImmediateGraphics()
    {
        m_graphicsCommandBuffer.End();
        m_pDevice->GetGraphicsQueue().Submit(m_graphicsCommandBuffer, m_fence);
        m_fence.WaitFor();

        m_fence.Reset();
        m_graphicsCommandBuffer.Reset();
        m_graphicsImmediateCommandPool.Reset();
    }

    CVulkanPrimaryCommandBuffer& CVulkanThreadCollection::BeginImmediateCompute()
    {
        m_computeCommandBuffer.Begin();
        return m_computeCommandBuffer;
    }

    void CVulkanThreadCollection::EndImmediateCompute()
    {
        m_computeCommandBuffer.End();
        m_pDevice->GetComputeQueue().Submit(m_computeCommandBuffer, m_fence);
        m_fence.WaitFor();

        m_fence.Reset();
        m_computeCommandBuffer.Reset();
        m_computeImmediateCommandPool.Reset();
    }

    CVulkanPrimaryCommandBuffer& CVulkanThreadCollection::BeginImmediateTransfer()
    {
        m_transferCommandBuffer.Begin();
        return m_transferCommandBuffer;
    }

    void CVulkanThreadCollection::EndImmediateTransfer()
    {
        m_transferCommandBuffer.End();
        m_pDevice->GetTransferQueue().Submit(m_transferCommandBuffer, m_fence);
        m_fence.WaitFor();

        m_fence.Reset();
        m_transferCommandBuffer.Reset();
        m_transferImmediateCommandPool.Reset();
    }
}