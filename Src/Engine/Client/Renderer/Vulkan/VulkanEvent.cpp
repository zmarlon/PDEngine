//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanEvent.hpp"
#include "VulkanDevice.hpp"

namespace PD
{
	void CVulkanEvent::OnDisposing()
	{
		vkDestroyEvent(m_pDevice->GetHandle(), m_handle, PD_VK_ALLOCATOR_FUTURE);
	}

	void CVulkanEvent::Initialize(CVulkanDevice& device)
	{
		m_pDevice = &device;

		VkEventCreateInfo eventCreateInfo;
		eventCreateInfo.sType = VK_STRUCTURE_TYPE_EVENT_CREATE_INFO;
		eventCreateInfo.pNext = nullptr;
		eventCreateInfo.flags = 0;

		PD_VK_THROW_IF_FAILED(vkCreateEvent(device.GetHandle(), &eventCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));

		PD_END_INITIALIZE;
	}

	void CVulkanEvent::Set()
	{
		PD_VK_THROW_IF_FAILED(vkSetEvent(m_pDevice->GetHandle(), m_handle));
	}

	void CVulkanEvent::Reset()
	{
		PD_VK_THROW_IF_FAILED(vkResetEvent(m_pDevice->GetHandle(), m_handle));
	}

	VkResult CVulkanEvent::GetStatus()
	{
		return vkGetEventStatus(m_pDevice->GetHandle(), m_handle);
	}
}