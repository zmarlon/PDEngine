//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#ifndef PD_ENGINE_RENDERER_VULKAN_VULKANSWAPCHAIN_HPP
#define PD_ENGINE_RENDERER_VULKAN_VULKANSWAPCHAIN_HPP

#pragma once

#include "../../../Core/Types.hpp"
#include "../../../Core/Disposable.hpp"
#include "../../../Core/Math/Color4f.hpp"
#include "VulkanHelper.hpp"
#include "VulkanSemaphore.hpp"
#include "VulkanFence.hpp"
#include "VulkanSurface.hpp"
#include "Image/VulkanRenderPass.hpp"
#include "Image/VulkanDepthStencilImage.hpp"

namespace PD
{
    class CVulkanPrimaryCommandBuffer;

    class CVulkanDevice;
    class CVulkanSurface;
    class CVulkanSwapchain
    {
        PD_MAKE_CLASS_DISPOSABLE(CVulkanSwapchain)
    private:
        VkSwapchainKHR m_handle = VK_NULL_HANDLE;
        VkSwapchainCreateInfoKHR m_swapchainCreateInfo;

        CVulkanDevice* m_pDevice;
        CVulkanSurface* m_pSurface;

        VkPresentModeKHR m_presentMode;
        VkSurfaceFormatKHR m_surfaceFormat;

        u32 m_numImages;
        CArray<VkImage> m_images;
        CArray<VkImageView> m_imageViews;
        CArray<VkFramebuffer> m_framebuffers;

        CVulkanDepthStencilImage m_depthImage;
        CVulkanRenderPass m_renderPass;

        CVulkanSemaphore m_semaphoreImageAvailable;
        CVulkanSemaphore m_semaphoreRenderingDone;
        CVulkanFence m_fence;

        u32 m_currentImageIndex;

        void CreateImageViews();
        void CreateFramebuffers(u32 width, u32 height);
        void Create();
        void CreateRenderPass();

        u32 FindOptimalImageCount() const;
        VkPresentModeKHR FindOptimalPresentMode(bool verticalSync) const;
        VkSurfaceFormatKHR FindOptimalSurfaceFormat(bool useHDR) const;

        void OnDisposing();
    public:
        void Initialize(CVulkanDevice& device, CVulkanSurface& surface, VkFormat depthFormat, bool verticalSync, bool useHDR);
        void Resize(CVulkanDevice& device, u32 width, u32 height);

        void Begin(CVulkanPrimaryCommandBuffer& commandBuffer, const SColor4f& color);
        void End(CVulkanSemaphore& semaphore, VkPipelineStageFlags waitStageFlags, CVulkanPrimaryCommandBuffer& commandBuffer);
        void End(CVulkanPrimaryCommandBuffer& commandBuffer);

        void PrintProperties();

        PD_FORCEINLINE CVulkanRenderPass& GetRenderPass() {return m_renderPass; }

        PD_FORCEINLINE VkSwapchainKHR& GetHandle() {return m_handle; }
        PD_FORCEINLINE const VkSwapchainKHR& GetHandle() const {return m_handle; }
    };
}

#endif //PD_ENGINE_RENDERER_VULKAN_VULKANSWAPCHAIN_HPP