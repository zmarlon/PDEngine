//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//

#include "VulkanDevice.hpp"
#include "VulkanPhysicalDevice.hpp"
#include "../Renderer.hpp"
#include "../../../Core/Logging/Logger.hpp"

namespace PD
{
    static f32 _S_SINGLE_PRIORITY = 1.0f;

    static void vmaAllocateDeviceMemoryFunction_Impl(VmaAllocator allocator, u32 memoryType, VkDeviceMemory memory,
                                                     VkDeviceSize size)
    {
        CLogger::GetInstance()->Log(ELogLevel::Information, PD_VK_RENDERER_PREFIX)
                << L"[VmaAllocator] vmaAllocateDeviceMemory Callback:\nMemory Type: "
                << memoryType << L"\nAddress: 0x" << std::hex << (u64)memory
                << L"\nSize: " << size << CLogger::GetInstance()->Flush();
    }

    static void
    vmaFreeDeviceMemoryFunction_Impl(VmaAllocator allocator, u32 memoryType, VkDeviceMemory memory, VkDeviceSize size)
    {
        CLogger::GetInstance()->Log(ELogLevel::Information, PD_VK_RENDERER_PREFIX)
                << L"[VmaAllocator] vmaFreeDeviceMemory Callback:\nMemory Type: "
                << memoryType << L"\nAddress: 0x" << std::hex << (u64)memory
                << L"\nSize: " << size << CLogger::GetInstance()->Flush();
    }

    void CVulkanDevice::OnDisposing()
    {
        m_descriptorSetLayoutTemplates.Dispose(*this);
        m_descriptorPool.Dispose(*this);
        vmaDestroyAllocator(m_allocator);

        for(auto& element : m_threadCollections) element.second.Dispose();

        vkDestroyDevice(m_handle, PD_VK_ALLOCATOR_FUTURE);
    }

    void CVulkanDevice::CreateMemoryAllocator()
    {
        m_deviceMemoryCallbacks.pfnAllocate = vmaAllocateDeviceMemoryFunction_Impl;
        m_deviceMemoryCallbacks.pfnFree = vmaFreeDeviceMemoryFunction_Impl;

        m_allocatorCreateInfo.flags = 0;
        m_allocatorCreateInfo.physicalDevice = m_pPhysicalDevice->GetHandle();
        m_allocatorCreateInfo.device = m_handle;
        m_allocatorCreateInfo.preferredLargeHeapBlockSize = 0;
        m_allocatorCreateInfo.pAllocationCallbacks = nullptr;
        m_allocatorCreateInfo.pDeviceMemoryCallbacks = m_pRenderer->GetDebugLevel() ? &m_deviceMemoryCallbacks : nullptr;
        m_allocatorCreateInfo.frameInUseCount = 0;
        m_allocatorCreateInfo.pHeapSizeLimit = nullptr;
        m_allocatorCreateInfo.pVulkanFunctions = nullptr;
        m_allocatorCreateInfo.pRecordSettings = nullptr;
        m_allocatorCreateInfo.vulkanApiVersion = VK_API_VERSION_1_0;

        PD_VK_THROW_IF_FAILED(vmaCreateAllocator(&m_allocatorCreateInfo, &m_allocator));
    }

    void CVulkanDevice::CreateDescriptorPool()
    {
        //TODO: civ

        CVulkanDescriptorPoolBuilder builder;
        builder.AddPoolSize(VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1024);
        builder.AddPoolSize(VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 64);
        builder.AddPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 512);
        //builder.AddPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1);
        builder.AddPoolSize(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 64);
        builder.AddPoolSize(VK_DESCRIPTOR_TYPE_SAMPLER, 64);
        //builder.AddPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1);
        //builder.AddPoolSize(VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1);

        m_descriptorPool.Create(*this, builder);
    }

    void CVulkanDevice::Initialize(CRenderer& renderer, CVulkanPhysicalDevice& physicalDevice)
    {
        m_pRenderer = &renderer;
        m_pPhysicalDevice = &physicalDevice;

        u32 graphicsQueueFamilyIndex = physicalDevice.GetGraphicsQueueFamilyIndex();
        u32 computeQueueFamilyIndex = physicalDevice.GetComputeQueueFamilyIndex();
        u32 transferQueueFamilyIndex = physicalDevice.GetTransferQueueFamilyIndex();

        VkDeviceQueueCreateInfo graphicsQueueCreateInfo;
        graphicsQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        graphicsQueueCreateInfo.pNext = nullptr;
        graphicsQueueCreateInfo.flags = 0;
        graphicsQueueCreateInfo.queueFamilyIndex = graphicsQueueFamilyIndex;
        graphicsQueueCreateInfo.queueCount = 1;
        graphicsQueueCreateInfo.pQueuePriorities = &_S_SINGLE_PRIORITY;
        m_deviceQueueCreateInfos.push_back(graphicsQueueCreateInfo);

        if(computeQueueFamilyIndex != graphicsQueueFamilyIndex)
        {
            VkDeviceQueueCreateInfo computeQueueCreateInfo;
            computeQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            computeQueueCreateInfo.pNext = nullptr;
            computeQueueCreateInfo.flags = 0;
            computeQueueCreateInfo.queueFamilyIndex = computeQueueFamilyIndex;
            computeQueueCreateInfo.queueCount = 1;
            computeQueueCreateInfo.pQueuePriorities = &_S_SINGLE_PRIORITY;
            m_deviceQueueCreateInfos.push_back(computeQueueCreateInfo);
        }
        if(transferQueueFamilyIndex != graphicsQueueFamilyIndex && transferQueueFamilyIndex != computeQueueFamilyIndex)
        {
            VkDeviceQueueCreateInfo transferQueueCreateInfo;
            transferQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            transferQueueCreateInfo.pNext = nullptr;
            transferQueueCreateInfo.flags = 0;
            transferQueueCreateInfo.queueFamilyIndex = transferQueueFamilyIndex;
            transferQueueCreateInfo.queueCount = 1;
            transferQueueCreateInfo.pQueuePriorities = &_S_SINGLE_PRIORITY;
            m_deviceQueueCreateInfos.push_back(transferQueueCreateInfo);
        }
    }

    void CVulkanDevice::Create()
    {
        m_deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        m_deviceCreateInfo.pNext = nullptr;
        m_deviceCreateInfo.flags = 0;
        m_deviceCreateInfo.queueCreateInfoCount = static_cast<u32>(m_deviceQueueCreateInfos.size());
        m_deviceCreateInfo.pQueueCreateInfos = m_deviceQueueCreateInfos.data();
        m_deviceCreateInfo.enabledLayerCount = static_cast<u32>(m_enabledLayers.size());
        m_deviceCreateInfo.ppEnabledLayerNames = m_enabledLayers.data();
        m_deviceCreateInfo.enabledExtensionCount = static_cast<u32>(m_enabledExtensions.size());
        m_deviceCreateInfo.ppEnabledExtensionNames = m_enabledExtensions.data();

        const auto& enabledFeatures = m_pPhysicalDevice->GetEnabledFeatures();
        m_deviceCreateInfo.pEnabledFeatures = &enabledFeatures;

        PD_VK_THROW_IF_FAILED(
                vkCreateDevice(m_pPhysicalDevice->GetHandle(), &m_deviceCreateInfo, PD_VK_ALLOCATOR_FUTURE, &m_handle));
        CLogger::GetInstance()->Log(ELogLevel::Success, PD_VK_RENDERER_PREFIX) << L"Created device: "
                                                                                   << m_pPhysicalDevice->GetProperties().deviceName
                                                                                   << CLogger::GetInstance()->Flush();
        volkLoadDevice(m_handle);

        u32 graphicsQueueFamilyIndex = m_pPhysicalDevice->GetGraphicsQueueFamilyIndex();
        u32 computeQueueFamilyIndex = m_pPhysicalDevice->GetComputeQueueFamilyIndex();
        u32 transferQueueFamilyIndex = m_pPhysicalDevice->GetTransferQueueFamilyIndex();

        CreateMemoryAllocator();

        auto& queueFamilyProperties = m_pPhysicalDevice->GetQueueFamilyProperties();

        m_queueGraphics.Initialize(*this, graphicsQueueFamilyIndex, 0,
                                   queueFamilyProperties[graphicsQueueFamilyIndex]);
        m_queueCompute.Initialize(*this, computeQueueFamilyIndex, 0, queueFamilyProperties[computeQueueFamilyIndex]);
        m_queueTransfer.Initialize(*this, transferQueueFamilyIndex, 0,
                                   queueFamilyProperties[transferQueueFamilyIndex]);

        AttachThread(std::this_thread::get_id());
        CreateDescriptorPool();
        m_descriptorSetLayoutTemplates.Initialize(*this);

        PD_END_INITIALIZE;
    }

    bool CVulkanDevice::AddLayer(const char* pLayerName)
    {
        bool result = m_pPhysicalDevice->IsLayerSupported(pLayerName);
        if(result)
        {
            m_enabledLayers.push_back(pLayerName);
        }
        return result;
    }

    bool CVulkanDevice::AddExtension(const char* pExtensionName)
    {
        bool result = m_pPhysicalDevice->IsExtensionSupported(pExtensionName);
        if(result)
        {
            m_enabledExtensions.push_back(pExtensionName);
        }
        return result;
    }

    void CVulkanDevice::AttachThread(std::thread::id threadId)
    {
        if(m_threadCollections.find(threadId) != m_threadCollections.end())
        {
            CLogger::GetInstance()->Log(ELogLevel::Error) << L"Thread Id " << threadId << L" has been attached multiple times!"
                                                          << CLogger::GetInstance()->Flush();
        }
        else
        {
            m_threadCollections.emplace(std::piecewise_construct, std::make_tuple(threadId), std::make_tuple());
            m_threadCollections[threadId].Initialize(*this);
        }
    }
}