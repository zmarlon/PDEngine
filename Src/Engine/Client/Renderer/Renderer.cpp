//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#include "Renderer.hpp"
#include "../../Core/Pak/PakLocation.hpp"
#include "../../Core/Pak/PakManager.hpp"
#include "../../Core/Managed/CommonLanguageRuntime.hpp"
#include "../../Core/Console/GameConsole.hpp"
#include "../../Core/Window/Window.hpp"

namespace PD
{
    PD_POSTDEFINE_SINGLETON(CRenderer)

    void CRenderer::OnDisposing()
    {
    	m_whitePixel.Dispose(m_device);

        m_spriteBatchFactory.Dispose();
        m_spriteBatch.Dispose(m_device);
        m_commandBuffer.Dispose();

        m_deferredRenderer.Dispose(m_device);
        DestroyDeviceAndSwapchain();

        DetachSingleton();
    }

    void CRenderer::CreateDeviceAndSwapchain(CWindow& window, const SGameInfo& gameInfo)
    {
        m_instance.Initialize(gameInfo, window);
        if(m_debugLevel) m_instance.AddLayer("VK_LAYER_LUNARG_standard_validation");
        m_properties.DebugMarkerEnabled = m_instance.AddExtension(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

        m_instance.AddLayer("pd, ii, hf, 27.03.2020");

        m_instance.Create();

        if(m_debugLevel) m_instance.PrintProperties();

        auto handlePhysicalDevice = m_instance.FindOptimalPhysicalDevice();
        m_surface.Initialize(m_instance, handlePhysicalDevice, window);
        if(m_debugLevel) m_surface.PrintProperties();

        m_physicalDevice.Initialize(m_surface, handlePhysicalDevice, [&](const VkPhysicalDeviceFeatures& supportedFeatures, VkPhysicalDeviceFeatures& enabledFeatures)
        {
            enabledFeatures.samplerAnisotropy = VK_TRUE;
        });
        if(m_debugLevel) m_physicalDevice.PrintProperties();

        m_device.Initialize(*this, m_physicalDevice);
        m_device.AddExtension(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
        m_properties.UsePushDescriptors = m_device.AddExtension(VK_KHR_PUSH_DESCRIPTOR_EXTENSION_NAME);
        m_device.Create();

        m_swapchain.Initialize(m_device, m_surface, VK_FORMAT_D24_UNORM_S8_UINT, false, false);
    }

    void CRenderer::DestroyDeviceAndSwapchain()
    {
        m_swapchain.Dispose();
        m_device.Dispose();
        m_physicalDevice.Dispose();
        m_surface.Dispose();
        m_instance.Dispose();
    }

    void CRenderer::RenderSprites(CVulkanPrimaryCommandBuffer& commandBuffer, const SFrameEventArgs& frameEventArgs)
    {
    	CWindow* pWindow = CWindow::GetInstance();

        m_spriteBatchFactory.SetCommandBuffer(commandBuffer);

        m_spriteBatch.Begin();
        m_spriteBatch.Draw(m_deferredRenderer.GetGBufferDiffuseImage().GetImageView(), pWindow->GetWidth(), pWindow->GetHeight(), SRectangleF(0.0f, 0.0f, 1600.0f, 900.0f), Color4f::White);
		CGameConsole::GetInstance()->Render(frameEventArgs, m_spriteBatch);
        m_spriteBatch.End();

        m_pfn_Renderer_RenderSprites(frameEventArgs);
    }

    void CRenderer::Initialize(CWindow& window, const SGameInfo& gameInfo, u32 debugLevel)
    {
        AttachSingleton();
        m_debugLevel = debugLevel;

        m_graphicsSettings.TextureQualityLevel = ETextureQualityLevel::UltraHigh; //TODO: Im Moment noch hier gehardcoded

        CreateDeviceAndSwapchain(window, gameInfo);

        m_deferredRenderer.Initialize(m_device);

        m_commandBuffer.Initialize(m_device, m_device.GetThreadCollection().GetGraphicsPerFrameCommandPool());
        m_spriteBatch.Create(m_device, m_swapchain.GetRenderPass());

        m_spriteBatchFactory.Create();
        m_spriteBatchFactory.AddExisting(m_spriteBatch);

        m_pfn_Renderer_RenderSprites = CCommonLanguageRuntime::GetInstance()->GetDelegate<void(SFrameEventArgs)>("PDEngine.Managed.Client.Renderer.Renderer", "NRenderSprites");

        u32 color = 0xFFFFFFFF;
        m_whitePixel.Load(m_device, 1, 1, VK_FORMAT_R8G8B8A8_UNORM, 4, &color);

        PD_END_INITIALIZE;
    }

    void CRenderer::ChangeSettings(const SGraphicsSettings& oldSettings, const SGraphicsSettings& newSettings)
    {
        //TODO:
    }

    void CRenderer::Resize(u32 newWidth, u32 newHeight)
    {
        m_swapchain.Resize(m_device, newWidth, newHeight);
        m_spriteBatchFactory.Resize(newWidth, newHeight);
    }

    void CRenderer::Render(const SFrameEventArgs& frameEventArgs)
    {
    	m_deferredRenderer.Render(frameEventArgs);

        m_commandBuffer.Begin();
        m_swapchain.Begin(m_commandBuffer, Color4f::CornflowerBlue);
        RenderSprites(m_commandBuffer, frameEventArgs);
		m_swapchain.End(m_deferredRenderer.GetGBufferSignalSemaphore(), VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, m_commandBuffer);

        m_device.EndFrame();
    }
}
