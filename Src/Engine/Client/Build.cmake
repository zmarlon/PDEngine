set(PD_ENGINE_CLIENT_DIRECTORY Src/Engine/Client)

include(Src/Engine/Client/Renderer/Build.cmake)

set(PD_ENGINE_CLIENT_SOURCE_FILES
        ${PD_ENGINE_CLIENT_DIRECTORY}/Camera.cpp
        ${PD_ENGINE_CLIENT_DIRECTORY}/Camera.hpp
        ${PD_ENGINE_CLIENT_DIRECTORY}/PlayerComponent.cpp
        ${PD_ENGINE_CLIENT_DIRECTORY}/PlayerComponent.hpp
        ${PD_ENGINE_CLIENT_DIRECTORY}/PlayerFreeCam.cpp
        ${PD_ENGINE_CLIENT_DIRECTORY}/PlayerFreeCam.hpp
        ${PD_ENGINE_CLIENT_RENDERER_SOURCE_FILES})