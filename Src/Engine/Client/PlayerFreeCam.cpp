//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#include "PlayerFreeCam.hpp"
#include "../Core/Input/KeyboardState.hpp"
#include "../Core/Input/MouseState.hpp"
#include "../Core/Input/Mouse.hpp"
#include <glm/gtc/constants.hpp>

namespace PD
{
	void CPlayerFreeCam::OnInitialize()
	{
		m_degFieldOfView = 90.0f; //TODO:
		m_nearPlane = 0.1f; //TODO:
		m_farPlane = 10000.0f; //TODO:
	}

	void CPlayerFreeCam::OnUpdate(const SFrameEventArgs& frameEventArgs)
	{
	    Mouse::DisableCursor();

		const auto keyboardState = CKeyboardState::Get();

		const float SPEED = 50.0f;

		float sinPitch = glm::sin(m_pEntityData->Rotation.x), cosPitch = glm::cos(m_pEntityData->Rotation.x);

		if(keyboardState.IsKeyDown(EKeys::Key_W))
		{
			m_pEntityData->Position.x += sinPitch * SPEED * frameEventArgs.DeltaTime;
			m_pEntityData->Position.z += cosPitch * SPEED * frameEventArgs.DeltaTime;
		}
		if(keyboardState.IsKeyDown(EKeys::Key_A))
		{
			m_pEntityData->Position.x += cosPitch * SPEED * frameEventArgs.DeltaTime;
			m_pEntityData->Position.z -= sinPitch * SPEED * frameEventArgs.DeltaTime;
		}
		if(keyboardState.IsKeyDown(EKeys::Key_S))
		{
			m_pEntityData->Position.x -= sinPitch * SPEED * frameEventArgs.DeltaTime;
			m_pEntityData->Position.z -= cosPitch * SPEED * frameEventArgs.DeltaTime;
		}
		if(keyboardState.IsKeyDown(EKeys::Key_D))
		{
			m_pEntityData->Position.x -= cosPitch * SPEED * frameEventArgs.DeltaTime;
			m_pEntityData->Position.z += sinPitch * SPEED * frameEventArgs.DeltaTime;
		}
		if(keyboardState.IsKeyDown(EKeys::Key_SPACE))
		{
			m_pEntityData->Position.y += SPEED * frameEventArgs.DeltaTime;
		}
		if(keyboardState.IsKeyDown(EKeys::Key_LSHIFT))
		{
			m_pEntityData->Position.y -= SPEED * frameEventArgs.DeltaTime;
		}

		const float SENSITIVITY_X = 0.165f;
		const float SENSITIVITY_Y = 0.165f;

		const auto mouseState = CMouseState::Get();
		glm::vec2 deltaPosition = m_lastMousePosition - mouseState.GetPosition<float>();

		deltaPosition.x *= SENSITIVITY_X * frameEventArgs.DeltaTime;
		deltaPosition.y *= SENSITIVITY_Y * frameEventArgs.DeltaTime;

		m_pEntityData->Rotation.x += deltaPosition.x;
		if(m_pEntityData->Rotation.x > glm::two_pi<float>())
		{
			m_pEntityData->Rotation.x = 0.0f;
		} else if(m_pEntityData->Rotation.x < 0.0f)
		{
			m_pEntityData->Rotation.x = glm::two_pi<float>();
		}

		m_pEntityData->Rotation.y += deltaPosition.y;
		if(m_pEntityData->Rotation.y > glm::half_pi<float>() - 0.15f)
		{
			m_pEntityData->Rotation.y = glm::half_pi<float>() - 0.15f;
		} else if(m_pEntityData->Rotation.y < 0.15f - glm::half_pi<float>())
		{
			m_pEntityData->Rotation.y = 0.15f - glm::half_pi<float>();
		}

		Mouse::SetPositionCentered();

		m_lastMousePosition = CMouseState::Get().GetPosition<float>();
	}
}