//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#include "Camera.hpp"
#include "../Core/Window/Window.hpp"
#include "../Core/Math/GlmExtensions.hpp"

namespace PD
{
	ICamera* ICamera::_INSTANCE = nullptr;

	void ICamera::Initialize(SEntityData* pEntityData)
	{
		m_pEntityData = pEntityData;

		OnInitialize();
		RefreshProjectionMatrix();
	}

	void ICamera::RefreshProjectionMatrix()
	{
		const auto* pWindow = CWindow::GetInstance();
		auto aspectRatio = static_cast<float>(pWindow->GetWidth()) / static_cast<float>(pWindow->GetHeight());
		m_projectionMatrix = GlmExtensions::ReverseDepthProjectionMatrixLH(m_degFieldOfView, aspectRatio, m_nearPlane, m_farPlane);
	}

	void ICamera::RefreshViewMatrix()
	{
		m_direction = GlmExtensions::RotationToDirection(m_pEntityData->Rotation);
		m_lookAt = m_pEntityData->Position + m_direction;

		m_viewMatrix = glm::lookAt(m_pEntityData->Position, m_lookAt, GlmExtensions::Vec3_UnitY<float>());
	}

	void ICamera::SetCurrent()
	{
		_INSTANCE = this;
	}

	void ICamera::Update(const SFrameEventArgs& frameEventArgs)
	{
		OnUpdate(frameEventArgs);
		RefreshViewMatrix();
	}

	void ICamera::Resize()
	{
		RefreshProjectionMatrix();
	}
}