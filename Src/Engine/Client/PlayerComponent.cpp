//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#include "PlayerComponent.hpp"

namespace PD
{
	PD_POSTDEFINE_SINGLETON(CPlayerComponent)

	void CPlayerComponent::OnDisposing()
	{
		DetachSingleton();
	}

	void CPlayerComponent::Initialize(SEntityData* pData)
	{
		m_pData = pData;

		m_playerFreeCam.Initialize(pData);
		m_playerFreeCam.SetCurrent();

		AttachSingleton();

		PD_END_INITIALIZE;
	}

	void CPlayerComponent::Update(const SFrameEventArgs& frameEventArgs)
	{
		ICamera::GetCurrent()->Update(frameEventArgs);
	}
}