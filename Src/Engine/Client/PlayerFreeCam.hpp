//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PD_ENGINE_CLIENT_PLAYERFREECAM_HPP
#define PD_ENGINE_CLIENT_PLAYERFREECAM_HPP

#pragma once

#include "Camera.hpp"

namespace PD
{
	class CPlayerFreeCam : public ICamera
	{
	private:
		glm::vec2 m_lastMousePosition = glm::vec2(0.0f, 0.0f);
	protected:
		void OnInitialize() override;
		void OnUpdate(const SFrameEventArgs& frameEventArgs) override;
	};
}

#endif //PD_ENGINE_CLIENT_PLAYERFREECAM_HPP
