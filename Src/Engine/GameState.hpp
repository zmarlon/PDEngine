//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 20.04.2020.
//

#ifndef PD_ENGINE_GAMESTATE_HPP
#define PD_ENGINE_GAMESTATE_HPP

namespace PD
{
	struct SGameState
	{
		bool IsPaused;
	};
}

#endif //PD_ENGINE_GAMESTATE_HPP
