//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 16.04.2020.
//

#include "World.hpp"
#include "WorldConstants.hpp"

namespace PD
{
    void CWorld::OnDisposing()
    {
        for(auto& iterator : m_chunks) CProxyAllocator::GetInstance()->DeallocateObject(iterator.second);
        m_chunks.clear();
    }

    void CWorld::Tick(const SFrameEventArgs& frameEventArgs)
    {

    }
}