//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 16.04.2020.
//
#include "WorldServer.hpp"

namespace PD
{
    PD_POSTDEFINE_SINGLETON(CWorldServer)
    void CWorldServer::OnDisposing()
    {
        DetachSingleton();

        for(auto& iterator : m_chunks) m_chunkAllocator.DeallocateObject(iterator.second);
        m_chunkAllocator.Dispose();

        m_regionCache.Dispose();

        CWorld::OnDisposing();
    }

    void CWorldServer::Create(const wchar_t* pWorldName, const wchar_t* pWorldDirectory)
    {
        PD_ASSERT(pWorldName);
        PD_ASSERT(pWorldDirectory);

        m_pName = pWorldName;
        m_pDirectory = pWorldDirectory;

        m_regionCache.Create(*this);
        m_chunkAllocator.Create(2048); //TODO: SIZE

        AttachSingleton();

        PD_END_INITIALIZE;
    }

    void CWorldServer::DoRandomTicks(u32 tickSpeed)
    {
        
    }

    CChunk& CWorldServer::GetChunk(const SChunkPosition& position)
    {
        auto iterator = m_chunks.find(position);
        if(iterator != m_chunks.end()) return *iterator->second;

        auto* pChunk = m_chunkAllocator.AllocateObject<CChunk>(position);
        if(!m_regionCache.LoadChunk(*pChunk))
        {
            //TODO: generate empty chunk with world generation
        }
        m_chunks[position] = pChunk;
        return *pChunk;
    }

    void CWorldServer::Tick(const SFrameEventArgs& frameEventArgs)
    {
        CWorld::Tick(frameEventArgs);
    }
}