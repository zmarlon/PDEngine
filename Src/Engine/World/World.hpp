//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 16.04.2020.
//
#ifndef PD_ENGINE_WORLD_WORLD_HPP
#define PD_ENGINE_WORLD_WORLD_HPP

#pragma once

#include "../Core/STL/UnorderedMap.hpp"
#include "../Core/STL/String.hpp"
#include "../Core/Math/MersenneTwister.hpp"
#include "Chunk/Chunk.hpp"
#include <filesystem>

namespace PD
{
    class CWorld
    {
        PD_MAKE_CLASS_DISPOSABLE(CWorld);
    private:
		STL::CUnorderedMap<SChunkPosition, CChunk*> m_chunks;
    protected:
        CMT19937 m_random;

        const wchar_t* m_pName;

        virtual void OnDisposing();
    public:
        virtual bool IsRemote() const = 0;

        //BEGIN-STATIC-ENTITY
        static SChunkPosition GetChunkPositionFromWorldPosition(const glm::vec3& position)
        {
            return SChunkPosition(static_cast<i32>(position.x) >> 4, static_cast<i32>(position.z));
        }

        virtual CChunk& GetChunk(const SChunkPosition& position) = 0;
        //END-STATIC-ENTITY

        virtual void Tick(const SFrameEventArgs& frameEventArgs);
    };
}

#endif //PD_ENGINE_WORLD_WORLD_HPP
