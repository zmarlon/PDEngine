//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 13.04.2020.
//
#ifndef PD_ENGINE_WORLD_WORLDCONSTANTS_HPP
#define PD_ENGINE_WORLD_WORLDCONSTANTS_HPP

#pragma once

#include "../Core/Types.hpp"
#include "../Core/STL/String.hpp"
#include <filesystem>

namespace PD
{
    enum EWorldConstants : u32
    {
        ChunkSizeX = 16,
        ChunkSizeY = 16,
        ChunkSizeZ = 16,
        NumChunksInRegionX = 16,
        NumChunksInRegionZ = 16,
        NumChunksInRegion = NumChunksInRegionX * NumChunksInRegionZ,
        ChunkToRegionShiftX = 4,
        ChunkToRegionShiftZ = 4
    };

    namespace WorldConstants
    {

    }
}

#endif //PD_ENGINE_WORLD_WORLDCONSTANTS_HPP
