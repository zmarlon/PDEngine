//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 16.04.2020.
//
#ifndef PD_ENGINE_WORLD_WORLDSERVER_HPP
#define PD_ENGINE_WORLD_WORLDSERVER_HPP

#pragma once

#include "World.hpp"
#include "../Core/Memory/PoolAllocator.hpp"
#include "../Core/STL/UnorderedMap.hpp"
#include "Chunk/ChunkRegionCache.hpp"

namespace PD
{
    class CWorldServer final : public CWorld
    {
        PD_MAKE_CLASS_SINGLETON(CWorldServer);
    private:
        u32 m_randomTickSpeed = 16;

        const wchar_t* m_pDirectory;

        CPoolAllocator<CChunk> m_chunkAllocator;
        CChunkRegionCache m_regionCache;
        STL::CUnorderedMap<SChunkPosition, CChunk*> m_chunks;

        void OnDisposing() override;
    public:
        void Create(const wchar_t* pWorldName, const wchar_t* pWorldDirectory);
        void DoRandomTicks(u32 tickSpeed);

        CChunk& GetChunk(const SChunkPosition& position) final;

        void Tick(const SFrameEventArgs& frameEventArgs) override;
        bool IsRemote() const final {return false; }

        PD_FORCEINLINE const wchar_t* GetDirectory() const {return m_pDirectory; }
        PD_FORCEINLINE u32 GetRandomTickSpeed() const { return m_randomTickSpeed; }
        PD_FORCEINLINE u32 SetRandomTickSpeed(u32 randomTickSpeed) { m_randomTickSpeed = randomTickSpeed; }
    };
}

#endif //PDENGINE_WORLDSERVER_HPP
