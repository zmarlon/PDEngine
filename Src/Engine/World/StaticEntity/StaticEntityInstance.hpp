//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//

#ifndef PD_ENGINE_WORLD_STATICENTITY_STATICENTITYINSTANCE_HPP
#define PD_ENGINE_WORLD_STATICENTITY_STATICENTITYINSTANCE_HPP

#pragma once

#include "../../Core/Types.hpp"
#include "StaticEntityType.hpp"
#include "../../Core/Hints/ForceInline.hpp"
#include <glm/glm.hpp>

namespace PD
{
	struct SStaticEntityInstance
	{
	private:
		u32 Data;
	public:
		glm::vec3 Position;
		glm::vec3 Rotation;

		PD_FORCEINLINE EStaticEntityType GetType() const
		{
			return static_cast<EStaticEntityType>((Data & 0xFFFFFF00u) >> 8u);
		}

		PD_FORCEINLINE u8 GetMetaData() const
		{
			return static_cast<u8>(Data & 0xFFu);
		}

		PD_FORCEINLINE void SetType(EStaticEntityType staticEntityType)
		{
			Data &= 0x000000FFu;
			Data |= static_cast<u32>(staticEntityType) << 8u;
		}

		PD_FORCEINLINE void SetMetaData(u8 metaData)
		{
			Data &= 0xFFFFFF00u;
			Data |= metaData;
		}

		PD_FORCEINLINE bool operator ==(const SStaticEntityInstance& other) const
		{
			return Position == other.Position && Rotation == other.Rotation && Data == other.Data;
		}

		PD_FORCEINLINE bool operator !=(const SStaticEntityInstance& other) const
		{
			return Position != other.Position || Rotation != other.Rotation || Data != other.Data;
		}
	};
}

#endif //PD_ENGINE_WORLD_STATICENTITY_STATICENTITYINSTANCE_HPP
