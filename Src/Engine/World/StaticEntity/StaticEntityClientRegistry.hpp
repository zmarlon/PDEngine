//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#ifndef PD_ENGINE_WORLD_STATICENTITY_STATICENTITYCLIENTREGISTRY_HPP
#define PD_ENGINE_WORLD_STATICENTITY_STATICENTITYCLIENTREGISTRY_HPP

#pragma once

#include "../../Core/Disposable.hpp"
#include "../../Client/Renderer/Common/BindlessTextureContainer.hpp"

namespace PD
{
    class CStaticEntityRegistry;
    class CVulkanDevice;
    class CVulkanSampler;
    class CStaticEntityClientRegistry
    {
        PD_MAKE_CLASS_DISPOSABLE(CStaticEntityClientRegistry)
    private:
        CStaticEntityRegistry* m_pRegistry;

        CBindlessTextureContainer m_textureContainer;

        void OnDisposing();
    public:
        void Initialize(CStaticEntityRegistry& registry);

        void RegisterTextures(CVulkanDevice& device, CVulkanSampler& sampler);
    };
}

#endif //PD_ENGINE_WORLD_STATICENTITY_STATICENTITYCLIENTREGISTRY_HPP
