//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//

#ifndef PD_ENGINE_WORLD_STATICENTITY_STATICENTITYREGISTRY_HPP
#define PD_ENGINE_WORLD_STATICENTITY_STATICENTITYREGISTRY_HPP

#pragma once

#include "StaticEntityType.hpp"
#include "../../Core/Memory/ProxyAllocator.hpp"
#include "../../Core/STL/Vector.hpp"

namespace PD
{
    class CStaticEntityClientRegistry;
	class CStaticEntity;
	class CStaticEntityRegistry
	{
	    friend class CStaticEntityClientRegistry;
	    
		PD_MAKE_CLASS_DISPOSABLE(CStaticEntityRegistry)
		PD_MAKE_CLASS_SINGLETON(CStaticEntityRegistry)
	private:
	    bool m_isOpened;
		STL::CVector<CStaticEntity*> m_staticEntities;

        CStaticEntityClientRegistry* m_pClientRegistry;

		void OnDisposing();
	public:
		void Initialize(bool isClient);

		void Register(CStaticEntity* pStaticEntity);
		EStaticEntityType RegisterCustom(CStaticEntity* pStaticEntity);
		void Close();

		template<typename T, typename... TArguments>
		PD_FORCEINLINE void RegisterObject(TArguments&&... arguments)
		{
			T* pObject = CProxyAllocator::GetInstance()->AllocateObject<T>(arguments...);
			Register(pObject);
		}

		template<typename T, typename... TArguments>
		PD_FORCEINLINE EStaticEntityType RegisterCustomObject(TArguments&&... arguments)
		{
			T* pObject = CProxyAllocator::GetInstance()->AllocateObject<T>(arguments...);
			return RegisterCustom(pObject);
		}

		PD_FORCEINLINE CStaticEntity* GetFromType(EStaticEntityType type)
		{
            PD_ASSERT(static_cast<u32>(type) < m_staticEntities.size());
			return m_staticEntities[static_cast<u32>(type)];
		}
	};
}

#endif //PD_ENGINE_WORLD_STATICENTITY_STATICENTITYREGISTRY_HPP
