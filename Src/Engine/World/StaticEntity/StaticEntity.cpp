//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//

#include "StaticEntity.hpp"
#include "../../Client/Renderer/Vulkan/Command/VulkanPrimaryCommandBuffer.hpp"
#include "../../Client/Renderer/Common/IndexedModel.hpp"
#include "StaticEntityInstance.hpp"
#include <glm/gtc/matrix_transform.hpp>

namespace PD
{
	void CStaticEntity::Render(CVulkanPrimaryCommandBuffer& commandBuffer, CWorld& world, SStaticEntityInstance& instance)
	{
		auto& model = GetModel(world, instance);
		commandBuffer.BindVertexBuffer(model.GetVertexBuffer());
		commandBuffer.BindIndexBuffer(model.GetIndexBuffer());

		glm::mat4 rotationMatrix = glm::rotate(glm::mat4(1.0f), instance.Rotation.z, glm::vec3(0.0f, 0.0f, 1.0f)) * glm::rotate(glm::mat4(1.0f), instance.Rotation.y, glm::vec3(0.0f, 1.0f, 0.0f))
				* glm::rotate(glm::mat4(1.0f), instance.Rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
		glm::mat4 modelMatrix = rotationMatrix * glm::translate(glm::mat4(1.0f), instance.Position);

		commandBuffer.PushConstants(VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::mat4), &modelMatrix);

		auto textureId = GetTextureId(world, instance);
		commandBuffer.PushConstants(VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(u32), &textureId);
	}
}