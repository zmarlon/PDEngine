//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#include "StaticEntityClientRegistry.hpp"
#include "StaticEntityRegistry.hpp"
#include "StaticEntity.hpp"
#include "../../Client/Renderer/Renderer.hpp"

namespace PD
{
    void CStaticEntityClientRegistry::OnDisposing()
    {
        m_textureContainer.Dispose(CRenderer::GetInstance()->GetDevice());
    }

    void CStaticEntityClientRegistry::Initialize(CStaticEntityRegistry& registry)
    {
        m_pRegistry = &registry;
    }

    void CStaticEntityClientRegistry::RegisterTextures(CVulkanDevice& device, CVulkanSampler& sampler)
    {
        m_textureContainer.Create(device, static_cast<u32>(m_pRegistry->m_staticEntities.size()));
        for(auto* pStaticEntity : m_pRegistry->m_staticEntities) pStaticEntity->RegisterTextures(m_textureContainer);

        m_textureContainer.Build(sampler);

        PD_END_INITIALIZE;
    }
}