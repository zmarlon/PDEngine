//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//

#include "StaticEntityRegistry.hpp"
#include "StaticEntity.hpp"
#include "StaticEntityClientRegistry.hpp"

namespace PD
{
	PD_POSTDEFINE_SINGLETON(CStaticEntityRegistry)

	void CStaticEntityRegistry::OnDisposing()
	{
	    if(m_pClientRegistry) CProxyAllocator::GetInstance()->DeallocateObject(m_pClientRegistry);

		DetachSingleton();
	}

	void CStaticEntityRegistry::Initialize(bool isClient)
	{
		AttachSingleton();

		m_isOpened = true;

		if(isClient)
        {
		    m_pClientRegistry = CProxyAllocator::GetInstance()->AllocateObject<CStaticEntityClientRegistry>();
        }

		PD_END_INITIALIZE;
	}

	void CStaticEntityRegistry::Register(CStaticEntity* pStaticEntity)
	{
	    PD_ASSERT(m_isOpened);
	    PD_ASSERT(static_cast<usize>(pStaticEntity->GetType()) == m_staticEntities.size());
        PD_ASSERT(pStaticEntity);

	    m_staticEntities.push_back(pStaticEntity);
	}

	EStaticEntityType CStaticEntityRegistry::RegisterCustom(CStaticEntity* pStaticEntity)
	{
	    PD_ASSERT(m_isOpened);
		PD_ASSERT(static_cast<u32>(pStaticEntity->GetType()) == -1);
		PD_ASSERT(pStaticEntity);

		auto newStaticEntityType = static_cast<EStaticEntityType>(m_staticEntities.size());
        m_staticEntities.push_back(pStaticEntity);
		return newStaticEntityType;
	}

    void CStaticEntityRegistry::Close()
    {
        PD_ASSERT(m_isOpened);
        m_isOpened = false;
    }
}