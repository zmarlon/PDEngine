//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//

#ifndef PD_ENGINE_WORLD_STATICENTITY_STATICENTITYTYPE_HPP
#define PD_ENGINE_WORLD_STATICENTITY_STATICENTITYTYPE_HPP

#pragma once

#include "../../Core/Types.hpp"

namespace PD
{
	enum class EStaticEntityType : u32
	{
		Cajon = 0
	};
}

#endif //PD_ENGINE_WORLD_STATICENTITY_STATICENTITYTYPE_HPP
