//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//

#ifndef PD_ENGINE_WORLD_STATICENTITY_STATICENTITY_HPP
#define PD_ENGINE_WORLD_STATICENTITY_STATICENTITY_HPP

#pragma once

#include "StaticEntityType.hpp"
#include "../../Core/Hints/ForceInline.hpp"
#include <glm/glm.hpp>

namespace PD
{
	struct SStaticEntityInstance;
	struct SFrameEventArgs;
	class CVulkanPrimaryCommandBuffer;
	class CWorld;
	class CIndexedModel;
	class CBindlessTextureContainer;
	class CStaticEntity
	{
		friend class CStaticEntityRegistry;
		friend class CStaticEntityClientRegistry;
	private:
		EStaticEntityType m_type;

	protected:
	    virtual void RegisterTextures(CBindlessTextureContainer& textureContainer) {}
	public:
		CStaticEntity(EStaticEntityType type) : m_type(type) {}

		virtual CIndexedModel& GetModel(CWorld& world, SStaticEntityInstance& instance) = 0;
		virtual u32 GetTextureId(CWorld& world, SStaticEntityInstance& instance) = 0; //TODO:
		virtual void Render(CVulkanPrimaryCommandBuffer& commandBuffer, CWorld& world, SStaticEntityInstance& instance);

		virtual void Tick(SStaticEntityInstance& instance) = 0; //TODO: soll 1x die sekunde aufgerufen werden, wird nur gecallt wenn sie inhalt hat (unnötige method calls sparen)

		PD_FORCEINLINE EStaticEntityType GetType() const {return m_type; }
	};
}

#endif //PD_ENGINE_WORLD_STATICENTITY_STATICENTITY_HPP
