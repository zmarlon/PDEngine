//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#ifndef PD_ENGINE_WORLD_VOXEL_VOXELVALUE_HPP
#define PD_ENGINE_WORLD_VOXEL_VOXELVALUE_HPP

#pragma once

#include "../../Core/Types.hpp"
#include "../../Core/Singleton.hpp"
#include "../../Core/Math/MathHelper.hpp"
#include <limits>

namespace PD
{
    template<typename T>
    struct SVoxelValueBase
    {
        static const usize MAX_VALUE = std::numeric_limits<T>::max() / 2;
        static const usize MIN_VALUE = -MAX_VALUE;
    private:
        using TVoxelValueBase = SVoxelValueBase<T>;

        T m_value;

    public:
        SVoxelValueBase() : m_value(static_cast<T>(0)) {}
        SVoxelValueBase(float value) : m_value(FMath::Clamp<int>(MathHelper::RoundToInt(MathHelper::Clamp<float>(InValue, -1, 1) * MAX_VALUE), std::numeric_limits<T>::min(), std::numeric_limits<T>::max())) {}

        PD_FORCEINLINE bool IsNull() const {return m_value == 0; }
        //TODO: Is valid with NaN
        PD_FORCEINLINE bool IsEmpty() const {return m_value > 0; }
        PD_FORCEINLINE bool IsTotallyEmpty() const {return m_value >= MAX_VALUE; }
        PD_FORCEINLINE bool IsTotallyFull() const {return m_value <= MIN_VALUE; }

        PD_FORCEINLINE TVoxelValueBase& Invert()
        {
            m_value = -m_value;
            return *this;
        }

        PD_FORCEINLINE TVoxelValueBase GetInverse()
        {
            TVoxelValueBase value;
            value.m_value = -m_value;
            return value;
        }

        PD_FORCEINLINE float ToFloat() const
        {
            return static_cast<float>(m_value) / static_cast<float>(MAX_VALUE);
        }

        PD_FORCEINLINE bool operator ==(const TVoxelValueBase& other) {return m_value == other.m_value; }
        PD_FORCEINLINE bool operator !=(const TVoxelValueBase& other) {return m_value != other.m_value; }
        PD_FORCEINLINE bool operator <(const TVoxelValueBase& other) {return m_value < other.m_value; }
        PD_FORCEINLINE bool operator >(const TVoxelValueBase& other) {return m_value > other.m_value; }
        PD_FORCEINLINE bool operator <=(const TVoxelValueBase& other) {return m_value <= other.m_value; }
        PD_FORCEINLINE bool operator >=(const TVoxelValueBase& other) {return m_value >= other.m_value; }
    };

    using SVoxelValue = SVoxelValueBase<i16>;
}

#endif //PD_ENGINE_WORLD_VOXEL_VOXELVALUE_HPP
