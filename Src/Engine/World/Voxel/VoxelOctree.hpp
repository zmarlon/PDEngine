//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 19.04.2020.
//

#ifndef PD_ENGINE_WORLD_VOXEL_VOXELOCTREE_HPP
#define PD_ENGINE_WORLD_VOXEL_VOXELOCTREE_HPP

#pragma once

#include "../../Core/Types.hpp"
#include "../../Core/Math/AABB.hpp"
#include "../../Core/Memory/ProxyAllocator.hpp"
#include "VoxelId.hpp"

namespace PD
{
	template<typename TElementType, u32 TChunkSize>
	class CVoxelOctree
	{
	private:
		glm::ivec3 m_position;
		u8 m_lod;
		SVoxelId m_id;
		SAABBI m_octreeBounds;
		bool m_isLeaf;

		TElementType* m_pChildren;
	protected:
		CVoxelOctree(const glm::ivec3& position, u8 lod, const SVoxelId& id)
			: m_position(position)
			, m_lod(lod)
			, m_id(id)
			, m_octreeBounds(GetCachedBounds())
			, m_isLeaf(true)
		{
			PD_ASSERT(m_lod >= EVoxelConstants::MaxWorldDepth);
		}

		CVoxelOctree(TElementType* pParent, u8 childIndex)
			: m_position(pParent->GetPosition() + glm::ivec3
				(
					 pParent->GetSize() / 4 * ((childIndex & 1) ? 1 : -1),
					 pParent->GetSize() / 4 * ((childIndex & 2) ? 1 : -1),
					 pParent->GetSize() / 4 * ((childIndex & 4) ? 1 : -1)
				)
			)
			, m_lod(pParent->GetLod() -1)
			, m_id(SVoxelId::FromParent(pParent->GetVoxelId(), pParent->GetLod() - 1, childIndex +1))
			, m_octreeBounds(GetCachedBounds())
			, m_isLeaf(true)
		{
			PD_ASSERT(childIndex < 8);
		}
	public:
		CVoxelOctree(u8 lod)
			: m_position(glm::ivec3(0)),
			  m_lod(lod),
			  m_id(true),
			  m_octreeBounds(GetCachedBounds()),
			  m_isLeaf(true)
		{
			PD_ASSERT(lod < EVoxelConstants::MaxWorldDepth);
		}

		PD_FORCEINLINE void CreateChildren()
		{
			PD_ASSERT(m_isLeaf);
			PD_ASSERT(!m_pChildren);
			PD_ASSERT(m_lod != 0);

			m_pChildren = CProxyAllocator::GetInstance()->AllocateAligned(sizeof(TElementType) * 8, alignof(TElementType));
			for(u32 ix = 0; ix < 8; ix++)
            {
                new(m_pChildren + ix) TElementType(reinterpret_cast<TElementType*>(this), ix);
            }

			m_isLeaf = false;
		}

		PD_FORCEINLINE void DestroyChildren()
		{
			PD_ASSERT(!m_isLeaf);
			PD_ASSERT(m_pChildren);

			for(int ix = 0; ix < 8; ix++)
			{
				m_pChildren[ix]->~TElementType();
			}

			CProxyAllocator::GetInstance()->DeallocateAligned(m_pChildren);
			m_pChildren = nullptr;
			m_isLeaf = true;
		}

		PD_FORCEINLINE bool IsIdChild(const SVoxelId& childId) const
		{
			return SVoxelId::IsChild(m_id, childId);
		}

		PD_FORCEINLINE SAABBI GetCachedBounds() const
		{
			glm::ivec3 vec = glm::ivec3(GetSize() >> 1);
			return SAABBI(m_position - vec, m_position + vec);
		}

		template<typename T>
		PD_FORCEINLINE bool IsInOctree(T x, T y, T z) const
		{
			return m_octreeBounds.Contains(glm::tvec3<T>(x, y, z));
		}

		template<typename T>
		PD_FORCEINLINE bool IsInOctree(const glm::tvec3<T>& vec) const
		{
			return m_octreeBounds.Contains(vec);
		}

		template<typename T>
		PD_FORCEINLINE void LocalToGlobal(T x, T y, T z, T& outX, T& outY, T& outZ) const
		{
			outX = x + static_cast<T>(m_octreeBounds.Min.x);
			outY = y + static_cast<T>(m_octreeBounds.Min.y);
			outZ = z + static_cast<T>(m_octreeBounds.Min.z);
		}

		template<typename T>
		PD_FORCEINLINE void GlobalToLocal(T x, T y, T z, T& outX, T& outY, T& outZ) const
		{
			outX = x - static_cast<T>(m_octreeBounds.Min.x);
			outY = y - static_cast<T>(m_octreeBounds.Min.y);
			outZ = z - static_cast<T>(m_octreeBounds.Min.z);
		}

		template<typename T>
		PD_FORCEINLINE TElementType* GetChild(T x, T y, T z) const
		{
			PD_ASSERT(!m_isLeaf);
			return m_pChildren + (x >= m_position.x) + 2 * (y >= m_position.y) + 4 * (z >= m_position.z);
		}

		template<typename T>
		PD_FORCEINLINE TElementType* GetChild(const glm::tvec3<T>& vec) const
		{
			return GetChild(vec.x, vec.y, vec.z);
		}

		template<typename T>
		PD_FORCEINLINE TElementType* GetLeaf(T x, T y, T z)
		{
			PD_ASSERT(IsInOctree(x, y, z));

			const TElementType* pElement = reinterpret_cast<const TElementType*>(this);

			while(!pElement->IsLeaf())
			{
				pElement = pElement->GetChild(x, y, z);
			}

			PD_ASSERT(pElement->IsInOctree(x, y, z));
			return const_cast<TElementType*>(pElement);
		}

		template<typename T>
		PD_FORCEINLINE TElementType* GetLeaf(const glm::tvec3<T>& vec)
		{
			return GetLeaf(vec.x, vec.y, vec.z);
		}

		PD_FORCEINLINE const glm::ivec3& GetPosition() const {return m_position; }
		PD_FORCEINLINE u8 GetLod() const {return m_lod; }
		PD_FORCEINLINE const SVoxelId& GetVoxelId() const {return m_id; }
		PD_FORCEINLINE const SAABBI& GetOctreeBounds() const {return m_octreeBounds; }
		PD_FORCEINLINE bool IsLeaf() const {return m_isLeaf; }
		PD_FORCEINLINE TElementType* GetChildren() const {return m_pChildren; }

        PD_FORCEINLINE int GetSize() const {return TChunkSize << m_lod; }
	};
}

#endif //PD_ENGINE_WORLD_VOXEL_VOXELOCTREE_HPP
