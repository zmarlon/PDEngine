//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 27.04.2020.
//
#include "VoxelThreadPool.hpp"
#include "../../Core/Threading/ThreadExtensions.hpp"
#include <thread>

namespace PD
{
    //CVoxelQueuedThread
    u32 CVoxelQueuedThread::Run()
    {
        while(!m_timeToDie)
        {
            bool continueWaiting = true;
            while(continueWaiting)
            {
                continueWaiting = !m_pDoWorkEvent->Wait(10);
            }

            IVoxelQueuedWork* pLocalQueuedWork = m_pQueuedWork;
            m_pQueuedWork = nullptr;
            ThreadExtensions::InsertMemoryBarrier();
            PD_ASSERT(pLocalQueuedWork || m_timeToDie);

            while(pLocalQueuedWork)
            {
                pLocalQueuedWork->DoThreadedWork();
                pLocalQueuedWork = m_pOwningThreadPool->ReturnToPoolOrGetNextJob(this);
            }
        }
        return 0;
    }

    CVoxelQueuedThread::CVoxelQueuedThread()
        : m_timeToDie(0),
        m_pQueuedWork(nullptr),
        m_pOwningThreadPool(nullptr),
        m_pThread(nullptr) {}

    bool CVoxelQueuedThread::Create(CVoxelQueuedThreadPool* pThreadPool, u32 stackSize, EThreadPriority priority)
    {
        return false;
    }

    bool CVoxelQueuedThread::KillThread()
    {
        bool didExitOK = true;


        return false;
    }

    void CVoxelQueuedThread::DoWork(IVoxelQueuedWork* pQueuedWork)
    {
        PD_ASSERT(!m_pQueuedWork);
        m_pQueuedWork = pQueuedWork;

        ThreadExtensions::InsertMemoryBarrier();
        m_pDoWorkEvent->Trigger();
    }

    //CVoxelQueuedThreadPool
    CVoxelQueuedThreadPool::CVoxelQueuedThreadPool() : m_timeToDie(false) {}

    CVoxelQueuedThreadPool::~CVoxelQueuedThreadPool()
    {
        Destroy();
    }

    bool CVoxelQueuedThreadPool::Create(u32 numQueuedThreads, u32 stackSize, EThreadPriority priority)
    {
        bool wasSuccessfull = true;

        std::scoped_lock lock(m_queueMutex);
        PD_ASSERT(m_queuedThreads.empty());
        m_queuedThreads.reserve(numQueuedThreads);

        for(u32 ix = 0; ix < numQueuedThreads && wasSuccessfull; ix++)
        {
            auto* pThread = CProxyAllocator::GetInstance()->AllocateObject<CVoxelQueuedThread>();
            if(pThread->Create(this, stackSize, priority))
            {
                m_queuedThreads.push_back(pThread);
                m_allThreads.push_back(pThread);
            }
            else
            {
                wasSuccessfull = false;
                CProxyAllocator::GetInstance()->DeallocateObject(pThread);
            }
        }

        if(!wasSuccessfull) Destroy();
        return wasSuccessfull;
    }

    void CVoxelQueuedThreadPool::Destroy()
    {
        {
            std::scoped_lock lock(m_queueMutex);
            m_timeToDie = true;
            ThreadExtensions::InsertMemoryBarrier();
            while(!m_queuedWork.empty())
            {
                auto* pWork = m_queuedWork.top().pWork;
                if(m_validQueuedWork.contains(pWork))
                {
                    pWork->Abandon();
                }
                m_queuedWork.pop();
            }
            m_validQueuedWork.clear();
        }

        while(true)
        {
            {
                std::scoped_lock lock(m_queueMutex);
                if(m_allThreads.size() == m_queuedThreads.size()) break;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(0));
        }
        {
            std::scoped_lock lock(m_queueMutex);
            for(auto pThread : m_allThreads)
            {
                pThread->KillThread();
                CProxyAllocator::GetInstance()->DeallocateObject(pThread);
            }
            m_queuedThreads.clear();
            m_allThreads.clear();
        }
    }

    i32 CVoxelQueuedThreadPool::GetNumQueuedJobs() const
    {
        return static_cast<i32>(m_queuedWork.size());
    }

    i32 CVoxelQueuedThreadPool::GetNumThreads() const
    {
        return static_cast<i32>(m_allThreads.size());
    }

    void CVoxelQueuedThreadPool::AddQueuedWork(IVoxelQueuedWork* pQueuedWork)
    {
        PD_ASSERT(pQueuedWork != nullptr);

        if(m_timeToDie)
        {
            pQueuedWork->Abandon();
            return;
        }

        CVoxelQueuedThread* pThread = nullptr;
        std::scoped_lock lock(m_queueMutex);

        if(!m_queuedThreads.empty())
        {
            i32 index = 0;
            pThread = m_queuedThreads[index];
            m_queuedThreads.erase(m_queuedThreads.begin() + index);
        }

        if(pThread)
        {
            pThread->DoWork(pQueuedWork);
        }
        else
        {
            m_validQueuedWork.insert(pQueuedWork);
            m_queuedWork.push(SVoxelQueuedWorkPointer(pQueuedWork, m_validQueuedWork));
        }
    }

    bool CVoxelQueuedThreadPool::RetractQueuedWork(IVoxelQueuedWork* pQueuedWork)
    {
        if(m_timeToDie) return false;
        PD_ASSERT(pQueuedWork);

        std::scoped_lock lock(m_queueMutex);

        auto iterator = m_validQueuedWork.find(pQueuedWork);
        if(iterator != m_validQueuedWork.end())
        {
            m_validQueuedWork.erase(pQueuedWork);
            return true;
        }
        return false;
    }

    IVoxelQueuedWork* CVoxelQueuedThreadPool::ReturnToPoolOrGetNextJob(CVoxelQueuedThread* pQueuedThread)
    {
        PD_ASSERT(pQueuedThread);
        IVoxelQueuedWork* pWork = nullptr;

        std::scoped_lock lock(m_queueMutex);
        if(m_timeToDie)
        {
            PD_ASSERT(m_queuedWork.empty());
        }

        bool workIsValid = false;
        while(!m_queuedWork.empty() && !workIsValid)
        {
            pWork = m_queuedWork.top().pWork;
            m_queuedWork.pop();

            workIsValid = m_validQueuedWork.contains(pWork);
        }
        if(workIsValid)
        {
            PD_ASSERT(pWork);
            m_validQueuedWork.erase(m_validQueuedWork.find(pWork));
        }
        else
        {
            m_queuedThreads.push_back(pQueuedThread);
        }
        return workIsValid ? pWork : nullptr;
    }

    //CVoxelAsyncWork
    CVoxelAsyncWork::CVoxelAsyncWork(IVoxelAsyncCallback* pCallback, bool autoDelete)
    {
        //TODO: GetSynchEvent From Pool and Reset
    }

    CVoxelAsyncWork::~CVoxelAsyncWork()
    {
        //TODO: Return done event to pool
    }

    void CVoxelAsyncWork::DoThreadedWork()
    {
        DoWork();

        std::scoped_lock lock(m_doneSection);

        m_pDoneEvent->Trigger();
        m_isDoneCounter++;

        if(m_pCallback) m_pCallback->OnCallback();
        if(m_autoDelete) CProxyAllocator::GetInstance()->DeallocateObject(this);
    }

    void CVoxelAsyncWork::Abandon()
    {
        std::scoped_lock lock(m_doneSection);
        m_pDoneEvent->Trigger();
        m_isDoneCounter++;
    }

    bool CVoxelAsyncWork::IsDone() const
    {
        return m_isDoneCounter > 0;
    }

    void CVoxelAsyncWork::WaitForCompletion()
    {
        m_pDoneEvent->Wait();
        std::scoped_lock lock(m_doneSection);
    }

    void CVoxelAsyncWork::CancelAndAutoDelete()
    {
        std::scoped_lock lock(m_doneSection);
        m_pCallback = nullptr;
        m_autoDelete = true;
        m_cancled = true;

        if(IsDone())
        {
            CProxyAllocator::GetInstance()->DeallocateObject(this);
        }
    }
}