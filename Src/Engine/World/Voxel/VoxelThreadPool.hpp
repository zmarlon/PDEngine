//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 27.04.2020.
//
#ifndef PD_ENGINE_WORLD_VOXEL_VOXELTHREADPOOL_HPP
#define PD_ENGINE_WORLD_VOXEL_VOXELTHREADPOOL_HPP

#pragma once

#include "../../Core/Types.hpp"
#include "../../Core/STL/Set.hpp"
#include "../../Core/STL/Queue.hpp"
#include "../../Core/Threading/Runnable.hpp"
#include "../../Core/Threading/ThreadPriority.hpp"
#include "../../Core/Threading/RunnableThread.hpp"
#include "../../Core/Threading/Event.hpp"
#include <limits>
#include <atomic>
#include <mutex>

namespace PD
{
    class IVoxelQueuedWork
    {
    public:
        virtual void DoThreadedWork() = 0;
        virtual void Abandon() = 0;
        virtual u64 GetPriority() const = 0;

        virtual ~IVoxelQueuedWork() {}
    };

    struct SVoxelQueuedWorkPointer
    {
        IVoxelQueuedWork* pWork;
        const STL::CSet<IVoxelQueuedWork*>* pValidQueuedWork;

        SVoxelQueuedWorkPointer(IVoxelQueuedWork* pWork, const STL::CSet<IVoxelQueuedWork*>& validQueuedWork)
            : pWork(pWork), pValidQueuedWork(&validQueuedWork) {}

        u64 GetPriority() const
        {
            if(pValidQueuedWork->contains(pWork)) return pWork->GetPriority();
            return std::numeric_limits<u64>::max();
        }
    };

    class CVoxelQueuedWorkCompare
    {
    public:
        bool operator()(const SVoxelQueuedWorkPointer& p1, const SVoxelQueuedWorkPointer& p2)
        {
            return p1.GetPriority() < p2.GetPriority();
        }
    };

    class CVoxelQueuedThreadPool;
    class CVoxelQueuedThread : public IRunnable
    {
    protected:
        CEvent* m_pDoWorkEvent;
        volatile i32 m_timeToDie;
        IVoxelQueuedWork* volatile m_pQueuedWork;
        CVoxelQueuedThreadPool* m_pOwningThreadPool;
        CRunnableThread* m_pThread;

        virtual u32 Run() override;
    public:
        CVoxelQueuedThread();
        bool Create(CVoxelQueuedThreadPool* pThreadPool, u32 stackSize = 0, EThreadPriority priority = EThreadPriority::Normal);
        bool KillThread();
        void DoWork(IVoxelQueuedWork* pQueuedWork);
    };

    class CVoxelQueuedThreadPool
    {
    protected:
        STL::CPriorityQueue<SVoxelQueuedWorkPointer, STL::CVector<SVoxelQueuedWorkPointer>, CVoxelQueuedWorkCompare> m_queuedWork;
        STL::CSet<IVoxelQueuedWork*> m_validQueuedWork;

        STL::CVector<CVoxelQueuedThread*> m_queuedThreads;
        STL::CVector<CVoxelQueuedThread*> m_allThreads;
        std::mutex m_queueMutex;
        bool m_timeToDie;
    public:
        CVoxelQueuedThreadPool();
        ~CVoxelQueuedThreadPool();

        bool Create(u32 numQueuedThreads, u32 stackSize = 32 * 1024, EThreadPriority priority = EThreadPriority::Normal);
        void Destroy();

        i32 GetNumQueuedJobs() const;
        i32 GetNumThreads() const;
        void AddQueuedWork(IVoxelQueuedWork* pQueuedWork);
        bool RetractQueuedWork(IVoxelQueuedWork* pQueuedWork);
        IVoxelQueuedWork* ReturnToPoolOrGetNextJob(CVoxelQueuedThread* pQueuedThread);
    };

    class IVoxelAsyncCallback
    {
    public:
        virtual void OnCallback() = 0;
    };

    class CVoxelAsyncWork : public IVoxelQueuedWork
    {
    private:
        std::atomic_size_t m_isDoneCounter;
        CEvent* m_pDoneEvent;
        std::mutex m_doneSection;

        IVoxelAsyncCallback* m_pCallback;
        bool m_autoDelete = false;
        bool m_cancled = false;
    protected:
        PD_FORCEINLINE bool IsCancled() const {return m_cancled; }
    public:
        CVoxelAsyncWork(IVoxelAsyncCallback* pCallback = nullptr, bool autoDelete = false);
        ~CVoxelAsyncWork() override;

        virtual void DoWork() = 0;

        virtual void DoThreadedWork() final;
        virtual void Abandon() final;

        bool IsDone() const;
        void WaitForCompletion();

        void CancelAndAutoDelete();
    };
}

#endif //PD_ENGINE_WORLD_VOXEL_VOXELTHREADPOOL_HPP
