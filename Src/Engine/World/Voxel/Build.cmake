set(PD_ENGINE_WORLD_VOXEL_DIRECTORY Src/Engine/World/Voxel)

include(Src/Engine/World/Voxel/Data/Build.cmake)
include(Src/Engine/World/Voxel/Generator/Build.cmake)
include(Src/Engine/World/Voxel/Material/Build.cmake)
include(Src/Engine/World/Voxel/Render/Build.cmake)

set(PD_ENGINE_WORLD_VOXEL_SOURCE_FILES
        ${PD_ENGINE_WORLD_VOXEL_DIRECTORY}/VoxelId.hpp
        ${PD_ENGINE_WORLD_VOXEL_DIRECTORY}/VoxelOctree.hpp
        ${PD_ENGINE_WORLD_VOXEL_DIRECTORY}/VoxelConstants.hpp
        ${PD_ENGINE_WORLD_VOXEL_DIRECTORY}/VoxelThreadPool.cpp
        ${PD_ENGINE_WORLD_VOXEL_DIRECTORY}/VoxelThreadPool.hpp
        ${PD_ENGINE_WORLD_VOXEL_DIRECTORY}/VoxelValue.hpp
        ${PD_ENGINE_WORLD_VOXEL_DATA_SOURCE_FILES}
        ${PD_ENGINE_WORLD_VOXEL_GENERATOR_SOURCE_FILES}
        ${PD_ENGINE_WORLD_VOXEL_MATERIAL_SOURCE_FILES}
        ${PD_ENGINE_WORLD_VOXEL_RENDER_SOURCE_FILES})