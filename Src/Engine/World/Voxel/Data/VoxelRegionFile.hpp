//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.05.2020.
//
#ifndef PD_ENGINE_WORLD_VOXEL_DATA_VOXELREGIONFILE_HPP
#define PD_ENGINE_WORLD_VOXEL_DATA_VOXELREGIONFILE_HPP

#pragma once

#include "../../../Core/Disposable.hpp"
#include "../../../Core/Types.hpp"
#include "../VoxelConstants.hpp"
#include <glm/glm.hpp>
#include <fstream>
#include <array>

namespace PD
{
    class CVoxelDataChunk;
    class CVoxelRegionCache;
    class CVoxelRegionFile
    {
        PD_MAKE_CLASS_DISPOSABLE(CVoxelRegionFile)
    private:
        std::fstream m_fileStream;
        usize m_fileSize;
        bool m_writeMode;

        struct SChunkHeader
        {
            u32 Offset;
            i32 SizeOrValue;
        };

        struct SHeader
        {
            std::array<SChunkHeader, EVoxelConstants::NumChunksInRegion> ChunkHeaders;
        } m_header;

        PD_FORCEINLINE SChunkHeader& GetChunkHeader(const glm::ivec3& position)
        {
            return m_header.ChunkHeaders[position.z * EVoxelConstants::NumChunksInRegionX * EVoxelConstants::NumChunksInRegionY
                + position.y * EVoxelConstants::NumChunksInRegionX + position.x];
        }

        void RewriteHeader();

        void OnDisposing();
    public:
        void Load(CVoxelRegionCache& regionCache, const glm::ivec3& regionPosition);

        bool LoadChunk(CVoxelDataChunk& chunk);
        bool SaveChunk(CVoxelDataChunk& chunk);
    };
}

#endif //PD_ENGINE_WORLD_VOXEL_DATA_VOXELREGIONFILE_HPP
