//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.05.2020.
//
#include "VoxelRegionCache.hpp"
#include "VoxelRegionFile.hpp"
#include "VoxelDataChunk.hpp"

namespace PD
{
    void CVoxelRegionCache::OnDisposing()
    {
        auto* pAllocator = CProxyAllocator::GetInstance();
        for(auto& iterator : m_regions) pAllocator->DeallocateObject(iterator.second); //TODO: Pool Allocator
        m_regions.clear();
    }

    void CVoxelRegionCache::Create(CWorldServer& world)
    {
        m_pWorld = &world;

        PD_END_INITIALIZE;
    }

    CVoxelRegionFile& CVoxelRegionCache::GetOrLoadFile(const glm::ivec3& regionPosition)
    {
        auto iterator = m_regions.find(regionPosition);
        if(iterator != m_regions.end()) return *iterator->second;

        auto* pRegionFile = CProxyAllocator::GetInstance()->AllocateObject<CVoxelRegionFile>(); //TODO: Pool Allocator
        pRegionFile->Load(*this, regionPosition);
        m_regions[regionPosition] = pRegionFile;
        return *pRegionFile;
    }

    bool CVoxelRegionCache::LoadChunk(CVoxelDataChunk& chunk)
    {
        const auto& gridPosition = chunk.GetGridPosition();

        glm::ivec3 regionPosition(gridPosition.x >> EVoxelConstants::ChunkToRegionShiftX, gridPosition.y >> EVoxelConstants::ChunkToRegionShiftY, gridPosition.z >> EVoxelConstants::ChunkToRegionShiftZ);
        return GetOrLoadFile(regionPosition).LoadChunk(chunk);
    }

    bool CVoxelRegionCache::SaveChunk(CVoxelDataChunk& chunk)
    {
        const auto& gridPosition = chunk.GetGridPosition();

        glm::ivec3 regionPosition(gridPosition.x >> EVoxelConstants::ChunkToRegionShiftX, gridPosition.y >> gridPosition.y >> EVoxelConstants::ChunkToRegionShiftY, gridPosition.z >> EVoxelConstants::ChunkToRegionShiftZ);
        return GetOrLoadFile(regionPosition).LoadChunk(chunk);
    }
}