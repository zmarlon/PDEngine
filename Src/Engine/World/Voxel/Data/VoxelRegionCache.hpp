//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.05.2020.
//
#ifndef PD_ENGINE_WORLD_VOXEL_DATA_VOXELREGIONCACHE_HPP
#define PD_ENGINE_WORLD_VOXEL_DATA_VOXELREGIONCACHE_HPP

#pragma once

#include "../../../Core/STL/UnorderedMap.hpp"
#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>

namespace PD
{
    class CVoxelDataChunk;
    class CWorldServer;
    class CVoxelRegionFile;
    class CVoxelRegionCache
    {
        PD_MAKE_CLASS_DISPOSABLE(CVoxelRegionCache)
    private:
        STL::CUnorderedMap<glm::ivec3, CVoxelRegionFile*> m_regions;
        CWorldServer* m_pWorld;

        void OnDisposing();
    public:
        void Create(CWorldServer& world);

        CVoxelRegionFile& GetOrLoadFile(const glm::ivec3& regionPosition);
        bool LoadChunk(CVoxelDataChunk& chunk);
        bool SaveChunk(CVoxelDataChunk& chunk);

        PD_FORCEINLINE CWorldServer& GetWorld() { return *m_pWorld; }
    };
}

#endif //PD_ENGINE_WORLD_VOXEL_DATA_VOXELREGIONCACHE_HPP
