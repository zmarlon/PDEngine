//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#include "VoxelDataChunk.hpp"
#include "../Material/VoxelMaterialRegistry.hpp"

namespace PD
{
    CVoxelDataChunk::CVoxelDataChunk(const glm::ivec3& gridPosition) : m_gridPosition(gridPosition)
    {
        //TODO:
    }

    CVoxelMaterial* CVoxelDataChunk::GetMaterial(const glm::ivec3& position) const
    {
        return CVoxelMaterialRegistry::GetInstance()->GetFromType(GetMaterialType(position));
    }

    void CVoxelDataChunk::FillChunkCompletely(EVoxelMaterialType materialType)
    {
        //TODO:
    }
}