//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 29.04.2020.
//
#ifndef PD_ENGINE_WORLD_VOXEL_VOXELDATAOCTREE_HPP
#define PD_ENGINE_WORLD_VOXEL_VOXELDATAOCTREE_HPP

#pragma once

#include "../VoxelOctree.hpp"
#include "../VoxelValue.hpp"
#include "../Material/VoxelMaterialType.hpp"

namespace PD
{
    class CVoxelDataOctree : public CVoxelOctree<CVoxelDataOctree, EVoxelConstants::ChunkSizeInMeters>
    {
    private:

    public:
    };
}

#endif //PD_ENGINE_WORLD_VOXEL_VOXELDATAOCTREE_HPP
