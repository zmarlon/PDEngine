//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#ifndef PD_ENGINE_WORLD_VOXEL_DATA_VOXELDATACHUNK_HPP
#define PD_ENGINE_WORLD_VOXEL_DATA_VOXELDATACHUNK_HPP

#pragma once

#include "../../../Core/Hints/ForceInline.hpp"
#include "../VoxelValue.hpp"
#include "../Material/VoxelMaterialType.hpp"
#include "../VoxelConstants.hpp"
#include <glm/glm.hpp>
#include <array>

namespace PD
{
    class CVoxelMaterial;
    class CVoxelDataChunk
    {
        friend class CVoxelRegionFile;
    private:
        glm::ivec3 m_gridPosition;

        struct SData
        {
            std::array<SVoxelValue, EVoxelConstants::NumChunkVoxels> Values;
            std::array<EVoxelMaterialType, EVoxelConstants::NumChunkVoxels> MaterialTypes;
        } m_data;

        bool m_valuesDirty;
        bool m_materialTypesDirty;
    public:
        CVoxelDataChunk(const glm::ivec3& gridPosition);

        void FillChunkCompletely(EVoxelMaterialType materialType); //TODO:

        //GETTER
        PD_FORCEINLINE SVoxelValue GetValue(const glm::uvec3& position) const
        {
            return m_data.Values[position.z * EVoxelConstants::ChunkVoxelSize * EVoxelConstants::ChunkVoxelSize + position.y * EVoxelConstants::ChunkVoxelSize + position.x];
        }

        PD_FORCEINLINE EVoxelMaterialType GetMaterialType(const glm::uvec3& position) const
        {
            return m_data.MaterialTypes[position.z * EVoxelConstants::ChunkVoxelSize * EVoxelConstants::ChunkVoxelSize + position.y * EVoxelConstants::ChunkVoxelSize + position.x];
        }

        CVoxelMaterial* GetMaterial(const glm::ivec3& position) const;
        PD_FORCEINLINE bool IsFilledCompletely(EVoxelMaterialType& materialType) const {return false; } //TODO:

        PD_FORCEINLINE std::array<SVoxelValue, EVoxelConstants::NumChunkVoxels>& GetValues() {return m_data.Values; }
        PD_FORCEINLINE const std::array<SVoxelValue, EVoxelConstants::NumChunkVoxels>& GetValues() const { return m_data.Values; }
        PD_FORCEINLINE std::array<EVoxelMaterialType, EVoxelConstants::NumChunkVoxels>& GetMaterialTypes() {return m_data.MaterialTypes; }
        PD_FORCEINLINE const std::array<EVoxelMaterialType, EVoxelConstants::NumChunkVoxels>& GetMaterialTypes() const { return m_data.MaterialTypes; }

        PD_FORCEINLINE const glm::ivec3& GetGridPosition() const {return m_gridPosition; }
    };
}

#endif //PD_ENGINE_WORLD_VOXEL_DATA_VOXELDATACHUNK_HPP
