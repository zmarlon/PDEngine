//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.05.2020.
//
#include "VoxelRegionFile.hpp"
#include "VoxelRegionCache.hpp"
#include "../../WorldServer.hpp"
#include "../../../Core/Memory/MemoryHelper.hpp"
#include "../../../Core/STL/StringStream.hpp"
#include "VoxelDataChunk.hpp"
#define MINIZ_HEADER_FILE_ONLY
#include <miniz/miniz.h>
#include <filesystem>

namespace PD
{
    void CVoxelRegionFile::OnDisposing()
    {
        m_fileStream.close();
    }

    void CVoxelRegionFile::RewriteHeader()
    {
        if(!m_writeMode)
        {
            m_fileStream.flush();
            m_writeMode = true;
        }
    }

    void CVoxelRegionFile::Load(CVoxelRegionCache& regionCache, const glm::ivec3& regionPosition)
    {
        STL::CStringStream filePathStream;
        filePathStream << regionCache.GetWorld().GetDirectory() << std::filesystem::path::preferred_separator <<
            L"Data" << std::filesystem::path::preferred_separator << regionPosition.x << L'_' << regionPosition.y << L'_' << regionPosition.z << L".pdvr";

        auto filePath = filePathStream.str();

        bool fileAlreadyExists = std::filesystem::exists(filePath);
        m_fileStream.open(filePath, std::ios::in | std::ios::out | std::ios::binary);
        if(!m_fileStream.is_open())
        {
            //TOOD: error, file is not openable
        }

        if(!fileAlreadyExists)
        {
            MemoryHelper::Zero(&m_header, sizeof(SHeader));
            m_fileStream.write((char*)&m_header, sizeof(SHeader));
            m_writeMode = true;
        }
        else
        {
            m_fileStream.read((char*)&m_header, sizeof(SHeader));
            m_writeMode = false;
        }

        PD_END_INITIALIZE;
    }

    bool CVoxelRegionFile::LoadChunk(CVoxelDataChunk& chunk)
    {
        const auto& header = GetChunkHeader(chunk.GetGridPosition());
        if(header.SizeOrValue < 1)
        {
            auto value = static_cast<EVoxelMaterialType>(-header.SizeOrValue);
            chunk.FillChunkCompletely(value);
            return true;
        }

        m_fileStream.seekg(sizeof(SHeader) + header.Offset);

        auto* pProxyAllocator = CProxyAllocator::GetInstance();
        auto* pCompressedData = reinterpret_cast<char*>(pProxyAllocator->Allocate(header.SizeOrValue));

        if(m_writeMode)
        {
            m_fileStream.flush();
            m_writeMode = false;
        }
        m_fileStream.read(pCompressedData, header.SizeOrValue);

        mz_ulong decompressedSize;
        if(mz_uncompress(reinterpret_cast<u8*>(&chunk.m_data), &decompressedSize, reinterpret_cast<u8*>(pCompressedData), header.SizeOrValue) != MZ_OK || decompressedSize != sizeof(CVoxelDataChunk::m_data)) return false;

        pProxyAllocator->Deallocate(pCompressedData);
    }

    bool CVoxelRegionFile::SaveChunk(CVoxelDataChunk& chunk)
    {
        auto& header = GetChunkHeader(chunk.GetGridPosition());

        EVoxelMaterialType materialType;
        if(chunk.IsFilledCompletely(materialType))
        {
            if(header.SizeOrValue < 1)
            {
                header.SizeOrValue = -static_cast<i32>(materialType);
                RewriteHeader();
                return true;
            }

        }
        //TODO: fixen die gesamte klasse
    }
}