//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 19.04.2020.
//
#ifndef PD_ENGINE_WORLD_VOXEL_VOXELID_HPP
#define PD_ENGINE_WORLD_VOXEL_VOXELID_HPP

#pragma once

#include "VoxelConstants.hpp"
#include "../../Core/Types.hpp"
#include "../../Core/Singleton.hpp"
#include "../../Core/Memory/MemoryHelper.hpp"
#include <array>

namespace PD
{
	struct SVoxelId
	{
	public:
		std::array<u8, EVoxelConstants::MaxWorldDepthHalf> m_data;

		SVoxelId() = default;
		SVoxelId(bool forceInitToZero)
		{
			if(forceInitToZero)
			{
			    MemoryHelper::Zero(m_data.data(), EVoxelConstants::MaxWorldDepthHalf * sizeof(u8));
			}
		}

		PD_FORCEINLINE static SVoxelId FromParent(const SVoxelId& parent, u8 childLOD, u8 childIndex)
		{
			PD_ASSERT(childIndex < 16);
			PD_ASSERT(childLOD < EVoxelConstants::MaxWorldDepth);
			SVoxelId id = parent;
			id.m_data[childLOD >> 1u] |= childIndex << ((childLOD & 1u) << 2u);
			return id;
		}

		PD_FORCEINLINE static bool IsChild(const SVoxelId& parent, const SVoxelId& child)
		{
			for(int ix = 0; ix < EVoxelConstants::MaxWorldDepthHalf; ix++)
			{
				if (((parent.m_data[ix] & 0x0Fu) != 0 && ((parent.m_data[ix] & 0x0Fu) != (child.m_data[ix] & 0x0Fu))) ||
					((parent.m_data[ix] & 0xF0u) != 0 && ((parent.m_data[ix] & 0xF0u) != (child.m_data[ix] & 0xF0u))))
				{
					return false;
				}
			}
			return true;
		}

		PD_FORCEINLINE bool operator==(const SVoxelId& other) const
		{
			for (int ix = 0; ix < EVoxelConstants::MaxWorldDepthHalf; ix++)
			{
				if (m_data[ix] != other.m_data[ix])
				{
					return false;
				}
			}
			return true;
		}

		PD_FORCEINLINE bool operator!=(const SVoxelId& other) const
		{
			for (int ix = 0; ix < EVoxelConstants::MaxWorldDepthHalf; ix++)
			{
				if (m_data[ix] != other.m_data[ix])
				{
					return true;
				}
			}
			return false;
		}
	};
}

#endif //PD_ENGINE_WORLD_VOXEL_VOXELID_HPP
