//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#ifndef PD_ENGINE_WORLD_VOXEL_MATERIAL_VOXELMATERIALTYPE_HPP
#define PD_ENGINE_WORLD_VOXEL_MATERIAL_VOXELMATERIALTYPE_HPP

#pragma once

#include "../../../Core/Types.hpp"

namespace PD
{
    enum class EVoxelMaterialType : byte
    {

    };
}

#endif //PD_ENGINE_WORLD_VOXEL_MATERIAL_VOXELMATERIALTYPE_HPP
