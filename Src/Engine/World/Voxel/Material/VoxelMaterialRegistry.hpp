//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#ifndef PD_ENGINE_WORLD_TERRAIN_MATERIAL_VOXELMATERIALREGISTRY_HPP
#define PD_ENGINE_WORLD_TERRAIN_MATERIAL_VOXELMATERIALREGISTRY_HPP

#pragma once

#include "../../../Core/Memory/ProxyAllocator.hpp"
#include "../../../Core/STL/Vector.hpp"
#include "VoxelMaterialType.hpp"

namespace PD
{
    class CVoxelMaterialClientRegistry;
    class CVoxelMaterial;
    class CVoxelMaterialRegistry
    {
        friend class CVoxelMaterialClientRegistry;

        PD_MAKE_CLASS_DISPOSABLE(CVoxelMaterialRegistry)
        PD_MAKE_CLASS_SINGLETON(CVoxelMaterialRegistry)
    private:
        bool m_isOpened;
        STL::CVector<CVoxelMaterial*> m_voxelMaterials;

        CVoxelMaterialClientRegistry* m_pClientRegistry;

        void OnDisposing();

    public:
        void Initialize(bool isClient);

        void Register(CVoxelMaterial* pVoxelMaterial);
        EVoxelMaterialType RegisterCustom(CVoxelMaterial* pStaticEntity);
        void Close();

        template<typename T, typename... TArguments>
        PD_FORCEINLINE void RegisterObject(TArguments&&... arguments)
        {
            T* pObject = CProxyAllocator::GetInstance()->AllocateObject<T>(arguments...);
            Register(pObject);
        }

        template<typename T, typename... TArguments>
        PD_FORCEINLINE EVoxelMaterialType RegisterCustomObject(TArguments&&... arguments)
        {
            T* pObject = CProxyAllocator::GetInstance()->AllocateObject<T>(arguments...);
            return RegisterCustom(pObject);
        }

        PD_FORCEINLINE CVoxelMaterial* GetFromType(EVoxelMaterialType type)
        {
            PD_ASSERT(static_cast<u32>(type) < m_voxelMaterials.size());
            return m_voxelMaterials[static_cast<u32>(type)];
        }
    };
}

#endif //PD_ENGINE_WORLD_TERRAIN_MATERIAL_VOXELMATERIALREGISTRY_HPP
