//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#include "VoxelMaterialClientRegistry.hpp"
#include "VoxelMaterialRegistry.hpp"
#include "VoxelMaterial.hpp"
#include "../../../Client/Renderer/Renderer.hpp"

namespace PD
{
    void CVoxelMaterialClientRegistry::OnDisposing()
    {
        m_textureContainer.Dispose(CRenderer::GetInstance()->GetDevice());
    }

    void CVoxelMaterialClientRegistry::Initialize(CVoxelMaterialRegistry& registry)
    {
        m_pRegistry = &registry;
    }

    void CVoxelMaterialClientRegistry::RegisterTextures(CVulkanDevice& device, CVulkanSampler& sampler)
    {
        m_textureContainer.Create(device, static_cast<u32>(m_pRegistry->m_voxelMaterials.size()));
        for(auto* pStaticEntity : m_pRegistry->m_voxelMaterials) pStaticEntity->RegisterTextures(m_textureContainer);

        m_textureContainer.Build(sampler);

        PD_END_INITIALIZE;
    }
}