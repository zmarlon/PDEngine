//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#include "VoxelMaterialRegistry.hpp"
#include "VoxelMaterial.hpp"

namespace PD
{
    PD_POSTDEFINE_SINGLETON(CVoxelMaterialRegistry)

    void CVoxelMaterialRegistry::OnDisposing()
    {
        DetachSingleton();
    }

    void CVoxelMaterialRegistry::Initialize(bool isClient)
    {
        AttachSingleton();

        m_isOpened = true;

        if(isClient)
        {
            //TODO: create client registry
        }

        PD_END_INITIALIZE;
    }

    void CVoxelMaterialRegistry::Register(CVoxelMaterial* pVoxelMaterial)
    {
        PD_ASSERT(m_isOpened);
        PD_ASSERT(static_cast<usize>(pVoxelMaterial->GetType()) == m_voxelMaterials.size());
        PD_ASSERT(pVoxelMaterial);

        m_voxelMaterials.push_back(pVoxelMaterial);
    }

    EVoxelMaterialType CVoxelMaterialRegistry::RegisterCustom(CVoxelMaterial* pVoxelMaterial)
    {
        PD_ASSERT(m_isOpened);
        PD_ASSERT(static_cast<u32>(pVoxelMaterial->GetType()) == -1);
        PD_ASSERT(pVoxelMaterial);

        auto newMaterialType = static_cast<EVoxelMaterialType>(m_voxelMaterials.size());
        m_voxelMaterials.push_back(pVoxelMaterial);
        return newMaterialType;
    }

    void CVoxelMaterialRegistry::Close()
    {
        PD_ASSERT(m_isOpened);
        m_isOpened = false;
    }
}