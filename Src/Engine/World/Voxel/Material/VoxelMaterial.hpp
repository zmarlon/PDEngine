//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#ifndef PD_ENGINE_WORLD_VOXEL_MATERIAL_VOXELMATERIAL_HPP
#define PD_ENGINE_WORLD_VOXEL_MATERIAL_VOXELMATERIAL_HPP

#pragma once

#include "VoxelMaterialType.hpp"
#include "../../../Core/Hints/ForceInline.hpp"

namespace PD
{
    class CBindlessTextureContainer;
    class CVoxelMaterial
    {
        friend class CVoxelMaterialRegistry;
        friend class CVoxelMaterialClientRegistry;
    private:
        EVoxelMaterialType m_type;

    protected:
        virtual void RegisterTextures(CBindlessTextureContainer& textureContainer) {}
    public:
        PD_FORCEINLINE EVoxelMaterialType GetType() const {return m_type; }
    };
}

#endif //PD_ENGINE_WORLD_VOXEL_MATERIAL_VOXELMATERIAL_HPP
