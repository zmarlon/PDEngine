//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 28.04.2020.
//
#ifndef PD_ENGINE_WORLD_VOXEL_VOXELMATERIALCLIENTREGISTRY_HPP
#define PD_ENGINE_WORLD_VOXEL_VOXELMATERIALCLIENTREGISTRY_HPP

#pragma once

#include "../../../Core/Disposable.hpp"
#include "../../../Client/Renderer/Common/BindlessTextureContainer.hpp"

namespace PD
{
    class CVoxelMaterialRegistry;
    class CVulkanDevice;
    class CVulkanSampler;
    class CVoxelMaterialClientRegistry
    {
        PD_MAKE_CLASS_DISPOSABLE(CVoxelMaterialClientRegistry)
    private:
        CVoxelMaterialRegistry* m_pRegistry;

        CBindlessTextureContainer m_textureContainer;

        void OnDisposing();
    public:
        void Initialize(CVoxelMaterialRegistry& registry);

        void RegisterTextures(CVulkanDevice& device, CVulkanSampler& sampler);
    };
}

#endif //PD_ENGINE_WORLD_VOXEL_VOXELMATERIALCLIENTREGISTRY_HPP
