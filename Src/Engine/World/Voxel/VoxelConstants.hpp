//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 19.04.2020.
//
#ifndef PDENGINE_WORLD_VOXEL_VOXELTERRAINCONSTANTS_HPP
#define PDENGINE_WORLD_VOXEL_VOXELTERRAINCONSTANTS_HPP

#pragma once

#include "../../Core/Types.hpp"

namespace PD
{
    enum EVoxelConstants : u32
    {
        MaxWorldDepth = 26,
        MaxWorldDepthHalf = MaxWorldDepth / 2,
        ChunkSizeInMeters = 8,
        ChunkVoxelSize = 16,
        NumChunkVoxels = ChunkVoxelSize * ChunkVoxelSize * ChunkVoxelSize,
        NumChunksInRegionX = 16,
        NumChunksInRegionY = 16,
        NumChunksInRegionZ = 16,
        NumChunksInRegion = NumChunksInRegionX * NumChunksInRegionY * NumChunksInRegionZ,
        ChunkToRegionShiftX = 4,
        ChunkToRegionShiftY = 4,
        ChunkToRegionShiftZ = 4
    };
}

#endif //PDENGINE_WORLD_VOXEL_VOXELTERRAINCONSTANTS_HPP
