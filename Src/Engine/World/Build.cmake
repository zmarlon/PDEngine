set(PD_ENGINE_WORLD_DIRECTORY Src/Engine/World)

include(Src/Engine/World/Chunk/Build.cmake)
include(Src/Engine/World/StaticEntity/Build.cmake)
include(Src/Engine/World/Voxel/Build.cmake)

set(PD_ENGINE_WORLD_SOURCE_FILES
        ${PD_ENGINE_WORLD_DIRECTORY}/World.cpp
        ${PD_ENGINE_WORLD_DIRECTORY}/World.hpp
        ${PD_ENGINE_WORLD_DIRECTORY}/WorldClient.cpp
        ${PD_ENGINE_WORLD_DIRECTORY}/WorldClient.hpp
        ${PD_ENGINE_WORLD_DIRECTORY}/WorldConstants.cpp
        ${PD_ENGINE_WORLD_DIRECTORY}/WorldConstants.hpp
        ${PD_ENGINE_WORLD_DIRECTORY}/WorldServer.cpp
        ${PD_ENGINE_WORLD_DIRECTORY}/WorldServer.hpp
        ${PD_ENGINE_WORLD_CHUNK_SOURCE_FILES}
        ${PD_ENGINE_WORLD_STATICENTITY_SOURCE_FILES}
        ${PD_ENGINE_WORLD_VOXEL_SOURCE_FILES})