//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//

#ifndef PD_ENGINE_WORLD_CHUNK_CHUNK_HPP
#define PD_ENGINE_WORLD_CHUNK_CHUNK_HPP

#pragma once

#include "ChunkPosition.hpp"
#include "../StaticEntity/StaticEntityInstance.hpp"
#include "../../Core/STL/Vector.hpp"
#include <fstream>

namespace PD
{
	struct SFrameEventArgs;
	class CChunk
	{
	private:
		static const u32 _CHUNK_SIZE_X = 16;
		static const u32 _CHUNK_SIZE_Z = 16;

		SChunkPosition m_position;
		STL::CVector<SStaticEntityInstance> m_staticEntities;

		usize m_lastTimestamp;

		u64 m_entitiesWhoNeedUpdate = 0;
		u64 m_entitiesToUpdatePerTick = 0;
		u64 m_entityUpdateIndex = 0;

		void OnDeserialize();
		void OnSerialize();

		void DoRandomTicks(u32 speed);
	public:
	    CChunk() = default;
		CChunk(const SChunkPosition& position) : m_position(position) {}

		void Deserialize(std::istream& binaryStream);
		void Serialize(std::ostream& binaryStream);

        PD_FORCEINLINE void AddStaticEntity(const SStaticEntityInstance& instance) { m_staticEntities.push_back(instance);}
		bool RemoveStaticEntity(const SStaticEntityInstance& instance);

		void Tick(const SFrameEventArgs& frameEventArgs);

        PD_FORCEINLINE const SChunkPosition& GetPosition() const {return m_position; }
	};
}

#endif //PD_ENGINE_WORLD_CHUNK_CHUNK_HPP
