//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 10.05.2020.
//
#ifndef PD_ENGINE_WORLD_CHUNK_CHUNKREGIONCACHE_HPP
#define PD_ENGINE_WORLD_CHUNK_CHUNKREGIONCACHE_HPP

#pragma once

#include "../../Core/Disposable.hpp"
#include "../../Core/STL/UnorderedMap.hpp"
#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>

namespace PD
{
    class CWorldServer;
    class CChunk;
    class CChunkRegionFile;
    class CChunkRegionCache
    {
        PD_MAKE_CLASS_DISPOSABLE(CChunkRegionCache)
    private:
        STL::CUnorderedMap<glm::ivec2, CChunkRegionFile*> m_regions;
        CWorldServer* m_pWorld;

        void OnDisposing();
    public:
        void Create(CWorldServer& world);

        CChunkRegionFile& GetOrLoadFile(const glm::ivec2& regionPosition);
        bool LoadChunk(CChunk& chunk);
        bool SaveChunk(CChunk& chunk);

        PD_FORCEINLINE CWorldServer& GetWorld() { return *m_pWorld; }
    };
}

#endif //PD_ENGINE_WORLD_CHUNK_CHUNKREGIONCACHE_HPP
