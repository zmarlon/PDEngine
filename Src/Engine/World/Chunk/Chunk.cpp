//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//

#include "Chunk.hpp"
#include "../../Core/Time/SystemTime.hpp"
#include "../../Core/Time/FrameEventArgs.hpp"
#include "../StaticEntity/StaticEntityRegistry.hpp"
#include "../StaticEntity/StaticEntity.hpp"
#include <algorithm>

namespace PD
{
	bool CChunk::RemoveStaticEntity(const SStaticEntityInstance& instance)
	{
		auto iterator = std::find(m_staticEntities.begin(), m_staticEntities.end(), instance);
		if(iterator != m_staticEntities.end())
		{
			m_staticEntities.erase(iterator);
			return true;
		}
		
		return false;
	}

	void CChunk::OnDeserialize()
	{

	}

	void CChunk::OnSerialize()
	{
		m_lastTimestamp = SystemTime::GetCurrentTimeMilliseconds();
	}

	void CChunk::DoRandomTicks(u32 speed)
	{
#define PD_MAX_ENTITIES_FOR_NORMAL_TICK 65536
		for (u32 ix = 0; ix < speed; ix++)
		{
			PD_ASSERT(m_staticEntities.size() < PD_MAX_ENTITIES_FOR_NORMAL_TICK); //TODO: anders ist es sauber, sodass die Chance für einen Tick gleich ist
			//nicht möglich. Über Zahl kann noch diskutiert werden, es muss dann allerdings auch die Variable m_randomTickSpeed proportional angepasst werden
			u32 randomValue; //TODO: es muss eine möglichkeit geben an einen random dran zu kommen
			SStaticEntityInstance& instance = m_staticEntities[randomValue];
			CStaticEntity* pEntity = CStaticEntityRegistry::GetInstance()->GetFromType(instance.GetType());
			pEntity->Tick(instance);
		}
#undef PD_MAX_ENTITIES_FOR_NORMAL_TICK
	}

	void CChunk::Deserialize(std::istream& binaryStream)
	{
		usize staticEntitySize;
		binaryStream >> m_lastTimestamp >> staticEntitySize;

		m_staticEntities.resize(staticEntitySize);
		binaryStream.read(reinterpret_cast<char*>(m_staticEntities.data()), sizeof(SStaticEntityInstance) * staticEntitySize);

		OnDeserialize();
	}

	void CChunk::Serialize(std::ostream& binaryStream)
	{
		OnSerialize();

		binaryStream << m_lastTimestamp << m_staticEntities.size();
		binaryStream.write(reinterpret_cast<const char*>(m_staticEntities.data()), sizeof(SStaticEntityInstance) * m_staticEntities.size());
	}

	void CChunk::Tick(const SFrameEventArgs& frameEventArgs)
	{
		DoRandomTicks(1); //TODO: make variable
	}
}