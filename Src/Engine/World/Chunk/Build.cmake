set(PD_ENGINE_WORLD_CHUNK_DIRECTORY Src/Engine/World/Chunk)

set(PD_ENGINE_WORLD_CHUNK_SOURCE_FILES
        ${PD_ENGINE_WORLD_CHUNK_DIRECTORY}/Chunk.cpp
        ${PD_ENGINE_WORLD_CHUNK_DIRECTORY}/Chunk.hpp
        ${PD_ENGINE_WORLD_CHUNK_DIRECTORY}/ChunkPosition.hpp
        ${PD_ENGINE_WORLD_CHUNK_DIRECTORY}/ChunkRegionCache.cpp
        ${PD_ENGINE_WORLD_CHUNK_DIRECTORY}/ChunkRegionCache.hpp
        ${PD_ENGINE_WORLD_CHUNK_DIRECTORY}/ChunkRegionFile.cpp
        ${PD_ENGINE_WORLD_CHUNK_DIRECTORY}/ChunkRegionFile.hpp)