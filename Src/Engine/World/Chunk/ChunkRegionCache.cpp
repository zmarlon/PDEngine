//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 10.05.2020.
//
#include "ChunkRegionCache.hpp"
#include "ChunkRegionFile.hpp"
#include "Chunk.hpp"
#include "../WorldConstants.hpp"

namespace PD
{
    void CChunkRegionCache::OnDisposing()
    {
        auto* pAllocator = CProxyAllocator::GetInstance();
        for(auto& iterator : m_regions) pAllocator->DeallocateObject(iterator.second); //TODO: Pool Allocator
        m_regions.clear();
    }

    void CChunkRegionCache::Create(CWorldServer& world)
    {
        m_pWorld = &world;

        PD_END_INITIALIZE;
    }

    CChunkRegionFile& CChunkRegionCache::GetOrLoadFile(const glm::ivec2& regionPosition)
    {
        auto iterator = m_regions.find(regionPosition);
        if(iterator != m_regions.end()) return *iterator->second;

        auto* pRegionFile = CProxyAllocator::GetInstance()->AllocateObject<CChunkRegionFile>(); //TODO: Pool Allocator
        pRegionFile->Load(*this, regionPosition);
        m_regions[regionPosition] = pRegionFile;
        return *pRegionFile;
    }

    bool CChunkRegionCache::LoadChunk(CChunk& chunk)
    {
        const auto& position = chunk.GetPosition();
        glm::ivec2 regionPosition(position.X >> EWorldConstants::ChunkToRegionShiftX, position.Z >> EWorldConstants::ChunkToRegionShiftZ);
        return GetOrLoadFile(regionPosition).LoadChunk(chunk);
    }

    bool CChunkRegionCache::SaveChunk(CChunk& chunk)
    {
        const auto& position = chunk.GetPosition();
        glm::ivec2 regionPosition(position.X >> EWorldConstants::ChunkToRegionShiftX, position.Z >> EWorldConstants::ChunkToRegionShiftZ);
        return GetOrLoadFile(regionPosition).SaveChunk(chunk);
    }
}