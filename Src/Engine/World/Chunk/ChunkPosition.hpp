//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 23.04.2020.
//

#ifndef PD_ENGINE_WORLD_CHUNK_CHUNKPOSITION_HPP
#define PD_ENGINE_WORLD_CHUNK_CHUNKPOSITION_HPP

#pragma once

#include "../../Core/Hints/ForceInline.hpp"
#include "../../Core/Types.hpp"
#include <functional>

namespace PD
{
	struct SChunkPosition
	{
		i32 X, Z;

		SChunkPosition() : X(0), Z(0) {}
		SChunkPosition(i32 x, i32 z) : X(x), Z(z) {}

		PD_FORCEINLINE bool operator ==(const SChunkPosition& other) const
		{
			return X == other.X && Z == other.Z;
		}

		PD_FORCEINLINE bool operator !=(const SChunkPosition& other) const
		{
			return X != other.X || Z != other.Z;
		}
	};
}

namespace std
{
	using namespace PD;

	template<>
	struct hash<SChunkPosition>
	{
		PD_FORCEINLINE usize operator()(const SChunkPosition& position) const
		{
			return std::hash<i32>()(position.X) ^ std::hash<i32>()(position.Z);
		}
	};
}

#endif //PD_ENGINE_WORLD_CHUNK_CHUNKPOSITION_HPP
