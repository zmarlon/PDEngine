//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 10.05.2020.
//
#ifndef PD_ENGINE_WORLD_CHUNK_CHUNKREGIONFILE_HPP
#define PD_ENGINE_WORLD_CHUNK_CHUNKREGIONFILE_HPP

#pragma once

#include <glm/glm.hpp>

namespace PD
{
    class CChunkRegionCache;
    class CChunk;
    class CChunkRegionFile
    {
    public:
        void Load(CChunkRegionCache& regionCache, const glm::ivec2& regionPosition);

        bool LoadChunk(CChunk& chunk);
        bool SaveChunk(CChunk& chunk);
    };
}

#endif //PD_ENGINE_WORLD_CHUNK_CHUNKREGIONFILE_HPP
