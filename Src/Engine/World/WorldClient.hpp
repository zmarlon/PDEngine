//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 16.04.2020.
//
#ifndef PDENGINE_WORLDCLIENT_HPP
#define PDENGINE_WORLDCLIENT_HPP

#pragma once

#include "World.hpp"

namespace PD
{
    class CWorldClient : public CWorld
    {
        PD_MAKE_CLASS_SINGLETON(CWorldClient);
    private:
        void OnDisposing();
    public:
        void Initialize();

        bool IsRemote() const final {return true; }
    };
}

#endif //PDENGINE_WORLDCLIENT_HPP
