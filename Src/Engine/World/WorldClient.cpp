//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 16.04.2020.
//
#include "WorldClient.hpp"

namespace PD
{
    PD_POSTDEFINE_SINGLETON(CWorldClient)
    void CWorldClient::OnDisposing()
    {
        DetachSingleton();
    }

    void CWorldClient::Initialize()
    {
        AttachSingleton();

        PD_END_INITIALIZE;
    }
}