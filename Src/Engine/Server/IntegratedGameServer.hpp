//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#ifndef PD_ENGINE_SERVER_INTEGRATEDGAMESERVER_HPP
#define PD_ENGINE_SERVER_INTEGRATEDGAMESERVER_HPP

#pragma once

#include "GameServer.hpp"

namespace PD
{
    class CGame;
    class CIntegratedGameServer final : public CGameServer
    {
    private:
        CGame* m_pClient;

        //CSharp-Functions
        void(*m_pfn_IntegratedGameServer_CreateInstance)(void*);

        void CreateManagedDelegates(CCommonLanguageRuntime* pCLRuntime) override;

        void OnDisposing() override;
    public:
        void Initialize();

        bool IsDedicated() const final {return false; }
        const SGameInfo& GetGameInfo() const final;
    };
}

#endif //PD_ENGINE_SERVER_INTEGRATEDGAMESERVER_HPP
