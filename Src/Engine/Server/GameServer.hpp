//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#ifndef PD_ENGINE_SERVER_GAMESERVER_HPP
#define PD_ENGINE_SERVER_GAMESERVER_HPP

#include "../Core/Disposable.hpp"
#include "../Core/Singleton.hpp"
#include "../GameInfo.hpp"
#include "../Core/Time/Timer.hpp"
#include <thread>

namespace PD
{
    class CWorldServer;
    class CCommonLanguageRuntime;
    class CGameServer
    {
        PD_MAKE_CLASS_DISPOSABLE(CGameServer)
        PD_MAKE_INTERFACE_SINGLETON(CGameServer)
    public:
        struct SData
        {
            bool ServerRunning;
            const wchar_t* pWorldName;
            const wchar_t* pWorldDirectory;
        };
    protected:
        CTimer m_loopTimer;
        std::thread m_serverThread;
        SData m_data;
        CWorldServer* m_pWorld;

        //CSharp-Functions
        void(*m_pfn_GameServer_PreInit)();
        void(*m_pfn_GameServer_Init)();
        void(*m_pfn_GameServer_PostInit)();
        void(*m_pfn_GameServer_TickInstance)(float, float);
        void(*m_pfn_GameServer_DisposeInstance)();

        virtual void CreateManagedDelegates(CCommonLanguageRuntime* pCLRuntime);

        virtual void PreInit();
        virtual void Init();
        virtual void PostInit();

        void RunServerLoop();
        void Tick(const SFrameEventArgs& frameEventArgs);

        virtual void OnDisposing();
    public:
        virtual bool IsDedicated() const = 0;
        virtual const SGameInfo& GetGameInfo() const = 0;

        PD_FORCEINLINE SData& GetData() {return m_data; }
        PD_FORCEINLINE const SData& GetData() const {return m_data; }

        PD_FORCEINLINE bool IsRunning() const {return m_data.ServerRunning; }
    };
}

#endif //PD_ENGINE_SERVER_GAMESERVER_HPP
