//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#ifndef PD_ENGINE_SERVER_DEDICATEDGAMESERVER_HPP
#define PD_ENGINE_SERVER_DEDICATEDGAMESERVER_HPP

#pragma once

#include "GameServer.hpp"

namespace PD
{
    class CProxyAllocator;
    class CLogger;
    class CPakManager;
    class CCommonLanguageRuntime;
    class CDedicatedGameServer final : public CGameServer
    {
    private:
        SGameInfo m_gameInfo = SGameInfo("WingertVoxel", 0, 0, 1);
        CProxyAllocator* m_pProxyAllocator = nullptr;
        CLogger* m_pLogger = nullptr;
        CPakManager* m_pPakManager = nullptr;
        CCommonLanguageRuntime* m_pCLRuntime = nullptr;

        //CSharp-Functions
        void(*m_pfn_DedicatedGameServer_CreateInstance)(void*);
        void(*m_pfn_DedicatedGameServer_HandleConsoleInput)();

        inline void CreateLogger();
        inline void DestroyLogger();
        void CreateManagedDelegates(CCommonLanguageRuntime* pCLRuntime) override;

        void OnDisposing() override;
    public:
        void Initialize();

        bool IsDedicated() const final {return true;}
        const SGameInfo& GetGameInfo() const final;
    };
}

#endif //PD_ENGINE_SERVER_DEDICATEDGAMESERVER_HPP
