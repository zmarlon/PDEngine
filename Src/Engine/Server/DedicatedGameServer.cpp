//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#include "DedicatedGameServer.hpp"
#include "../Core/Logging/Logger.hpp"
#include "../Core/Logging/Layout/BasicLayout.hpp"
#include "../Core/Logging/Layout/SimpleLayout.hpp"
#include "../Core/Logging/Appender/OstreamAppender.hpp"
#include "../Core/Logging/Appender/MessageBoxAppender.hpp"
#include "../Core/Logging/Appender/FileAppender.hpp"
#include "../Core/Threading/ThreadExtensions.hpp"
#include "../Core/Pak/PakManager.hpp"
#include "../Core/Managed/CommonLanguageRuntime.hpp"

#include <iostream>

namespace PD
{
    void CDedicatedGameServer::OnDisposing()
    {
        CGameServer::OnDisposing();

        DetachSingleton();

        m_pPakManager->Dispose();
        delete m_pPakManager;

        m_pCLRuntime->Dispose();
        delete m_pCLRuntime;

        DestroyLogger();
    }

    void CDedicatedGameServer::CreateLogger()
    {
        m_pLogger = new CLogger;
        m_pLogger->Initialize();

        auto basicLayout = std::make_shared<CBasicLayout>(L"%Y-%m-%d %X");
        auto simpleLayout = std::make_shared<CSimpleLayout>();

        auto consoleAppender = std::make_shared<COstreamAppender>(L"console", &std::wcout);
        consoleAppender->SetLayout(basicLayout);
        m_pLogger->AddAppender(consoleAppender);

        auto messageBoxAppender = std::make_shared<CMessageBoxAppender>(L"message_box");
        messageBoxAppender->SetLayout(simpleLayout);
        messageBoxAppender->SetMinLevel(ELogLevel::Error);
        m_pLogger->AddAppender(messageBoxAppender);

        auto fileAppender = std::make_shared<CFileAppender>(L"console", "PDEngine_Log_", "log", true, L"%Y-%m-%d_%H-%M-%S");
        fileAppender->SetLayout(basicLayout);
        m_pLogger->AddAppender(fileAppender);
    }

    void CDedicatedGameServer::DestroyLogger()
    {
        m_pLogger->Dispose();
        delete m_pLogger;
    }

    void CDedicatedGameServer::CreateManagedDelegates(CCommonLanguageRuntime* pCLRuntime)
    {
        m_pfn_DedicatedGameServer_CreateInstance = pCLRuntime->GetDelegate<void(void*)>("PDEngine.Managed.Server.DedicatedGameServer", "NCreateInstance");
        m_pfn_DedicatedGameServer_HandleConsoleInput = pCLRuntime->GetDelegate<void()>("PDEngine.Managed.Server.DedicatedGameServer", "NHandleConsoleInput");

        CGameServer::CreateManagedDelegates(pCLRuntime);
    }

    void CDedicatedGameServer::Initialize()
    {
        AttachSingleton();

        ThreadExtensions::SetCurrentThreadName(L"MAIN");

        m_pProxyAllocator = new CProxyAllocator;
        m_pProxyAllocator->Initialize();

        CreateLogger();

        m_pCLRuntime = new CCommonLanguageRuntime;
        m_pCLRuntime->Create();

        m_pPakManager = new CPakManager;
        m_pPakManager->Create();

        CreateManagedDelegates(m_pCLRuntime);
        m_pfn_DedicatedGameServer_CreateInstance(this);

        PreInit();
        Init();
        PostInit();

        m_serverThread = std::thread([this](){RunServerLoop();});
        m_serverThread.detach();

        m_pfn_DedicatedGameServer_HandleConsoleInput();

        PD_END_INITIALIZE;
    }

    const SGameInfo& CDedicatedGameServer::GetGameInfo() const
    {
        return m_gameInfo;
    }
}