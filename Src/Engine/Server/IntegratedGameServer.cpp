//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#include "IntegratedGameServer.hpp"
#include "../Game.hpp"
#include "../Core/Managed/CommonLanguageRuntime.hpp"

namespace PD
{
    void CIntegratedGameServer::OnDisposing()
    {
        CGameServer::OnDisposing();
    }

    void CIntegratedGameServer::CreateManagedDelegates(CCommonLanguageRuntime* pCLRuntime)
    {
        m_pfn_IntegratedGameServer_CreateInstance = pCLRuntime->GetDelegate<void(void*)>("PDEngine.Managed.Server.IntegratedGameServer", "CreateInstance");

        CGameServer::CreateManagedDelegates(pCLRuntime);
    }

    void CIntegratedGameServer::Initialize()
    {
        auto* pCLRuntime = CCommonLanguageRuntime::GetInstance();
        CreateManagedDelegates(pCLRuntime);
        m_pfn_IntegratedGameServer_CreateInstance(this);

        PD_END_INITIALIZE;
    }

    const SGameInfo& CIntegratedGameServer::GetGameInfo() const
    {
        return m_pClient->GetGameInfo();
    }
}