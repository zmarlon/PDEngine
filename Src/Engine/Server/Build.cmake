set(PD_ENGINE_SERVER_DIRECTORY Src/Engine/Server)

set(PD_ENGINE_SERVER_SOURCE_FILES
        ${PD_ENGINE_SERVER_DIRECTORY}/DedicatedGameServer.cpp
        ${PD_ENGINE_SERVER_DIRECTORY}/DedicatedGameServer.hpp
        ${PD_ENGINE_SERVER_DIRECTORY}/GameServer.cpp
        ${PD_ENGINE_SERVER_DIRECTORY}/GameServer.hpp
        ${PD_ENGINE_SERVER_DIRECTORY}/IntegratedGameServer.cpp
        ${PD_ENGINE_SERVER_DIRECTORY}/IntegratedGameServer.hpp)