//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 07.04.2020.
//
#include "GameServer.hpp"
#include "../Core/Managed/CommonLanguageRuntime.hpp"
#include "../Core/Threading/ThreadExtensions.hpp"
#include "../World/WorldServer.hpp"

namespace PD
{
    PD_POSTDEFINE_SINGLETON(CGameServer)
    void CGameServer::OnDisposing()
    {
        m_pWorld->Dispose();
        delete m_pWorld;

        m_pfn_GameServer_DisposeInstance();
    }

	void CGameServer::Tick(const SFrameEventArgs& frameEventArgs)
	{
        m_pfn_GameServer_TickInstance(frameEventArgs.DeltaTime, frameEventArgs.ElapsedTime);
	}

	void CGameServer::CreateManagedDelegates(CCommonLanguageRuntime* pCLRuntime)
    {
        m_pfn_GameServer_PreInit = pCLRuntime->GetDelegate<void()>("PDEngine.Managed.Server.GameServer", "NPreInit");
        m_pfn_GameServer_Init = pCLRuntime->GetDelegate<void()>("PDEngine.Managed.Server.GameServer", "NInit");
        m_pfn_GameServer_PostInit = pCLRuntime->GetDelegate<void()>("PDEngine.Managed.Server.GameServer", "NPostInit");

        m_pfn_GameServer_TickInstance = pCLRuntime->GetDelegate<void(float, float)>("PDEngine.Managed.Server.GameServer", "NTickInstance");
        m_pfn_GameServer_DisposeInstance = pCLRuntime->GetDelegate<void()>("PDEngine.Managed.Server.GameServer", "NDisposeInstance");
    }

    void CGameServer::PreInit()
    {
        m_pfn_GameServer_PreInit();
    }

    void CGameServer::Init()
    {
        m_pWorld = new CWorldServer;
        m_pWorld->Create(m_data.pWorldName, m_data.pWorldDirectory);

        m_pfn_GameServer_Init();
    }

    void CGameServer::PostInit()
    {
        m_pfn_GameServer_PostInit();
    }

    void CGameServer::RunServerLoop()
    {
        m_data.ServerRunning = true;
        ThreadExtensions::SetCurrentThreadName(L"SERVER");

        m_loopTimer.SetFrameLimit(30);

        while(m_data.ServerRunning)
        {
            m_loopTimer.Tick([this](const SFrameEventArgs& frameEventArgs)
            {
                Tick(frameEventArgs);
            });
        }
    }
}
