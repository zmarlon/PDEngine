#version 460
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 positionVS;
layout(location = 1) in vec2 texCoordVS;
layout(location = 2) in vec3 normalVS;
layout(location = 3) in vec3 tangentVS;
layout(location = 4) in vec3 bitangentVS;

layout(location = 0) out vec2 texCoordFS;
layout(location = 1) out vec3 normalFS;
layout(location = 2) out vec3 tangentFS;
layout(location = 3) out vec3 bitangentFS;

layout(set = 0, binding = 0) uniform UBO
{
    mat4 viewProjectionMatrix;
} ubo; //TODO: merge to camera constants thing in future

layout(push_constant) uniform PushConstants
{
    mat4 modelMatrix;
} constants;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    texCoordFS = texCoordVS;
    normalFS = normalVS;
    tangentFS = tangentVS;
    bitangentFS = bitangentVS;
    gl_Position = ubo.viewProjectionMatrix * constants.modelMatrix * vec4(positionVS, 1.0);
}
