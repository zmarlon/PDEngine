#version 460
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec4 colorFS;
layout(location = 1) in vec2 texCoordFS;

layout(set = 0, binding = 0) uniform sampler samplerState;
layout(set = 1, binding = 0) uniform texture2D tex;

layout(location = 0) out vec4 outColor;

void main()
{
    outColor = colorFS * texture(sampler2D(tex, samplerState), texCoordFS);
}
