#version 460
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 positionVS;
layout(location = 1) in vec4 colorVS;
layout(location = 2) in vec2 texCoordVS;

layout(set = 0, binding = 1) uniform UBO
{
    mat4 transformationMatrix;
};

out gl_PerVertex
{
    vec4 gl_Position;
};

layout(location = 0) out vec4 colorFS;
layout(location = 1) out vec2 texCoordFS;

void main()
{
    gl_Position = transformationMatrix * vec4(positionVS, 1.0);
    colorFS = colorVS;
    texCoordFS = texCoordVS;
}
