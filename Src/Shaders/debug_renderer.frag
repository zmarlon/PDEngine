#version 460
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 texCoordFS;
layout(location = 1) in vec3 normalFS;
layout(location = 2) in vec3 tangentFS;
layout(location = 3) in vec3 bitangentFS;

layout(location = 0) out vec4 colorAlbedo;
layout(location = 1) out vec4 colorNormal;
layout(location = 2) out vec4 colorSpecular;

void main()
{
    vec3 N = normalize(normalFS);
    vec3 L = normalize(vec3(3.0, 0.0, 1.0));

    float NDotL = max(dot(N, L), 0.07);

    colorAlbedo = vec4(vec3(0.1, 1.0, 0.1) * NDotL, 1.0);
    colorNormal = vec4(normalFS * 0.5 + 0.5, 1.0);
    colorSpecular = vec4(1.0, 1.0, 1.0, 1.0);
}
