//
// This file is part of PDEngine and was created by BeastLe9end(instagram.com/beastle9end) and zMarLoNYT(instagram.com/zmarlonyt) on 03.04.2020.
//

#include "Engine/GameLauncher.hpp"

int main(int argc, char** ppArgs)
{
	PD::GameLauncher::Launch(argc, ppArgs);
}