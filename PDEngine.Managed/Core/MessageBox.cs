﻿using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.Core
{
    public unsafe class MessageBox
    {
        public static void Show(Window window, string message, string caption, MessageBoxStyle style)
        {
            fixed(char* pMessage = message, pCaption = caption)
            {
                NMessageBox_ShowWithWindow(window.Handle, pMessage, pCaption, style);
            }
        }
        
        public static void Show(string message, string caption, MessageBoxStyle style)
        {
            fixed (char* pMessage = message, pCaption = caption)
            {
                NMessageBox_Show(pMessage, pCaption, style);
            }
        }
        
        #region NATIVE_METHODS
        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern void NMessageBox_ShowWithWindow(void* pWindow, char* pMessage, char* pCaption, MessageBoxStyle style);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern void NMessageBox_Show(char* pMessage, char* pCaption, MessageBoxStyle style);
        #endregion
    }

}