﻿namespace PDEngine.Managed.Core
{
    public enum MessageBoxStyle : uint
    {
        Information = 0,
        Warning = 1,
        Error = 2
    }
}