﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace PDEngine.Managed.Core.Math
{
    [StructLayout(LayoutKind.Sequential)]
    public struct RectangleF
    {
        public float X, Y;
        public float Width, Height;

        public static RectangleF Empty
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get;
        }

        public RectangleF(float x, float y, float width, float height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public RectangleF(PointF position, PointF size)
        {
            X = position.X;
            Y = position.Y;
            Width = size.X;
            Height = size.Y;
        }

        public PointF Position
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => new PointF(X, Y);
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        public PointF Size
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => new PointF(Width, Height);
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                Width = value.X;
                Height = value.Y;
            }
        }

        public float Left
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => X;
        }

        public float Right
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => X + Width;
        }

        public float Top
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => Y;
        }

        public float Bottom
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => Y + Height;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(PointF point)
        {
            return point.X >= X && point.Y >= Y && point.X <= X + Width && point.Y <= Y + Height;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Intersects(RectangleF rect)
        {
            return rect.X < X + Width && X < rect.X + rect.Width && rect.Y < Y + Height && Y < rect.Y + rect.Height;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Intersects(ref RectangleF rect)
        {
            return rect.X < X + Width && X < rect.X + rect.Width && rect.Y < Y + Height && Y < rect.Y + rect.Height;
        }

        public bool this[PointF point]
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => point.X >= X && point.Y >= Y && point.X <= X + Width && point.Y <= Y + Height;
        }

        public bool Equals(RectangleF other)
        {
            return X.Equals(other.X) && Y.Equals(other.Y) && Width.Equals(other.Width) && Height.Equals(other.Height);
        }

        public override bool Equals(object obj)
        {
            return obj is RectangleF other && Equals(other);
        }

        public RectangleI ToIntRect()
        {
            return new RectangleI((int)X, (int)Y, (int)Width, (int)Height);
        }

        public override int GetHashCode()
        {
            var hashCode = X.GetHashCode();
            hashCode = (hashCode * 397) ^ Y.GetHashCode();
            hashCode = (hashCode * 397) ^ Width.GetHashCode();
            hashCode = (hashCode * 397) ^ Height.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return $"X:{X} Y:{Y} Width:{Width} Height:{Height}";
        }
    }
}
