﻿using System;

namespace PDEngine.Managed.Core.Math
{
    public struct Color4F : IEquatable<Color4F>
    {
        public float R, G, B, A;

        public static readonly Color4F White = new Color4F(1.0f);
        public static readonly Color4F Black = new Color4F(0.0f, 0.0f, 0.0f);
        public static readonly Color4F Red = new Color4F(1.0f, 0.0f, 0.0f);
        public static readonly Color4F Green = new Color4F(0.0f, 1.0f, 0.0f);
        public static readonly Color4F Blue = new Color4F(0.0f, 0.0f, 1.0f);

        public Color4F(int packedValue)
        {
            R = ((packedValue >> 24) & 0xFF) / 255.0F;
            G = ((packedValue >> 16) & 0xFF) / 255.0F;
            B = ((packedValue >> 8) & 0xFF) / 255.0F;
            A = (packedValue & 0xFF) / 255.0F;
        }

        public Color4F(float value)
        {
            R = value;
            G = value;
            B = value;
            A = value;
        }

        public Color4F(float r, float g, float b, float a = 1.0f)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public Color4F WithAlpha(float alpha) => new Color4F(R, G, B, alpha);
        public Color4F MultiplyAlpha(float alphaMultiplier) => new Color4F(R, G, B, A * alphaMultiplier);

        public bool Equals(Color4F other)
        {
            return R.Equals(other.R) && G.Equals(other.G) && B.Equals(other.B) && A.Equals(other.A);
        }

        public override bool Equals(object obj)
        {
            return obj is Color4F other && Equals(other);
        }

        public override int GetHashCode()
        {
            var hashCode = R.GetHashCode();
            hashCode = (hashCode * 397) ^ G.GetHashCode();
            hashCode = (hashCode * 397) ^ B.GetHashCode();
            hashCode = (hashCode * 397) ^ A.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return $"R:{R} G:{G} B:{B} A:{A}";
        }
    }
}
