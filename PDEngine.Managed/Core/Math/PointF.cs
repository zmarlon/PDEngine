﻿using System;
using System.Runtime.InteropServices;

namespace PDEngine.Managed.Core.Math
{
    [StructLayout(LayoutKind.Sequential)]
    public struct PointF : IEquatable<PointF>
    {
        public float X;
        public float Y;

        public PointF(float x, float y)
        {
            X = x;
            Y = y;
        }

        public static PointF operator +(PointF point1, PointF point2)
        {
            return new PointF(point1.X + point2.X, point1.Y + point2.Y);
        }

        public static PointF operator -(PointF point1, PointF point2)
        {
            return new PointF(point1.X - point2.X, point1.Y - point2.Y);
        }

        public static PointF operator *(PointF point, float factor)
        {
            return new PointF(factor * point.X, factor * point.Y);
        }

        public static PointF operator /(PointF point, float factor)
        {
            return new PointF(point.X / factor, point.Y / factor);
        }

        public bool Equals(PointF other)
        {
            return X.Equals(other.X) && Y.Equals(other.Y);
        }

        public static bool operator ==(PointF p1, PointF p2)
        {
            return p1.X.Equals(p2.X) && p1.Y.Equals(p2.Y);
        }

        public static bool operator !=(PointF p1, PointF p2)
        {
            return !p1.X.Equals(p2.X) || !p1.Y.Equals(p2.Y);
        }

        public override bool Equals(object obj)
        {
            return obj is PointF other && Equals(other);
        }

        public override int GetHashCode()
        {
            return (X.GetHashCode() * 397) ^ Y.GetHashCode();
        }
        
        public override string ToString()
        {
            return $"X:{X} Y:{Y}";
        }
    }
}