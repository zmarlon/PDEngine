﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace PDEngine.Managed.Core.Math
{
    [StructLayout(LayoutKind.Sequential)]
    public struct RectangleI : IEquatable<RectangleI>
    {
        public int X, Y;
        public int Width, Height;

        public static RectangleI Empty
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get;
        }

        public RectangleI(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public PointI Position
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => new PointI(X, Y);
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        public PointI Size
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => new PointI(Width, Height);
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                Width = value.X;
                Height = value.Y;
            }
        }

        public int Left
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => X;
        }

        public int Right
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => X + Width;
        }

        public int Top
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => Y;
        }

        public int Bottom
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => Y + Height;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(PointI point)
        {
            return point.X >= X && point.Y >= Y && point.X <= X + Width && point.Y <= Y + Height;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Intersects(RectangleI rect)
        {
            return rect.X < X + Width && X < rect.X + rect.Width && rect.Y < Y + Height && Y < rect.Y + rect.Height;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Intersects(ref RectangleI rect)
        {
            return rect.X < X + Width && X < rect.X + rect.Width && rect.Y < Y + Height && Y < rect.Y + rect.Height;
        }

        public bool this[PointI point]
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => point.X >= X && point.Y >= Y && point.X <= X + Width && point.Y <= Y + Height;
        }

        public bool Equals(RectangleI other)
        {
            return X == other.X && Y == other.Y && Width == other.Width && Height == other.Height;
        }

        public override bool Equals(object obj)
        {
            return obj is RectangleI other && Equals(other);
        }

        public RectangleF ToFloatRect()
        {
            return new RectangleF(X, Y, Width, Height);
        }
        
        public override int GetHashCode()
        {
            var hashCode = X;
            hashCode = (hashCode * 397) ^ Y;
            hashCode = (hashCode * 397) ^ Width;
            hashCode = (hashCode * 397) ^ Height;
            return hashCode;
        }
        
        public override string ToString()
        {
            return $"X:{X} Y:{Y} Width:{Width} Height:{Height}";
        }
    }
}
