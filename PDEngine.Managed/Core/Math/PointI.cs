﻿using System;
using System.Runtime.InteropServices;

namespace PDEngine.Managed.Core.Math
{
    [StructLayout(LayoutKind.Sequential)]
    public struct PointI : IEquatable<PointI>
    {
        public int X, Y;

        public PointI(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static PointI operator +(PointI point1, PointI point2)
        {
            return new PointI(point1.X + point2.X, point1.Y + point2.Y);
        }

        public static PointI operator -(PointI point1, PointI point2)
        {
            return new PointI(point1.X - point2.X, point1.Y - point2.Y);
        }

        public static PointI operator *(PointI point, int factor)
        {
            return new PointI(factor * point.X, factor * point.Y);
        }

        public static PointI operator /(PointI point, int factor)
        {
            return new PointI(point.X / factor, point.Y / factor);
        }

        public bool Equals(PointI other)
        {
            return X == other.X && Y == other.Y;
        }

        public static bool operator ==(PointI p1, PointI p2)
        {
            return p1.X == p2.X && p1.Y == p2.Y;
        }

        public static bool operator !=(PointI p1, PointI p2)
        {
            return p1.X != p2.X || p1.Y != p2.Y;
        }

        public override bool Equals(object obj)
        {
            return obj is PointI other && Equals(other);
        }

        public override int GetHashCode()
        {
            return (X * 397) ^ Y;
        }
        
        public override string ToString()
        {
            return $"X:{X} Y:{Y}";
        }
    }
}