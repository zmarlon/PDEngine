﻿using System;
using System.Runtime.InteropServices;

namespace PDEngine.Managed.Core.Math
{
    [StructLayout(LayoutKind.Sequential)]
    public struct PointUI : IEquatable<PointUI>
    {
        public uint X;
        public uint Y;

        public PointUI(uint x, uint y)
        {
            X = x;
            Y = y;
        }

        public static PointUI operator +(PointUI point1, PointUI point2)
        {
            return new PointUI(point1.X + point2.X, point1.Y + point2.Y);
        }

        public static PointUI operator -(PointUI point1, PointUI point2)
        {
            return new PointUI(point1.X - point2.X, point1.Y - point2.Y);
        }

        public static PointUI operator *(PointUI point, uint factor)
        {
            return new PointUI(factor * point.X, factor * point.Y);
        }

        public static PointUI operator /(PointUI point, uint factor)
        {
            return new PointUI(point.X / factor, point.Y / factor);
        }

        public bool Equals(PointUI other)
        {
            return X == other.X && Y == other.Y;
        }

        public static bool operator ==(PointUI p1, PointUI p2)
        {
            return p1.X == p2.X && p1.Y == p2.Y;
        }

        public static bool operator !=(PointUI p1, PointUI p2)
        {
            return p1.X != p2.X || p1.Y != p2.Y;
        }

        public override bool Equals(object obj)
        {
            return obj is PointUI other && Equals(other);
        }

        public override int GetHashCode()
        {
            return ((int) X * 397) ^ (int) Y;
        }
        
        public override string ToString()
        {
            return $"X:{X} Y:{Y}";
        }
    }
}