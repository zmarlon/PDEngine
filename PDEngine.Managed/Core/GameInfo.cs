﻿using System;

namespace PDEngine.Managed.Core
{
    public struct GameInfo : IEquatable<GameInfo>
    {
        public string Name;
        public uint Major;
        public uint Minor;
        public uint Patch;

        public GameInfo(string name, uint major, uint minor, uint patch)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Major = major;
            Minor = minor;
            Patch = patch;
        }

        public bool Equals(GameInfo other)
        {
            return Name == other.Name && Major == other.Major && Minor == other.Minor && Patch == other.Patch;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is GameInfo gameInfo) return Equals(gameInfo);
            return false;
        }

        public static bool operator ==(GameInfo gameInfo1, GameInfo gameInfo2)
        {
            return gameInfo1.Name == gameInfo2.Name && gameInfo1.Major == gameInfo2.Major &&
                   gameInfo1.Minor == gameInfo2.Minor && gameInfo1.Patch == gameInfo2.Patch;
        }

        public static bool operator !=(GameInfo gameInfo1, GameInfo gameInfo2)
        {
            return gameInfo1.Name != gameInfo2.Name || gameInfo1.Major != gameInfo2.Major || gameInfo1.Minor !=
                gameInfo2.Minor || gameInfo1.Patch != gameInfo2.Patch;
        }
        
        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Major, Minor, Patch);
        }
    }
}