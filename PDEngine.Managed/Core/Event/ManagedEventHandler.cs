﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Core.Input;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.Core.Event
{
    public delegate void HandlerWindowResize(uint newWidth, uint newHeight);

    public delegate void HandlerWindowMove(int newX, int newY);

    public delegate void HandlerWindowMinimize();

    public delegate void HandlerWindowMaximize();
    
    public delegate void HandlerGamePause();

    public delegate void HandlerGameResume();

    public delegate void HandlerTextInput(char c);

    public delegate void HandlerKeyDown(Keys key);

    public delegate void HandlerKeyUp(Keys key);

    public delegate void HandlerKeyPress(Keys key);

    public delegate void HandlerKeyRelease(Keys key);
    
    public unsafe class ManagedEventHandler
    {
        private static HandlerWindowResize _HANDLER_WINDOW_RESIZE;
        private static HandlerWindowMove _HANDLER_WINDOW_MOVE;
        private static HandlerWindowMinimize _HANDLER_WINDOW_MINIMIZE;
        private static HandlerWindowMaximize _HANDLER_WINDOW_MAXIMIZE;
        private static HandlerGamePause _HANDLER_GAME_PAUSE;
        private static HandlerGameResume _HANDLER_GAME_RESUME;
        private static HandlerTextInput _HANDLER_TEXT_INPUT;
        private static HandlerKeyDown _HANDLER_KEY_DOWN;
        private static HandlerKeyUp _HANDLER_KEY_UP;
        private static HandlerKeyPress _HANDLER_KEY_PRESS;
        private static HandlerKeyRelease _HANDLER_KEY_RELEASE;
        
        #region PROPERTIES

        public static event HandlerWindowResize WindowResize
        {
            add => _HANDLER_WINDOW_RESIZE += value; 
            remove => _HANDLER_WINDOW_RESIZE -= value; 
        }
        
        public static event HandlerWindowMove WindowMove
        {
            add => _HANDLER_WINDOW_MOVE += value; 
            remove => _HANDLER_WINDOW_MOVE -= value; 
        }
        
        public static event HandlerWindowMinimize WindowMinimize
        {
            add => _HANDLER_WINDOW_MINIMIZE += value; 
            remove => _HANDLER_WINDOW_MINIMIZE -= value; 
        }
        
        public static event HandlerWindowMaximize WindowMaximize
        {
            add => _HANDLER_WINDOW_MAXIMIZE += value; 
            remove => _HANDLER_WINDOW_MAXIMIZE -= value; 
        }
        
        public static event HandlerGamePause GamePause
        {
            add => _HANDLER_GAME_PAUSE += value; 
            remove => _HANDLER_GAME_PAUSE -= value; 
        }
        
        public static event HandlerGameResume GameResume
        {
            add => _HANDLER_GAME_RESUME += value; 
            remove => _HANDLER_GAME_RESUME -= value; 
        }
        
        public static event HandlerTextInput TextInput
        {
            add => _HANDLER_TEXT_INPUT += value; 
            remove => _HANDLER_TEXT_INPUT -= value; 
        }
        
        public static event HandlerKeyDown KeyDown
        {
            add => _HANDLER_KEY_DOWN += value;
            remove => _HANDLER_KEY_DOWN -= value;
        }
        
        public static event HandlerKeyUp KeyUp
        {
            add => _HANDLER_KEY_UP += value; 
            remove => _HANDLER_KEY_UP -= value; 
        }
        
        public static event HandlerKeyPress KeyPress
        {
            add => _HANDLER_KEY_PRESS += value; 
            remove => _HANDLER_KEY_PRESS -= value; 
        }
        
        public static event HandlerKeyRelease KeyRelease
        {
            add => _HANDLER_KEY_RELEASE += value; 
            remove => _HANDLER_KEY_RELEASE -= value; 
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void BeginTextInput()
        {
            NManagedEventHandler_BeginTextInput();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void EndTextInput()
        {
            NManagedEventHandler_EndTextInput();
        }

        #endregion
        
        #region NATIVE_METHODS
        private static void NInvokeWindowResizeListeners(uint newWidth, uint newHeight) => _HANDLER_WINDOW_RESIZE?.Invoke(newWidth, newHeight);
        private static void NInvokeWindowMoveListeners(int newX, int newY) => _HANDLER_WINDOW_MOVE?.Invoke(newX, newY);
        private static void NInvokeWindowMinimizeListeners() => _HANDLER_WINDOW_MINIMIZE?.Invoke();
        private static void NInvokeWindowMaximizeListeners() => _HANDLER_WINDOW_MAXIMIZE?.Invoke();
        private static void NInvokeGamePauseListeners() => _HANDLER_GAME_PAUSE?.Invoke();
        private static void NInvokeGameResumeListeners() => _HANDLER_GAME_RESUME?.Invoke();
        private static void NInvokeTextInputListeners(char c) => _HANDLER_TEXT_INPUT?.Invoke(c);
        private static void NInvokeKeyDownListeners(Keys key) => _HANDLER_KEY_DOWN?.Invoke(key);
        private static void NInvokeKeyUpListeners(Keys key) => _HANDLER_KEY_UP?.Invoke(key);
        private static void NInvokeKeyPressListeners(Keys key) => _HANDLER_KEY_PRESS?.Invoke(key);
        private static void NInvokeKeyReleaseListeners(Keys key) => _HANDLER_KEY_RELEASE?.Invoke(key);

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void NManagedEventHandler_BeginTextInput();

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void NManagedEventHandler_EndTextInput();

        #endregion
    }
}