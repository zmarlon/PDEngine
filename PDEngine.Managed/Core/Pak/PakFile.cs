﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.Core.Pak
{
    public unsafe class PakFile : IDisposable
    {
        private readonly void* m_pHandle;
        private readonly string m_name;
        private readonly string m_md5;
        
        #region PROPERTIES
        public void* Handle => m_pHandle;
        public string Name => m_name;
        public string Md5 => m_md5;
        #endregion

        public PakFile(void* pHandle)
        {
            if(pHandle == null) throw new ArgumentNullException(nameof(pHandle));
            m_pHandle = pHandle;

            var data = NPakFile_GetData(pHandle);
            m_name = StringHelper.FromNativeAnsiString(data.pName);
            m_md5 = StringHelper.FromNativeAnsiString(data.pMd5);
        }

        public PakFile(string name, string md5)
        {
            m_name = name ?? throw new ArgumentNullException(nameof(name));
            m_md5 = md5 ?? throw new ArgumentNullException(nameof(md5));

            var pName = StringHelper.ToNativeAnsiString(m_name);
            var pMd5 = StringHelper.ToNativeAnsiString(m_md5);
            NPakFile_Open(pName, pMd5);

            Marshal.FreeHGlobal(pMd5);
            Marshal.FreeHGlobal(pName);
        }
        
        public void Dispose() => NPakFile_Close(m_pHandle);

        #region NATIVE_METHODS
        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void* NPakFile_Open(IntPtr pName, IntPtr pMd5);

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern Data NPakFile_GetData(void* pHandle);
        
        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void NPakFile_Close(void* pHandle);

        #endregion
        
        #region NATIVE_STRUCTS
        [StructLayout(LayoutKind.Sequential)]
        private struct Data
        {
            public IntPtr pName;
            public IntPtr pMd5;
        }
        #endregion
    }
}