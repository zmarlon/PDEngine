﻿using System.Runtime.InteropServices;

namespace PDEngine.Managed.Core.Pak
{
    public unsafe class PakManager
    {
        public static PakFile _SHADERS;
        public static PakFile _USER_INTERFACE;
        
        #region PROPERTIES
        public static PakFile Shaders => _SHADERS;
        public static PakFile UserInterface => _USER_INTERFACE;
        #endregion

        #region NATIVE_METHODS
        private static void NRegisterNatives(Data* pData)
        {
            _SHADERS = new PakFile(pData->pShaders);
            _USER_INTERFACE = new PakFile(pData->pUserInterface);
        }
        #endregion
        
        #region NATIVE_STRUCTS
        [StructLayout(LayoutKind.Sequential)]
        private struct Data
        {
            public void* pShaders;
            public void* pUserInterface;
        }
        #endregion
    }
}