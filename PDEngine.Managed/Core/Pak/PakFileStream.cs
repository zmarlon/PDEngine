﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.Core.Pak
{
    public unsafe class PakFileStream : UnmanagedMemoryStream
    {
        private readonly byte* m_pData;

        public PakFileStream(PakFile pakFile, string entryName)
        {
            var pEntryName = StringHelper.ToNativeAnsiString(entryName);
            var data = NPakFileStream_GetEntry(pakFile.Handle, pEntryName);
            m_pData = data.pData;
            
            Initialize(data.pData, data.Length, data.Length, FileAccess.Read);

            StringHelper.FreeNativeString(pEntryName);
        }

        public new void Dispose()
        {
            base.Dispose();
            NPakFileStream_FreeData(m_pData);
        }

        #region NATIVE_METHODS
        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern Data NPakFileStream_GetEntry(void* pHandle, IntPtr pEntryName);

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void NPakFileStream_FreeData(byte* pData);
        #endregion
        
        #region NATIVE_STRUCTS
        [StructLayout(LayoutKind.Sequential)]
        private struct Data
        {
            public byte* pData;
            public long Length;
        }
        #endregion
    }
}