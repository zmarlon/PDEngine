﻿namespace PDEngine.Managed.Core.Pak
{
    public struct PakLocation
    {
        public readonly PakFile PakFile;
        public readonly string EntryName;

        public PakLocation(PakFile pakFile, string entryName)
        {
            PakFile = pakFile;
            EntryName = entryName;
        }

        public override string ToString() => $"{PakFile.Name}:{EntryName}";
    }
}