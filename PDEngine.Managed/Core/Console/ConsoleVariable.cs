﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Core.Memory;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.Core.Console
{
    public enum ConsoleVariableType : uint
    {
        I32,
        I64,
        U32,
        U64,
        Float,
        Double
    }
    
    public unsafe struct ConsoleVariable<T> : IDisposable where T: unmanaged
    {
        private GameConsole.Variable m_variable;

        public T Value
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => *(T*)m_variable.pData;
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set => *(T*)m_variable.pData = value;
        }

        private static ConsoleVariableType GetVariableType(Type type)
        {
            if (type == typeof(int)) return ConsoleVariableType.I32;
            if (type == typeof(long)) return ConsoleVariableType.I64;
            if (type == typeof(uint)) return ConsoleVariableType.U32;
            if (type == typeof(ulong)) return ConsoleVariableType.U64;
            if (type == typeof(float)) return ConsoleVariableType.Float;
            if (type == typeof(double)) return ConsoleVariableType.Double;
            throw new ArgumentException($"Invalid console variable type {type}");
        }
        
        public ConsoleVariable(string name, T value, Action callback = null)
        {
            if(string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            
            m_variable.Type = GetVariableType(typeof(T));
            m_variable.pData = (T*)ProxyAllocator.Allocate((ulong)sizeof(T));
            *(T*) m_variable.pData = value;
            m_variable.pCallback = callback == null ? null : Marshal.GetFunctionPointerForDelegate(callback).ToPointer();

            fixed (char* pName = name) GameConsole.NGameConsole_RegisterVariable(pName, (ulong)name.Length, m_variable);
        }

        public void Dispose() => ProxyAllocator.Deallocate(m_variable.pData);
    }

    internal static unsafe class GameConsole
    {
        #region NATIVE_METHODS
        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        internal static extern void NGameConsole_RegisterVariable(char* pName, ulong length, Variable variable);
        #endregion
        
        #region NATIVE_STRUCTS
        [StructLayout(LayoutKind.Sequential)]
        internal struct Variable
        {
            public ConsoleVariableType Type;
            public void* pData;
            public void* pCallback;
        }
        #endregion
    }
}