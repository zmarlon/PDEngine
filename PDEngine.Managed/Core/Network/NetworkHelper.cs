﻿using Lidgren.Network;
using PDEngine.Managed.Core.Logging;

namespace PDEngine.Managed.Core.Network
{
    public class NetworkHelper
    {
        public static LogLevel MessageTypeToLogLevel(NetIncomingMessageType messageType)
        {
            switch (messageType)
            {
                case NetIncomingMessageType.DebugMessage:
                    return LogLevel.Information;
                case NetIncomingMessageType.WarningMessage:
                    return LogLevel.Warning;
                case NetIncomingMessageType.ErrorMessage:
                    return LogLevel.Error;
                default:
                    return LogLevel.Information;
            }
        }
    }
}