﻿using Lidgren.Network;

namespace PDEngine.Managed.Core.Network
{
    public interface IPacket
    {
        void Serialize(NetOutgoingMessage message);

        void Deserialize(NetIncomingMessage message);
    }
}