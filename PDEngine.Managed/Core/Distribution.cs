﻿using System;

namespace PDEngine.Managed.Core
{
    [Flags]
    public enum Distribution
    {
        Client = 1,
        Server = 2
    }
}