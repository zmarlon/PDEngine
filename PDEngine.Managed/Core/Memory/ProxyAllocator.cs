﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.Core.Memory
{
    public static unsafe class ProxyAllocator
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void* Allocate(ulong size) => NProxyAllocator_Allocate(size);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Deallocate(void* pData) => NProxyAllocator_Deallocate(pData);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void* AllocateAligned(ulong size, ulong alignment) => NProxyAllocator_AllocateAligned(size, alignment);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DeallocateAligned(void* pData) => NProxyAllocator_DeallocateAligned(pData);
        
        #region NATIVE_METHODS
        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void* NProxyAllocator_Allocate(ulong size);

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void NProxyAllocator_Deallocate(void* pData);

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void* NProxyAllocator_AllocateAligned(ulong size, ulong alignment);

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void NProxyAllocator_DeallocateAligned(void* pData);
        #endregion
    }
}