﻿namespace PDEngine.Managed.Core.Config
{
    public interface IConfig
    { 
        public void RestoreDefaults();
    }
}