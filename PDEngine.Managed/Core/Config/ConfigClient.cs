﻿using System.Runtime.InteropServices;
using System.Security;
using System.Text.Json.Serialization;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.Core.Config
{
    public unsafe class ConfigClient : IConfig
    {
        private Data* m_pData;
        
        [JsonPropertyName("Window Width")]
        public uint WindowWidth
        {
            get => m_pData->WindowWidth;
            set => m_pData->WindowWidth = value;
        }

        [JsonPropertyName("Window Height")]
        public uint WindowHeight
        {
            get => m_pData->WindowHeight;
            set => m_pData->WindowHeight = value;
        }

        public bool Fullscreen
        {
            get => m_pData->Fullscreen != 0;
            set => m_pData->Fullscreen = value ? (byte) 1 : (byte) 0;
        }

        [JsonPropertyName("Frame Limit")]
        public uint FrameLimit
        {
            get => m_pData->FrameLimit;
            set => m_pData->FrameLimit = value;
        }

        [JsonPropertyName("Vertical Sync")]
        public bool VerticalSync
        {
            get => m_pData->VerticalSync != 0;
            set => m_pData->VerticalSync = value ? (byte) 1 : (byte) 0;
        }

        [JsonPropertyName("Debug Level")]
        public uint DebugLevel
        {
            get => m_pData->DebugLevel;
            set => m_pData->DebugLevel = value;
        }
        
        [StructLayout(LayoutKind.Sequential)]
        private struct Data
        {
            public uint WindowWidth;
            public uint WindowHeight;
            public uint FrameLimit;
            public uint DebugLevel;
            public byte Fullscreen;
            public byte VerticalSync;
        }

        public ConfigClient()
        {
            m_pData = NConfigClient_GetGameData(Game.Instance.Handle);
        }

        public void RestoreDefaults()
        {
            WindowWidth = 1600;
            WindowHeight = 900;
            Fullscreen = false;
            FrameLimit = 0;
            VerticalSync = true;
            DebugLevel = 0;
        }
        
        #region NATIVE_METHODS

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern Data* NConfigClient_GetGameData(void* pGame);

        #endregion
    }
}