﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text.Json;
using PDEngine.Managed.Core.Logging;
using PDEngine.Managed.Util.Serialization.Json;

namespace PDEngine.Managed.Core.Config
{
    public class ConfigParser
    {
        private static JsonSerializerOptions _JSON_SERIALIZE_OPTIONS;

        static ConfigParser()
        {
            _JSON_SERIALIZE_OPTIONS = new JsonSerializerOptions
            {
                WriteIndented = true
            };
            _JSON_SERIALIZE_OPTIONS.Converters.Add(new IPAddressJsonConverter());
        }
        
        public static T TryLoadingConfig<T>(string location, string name) where T: class, IConfig
        {
            if(string.IsNullOrEmpty(location)) throw new ArgumentNullException(nameof(location));
            if(string.IsNullOrEmpty(location)) throw new ArgumentNullException(nameof(name));

            T config = null;
            try
            {
                using (var fileStream = new FileStream(Path.Combine(location, name + ".json"), FileMode.Open))
                {
                    using (TextReader textReader = new StreamReader(fileStream))
                    {
                        var text = textReader.ReadToEnd();
                        config = JsonSerializer.Deserialize<T>(text, _JSON_SERIALIZE_OPTIONS);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Instance.Warn(typeof(ConfigParser), $"No config found, restoring defaults: {name}: {exception}");
                config = Activator.CreateInstance<T>();
                config.RestoreDefaults();
            }

            return config;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T TryLoadingConfig<T>(string name) where T : class, IConfig
        {
            return TryLoadingConfig<T>(ConfigPath, name);
        }

        public static void SaveConfig<T>(string location, string name, T config) where T : class, IConfig
        {
            if(string.IsNullOrEmpty(location)) throw new ArgumentNullException(nameof(location));
            if(string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            if(config == null) throw new ArgumentNullException(nameof(config));

            if (!Directory.Exists(location))
            {
                try
                {
                    Directory.CreateDirectory(location);
                }
                catch (Exception exception)
                {
                    Logger.Instance.Warn($"Failed to create config directory: {exception}");
                }
            }
            
            try
            {
                using (var fileStream = new FileStream(Path.Combine(location, name + ".json"), FileMode.Create))
                {
                    using (TextWriter textWriter = new StreamWriter(fileStream))
                    {
                        var json = JsonSerializer.Serialize(config, _JSON_SERIALIZE_OPTIONS);
                        textWriter.Write(json);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Instance.Warn(typeof(ConfigParser), $"Failed to save config: {name}: {exception}");
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SaveConfig<T>(string name, T config) where T : class, IConfig
        {
            SaveConfig(ConfigPath, name, config);
        }
        
        public static readonly string ConfigPath = Path.Combine(Directory.GetCurrentDirectory(), "Configs");
    }
}