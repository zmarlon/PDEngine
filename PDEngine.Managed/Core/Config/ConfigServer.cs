﻿using System.Text.Json.Serialization;

namespace PDEngine.Managed.Core.Config
{
    public class ConfigServer : IConfig
    {
        [JsonPropertyName("Max Players")]
        public int MaxPlayers { get; set; }

        public ushort Port { get; set; }
        [JsonPropertyName("World Name")]
        public string WorldName { get; set; }

        public void RestoreDefaults()
        {
            MaxPlayers = 16;
            Port = 13372;
            WorldName = "World";
        }
    }
}