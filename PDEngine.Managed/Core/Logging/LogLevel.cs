﻿namespace PDEngine.Managed.Core.Logging
{
    public enum LogLevel : uint
    {
        Information = 0,
        Success = 1,
        Warning = 2,
        Error = 3,
        Fatal = 4
    }
}