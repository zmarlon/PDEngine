﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.Core.Logging
{
    public unsafe class Logger
    {
        private readonly void* m_pHandle;
        
        #region PROPERTIES
        public void* Handle => m_pHandle;
        #endregion

        private Logger(void* pHandle)
        {
            if(pHandle == null) throw new ArgumentNullException(nameof(pHandle));
            m_pHandle = pHandle;

            _INSTANCE = this;
        }

        public void Log(LogLevel logLevel, string message)
        {
            fixed (char* pMessage = message)
            {
                NLogger_Log(m_pHandle, logLevel, pMessage, (ulong) message.Length);
            }
        }

        public void Log(LogLevel logLevel, Type type, string message)
        {
            var formattedMessage = $"[{type.FullName}] {message}";

            fixed (char* pMessage = formattedMessage)
            {
                NLogger_Log(m_pHandle, logLevel, pMessage, (ulong)formattedMessage.Length);
            }
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Info(string message) => Log(LogLevel.Information, message);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Info(Type type, string message) => Log(LogLevel.Information, type, message);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Success(string message) => Log(LogLevel.Success, message);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Success(Type type, string message) => Log(LogLevel.Success, type, message);
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Warn(string message) => Log(LogLevel.Warning, message);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Warn(Type type, string message) => Log(LogLevel.Warning, type, message);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Error(string message) => Log(LogLevel.Error, message);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Error(Type type, string message) => Log(LogLevel.Error, type, message);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Fatal(string message) => Log(LogLevel.Fatal, message);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Fatal(Type type, string message) => Log(LogLevel.Fatal, type, message);
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Logger CreateSingleton() => new Logger(NLogger_GetInstance());
        
        #region INSTANCE
        private static Logger _INSTANCE;

        public static Logger Instance
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _INSTANCE;
        }
        #endregion
        
        #region NATIVE_METHODS
        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern void* NLogger_GetInstance();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern void NLogger_Log(void* pLogger, LogLevel logLevel, char* pMessage, ulong size);
        #endregion
    }
}