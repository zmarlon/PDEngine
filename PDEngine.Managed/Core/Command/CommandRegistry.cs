﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PDEngine.Managed.Core.Command.Blueprints;
using PDEngine.Managed.Server;

namespace PDEngine.Managed.Core.Command
{
    public class CommandRegistry
    {
        private static readonly Dictionary<string, ICommand> _COMMANDS = new Dictionary<string, ICommand>();
        private static readonly ReadOnlyDictionary<string, ICommand> _COMMAND_VIEW = new ReadOnlyDictionary<string, ICommand>(_COMMANDS);

        public static ReadOnlyDictionary<string, ICommand> Commands => _COMMAND_VIEW;

        static CommandRegistry()
        {
            AddCommand(new CommandHelp());
            AddCommand(new CommandBase("exit", "Closes the server", (sender, arguments) =>
            {
                GameServer.Instance.Stop();
            }));
        }
        
        public static void AddCommand(ICommand command)
        {
            if (command == null) throw new ArgumentNullException(nameof(command));
            if (_COMMANDS.ContainsKey(command.Name)) throw new ArgumentException($"Command with name {command.Name} already registered!");
            _COMMANDS.Add(command.Name, command);
        }

        public static ICommand GetCommand(string name)
        {
            return _COMMANDS.TryGetValue(name, out var command) ? command : null;
        }
    }
}