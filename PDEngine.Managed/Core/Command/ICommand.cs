﻿using System;

namespace PDEngine.Managed.Core.Command
{
    public interface ICommand
    {
        public string Name { get; }
        public string Description { get; }

        public void Execute(object sender, Span<string> arguments);
    }
}