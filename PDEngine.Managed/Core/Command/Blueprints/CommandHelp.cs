﻿using System;
using System.Linq;
using System.Text;
using PDEngine.Managed.Core.Logging;

namespace PDEngine.Managed.Core.Command.Blueprints
{
    public class CommandHelp : ICommand
    {
        public string Name => "help";
        public string Description => "Lists all available commands";
        
        public void Execute(object sender, Span<string> arguments)
        {
            var commands = CommandRegistry.Commands;

            var builder = new StringBuilder();
            builder.Append("Available commands:");

            var sortedCommands = commands.OrderBy(key => key.Key);
            foreach (var keyValuePair in sortedCommands)
            {
                builder.Append("\n  *").Append(keyValuePair.Key).Append("* : ").Append(keyValuePair.Value.Description);
            }

            Logger.Instance.Info(builder.ToString());
        }
    }
}