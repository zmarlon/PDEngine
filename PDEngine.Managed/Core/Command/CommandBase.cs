﻿using System;

namespace PDEngine.Managed.Core.Command
{
    public delegate void SimpleCommandAction(object sender, Span<string> arguments);
    
    public class CommandBase : ICommand
    {
        private readonly string _name;
        private readonly string _description;
        private readonly SimpleCommandAction _action;

        public string Name => _name;
        public string Description => _description;
        
        public CommandBase(string name, string description, SimpleCommandAction action)
        {
            _name = name ?? throw new ArgumentNullException(nameof(name));
            _description = description ?? throw new ArgumentNullException(nameof(description));
            _action = action ?? throw new ArgumentNullException(nameof(action));
        }
        
        public void Execute(object sender, Span<string> arguments)
        {
            _action(sender, arguments);
        }
    }
}