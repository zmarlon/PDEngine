﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Core.Math;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.Core
{
    public unsafe class Window
    {
        private void* m_pHandle;
        private Data* m_pData;

        #region PROPERTIES
        public void* Handle => m_pHandle;
        #endregion

        public int X
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => m_pData->X;
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set => NWindow_SetX(m_pHandle, value);
        }

        public int Y
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => m_pData->Y;
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set => NWindow_SetY(m_pHandle, value);
        }

        public PointI Location
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => new PointI(m_pData->X, m_pData->Y);
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set => NWindow_SetPosition(m_pHandle, value.X, value.Y);
        }

        public uint Width
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => m_pData->Width;
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set => NWindow_SetWidth(m_pHandle, value);
        }

        public uint Height
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => m_pData->Height;
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set => NWindow_SetHeight(m_pHandle, value);
        }

        public PointUI Size
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => new PointUI(m_pData->Width, m_pData->Height);
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set => NWindow_SetSize(m_pHandle, value.X, value.Y);
        }

        private Window(void* pHandle)
        {
            if(pHandle == null) throw new ArgumentNullException(nameof(pHandle));
            m_pHandle = pHandle;
            
            m_pData = NWindow_GetData(m_pHandle);

            _INSTANCE = this;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Window CreateSingleton() => new Window(NWindow_GetInstance());

        #region INSTANCE
        private static Window _INSTANCE;
        public static Window Instance => _INSTANCE;
        #endregion
        
        #region NATIVE_METHODS
        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern void* NWindow_GetInstance();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern Data* NWindow_GetData(void* pWindow);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern void NWindow_SetX(void* pWindow, int x);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern void NWindow_SetY(void* pWindow, int y);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern void NWindow_SetPosition(void* pWindow, int x, int y);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern void NWindow_SetWidth(void* pWindow, uint width);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern void NWindow_SetHeight(void* pWindow, uint height);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule)]
        private static extern void NWindow_SetSize(void* pWindow, uint width, uint height);
        #endregion
        
        #region NATIVE_STRUCTS
        [StructLayout(LayoutKind.Sequential)]
        private struct Data
        {
            public int X, Y;
            public uint Width, Height;
            public byte Fullscreen;
        }
        #endregion
    }
}
