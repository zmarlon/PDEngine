﻿using System.Runtime.InteropServices;

namespace PDEngine.Managed
{
    [StructLayout(LayoutKind.Sequential)]
    public struct GameState
    {
        private byte m_isPaused;

        public bool IsPaused
        {
            get => m_isPaused != 0;
            set => m_isPaused = value ? (byte) 1 : (byte) 0;
        }
    }
}