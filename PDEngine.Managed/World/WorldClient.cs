﻿using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.World
{
    public unsafe class WorldClient : World
    {
        public override bool IsRemote => true;

        public WorldClient(void* pHandle) : base(pHandle)
        {
            
        }
        
        public static WorldClient CreateSingleton() => new WorldClient(NWorldClient_GetInstance());
        
        #region NATIVE_METHODS
        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void* NWorldClient_GetInstance();
        #endregion
    }
}