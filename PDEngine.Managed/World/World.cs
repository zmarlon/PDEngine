﻿using System;

namespace PDEngine.Managed.World
{
    public abstract unsafe class World
    {
        protected readonly void* m_pHandle;
        
        #region PROPERTIES
        public abstract bool IsRemote { get; }
        #endregion

        protected World(void* pHandle)
        {
            if(pHandle == null) throw new ArgumentNullException(nameof(pHandle));
            m_pHandle = pHandle;
        }
    }
}