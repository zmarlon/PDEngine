﻿using System.IO;
using PDEngine.Managed.Core.Logging;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.World
{
    public class WorldHelper
    {
        private static readonly string _WORLD_TEMPLATE_DIRECTORY = Path.Combine(Directory.GetCurrentDirectory(), "Common", "WorldTemplate");
        
        public static string PrepareWorld(string name, bool isDedicatedServer)
        {
            if (!Directory.Exists(_WORLD_TEMPLATE_DIRECTORY))
            {
                Logger.Instance.Fatal("Template world directory does not exist, your game files are corrupted!");
            }

            if (name == "$WORLD_TEMPLATE") return _WORLD_TEMPLATE_DIRECTORY;

            var worldDirectory = GetWorldDirectory(name, isDedicatedServer);

            if (!Directory.Exists(worldDirectory))
            {
                Directory.CreateDirectory(worldDirectory);
                FileHelper.CopyDirectory(worldDirectory, _WORLD_TEMPLATE_DIRECTORY);
            }

            return worldDirectory;
        }

        public static string GetWorldDirectory(string name, bool isDedicatedServer)
        {
            return isDedicatedServer ? Path.Combine(Directory.GetCurrentDirectory(), "Worlds", name) : "TEST"; //TODO: make integrated server work
        }
    }
}