﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.World
{
    public unsafe class WorldServer : World
    {
        private readonly string m_name;
        private readonly string m_directory;
        
        #region PROPERTIES
        public override bool IsRemote => false;
        public string Name => m_name;
        public string Directory;
        #endregion

        public WorldServer(void* pHandle, string name, string directory) : base(pHandle)
        {
            if(string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            if(string.IsNullOrEmpty(directory)) throw new ArgumentNullException(nameof(directory));
            
            m_name = name;
            m_directory = directory;
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static WorldServer CreateSingleton(string name, string directory) => new WorldServer(NWorldServer_GetInstance(), name, directory);
        
        #region NATIVE_METHODS
        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void* NWorldServer_GetInstance();
        #endregion
    }
}