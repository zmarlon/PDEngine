﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace PDEngine.Managed.Util.Extensions
{
    public static class ListExtensions
    {
        public static T[] GetData<T>(this List<T> list)
        {
            return ArrayAccessor<T>.Getter(list);
        }

        private static class ArrayAccessor<T>
        {
            public static readonly Func<List<T>, T[]> Getter;

            static ArrayAccessor()
            {
                var method = new DynamicMethod("get", MethodAttributes.Static | MethodAttributes.Public,
                    CallingConventions.Standard, typeof(T[]), new[] {typeof(List<T>)}, typeof(ArrayAccessor<T>), true);
                var ilGenerator = method.GetILGenerator();
                ilGenerator.Emit(OpCodes.Ldarg_0);
                ilGenerator.Emit(OpCodes.Ldfld,
                    typeof(List<T>).GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance));
                ilGenerator.Emit(OpCodes.Ret);
                Getter = (Func<List<T>, T[]>) method.CreateDelegate(typeof(Func<List<T>, T[]>));
            }
        }
    }

}