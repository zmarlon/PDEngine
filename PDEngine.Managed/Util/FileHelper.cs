﻿using System;
using System.IO;

namespace PDEngine.Managed.Util
{
    public class FileHelper
    {
        public static void CopyDirectory(string destination, string source, bool copySubdirectories = true)
        {
            if(string.IsNullOrEmpty(destination)) throw new ArgumentNullException(nameof(destination));
            if(string.IsNullOrEmpty(source)) throw new ArgumentNullException(nameof(source));

            var directory = new DirectoryInfo(source);
            if(!directory.Exists) throw new DirectoryNotFoundException(directory.FullName);

            if (!Directory.Exists(destination)) Directory.CreateDirectory(destination);
            
            var files = directory.GetFiles();
            foreach (var file in files)
            {
                var newFilePath = Path.Combine(destination, file.Name);
                file.CopyTo(newFilePath, false);
            }

            if (copySubdirectories)
            {
                var directories = directory.GetDirectories();
                foreach (var subDirectory in directories)
                {
                    CopyDirectory(Path.Combine(destination, subDirectory.Name), subDirectory.FullName, true);
                }
            }
        }
    }
}