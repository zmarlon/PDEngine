﻿using System.Runtime.InteropServices;

namespace PDEngine.Managed.Util
{
    [StructLayout(LayoutKind.Sequential)]
    public struct FrameEventArgs
    {
        public readonly float DeltaTime;
        public readonly float TotalTime;

        public FrameEventArgs(float deltaTime, float totalTime)
        {
            DeltaTime = deltaTime;
            TotalTime = totalTime;
        }
    }
}