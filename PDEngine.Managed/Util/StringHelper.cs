﻿using System;
using System.Runtime.InteropServices;

namespace PDEngine.Managed.Util
{
    public class StringHelper
    {
        public static IntPtr ToNativeAnsiString(string message)
        {
            return Marshal.StringToHGlobalAnsi(message);
        }

        public static IntPtr ToNativeUniString(string message)
        {
            return Marshal.StringToHGlobalUni(message);
        }

        public static string FromNativeAnsiString(IntPtr pMessage)
        {
            return Marshal.PtrToStringAnsi(pMessage);
        }

        public static string FromNativeUniString(IntPtr pMessage)
        {
            return Marshal.PtrToStringUni(pMessage);
        }

        public static void FreeNativeString(IntPtr pMessage)
        {
            Marshal.FreeHGlobal(pMessage);
        }
    }
}