﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Core;
using PDEngine.Managed.Core.Config;
using PDEngine.Managed.Core.Logging;
using PDEngine.Managed.Server.Core;
using PDEngine.Managed.Util;
using PDEngine.Managed.World;

namespace PDEngine.Managed.Server
{
    public abstract unsafe class GameServer : IDisposable
    {
        private readonly void* m_pHandle;
        private Data* m_pData;
        private ConfigServer m_config;
        protected WorldServer m_world;
        private NetworkServer m_networkServer;
        private PlayerManager m_playerManager;

        #region GETTER
        public abstract bool IsDedicated { get; }
        public abstract GameInfo GameInfo { get; }
        public ConfigServer Config => m_config;
        public NetworkServer NetworkServer => m_networkServer;
        public PlayerManager PlayerManager => m_playerManager;
        public bool Running => m_pData->ServerRunning;
        #endregion

        protected GameServer(void* pHandle)
        {
            if(pHandle == null) throw new ArgumentNullException(nameof(pHandle));
            m_pHandle = pHandle;

            m_pData = NGameServer_GetData(pHandle);
            
            _INSTANCE = this;
        }

        protected virtual void PreInit()
        {
            m_config = ConfigParser.TryLoadingConfig<ConfigServer>("ServerConfig");

            var worldDirectory = WorldHelper.PrepareWorld(m_config.WorldName, IsDedicated);
            m_pData->pWorldName = StringHelper.ToNativeUniString(m_config.WorldName);
            m_pData->pWorldDirectory = StringHelper.ToNativeUniString(worldDirectory);
            Logger.Instance.Info(GetType(), $"Loading world *{m_config.WorldName}* from path: {worldDirectory}");
        }

        protected virtual void Init()
        {
            m_world = WorldServer.CreateSingleton(m_config.WorldName, WorldHelper.PrepareWorld(m_config.WorldName, IsDedicated));
            
            m_playerManager = new PlayerManager(this);
            
            m_networkServer = new NetworkServer(this);
            m_networkServer.Start();
        }

        protected virtual void PostInit()
        {
            
        }
        
        private void Tick(FrameEventArgs frameEventArgs)
        {
            m_networkServer.Tick(frameEventArgs);
        }

        public void Stop()
        {
            m_pData->ServerRunning = false;
        }

        public void Dispose()
        {
            m_networkServer.Stop();

            m_playerManager.Dispose();

            StringHelper.FreeNativeString(m_pData->pWorldDirectory);
            StringHelper.FreeNativeString(m_pData->pWorldName);
            
            ConfigParser.SaveConfig("ServerConfig", m_config);
        }

        #region INSTANCE
        protected static GameServer _INSTANCE;
        public static GameServer Instance
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _INSTANCE;
        }
        #endregion
        #region NATIVE_STRUCTS
        [StructLayout(LayoutKind.Sequential)]
        public struct Data
        {
            private byte m_serverRunning;
            public IntPtr pWorldName;
            public IntPtr pWorldDirectory;

            public bool ServerRunning
            {
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                get => m_serverRunning != 0;
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                set => m_serverRunning = value ? (byte) 1 : (byte) 0;
            }
        }
        #endregion
        #region NATIVE_METHODS
        private static void NPreInit() => _INSTANCE.PreInit();
        private static void NInit() => _INSTANCE.Init();
        private static void NPostInit() => _INSTANCE.PostInit();
        private static void NTickInstance(float deltaTime, float elapsedTime) => _INSTANCE.Tick(new FrameEventArgs(deltaTime, elapsedTime));
        private static void NDisposeInstance() => _INSTANCE.Dispose();

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern Data* NGameServer_GetData(void* pInstance);
        #endregion
    }
}