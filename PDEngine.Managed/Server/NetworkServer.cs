﻿using System;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading;
using Lidgren.Network;
using PDEngine.Managed.Core.Logging;
using PDEngine.Managed.Core.Network;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.Server
{
    public class NetworkServer
    {
        private readonly GameServer m_gameServer;
        private NetServer m_lidgrenServer;
        
        #region PROPERTIES

        public NetServer LidgrenServer
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => m_lidgrenServer;
        }
        #endregion

        public NetworkServer(GameServer gameServer)
        {
            m_gameServer = gameServer ?? throw new ArgumentNullException(nameof(gameServer));
        }

        public void Start()
        {
            Logger.Instance.Info(GetType(), $"Starting server on port {m_gameServer.Config.Port}");

            var config = new NetPeerConfiguration(m_gameServer.GameInfo.Name)
            {
                Port = m_gameServer.Config.Port,
                ConnectionTimeout = 30.0f,
                PingInterval = 5.0f,
                MaximumConnections = 32,
                MaximumTransmissionUnit = 1200,
            };
            config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            m_lidgrenServer = new NetServer(config);

            try
            {
                m_lidgrenServer.Start();
            }
            catch (SocketException exception)
            {
                Logger.Instance.Warn(exception.ToString());
                Logger.Instance.Fatal(GetType(), $"Failed to start server, maybe a server is already running on this port?");
            }
        }

        public void Stop()
        {
            var message = "Shutting down server";
            Logger.Instance.Info(GetType(), message);
            m_lidgrenServer.Shutdown(message);
            m_lidgrenServer.FlushSendQueue();
        }

        public void Tick(FrameEventArgs frameEventArgs)
        {
            NetIncomingMessage message;
            while ((message = m_lidgrenServer.ReadMessage()) != null)
            {
                switch (message.MessageType)
                {
                    case NetIncomingMessageType.DiscoveryRequest:
                    {
                        m_gameServer.PlayerManager.HandleDiscoveryRequest(message);
                    }
                        break;
                    case NetIncomingMessageType.ConnectionApproval:
                    {
                        message.SenderConnection.Approve();
                    }
                        break;
                    case NetIncomingMessageType.Data:
                        HandleData(message);
                        break;
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    {
                        Logger.Instance.Log(NetworkHelper.MessageTypeToLogLevel(message.MessageType), GetType(), message.ReadString());
                    }
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        HandleStatusChanged(message);
                        break;
                }

                m_lidgrenServer.Recycle(message);
            }
        }

        private void HandleData(NetIncomingMessage message)
        {
            var packetType = (PacketType) message.ReadUInt32();
            Console.WriteLine(packetType);
        }
        
        private void HandleStatusChanged(NetIncomingMessage message)
        {
            var status = (NetConnectionStatus) message.ReadByte();
            switch (status)
            {
                case NetConnectionStatus.Connected:
                    OnConnect(message);
                    break;
                case NetConnectionStatus.Disconnected:
                case NetConnectionStatus.Disconnecting:
                    OnDisconnect(message);
                    break;
            }
        }

        private void OnConnect(NetIncomingMessage message)
        {
            Logger.Instance.Info($"Client {message.SenderEndPoint.Address}:{message.SenderEndPoint.Port} connected to server");
        }

        private void OnDisconnect(NetIncomingMessage message)
        {
            Logger.Instance.Info($"Client {message.SenderEndPoint.Address}:{message.SenderEndPoint.Port} disconnected from server");
        }
    }
}