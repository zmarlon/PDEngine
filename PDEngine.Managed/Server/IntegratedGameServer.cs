﻿using PDEngine.Managed.Core;

namespace PDEngine.Managed.Server
{
    public unsafe class IntegratedGameServer : GameServer
    {
        #region PROPERTIES
        public override bool IsDedicated => false;
        public override GameInfo GameInfo => Game.Instance.GameInfo;
        #endregion
        
        public IntegratedGameServer(void* pHandle) : base(pHandle)
        {

        }

        #region NATIVE_METHODS
        private static void CreateInstance(void* pHandle) => _INSTANCE = new IntegratedGameServer(pHandle);
        #endregion
    }
}