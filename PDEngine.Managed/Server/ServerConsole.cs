﻿using System;
using PDEngine.Managed.Core.Command;
using PDEngine.Managed.Core.Logging;

namespace PDEngine.Managed.Server
{
    public class ServerConsole
    {
        private DedicatedGameServer m_gameServer;
        
        public ServerConsole(DedicatedGameServer gameServer)
        {
            m_gameServer = gameServer ?? throw new ArgumentNullException(nameof(gameServer));
        }

        public void HandleInput()
        {
            while (m_gameServer.Running)
            {
                var oldColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(">: ");
                Console.ForegroundColor = oldColor;

                var line = Console.ReadLine();
                var split = line.Split(' ');

                var command = CommandRegistry.GetCommand(split[0]);
                if (command == null)
                {
                    Logger.Instance.Warn($"[Warning] Unknown command *{split[0]}*, type *help* for a list of commands");
                }
                else
                {
                    command.Execute(null, split.AsSpan(1));
                }
            }
        }
    }
}