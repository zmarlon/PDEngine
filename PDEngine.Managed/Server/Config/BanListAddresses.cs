﻿using System.Collections.Generic;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text.Json.Serialization;
using PDEngine.Managed.Core.Config;

namespace PDEngine.Managed.Server.Config
{
    public class BanListAddresses : IConfig
    {
        private HashSet<IPAddress> m_bannedAddresses;

        [JsonPropertyName("Banned Addresses")]
        public HashSet<IPAddress> BannedAddresses
        {
            get => m_bannedAddresses;
            set => m_bannedAddresses = value;
        }

        public void RestoreDefaults()
        {
            m_bannedAddresses = new HashSet<IPAddress>();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsBanned(IPAddress address)
        {
            return m_bannedAddresses.Contains(address);
        }
    }
}