﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text.Json.Serialization;
using PDEngine.Managed.Core.Config;

namespace PDEngine.Managed.Server.Config
{
    public class BanListPlayers : IConfig
    {
        private HashSet<string> m_bannedPlayers;

        [JsonPropertyName("Banned Players")]
        public HashSet<string> BannedPlayers
        {
            get => m_bannedPlayers;
            set => m_bannedPlayers = value;
        }

        public void RestoreDefaults()
        {
            m_bannedPlayers = new HashSet<string>();
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsBanned(string playerName)
        {
            return m_bannedPlayers.Contains(playerName);
        }
    }
}