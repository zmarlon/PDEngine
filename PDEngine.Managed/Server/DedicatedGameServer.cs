﻿using System;
using System.Runtime.CompilerServices;
using PDEngine.Managed.Core;
using PDEngine.Managed.Core.Logging;

namespace PDEngine.Managed.Server
{
    public unsafe class DedicatedGameServer : GameServer
    {
        private readonly GameInfo m_gameInfo;
        private readonly Logger m_logger;
        private readonly ServerConsole m_console;

        #region GETTER
        public override bool IsDedicated => true;
        public override GameInfo GameInfo => m_gameInfo;
        #endregion
        
        public DedicatedGameServer(void* pHandle) : base(pHandle)
        {
            m_gameInfo = new GameInfo("WingertVoxel", 0, 0, 1);
            m_logger = Logger.CreateSingleton();
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
                m_logger.Fatal($"Unhandled exception. {(Exception)eventArgs.ExceptionObject}");
            
            m_console = new ServerConsole(this);
        }
        
        private void HandleConsoleInput()
        {
            m_console.HandleInput();
        }
        
        #region NATIVE_METHODS
        private static void NCreateInstance(void* pHandle) => _INSTANCE = new DedicatedGameServer(pHandle);
        private static void NHandleConsoleInput() => ((DedicatedGameServer)_INSTANCE).HandleConsoleInput();
        #endregion
    }
}

