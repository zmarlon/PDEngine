﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Lidgren.Network;
using PDEngine.Managed.Core.Config;
using PDEngine.Managed.Core.Network;
using PDEngine.Managed.EntitySystem;
using PDEngine.Managed.Server.Config;

namespace PDEngine.Managed.Server.Core
{
    public class PlayerManager : IDisposable
    {
        #region CONSTANTS
        private const string _BANNED_PLAYERS_NAME = "BannedPlayers";
        private const string _BANNED_ADDRESSES_NAME = "BannedAddresses";
        #endregion
        
        private readonly GameServer m_gameServer;
        private readonly List<EntityServerPlayer> m_players = new List<EntityServerPlayer>();
        private readonly ReadOnlyCollection<EntityServerPlayer> m_readOnlyPlayers;
        
        private readonly Dictionary<Guid, EntityServerPlayer> m_guidToPlayer = new Dictionary<Guid, EntityServerPlayer>();

        private readonly BanListPlayers m_bannedPlayers;
        private readonly BanListAddresses m_bannedAddresses;

        #region PROPERTIES
        #endregion
        
        public PlayerManager(GameServer gameServer)
        {
            m_gameServer = gameServer ?? throw new ArgumentNullException(nameof(GameServer));

            m_bannedPlayers = ConfigParser.TryLoadingConfig<BanListPlayers>(_BANNED_PLAYERS_NAME);
            m_bannedAddresses = ConfigParser.TryLoadingConfig<BanListAddresses>(_BANNED_ADDRESSES_NAME);
            
            m_readOnlyPlayers = m_players.AsReadOnly();
        }

        public NetOutgoingMessage CreateMessage(PacketType packetType, int initialSize)
        {
            return m_gameServer.NetworkServer.LidgrenServer.CreateMessage(sizeof(PacketType) + initialSize);
        }

        public void SendMessageToAllClients(NetOutgoingMessage message, NetDeliveryMethod deliveryMethod)
        {
            m_gameServer.NetworkServer.LidgrenServer.SendToAll(message, deliveryMethod);
        }

        public void SendMessageToAllClientsWithCondition(NetOutgoingMessage message, Func<EntityServerPlayer, bool> condition, NetDeliveryMethod deliveryMethod)
        {
            var lidgrenServer = m_gameServer.NetworkServer.LidgrenServer;
            for(var ix = 0; ix < m_players.Count; ix++)
            {
                var player = m_players[ix];
                if (condition(player)) lidgrenServer.SendMessage(message, player.Connection, deliveryMethod);
            }
        }

        public void HandleDiscoveryRequest(NetIncomingMessage message)
        {
            if (m_bannedAddresses.IsBanned(message.SenderEndPoint.Address))
            {
                //TODO: handle ban message
            }
            else
            {
                var gameInfo = m_gameServer.GameInfo;
                var lidgrenServer = m_gameServer.NetworkServer.LidgrenServer;
                var outgoingMessage = lidgrenServer.CreateMessage();

                outgoingMessage.Write(gameInfo.Name);
                outgoingMessage.Write(gameInfo.Major);
                outgoingMessage.Write(gameInfo.Minor);
                outgoingMessage.Write(gameInfo.Patch);
                lidgrenServer.SendDiscoveryResponse(outgoingMessage, message.SenderEndPoint);
            }
        }

        public void Dispose()
        {
            ConfigParser.SaveConfig(_BANNED_ADDRESSES_NAME, m_bannedAddresses);
            ConfigParser.SaveConfig(_BANNED_PLAYERS_NAME, m_bannedPlayers);
        }
    }
}