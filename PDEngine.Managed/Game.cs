﻿using System;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Client;
using PDEngine.Managed.Client.Renderer;
using PDEngine.Managed.Core;
using PDEngine.Managed.Core.Config;
using PDEngine.Managed.Core.Logging;
using PDEngine.Managed.EntitySystem;
using PDEngine.Managed.EntitySystem.Components;
using PDEngine.Managed.Util;

namespace PDEngine.Managed
{
    public unsafe class Game : IDisposable
    {
        private readonly void* m_pHandle;
        private readonly GameState* m_pGameState;
        
        private GameInfo m_gameInfo;
        private Logger m_logger;
        private ConfigClient m_config;
        private Window m_window;
        private Renderer m_renderer;
        private NetworkClient m_networkClient;

        #region GETTER
        public void* Handle => m_pHandle;
        public GameState* GameState => m_pGameState;
        public GameInfo GameInfo => m_gameInfo;
        public Logger Logger => m_logger;
        public Window Window => m_window;
        #endregion

        private Entity m_playerEntity; //TODO:
        
        private Game(void* pHandle)
        {
            if(pHandle == null) throw new ArgumentNullException(nameof(pHandle));
            m_pHandle = pHandle;
            
            if(_INSTANCE != null) throw new ArgumentException("Instance must be null");
            _INSTANCE = this;
            
            m_pGameState = NGame_GetGameState(m_pHandle);
        }
        
        private void PreInit()
        {
            m_gameInfo = new GameInfo("WingertVoxel", 0, 0, 1);
            m_logger = Logger.CreateSingleton();
            
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
                m_logger.Fatal($"Unhandled exception. {(Exception)eventArgs.ExceptionObject}");

            m_config = ConfigParser.TryLoadingConfig<ConfigClient>("ClientConfig");
        }

        private void Init()
        {
            m_window = Window.CreateSingleton();
            m_renderer = Renderer.CreateSingleton();
        }

        private void PostInit()
        {
            m_networkClient = new NetworkClient(this, "127.0.0.1", 25565);
            m_networkClient.Connect();

            m_playerEntity = new Entity(Vector3.Zero, Vector3.Zero);
            m_playerEntity.AddComponent<ComponentPlayer>();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Tick(FrameEventArgs frameEventArgs)
        {
            m_networkClient.Tick();
            if (!m_pGameState->IsPaused)
            {
                m_playerEntity.HandleClientTick(frameEventArgs);
            }
        }
        
        public void Dispose()
        {
            m_networkClient.Disconnect();
            m_renderer.Dispose();

            ConfigParser.SaveConfig("ClientConfig", m_config);
        }
        
        #region INSTANCE
        private static Game _INSTANCE;

        public static Game Instance
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _INSTANCE;
        }
        #endregion
        #region NATIVE_METHODS
        private static void NCreateInstance(void* pGame) => _INSTANCE = new Game(pGame);
        private static void NPreInit() => _INSTANCE.PreInit();
        private static void NInit() => _INSTANCE.Init();
        private static void NPostInit() => _INSTANCE.PostInit();
        private static void NTickInstance(float deltaTime, float elapsedTime) => _INSTANCE.Tick(new FrameEventArgs(deltaTime, elapsedTime));
        private static void NDisposeInstance() => _INSTANCE.Dispose();

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern GameState* NGame_GetGameState(void* pInstance);

        #endregion
    }
}