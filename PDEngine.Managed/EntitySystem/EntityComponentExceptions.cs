﻿using System;

namespace PDEngine.Managed.EntitySystem
{
    public class ComponentAlreadyAttachedException : Exception
    {
        public ComponentAlreadyAttachedException(Type componentType) : base($"Component {componentType.FullName} has already been attached") { }
    }
}