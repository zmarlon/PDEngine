﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using PDEngine.Managed.Core.Memory;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.EntitySystem
{
    public unsafe class Entity : IDisposable
    {
        internal readonly Data* m_pData;
        private readonly List<EntityComponent> m_components = new List<EntityComponent>();
        
        #region GETTER

        public Vector3 Position
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => m_pData->Position;
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set => m_pData->Position = value;
        }

        public Vector3 Rotation
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => m_pData->Rotation;
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set => m_pData->Rotation = value;
        }

        public bool Active
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => m_pData->Active != 0;
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set => m_pData->Active = value ? (byte)1 : (byte)0;
        }
        #endregion

        public Entity(Vector3 position, Vector3 rotation, bool visible = true)
        {
            m_pData = (Data*)ProxyAllocator.AllocateAligned((ulong)sizeof(Data), 4);
            m_pData->Position = position;
            m_pData->Rotation = rotation;
            m_pData->Active = visible ? (byte)1 : (byte)0;
        }

        public Entity(Vector3 position, bool visible = true) : this(position, Vector3.Zero, visible) { }
        
        public void AddComponent(EntityComponent component, void* pNativeData = null, object managedData = null)
        {
            if(component == null) throw new ArgumentNullException(nameof(component));
            var componentType = component.GetType();
            
            if(HasComponent(componentType)) throw new ComponentAlreadyAttachedException(componentType);
            component.m_entity = this;
            m_components.Add(component);

            var eventMask = component.GetEventMask();
            if((eventMask & EntityEventType.Attach) == EntityEventType.Attach) component.OnAttach(pNativeData, managedData);
        }
        
        public bool TryAddComponent(EntityComponent component, void* pNativeData = null, object managedData = null) 
        {
            if(component == null) throw new ArgumentNullException(nameof(component));

            if (HasComponent(component.GetType())) return false;
            component.m_entity = this;
            m_components.Add(component);

            var eventMask = component.GetEventMask();
            if((eventMask & EntityEventType.Attach) == EntityEventType.Attach) component.OnAttach(pNativeData, managedData);
            return true;
        }

        public T AddComponent<T>(void* pNativeData = null, object managedData = null) where T : EntityComponent
        {
            if(HasComponent<T>()) throw new ComponentAlreadyAttachedException(typeof(T));
            
            var component = Activator.CreateInstance<T>();
            component.m_entity = this;
            m_components.Add(component);

            var eventMask = component.GetEventMask();
            if((eventMask & EntityEventType.Attach) == EntityEventType.Attach) component.OnAttach(pNativeData, managedData);
            return component;
        }

        public T TryAddComponent<T>(void* pNativeData = null, object managedData = null) where T : EntityComponent
        {
            if (HasComponent<T>()) return null;
            
            var component = Activator.CreateInstance<T>();
            component.m_entity = this;
            m_components.Add(component);

            var eventMask = component.GetEventMask();
            if ((eventMask & EntityEventType.Attach) == EntityEventType.Attach) component.OnAttach(pNativeData, managedData);
            return component;
        }

        public EntityComponent AddComponent(Type componentType, void* pNativeData = null, object managedData = null)
        {
            if(componentType == null) throw new ArgumentNullException(nameof(componentType));
            if(HasComponent(componentType)) throw new ComponentAlreadyAttachedException(componentType);
            
            var component = (EntityComponent) Activator.CreateInstance(componentType) ?? throw new ArgumentNullException($"Failed to create entity component for type {componentType.FullName}");
            component.m_entity = this;
            m_components.Add(component);

            var eventMask = component.GetEventMask();
            if ((eventMask & EntityEventType.Attach) == EntityEventType.Attach) component.OnAttach(pNativeData, managedData);
            return component;
        }

        public EntityComponent TryAddComponent(Type componentType, void* pNativeData = null, object managedData = null)
        {
            if(componentType == null) throw new ArgumentNullException(nameof(componentType));
            if (HasComponent(componentType)) return null;
            
            var component = (EntityComponent) Activator.CreateInstance(componentType) ?? throw new ArgumentNullException($"Failed to create entity component for type {componentType.FullName}");
            component.m_entity = this;
            m_components.Add(component);
            
            var eventMask = component.GetEventMask();
            if ((eventMask & EntityEventType.Attach) == EntityEventType.Attach) component.OnAttach(pNativeData, managedData);
            return component;
        }
        
        public T GetComponent<T>() where T : EntityComponent
        {
            var type = typeof(T);
            for (var ix = 0; ix < m_components.Count; ix++)
            {
                var component = m_components[ix];
                if (component.GetType() == type) return (T)component;
            }

            throw new ArgumentException($"Entity does not have a component of type {typeof(T).FullName}");
        }

        public EntityComponent GetComponent(Type componentType)
        {
            if(componentType == null) throw new ArgumentNullException(nameof(componentType));
            for (var ix = 0; ix < m_components.Count; ix++)
            {
                var component = m_components[ix];
                if (component.GetType() == componentType) return component;
            }

            throw new ArgumentException($"Entity does not have a component of type {componentType.FullName}");
        }
        
        public T TryGetComponent<T>() where T : EntityComponent
        {
            var type = typeof(T);
            for (var ix = 0; ix < m_components.Count; ix++)
            {
                var component = m_components[ix];
                if (component.GetType() == type) return (T) component;
            }

            return null;
        }

        public EntityComponent TryGetComponent(Type componentType)
        {
            if(componentType == null) throw new ArgumentNullException(nameof(componentType));
            for (var ix = 0; ix < m_components.Count; ix++)
            {
                var component = m_components[ix];
                if (component.GetType() == componentType) return component;
            }

            return null;
        }
        
        public bool HasComponent<T>() where T : EntityComponent
        {
            var type = typeof(T);
            for (var ix = 0; ix < m_components.Count; ix++)
            {
                var component = m_components[ix];
                if (component.GetType() == type) return true;
            }

            return false;
        }

        public bool HasComponent(Type componentType)
        {
            if(componentType == null) throw new ArgumentNullException(nameof(componentType));
            for (var ix = 0; ix < m_components.Count; ix++)
            {
                var component = m_components[ix];
                if (component.GetType() == componentType) return true;
            }

            return false;
        }
        
        public T GetOrAddComponent<T>(void* pNativeData = null, object managedData = null) where T : EntityComponent
        {
            var component = TryGetComponent<T>() ?? AddComponent<T>(pNativeData, managedData);
            return component;
        }

        public EntityComponent GetOrCreateComponent(Type componentType, void* pNativeData = null, object managedData = null)
        {
            if(componentType == null) throw new ArgumentNullException(nameof(componentType));
            var component = TryGetComponent(componentType) ?? AddComponent(componentType, pNativeData, managedData);
            return component;
        }

        public void RemoveComponent<T>(void* pNativeData = null, object managedData = null) where T : EntityComponent
        {
            var component = TryGetComponent<T>() ?? throw new ArgumentException($"Entity does not have a component of type {typeof(T).FullName}");
            var eventMask = component.GetEventMask();
            if ((eventMask & EntityEventType.Detach) == EntityEventType.Detach) component.OnDetach(pNativeData, managedData);

            m_components.Remove(component);
        }

        public void RemoveComponent(Type componentType, void* pNativeData = null, object managedData = null)
        {
            if(componentType == null) throw new ArgumentNullException(nameof(componentType));
            if (!TryRemoveComponent(componentType, pNativeData, managedData))
            {
                throw new ArgumentException($"Entity does not have a component of type {componentType.FullName}");
            }
        }

        public bool TryRemoveComponent<T>(void* pNativeData, object managedData) where T : EntityComponent
        {
            var component = TryGetComponent<T>();
            if (component == null) return false;

            var eventMask = component.GetEventMask();
            if ((eventMask & EntityEventType.Detach) == EntityEventType.Detach) component.OnDetach(pNativeData, managedData);
            m_components.Remove(component);
            
            return true;
        }

        public bool TryRemoveComponent(Type componentType, void* pNativeData = null, object managedData = null)
        {
            if(componentType == null) throw new ArgumentNullException(nameof(componentType));
            var component = TryGetComponent(componentType);
            if (component == null) return false;

            var eventMask = component.GetEventMask();
            if ((eventMask & EntityEventType.Detach) == EntityEventType.Detach) component.OnDetach(pNativeData, managedData);
            m_components.Remove(component);

            return true;
        }

        public void RemoveAllComponents(void* pNativeData = null, object managedData = null)
        {
            for (var ix = 0; ix < m_components.Count; ix++)
            {
                var component = m_components[ix];
                var eventMask = component.GetEventMask();

                if ((eventMask & EntityEventType.Detach) == EntityEventType.Detach) component.OnDetach(pNativeData, managedData);
            }

            m_components.Clear();
        }

        public void HandleClientTick(FrameEventArgs tickParams)
        {
            for (var ix = 0; ix < m_components.Count; ix++)
            {
                var component = m_components[ix];
                var eventMask = component.GetEventMask();
                if ((eventMask & EntityEventType.ClientTick) == EntityEventType.ClientTick)
                    component.OnClientTick(&tickParams, null);
            }
        }

        public void Dispose()
        {
            ProxyAllocator.DeallocateAligned(m_pData);
        }
        
        #region NATIVE_STRUCTS

        [StructLayout(LayoutKind.Sequential)]
        public struct Data
        {
            public Vector3 Position;
            public Vector3 Rotation;
            public byte Active;
        }
        #endregion
    }
}
