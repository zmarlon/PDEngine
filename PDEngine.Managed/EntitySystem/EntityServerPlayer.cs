﻿using System.Numerics;
using System.Runtime.CompilerServices;
using Lidgren.Network;

namespace PDEngine.Managed.EntitySystem
{
    public class EntityServerPlayer : Entity
    {
        private NetConnection m_connection;
        
        #region PROPERTIES
        public NetConnection Connection
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => m_connection;
        }
        #endregion
        
        public EntityServerPlayer(Vector3 position, Vector3 rotation, bool visible = true) : base(position, rotation, visible)
        {
        }

        public EntityServerPlayer(Vector3 position, bool visible = true) : base(position, visible)
        {
            
        }
    }
}