﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Core;

using PDEngine.Managed.Util;

namespace PDEngine.Managed.EntitySystem.Components
{
    public unsafe class ComponentPlayer : EntityComponent
    {
        private void* m_pNativeComponent;
        
        #region INSTANCE
        private static ComponentPlayer _INSTANCE;
        public static ComponentPlayer Instance => _INSTANCE;
        #endregion
        
        public override EntityEventType GetEventMask()
        {
            return EntityEventType.Attach | EntityEventType.Detach | EntityEventType.ClientTick;
        }

        public override Distribution GetDistribution()
        {
            return Distribution.Client;
        }

        public override void OnAttach(void* pNativeData, object managedData)
        {
            if(_INSTANCE != null) throw new ArgumentException("ComponentPlayer already created!");
            m_pNativeComponent = NPlayerComponent_Create(m_entity.m_pData);
            _INSTANCE = this;
        }

        public override void OnDetach(void* pNativeData, object managedData)
        {
            if(_INSTANCE == null) throw new ArgumentNullException(nameof(_INSTANCE));
            NPlayerComponent_Dispose(m_pNativeComponent);
            _INSTANCE = null;
        }

        public override void OnClientTick(void* pNativeData, object managedData)
        {
            NPlayerComponent_Update(m_pNativeComponent, (FrameEventArgs*)pNativeData);
        }
        
        #region NATIVE_METHODS

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void* NPlayerComponent_Create(Entity.Data* pData);

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void NPlayerComponent_Update(void* pNativeComponent, FrameEventArgs* pTickParams);

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void NPlayerComponent_Dispose(void* pNativeComponent);

        #endregion
    }
}