﻿using System.Runtime.CompilerServices;
using PDEngine.Managed.Core;

namespace PDEngine.Managed.EntitySystem
{
    public abstract unsafe class EntityComponent
    {
        internal Entity m_entity;

        #region GETTER
        public Entity Entity
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => m_entity;
        }

        public abstract EntityEventType GetEventMask();
        public abstract Distribution GetDistribution();
        #endregion

        public virtual void OnAttach(void* pNativeData, object managedData) {}
        public virtual void OnDetach(void* pNativeData, object managedData) {}
        public virtual void OnClientTick(void* pNativeData, object managedData) {}
    }
}