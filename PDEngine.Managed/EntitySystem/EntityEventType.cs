﻿using System;

namespace PDEngine.Managed.EntitySystem
{
    [Flags]
    public enum EntityEventType
    {
        Attach = 1,
        Detach = 2,
        ClientTick = 3
    }
}