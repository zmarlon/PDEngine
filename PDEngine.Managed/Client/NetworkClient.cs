﻿using System;
using Lidgren.Network;
using PDEngine.Managed.Core;
using PDEngine.Managed.Core.Logging;
using PDEngine.Managed.Core.Network;

namespace PDEngine.Managed.Client
{
    public class NetworkClient
    {
        private readonly Game m_game;
        private readonly string m_address;
        private readonly ushort m_port;
        private NetClient m_lidgrenClient;
        private bool m_serverDiscovered;
        private float m_lastLatency;
        
        #region GETTER
        public float Ping => m_lastLatency * 0.5f;
        #endregion

        public NetworkClient(Game game, string address, ushort port)
        {
            m_game = game ?? throw new ArgumentNullException(nameof(game));
            m_address = address ?? throw new ArgumentNullException(nameof(address));
            m_port = port;
        }

        public void Connect()
        {
            Logger.Instance.Info(GetType(), $"Connecting to server: {m_address}:{m_port}");

            var config = new NetPeerConfiguration(m_game.GameInfo.Name)
            {
                ConnectionTimeout = 30.0f,
                PingInterval = 5.0f,
                MaximumTransmissionUnit = 1200,
            };
            config.EnableMessageType(NetIncomingMessageType.DiscoveryResponse);
            config.EnableMessageType(NetIncomingMessageType.ConnectionLatencyUpdated);

            m_lidgrenClient = new NetClient(config);
            m_lidgrenClient.Start();

            m_lidgrenClient.DiscoverKnownPeer(m_address, m_port);
        }

        public void Disconnect()
        {
            if (m_lidgrenClient.ConnectionStatus != NetConnectionStatus.Disconnected &&
                m_lidgrenClient.ConnectionStatus != NetConnectionStatus.Disconnecting)
            {
                //TODO: maybe send quit message
                m_lidgrenClient.FlushSendQueue();
                m_lidgrenClient.Disconnect(string.Empty);
                m_lidgrenClient.FlushSendQueue();
            }
        }
        
        public void Tick()
        {
            NetIncomingMessage message;
            while ((message = m_lidgrenClient.ReadMessage()) != null)
            {
                switch (message.MessageType)
                {
                    case NetIncomingMessageType.ConnectionLatencyUpdated:
                        HandleLatencyUpdated(message);
                        break;
                    case NetIncomingMessageType.DiscoveryResponse:
                    {
                        if (!m_serverDiscovered)
                        {
                            var gameInfo = new GameInfo
                            {
                                Name = message.ReadString(),
                                Major = message.ReadUInt32(),
                                Minor = message.ReadUInt32(),
                                Patch = message.ReadUInt32()
                            };

                            if (gameInfo == Game.Instance.GameInfo)
                            {
                                m_serverDiscovered = true;

                                var senderEndPoint = message.SenderEndPoint;
                                m_lidgrenClient.Connect(senderEndPoint.Address.ToString(), senderEndPoint.Port);

                                OnConnect(message);
                            }
                            else
                            {
                                m_lidgrenClient.Disconnect(string.Empty);
                                Logger.Instance.Info($"Failed to connect to server: Client version {Game.Instance.GameInfo} does not match with server version {gameInfo}");
                            }
                        }
                    }
                        break;
                    case NetIncomingMessageType.Data:
                        HandleData(message);
                        break;
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    {
                        Logger.Instance.Log(NetworkHelper.MessageTypeToLogLevel(message.MessageType), typeof(NetworkClient), message.ReadString());
                    }
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        HandleStatusChanged(message);
                        break;
                }

                m_lidgrenClient.Recycle(message);
            }
        }

        private void HandleLatencyUpdated(NetIncomingMessage message)
        {
            m_lastLatency = message.ReadFloat() * 1000.0f;
        }

        private void HandleData(NetIncomingMessage message)
        {
            //TODO:
        }

        private void HandleStatusChanged(NetIncomingMessage message)
        {
            var status = (NetConnectionStatus) message.ReadByte();
            if (status == NetConnectionStatus.Disconnected || status == NetConnectionStatus.Disconnecting)
                OnDisconnect(message);
        }

        private void OnConnect(NetIncomingMessage message)
        {
            //TODO:
        }

        private void OnDisconnect(NetIncomingMessage message)
        {
            var disconnectMessage = message.ReadString();
            //TODO: HANDLE
        }
    }
}