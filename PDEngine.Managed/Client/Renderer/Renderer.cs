﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Client.Renderer.Common;

using PDEngine.Managed.Core.Pak;

using PDEngine.Managed.Util;

namespace PDEngine.Managed.Client.Renderer
{
    public unsafe class Renderer : IDisposable
    {
        private void* m_pHandle;
        private SpriteBatch m_spriteBatch;
        
        public Renderer(void* pHandle)
        {
            if(pHandle == null) throw new ArgumentNullException(nameof(pHandle));
            m_pHandle = pHandle;
          
            if(_INSTANCE != null) throw new ArgumentException("Instance must be null");
            _INSTANCE = this;

            m_spriteBatch = new SpriteBatch();
        }

        private void RenderSprites(FrameEventArgs frameEventArgs)
        {
            m_spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Blended);
            m_spriteBatch.End();
        }

        public void Dispose()
        {
            m_spriteBatch.Dispose();
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Renderer CreateSingleton() => new Renderer(NRenderer_GetInstance());

        #region INSTANCE
        private static Renderer _INSTANCE;
        public static Renderer Instance => _INSTANCE;
        #endregion
        
        #region NATIVE_METHODS
        private static void NRenderSprites(FrameEventArgs frameEventArgs)
        {
            _INSTANCE.RenderSprites(frameEventArgs);
        }

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void* NRenderer_GetInstance();

        #endregion
    }
}