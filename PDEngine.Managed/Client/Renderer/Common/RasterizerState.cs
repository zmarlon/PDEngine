﻿namespace PDEngine.Managed.Client.Renderer.Common
{
    public enum RasterizerState : uint
    {
        None = 0,
        Clockwise = 1,
        CounterClockwise = 2,
        Wireframe = 3
    }
}