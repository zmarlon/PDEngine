﻿namespace PDEngine.Managed.Client.Renderer.Common
{
    public enum DepthStencilState : uint
    {
        None = 0,
        Default = 1,
        ReadOnly = 2
    }
}