﻿namespace PDEngine.Managed.Client.Renderer.Common
{
    public enum SamplerState : uint
    {
        PointWrap = 0,
        PointClamp = 1,
        LinearWrap = 2,
        LinearClamp = 3,
        AnisotropicWrap = 4,
        AnisotropicClamp = 5
    }
}