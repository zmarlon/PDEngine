﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Core.Pak;
using PDEngine.Managed.Util;

namespace PDEngine.Managed.Client.Renderer.Common
{
    public unsafe class Texture2D : IDisposable
    {
        private Data m_data;
        
        #region PROPERTIES
        public uint Width => m_data.Width;
        public uint Height => m_data.Height;
        public uint MipLevels => m_data.MipLevels;
        public void* pHandle => m_data.pHandle;
        public void* pViewHandle => m_data.pViewHandle;
        #endregion

        public Texture2D(PakLocation location)
        {
            var pEntryName = Marshal.StringToHGlobalAnsi(location.EntryName);
            m_data = NTexture2D_Load(location.PakFile.Handle, pEntryName);
            Marshal.FreeHGlobal(pEntryName);
        }

        public Texture2D(void* pHandle)
        {
            if(pHandle == null) throw new ArgumentNullException(nameof(pHandle));
            m_data = NTexture2D_GetData(pHandle);
        }
        
        public void Dispose()
        {
            NTexture2D_Dispose(m_data.pHandle);
        }
        
        #region NATIVE_STRUCTS

        [StructLayout(LayoutKind.Sequential)]
        private struct Data
        {
            public void* pHandle;
            public void* pViewHandle;
            public uint Width;
            public uint Height;
            public uint MipLevels;
        }
        #endregion
        
        #region NATIVE_METHODS

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern Data NTexture2D_Load(void* pPakFile, IntPtr pEntryName);

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern Data NTexture2D_GetData(void* pHandle);

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void NTexture2D_Dispose(void* pHandle);

        #endregion
    }
}