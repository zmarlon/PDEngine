﻿using System;
using System.Collections.Generic;
using System.Numerics;
using PDEngine.Managed.Core.Math;

namespace PDEngine.Managed.Client.Renderer.Common
{
    public unsafe struct StaticFontMesh : IEquatable<StaticFontMesh>
    {
        private List<SpriteBatch.SpriteInfo> m_spriteInfos;

        private Vector2 m_size;

        public Vector2 Size
        {
            get => m_size;
            set => m_size = value;
        }
        public List<SpriteBatch.SpriteInfo> SpriteInfos => m_spriteInfos;
        
        public StaticFontMesh(SpriteFont font, string message, Color4F color)
        {
            m_spriteInfos = new List<SpriteBatch.SpriteInfo>();
            m_size = default(Vector2);
            
            Generate(font, message, color);
        }

        public void Reset()
        {
            m_spriteInfos.Clear();
            m_size = default(Vector2);
        }

        public void Reset(SpriteFont font, string message, Color4F color)
        {
            Generate(font, message, color);
        }

        private void Generate(SpriteFont font, string message, Color4F color)
        {
            var firstGlyphOfLine = true;

            var texture = font.Texture;
            float textureWidth = texture.Width, textureHeight = texture.Height;
            var fontSize = font.Size;

            var offset = default(Vector2);
            for (int ix = 0; ix < message.Length; ix++)
            {
                var c = message[ix];
                if (c == '\r') continue;
                if (c == '\n')
                {
                    if (offset.X > m_size.X) m_size.X = offset.X;
                    offset.X = 0.0f;
                    offset.Y += font.LineSpacing;
                    m_size.Y += font.LineSpacing;
                    firstGlyphOfLine = true;
                    continue;
                }

                SpriteFont.GlyphInfo glyphInfo;
                var result = font.GetGlyph(c, out glyphInfo);
                if (!result) continue;

                var xCharOffset = glyphInfo.OffsetX;
                if (firstGlyphOfLine)
                {
                    if (glyphInfo.OffsetX < 0.0f)
                        xCharOffset = 0.0f;
                    else
                        xCharOffset = glyphInfo.OffsetX;

                    firstGlyphOfLine = false;
                }
                
                var spriteInfo = new SpriteBatch.SpriteInfo
                {
                    TexCoordTL = new Vector2(glyphInfo.TexCoordX / textureWidth, glyphInfo.TexCoordY / textureHeight),
                    TexCoordBR = new Vector2((glyphInfo.TexCoordX + glyphInfo.Width) / textureWidth,
                        (glyphInfo.TexCoordY + glyphInfo.Height) / textureHeight),
                    Destination = new RectangleF(offset.X + xCharOffset,
                        offset.Y + (fontSize - glyphInfo.OffsetY),
                        glyphInfo.Width, glyphInfo.Height),
                    Color = color,
                    Origin = default(Vector2),
                    Rotation = 0.0f,
                    Depth = 0.0f,
                    pTexture = texture.pViewHandle
                };

                m_spriteInfos.Add(spriteInfo);
                offset.X += glyphInfo.XAdvance;
            }

            if (offset.X > m_size.X) m_size.X = offset.X;
            m_size.Y += font.LineSpacing;
        }
        
        public bool Equals(StaticFontMesh other)
        {
            return m_size == other.Size && m_spriteInfos.Equals(other.m_spriteInfos);
        }
    }
}