﻿namespace PDEngine.Managed.Client.Renderer.Common
{
    public enum SpriteSortMode : uint
    {
        Deferred = 0,
        Texture = 1,
        BackToFront = 2,
        FrontToBack = 3
    }
}