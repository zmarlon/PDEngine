﻿namespace PDEngine.Managed.Client.Renderer.Common
{
    public enum BlendState : uint
    {
        Opaque = 0,
        Blended = 1,
        Additive = 2
    }
}