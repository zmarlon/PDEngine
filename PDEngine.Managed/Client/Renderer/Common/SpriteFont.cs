﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Core;
using PDEngine.Managed.Core.Pak;
using PDEngine.Managed.Util;


namespace PDEngine.Managed.Client.Renderer.Common
{
    public unsafe class SpriteFont : IDisposable
    {
        private readonly Dictionary<char, GlyphInfo> m_glyphInfos = new Dictionary<char, GlyphInfo>();
        private readonly Data* m_pData;
        private readonly Texture2D m_texture;
        
        private readonly void* m_pNativeSpriteFont;

        public Texture2D Texture => m_texture;
        public float Size => m_pData->Size;
        public float LineSpacing => m_pData->LineSpacing;
        
        public SpriteFont(PakLocation location, uint fontSize)
        {
            void* pEntryName = Marshal.StringToHGlobalAnsi(location.EntryName).ToPointer();
            
            m_pNativeSpriteFont = NSpriteFont_Load(location.PakFile.Handle, pEntryName, fontSize);

            m_pData = NSpriteFont_GetData(m_pNativeSpriteFont);
            m_texture = new Texture2D(NSpriteFont_GetTexture(m_pNativeSpriteFont));

            for (var ix = 0; ix < m_pData->GlyphCount; ix++)
            {
                var currentGlyphInfo = m_pData->pGlyphInfos[ix];
                m_glyphInfos.Add(currentGlyphInfo.Value, currentGlyphInfo);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool GetGlyph(char value, out GlyphInfo glyphInfo)
        {
            return m_glyphInfos.TryGetValue(value, out glyphInfo);
        }

        public Vector2 MeasureString(string message)
        {
            Vector2 offset = new Vector2(0.0f, LineSpacing);
            bool firstGlyphOfLine = true;

            float tempOffsetX = 0.0f;
            for(int ix = 0; ix < message.Length; ix++)
            {
                char c = message[ix];
                if(c == '\r') continue;
                if(c == '\n')
                {
                    if(tempOffsetX > offset.X) offset.X = tempOffsetX;
                    tempOffsetX = 0.0f;
                    offset.Y += LineSpacing;
                    continue;
                }

                GlyphInfo glyphInfo; //TODO: vllt nicht das ganze ding kopieren?
                bool result = GetGlyph(c, out glyphInfo);
                if(!result) continue;

                tempOffsetX += glyphInfo.XAdvance;
            }

            if(tempOffsetX > offset.X) offset.X = tempOffsetX;

            return offset;
        }
        
        public void Dispose()
        {
            NSpriteFont_Dispose(m_pNativeSpriteFont);
        }
        
        #region NATIVE_STRUCTS
        [StructLayout(LayoutKind.Sequential)]
        private struct Data
        {
            public float Size;
            public float LineSpacing;
            public GlyphInfo* pGlyphInfos;
            public uint GlyphCount;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct GlyphInfo
        {
            public char Value;
            public float XAdvance;
            public float YAdvance;
            public float Width;
            public float Height;
            public float OffsetX;
            public float OffsetY;
            public float TexCoordX;
            public float TexCoordY;

            public GlyphInfo(char value, float xAdvance, float yAdvance, float width, float height, float offsetX,
                float offsetY, float texCoordX, float texCoordY)
            {
                Value = value;
                XAdvance = xAdvance;
                YAdvance = yAdvance;
                Width = width;
                Height = height;
                OffsetX = offsetX;
                OffsetY = offsetY;
                TexCoordX = texCoordX;
                TexCoordY = texCoordY;
            }
        }
        
        #endregion

        #region NATIVE_METHODS

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule, CallingConvention = CallingConvention.Cdecl)]
        private static extern void* NSpriteFont_Load(void* pPakFile, void* pEntryName, uint fontSize);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule, CallingConvention = CallingConvention.Cdecl)]
        private static extern Data* NSpriteFont_GetData(void* pSpriteFont);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule, CallingConvention = CallingConvention.Cdecl)]
        private static extern void* NSpriteFont_GetTexture(void* pSpriteFont);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Reference.NativeModule, CallingConvention = CallingConvention.Cdecl)]
        private static extern void NSpriteFont_Dispose(void* pSpriteFont);

        #endregion
    }
}