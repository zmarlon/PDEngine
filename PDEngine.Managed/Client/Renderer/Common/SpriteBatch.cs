﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Security;
using PDEngine.Managed.Core.Math;
using PDEngine.Managed.Util;
using PDEngine.Managed.Util.Extensions;

namespace PDEngine.Managed.Client.Renderer.Common
{
    public unsafe class SpriteBatch : IDisposable
    {
        private static readonly Vector2 _texCoordTL = new Vector2(0.0F, 0.0F);
        private static readonly Vector2 _texCoordBR = new Vector2(1.0f, 1.0f);
        
        private const int _INITIAL_QUEUE_SIZE = 64;

        private readonly void* m_pHandle;
        private readonly bool m_isDefault;
        private readonly List<SpriteInfo> m_spriteInfos = new List<SpriteInfo>(_INITIAL_QUEUE_SIZE);
        private readonly Data* m_pData;
        private bool m_drawing;

        public SpriteBatch(bool isDefault = false)
        {
            var createData = isDefault ? NSpriteBatch_GetDefault() : NSpriteBatch_Create();
            m_isDefault = isDefault;
            
            m_pHandle = createData.pHandle;
            m_pData = createData.pData;
        }
        
        public void Begin(SpriteSortMode spriteSortMode = SpriteSortMode.Deferred, BlendState blendState = BlendState.Opaque,
            SamplerState samplerState = SamplerState.LinearClamp, DepthStencilState depthStencilState = DepthStencilState.None,
            RasterizerState rasterizerState = RasterizerState.None, Matrix4x4? transformationMatrix = null)
        {
            if (m_drawing) throw new ArgumentException("Already drawing!");
            
            m_pData->SpriteSortMode = spriteSortMode;
            m_pData->BlendState = blendState;
            m_pData->SamplerState = samplerState;
            m_pData->DepthStencilState = depthStencilState;
            m_pData->RasterizerState = rasterizerState;
            m_pData->TransformationMatrix = transformationMatrix ?? Matrix4x4.Identity;

            m_drawing = true;
        }

        public void End()
        {
            if (!m_drawing) throw new ArgumentException("Not drawing!");

            if (m_spriteInfos.Count > 0)
            {
                var spriteInfosArray = m_spriteInfos.GetData();
                fixed (SpriteInfo* pSpriteInfos = spriteInfosArray)
                {
                    NSpriteBatch_Flush(m_pHandle, pSpriteInfos, (uint)m_spriteInfos.Count);
                }
            }
            m_spriteInfos.Clear();
            m_drawing = false;
        }

        public void Draw(Texture2D texture, RectangleF destinationRectangle, Color4F color, Vector2? origin = null, float rotation = 0, float depth = 0)
        {
            var spriteInfo = new SpriteInfo
            {
                TexCoordTL = _texCoordTL,
                TexCoordBR = _texCoordBR,
                Destination = destinationRectangle,
                Color = color,
                Origin = origin ?? Vector2.Zero,
                Rotation = rotation,
                Depth = depth,
                pTexture = texture.pViewHandle,
            };

            m_spriteInfos.Add(spriteInfo);
        }
        
        public void Draw(Texture2D texture, RectangleF destinationRectangle, RectangleF sourceRectangle, Color4F color, Vector2? origin = null, float rotation = 0, float depth = 0)
        {
            var fTextureWidth = (float) texture.Width;
            var fTextureHeight = (float) texture.Height;
            
            var spriteInfo = new SpriteInfo
            {
                TexCoordTL = new Vector2(sourceRectangle.X / fTextureWidth, sourceRectangle.Y / fTextureHeight),
                TexCoordBR = new Vector2((sourceRectangle.X + sourceRectangle.Width) / fTextureWidth, (sourceRectangle.Y + sourceRectangle.Height) / fTextureHeight),
                Destination = destinationRectangle,
                Color = color,
                Origin = origin ?? Vector2.Zero,
                Rotation = rotation,
                Depth = depth,
                pTexture = texture.pViewHandle,
            };

            m_spriteInfos.Add(spriteInfo);
        }
        
        public void DrawString(ref StaticFontMesh fontMesh, Vector2 position, Vector2? origin = null, float rotation = 0.0f,
            float depth = 0.0f)
        {
            var realOrigin = origin ?? Vector2.Zero;
            
            var spriteInfos = fontMesh.SpriteInfos;
            for (int ix = 0; ix < spriteInfos.Count; ix++)
            {
                var spriteInfo = spriteInfos[ix];
                spriteInfo.Destination.X += position.X;
                spriteInfo.Destination.Y += position.Y;
                spriteInfo.Origin = realOrigin;
                spriteInfo.Rotation = rotation;
                spriteInfo.Depth = depth;

                m_spriteInfos.Add(spriteInfo);
            }
        }
        
        public void DrawString(SpriteFont font, string message, Vector2 position, Color4F color,
            Vector2? origin = null, float rotation = 0.0f, float depth = 0.0f)
        {
            var offset = default(Vector2);
            var firstGlyphOfLine = true;

            var texture = font.Texture;
            float textureWidth = texture.Width, textureHeight = texture.Height;
            var fontSize = font.Size;

            var realOrigin = origin ?? Vector2.Zero;

            for (var ix = 0; ix < message.Length; ix++)
            {
                var c = message[ix];
                if (c == '\r') continue;
                if (c == '\n')
                {
                    offset.X = 0.0f;
                    offset.Y += font.LineSpacing;
                    firstGlyphOfLine = true;
                    continue;
                }

                SpriteFont.GlyphInfo glyphInfo;
                var result = font.GetGlyph(c, out glyphInfo);
                if (!result) continue;

                var xCharOffset = glyphInfo.OffsetX;
                if (firstGlyphOfLine)
                {
                    if (glyphInfo.OffsetX < 0.0f)
                        xCharOffset = 0.0f;
                    else
                        xCharOffset = glyphInfo.OffsetX;

                    firstGlyphOfLine = false;
                }

                var spriteInfo = new SpriteInfo
                {
                    TexCoordTL = new Vector2(glyphInfo.TexCoordX / textureWidth, glyphInfo.TexCoordY / textureHeight),
                    TexCoordBR = new Vector2((glyphInfo.TexCoordX + glyphInfo.Width) / textureWidth,
                        (glyphInfo.TexCoordY + glyphInfo.Height) / textureHeight),
                    Destination = new RectangleF(position.X + offset.X + xCharOffset,
                        position.Y + offset.Y + (fontSize - glyphInfo.OffsetY),
                        glyphInfo.Width, glyphInfo.Height),
                    Color = color,
                    Origin = realOrigin,
                    Rotation = rotation,
                    Depth = depth,
                    pTexture = texture.pViewHandle
                };

                m_spriteInfos.Add(spriteInfo);
                offset.X += glyphInfo.XAdvance;
            }
        }


        public void Dispose()
        {
            if(!m_isDefault) NSpriteBatch_Dispose(m_pHandle);
        }
        
        #region NATIVE_STRUCTS
        [StructLayout(LayoutKind.Sequential)]
        private struct CreateData
        {
            public void* pHandle;
            public Data* pData;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct Data
        {
            public SpriteSortMode SpriteSortMode;
            public BlendState BlendState;
            public SamplerState SamplerState;
            public DepthStencilState DepthStencilState;
            public RasterizerState RasterizerState;
            public Matrix4x4 TransformationMatrix;
        }
        
        [StructLayout(LayoutKind.Sequential)]
        public struct SpriteInfo
        {
            public RectangleF Destination;
            public Vector2 TexCoordTL;
            public Vector2 TexCoordBR;
            public Color4F Color;
            public Vector2 Origin;
            public float Rotation;
            public float Depth;
            public void* pTexture;
        }
        #endregion
        
        #region NATIVE_METHODS

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern CreateData NSpriteBatch_Create();

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern CreateData NSpriteBatch_GetDefault();

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void NSpriteBatch_Dispose(void* pHandle);

        [DllImport(Reference.NativeModule)]
        [SuppressUnmanagedCodeSecurity]
        private static extern void NSpriteBatch_Flush(void* pHandle, SpriteInfo* pSpriteInfos, uint numSpriteInfos);

        #endregion
    }
}